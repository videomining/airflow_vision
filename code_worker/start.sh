#!/bin/bash


cp -r /home/ubuntu/airflow_demo/main/dags /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/giant-6097 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/config/airflow.cfg /home/ubuntu/airflow

cd /home/ubuntu/airflow/dags
export AIRFLOW_HOME=/home/ubuntu/airflow
chmod +x ./Face_Postprocessor
cd /home/ubuntu/airflow
#airflow worker -D
rm -r /home/ubuntu/airflow_demo