import os,sys
import subprocess
import utilsAmazonS3

def updateFaceBinary(bucket_name,file_type):
    os.chdir(r'/home/ubuntu/airflow/dags/')
    #first updating the binary
    FaceFilename = '/home/ubuntu/airflow/dags/Face_Postprocessor'
    try:
        os.remove(FaceFilename)
        utilsAmazonS3.downloadFilesFromS3(bucket_name,file_type,FaceFilename)
        print 'enabling the binary'
        enable = subprocess.Popen(["chmod","+x","./Face_Postprocessor"],
                                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        enable.wait()
        print 'enabling the binary completed'
    except:
        pass
    