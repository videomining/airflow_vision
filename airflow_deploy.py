# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.
"""
A Bitbucket Builds template for deploying an application to AWS Lambda
joshcb@amazon.com
v1.0.0
"""
from __future__ import print_function
import sys
#import boto3
import boto
import boto.s3
import os
import math 
import pymongo
from filechunkio import FileChunkIO
chunk_size = 5242880

def connectToS3(AWS_KEY_ID,AWS_SECRET_KEY):
    conn = boto.connect_s3(aws_access_key_id=AWS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)
    return conn

def publish_new_version(zipFile,AWS_KEY_ID, AWS_SECRET_KEY, AWS_DEFAULT_REGION, bucketName):
    """
    Publishes new version of the AWS S3
    """
    try:
        conn = connectToS3(AWS_KEY_ID,AWS_SECRET_KEY)
        bucket = conn.lookup(bucketName)
        if bucket == None:
            bucket = conn.create_bucket(bucketName)
        file = zipFile
        file_size = os.stat(file).st_size
        # Create a multipart upload request
        print('Uploading files using multipart')
        mp = bucket.initiate_multipart_upload(os.path.basename(file))
        chunk_count = int(math.ceil(file_size/float(chunk_size)))
        # Send the file parts, using FileChunkIO to create a file-like object
        # that points to a certain byte range within the original file. We
        # set bytes to never exceed the original file size.
        for i in range(chunk_count):
            offset = chunk_size * i
            bytes = min(chunk_size,file_size-offset)
            with FileChunkIO(file,'r',offset=offset,bytes= bytes) as fp:
                mp.upload_part_from_file(fp,part_num=i+1)
        mp.complete_upload()
    except:
        print("Failed to create boto3 s3 resouce and upload file.\n" + str(err))
        return False
    print("done")
    return True
 
def main(zipName, AWS_KEY_ID,AWS_SECRET_KEY,AWS_DEFAULT_REGION, bucket):
    " Your favorite wrapper's favorite wrapper "
    
    if not publish_new_version('{}.zip'.format(zipName), AWS_KEY_ID, AWS_SECRET_KEY, AWS_DEFAULT_REGION, bucket):
        sys.exit(1)

if __name__ == "__main__":
    print("starting upload")
    if len(sys.argv) < 6:
        print("exiting")
        sys.exit(1)
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
