#!/bin/bash
cp -r /home/ubuntu/airflow_demo/main/dags /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/giant-6097 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/swy-1751 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/giant-316 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/swy-2030 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/swy-1480 /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/config/airflow.cfg /home/ubuntu/airflow
cd /home/ubuntu/airflow/dags
chmod +x ./Face_Postprocessor
export AIRFLOW_HOME=/home/ubuntu/airflow
cd /home/ubuntu/airflow
airflow initdb
airflow webserver -p 8081 -D
airflow scheduler -D
rm -r /home/ubuntu/airflow_demo