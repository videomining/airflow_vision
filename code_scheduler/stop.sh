#!/bin/bash
# Stop the web server.

cd /home/ubuntu/airflow
kill `sudo cat airflow-scheduler.pid | xargs`
kill `sudo cat airflow-webserver.pid | xargs`

# sudo docker rm -f $(sudo docker stop $(sudo docker ps -a -q  --filter ancestor=isabella6151/airflow))
# kill `pgrep python`
