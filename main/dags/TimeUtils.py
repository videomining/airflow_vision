from datetime import *
from time import strptime
from calendar import timegm


EPOCH_DIFF = 11644473600

def secsToDate(secs):
# ASSUMES SECONDS WERE GENERATED USING WINDOWS LIBRARY(VMS) or using DateStringToSecs function
# HENCE EPOCH_DIFF is added to convert from secs since python epoch to seconds since windows epoch

  global EPOCH_DIFF

  epochDt = datetime(1970,1,1,0,0,0)
  originSecs = timegm(epochDt.timetuple()) + EPOCH_DIFF # timegm(epochDt.timetuple()) = 0
  diff = secs - originSecs

  timeDiff = timedelta(seconds = diff)
  return epochDt + timeDiff

def DateStringToSecs(datestring):

  #print '"%s"' % datestring
  dateStringSplit = datestring.split('.') # first element is date, second is microseconds 
  datestring = dateStringSplit[0] 
  vals = strptime(datestring, '%Y-%m-%dT%H:%M:%S')


  global EPOCH_DIFF

  #offset because c library epoch differs from pythons

  result = timegm(vals) + EPOCH_DIFF

  if len(dateStringSplit) == 2:
    #print len(dateStringSplit[1])
    factor = 10**len(dateStringSplit[1])
    result += float(dateStringSplit[1])/factor
    #print factor
    
  return result

def DateTimeToSecs(dt):
  global EPOCH_DIFF
  return timegm(dt.timetuple()) + EPOCH_DIFF
