import extractBaseTrajPython
import GenerateJobs
import getLiveCams
import extractBaseTrajNew


optsFile = r'\\vmnode3\ProjectData\psm-434\Documents\optsFiles\434_optsFile_petsmart.yml'
optsDict = GenerateJobs._ParseOptsFile(optsFile)
msg, camArgv = getLiveCams.getLiveCams(optsDict)
siteID = "psm-434"
outputDir = r"D:\work\scripts\POSSE\database"
dateList = ['20170127']
project_type = 'new'
for date in dateList:
    # extractBaseTrajNew.main(projPath, camArgv, date, outputDir,siteID)
    extractBaseTrajNew.main(camArgv, date, outputDir,siteID,project_type)
