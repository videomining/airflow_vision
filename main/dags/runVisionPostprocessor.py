import sys,os
import subprocess
import time
import random
def runVisionPP(resolution,date,join1_val,join2_val,join3_val,location):
    randomVal = random.randint(10,50)
    time.sleep(randomVal)
def runVisionPP_1(resolution,date,join1_val,join2_val,join3_val,location):
    join1Str = "PO:%s"%join1_val
    join2Str = "PO:%s"%join2_val
    join3Str = "PO:%s"%join3_val
    print "./Vision_PostProcessor -i %s -s -d %s -1 %s -2 %s -3 %s"%(resolution,date,join1Str,join3Str,join3Str)
    process = subprocess.Popen(["./Vision_PostProcessor","-i",resolution,"-s","-d",date,"-1",join1Str,"-2",join2Str,"-3",join3Str])
    process.wait()
    stdout,stderr = process.communicate()
    print stdout
    print stderr


if __name__ == '__main__':
    join1_val = 10
    join2_val = 20
    join3_val = 30
    location = r'~/airflow/swy-1751'
    dateStr = '20161201'
    resolution = '320,240'
    runVisionPP(resolution,dateStr,join1_val,join2_val,join3_val,location)
    
