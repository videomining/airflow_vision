import sys
import pdb
from numpy import *
from numpy import linalg

import xml.parsers.expat

from math import radians

import random

from Trajectory import *

import os.path
import glob

from PIL import Image
from PIL import ImageDraw
import random
import VmsUtils

def translate(x = 0.0,y = 0.0,z = 0.0):
  x = float(x)
  y = float(y)
  z = float(z)

  m = identity(4, float32)

  m[0,3] = x
  m[1,3] = y
  m[2,3] = z

  return m

def rotate(x = 0.0, y = 0.0, z = 0.0, theta = 0.0):
  u = float(x)
  v = float(y)
  w = float(z)
  theta = radians(float(theta))

  Lsquared = u*u + v*v + w*w
  L = sqrt(Lsquared)

  m = identity(4, float32)

  m[0,0] = (u**2 + (v**2 + w**2)*cos(theta)) / Lsquared
  m[0,1] = (u*v*(1 - cos(theta)) - w*L*sin(theta)) / Lsquared
  m[0,2] = (u*w*(1 - cos(theta)) + v*L*sin(theta)) / Lsquared  
  
  m[1,0] = (u*v*(1 - cos(theta)) + w*L*sin(theta)) / Lsquared
  m[1,1] = (v**2 + (u**2 + w**2)*cos(theta)) / Lsquared
  m[1,2] = (v*w*(1 - cos(theta)) - u*L*sin(theta)) / Lsquared

  m[2,0] = (u*w*(1 - cos(theta)) - v*L*sin(theta)) / Lsquared
  m[2,1] = (v*w*(1 - cos(theta)) + u*L*sin(theta)) / Lsquared
  m[2,2] = (w**2 + (u**2 + v**2)*cos(theta)) / Lsquared
  return m

def scale(x = 0.0, y = 0.0, z = 0.0):
  m = identity(4,float32)

  m[0,0] = float(x)
  m[1,1] = float(y)
  m[2,2] = float(z)  

  return m

def homography(s, t):

  #generate homography matrix
  x1 = float(s[0][0])
  x2 = float(s[1][0])
  x3 = float(s[2][0])
  x4 = float(s[3][0])

  y1 = float(s[0][1])
  y2 = float(s[1][1])
  y3 = float(s[2][1])
  y4 = float(s[3][1])

  u1 = float(t[0][0])
  u2 = float(t[1][0])
  u3 = float(t[2][0])
  u4 = float(t[3][0])

  v1 = float(t[0][1])
  v2 = float(t[1][1])
  v3 = float(t[2][1])
  v4 = float(t[3][1]) 
  
  m = zeros((8,8), float32)  

  m[0,0] = x1
  m[0,1] = y1
  m[0,2] = 1.0
  m[0,6] = -u1*x1
  m[0,7] = -u1*y1

  m[1,3] = x1
  m[1,4] = y1
  m[1,5] = 1.0
  m[1,6] = -v1*x1
  m[1,7] = -v1*y1

  m[2,0] = x2
  m[2,1] = y2
  m[2,2] = 1.0
  m[2,6] = -u2*x2
  m[2,7] = -u2*y2

  m[3,3] = x2
  m[3,4] = y2
  m[3,5] = 1.0
  m[3,6] = -v2*x2
  m[3,7] = -v2*y2

  m[4,0] = x3
  m[4,1] = y3
  m[4,2] = 1.0
  m[4,6] = -u3*x3
  m[4,7] = -u3*y3

  m[5,3] = x3
  m[5,4] = y3
  m[5,5] = 1.0
  m[5,6] = -v3*x3
  m[5,7] = -v3*y3

  m[6,0] = x4
  m[6,1] = y4
  m[6,2] = 1.0
  m[6,6] = -u4*x4
  m[6,7] = -u4*y4

  m[7,3] = x4
  m[7,4] = y4
  m[7,5] = 1.0
  m[7,6] = -v4*x4
  m[7,7] = -v4*y4  

  #return a useful tranform

  p = array([u1,v1,u2,v2,u3,v3,u4,v4], float32)

  # Mh = p, so h = (M^-1)P

  h = dot(linalg.inv(m),p)
  
  transform = zeros((3,3), float32)

  transform[0,0] = h[0]
  transform[0,1] = h[1]
  transform[0,2] = h[2]  
  transform[1,0] = h[3]
  transform[1,1] = h[4]
  transform[1,2] = h[5]
  transform[2,0] = h[6]
  transform[2,1] = h[7]
  transform[2,2] = 1.0

  return transform

class Camera:
  def __init__(self, name):
    self.id = None
    self.name = name

    self.location = None
    self.orientation = None
    self.focalLength = None
    self.ccdSize = None
    self.imageSize = None

  def __str__(self):
    s = '%s:\n' % (self.name)

    s+= '-Location: %s\n' % (self.location)
    s+= '-Orientation: %s\n' % (self.orientation)
    s+= '-Focal Length: %s\n' % (self.focalLength)
    s+= '-CCD Size: %s\n' % (str(self.ccdSize))
    s+= '-Image Size: %s\n' % (str(self.imageSize))    

    return s   

  def getName(self):
    return self.name 

  def getOrientation(self):
    return self.orientation

  def getLocation(self):
    return self.location

  def getFocalLength(self):
    return self.focalLength

  def getCcdSize(self):
    return self.ccdSize

  def getImageSize(self):
    return self.imageSize

  def getXmlString(self):
    xmlString = """<camera name="%s">\n
	<calibration location="(%f,%f,%f)" orientation="%f" 
	focalLength="%f" ccdSize="(%f,%f)" imageSize="(%d,%d)" />\n</camera>"""


    xmlString = xmlString % (self.name,self.location[0],self.location[1],self.location[2],\
		self.orientation,self.focalLength,self.ccdSize[0],self.ccdSize[1],\
		self.imageSize[0],self.imageSize[1])

    return xmlString
    

class Floorplan:
  def __init__(self):
    self.local = None
    self.world = None
    self.image = None

  def __str__(self):
    s = 'Floorplan:\n'
    s+= '-Local: %s\n' % (str(self.local))
    s+= '-World: %s\n' % (str(self.world))
    return s

  def getImageName(self):
    return self.image

  def getLocalCoords(self):
    return self.local

  def getWorldCoords(self):
    return self.world

  def getXmlStr(self):
    xmlStr = '<floorplan image="%s">' % (self.image)
    localStr = ""
    for x,y in self.local:
      localStr += "(%f,%f)," % (x,y)
    localStr.rstrip(",")
    
    worldStr = ""
    for x,y in self.local:
      worldStr += "(%f,%f)," % (x,y)
    worldStr.rstrip(",")

    xmlStr += '\n<coord_mapping local="%s" world="%s" />\n</floorplan>' % (localStr,worldStr)

class calibrationParser:
  def __init__(self):
    self.stack = []
    self.currentCamera = None    
    self.cameraList = []    
    self.floorplan = None
    self.rootPath = '' # stores root path of calib.xml

  def getCameraList(self):
    return self.cameraList

  def getFloorplan(self):
    return self.floorplan

  def parse(self,filename):
    self.rootPath = os.path.split(filename)[0]
    p = xml.parsers.expat.ParserCreate()

    p.StartElementHandler = self.startElement
    p.EndElementHandler = self.endElement
    p.CharacterDataHandler = self.charData
    
    
    fileObj = open(filename)
    # fileObj = VmsUtils.OpenRobustly(filename)
    
    p.ParseFile(fileObj)

  def startElement(self,name, attrs):
    self.stack.insert(0,name)

    if self.stack[0] == 'camera':
      self.currentCamera = Camera(attrs['name'])    
      
    elif self.stack[0] == 'calibration':
      if self.stack[1] == 'camera':

        coords = tuple(attrs['location'].strip('()').split(','))
        coords = map(float, coords)
        self.currentCamera.location = coords
        
        self.currentCamera.orientation = float(attrs['orientation'])
        self.currentCamera.focalLength = float(attrs['focalLength'])
        self.currentCamera.ccdSize = self.convertCoords(attrs['ccdSize'])[0]
        self.currentCamera.imageSize = self.convertCoords(attrs['imageSize'])[0]
        
    elif self.stack[0] == 'coord_mapping':
      assert self.stack[1] == 'floorplan'
      self.floorplan.local = self.convertCoords(attrs['local'])
      self.floorplan.world = self.convertCoords(attrs['world'])
        
    elif self.stack[0] == 'floorplan':
      self.floorplan = Floorplan()
      self.floorplan.image = os.path.join(self.rootPath,attrs['image'])

  def endElement(self,name):
    if self.stack[0] == 'camera':
      self.cameraList.append(self.currentCamera)
      assert self.currentCamera.location is not None
      assert self.currentCamera.orientation is not None
      assert self.currentCamera.focalLength is not None
      assert self.currentCamera.ccdSize is not None
      assert self.currentCamera.imageSize is not None      
      self.currentCamera = None
    elif self.stack[0] == 'floorplan':
      assert self.floorplan.local is not None
      assert self.floorplan.world is not None
      assert self.floorplan.image is not None
    self.stack.pop(0)

  def charData(self,data):
    pass

  def convertDistort(values):
    s = values.split(',')
    return (float(s[0]),float(s[1]),float(s[2]))
  convertDistort = staticmethod(convertDistort)

  def convertCoords(coords):
    coords = coords.replace('(','').replace(')','')
    coords = coords.split(',')
    coords = map(float,coords)

    l = []    
    for x in range(0,len(coords),2):
      l.append((coords[x],coords[x+1]))

    return l
  convertCoords = staticmethod(convertCoords)


def convertToWorldView(trajList, camera, correctDistortion, distortionParameters):
  """Convert all trajectories in trajList into the world view,
  based on calibration information within camera"""
  # if correctDistortion:
    # print "CorrectDistortion"
  ### TODO: HANDLE LENS DISTORTION
  ### TODO: HANDLE OFFSET FOR FOOT LOCATION VS BLOB CENTER

  xOff = camera.getImageSize()[0]/2
  yOff = camera.getImageSize()[1]/2

  #Translate to center of image coords
  t = translate(-xOff,-yOff)

  
  s = scale(1/40.0,1/40.0)
  #Scale according to camera settings
  ccdW, ccdH = camera.getCcdSize()
  dist = camera.getLocation()[2]
  f = camera.getFocalLength()

  s = scale(ccdW * (1.0 + dist/f)/camera.getImageSize()[0], ccdH * (1.0 + dist/f)/camera.getImageSize()[1],dist)

  #print s
  
  

  temp = dot(s,t)

  temp = dot(rotate(z=1.0, theta = camera.getOrientation()), temp)

  xPos = camera.getLocation()[0]
  yPos = camera.getLocation()[1]
  
  m = dot(translate(xPos,yPos), temp)

  for traj in trajList:
    if correctDistortion:
      traj.distort(distortionParameters,camera.getImageSize())
    traj.transform(m)

def loadProcessDict(filelist):
  d = {}
  completeFileList = []
  for line in filelist:
    camera, filelist = line.split(':')
    # filelist = filelist.split(',')
    completeFileList = []
    keys = d.keys()
    if camera not in d:
      d[camera] = []
    completeFileList.extend(glob.glob(filelist))
    d[camera].extend(completeFileList)

  return d

def convertCameraToFloorplanCoords(trajectoriesByCamera, worldCoords, localCoords):
    homographyMatrix = homography(worldCoords, localCoords)
    for camera in trajectoriesByCamera.keys():
        trajList = trajectoriesByCamera[camera]
        convertToWorldView(trajList, camera, False)
        
        for traj in trajList:
            traj.transform(homographyMatrix)
        
def Calib(calibrationFilename, processList, outputPath, correctDistortion, distortionParameters, drawFloorplanFlag, fractionTrajToBeDrawn):
  # usage: <calibFilename> <processList> <outputPath> <correctDistortion> <distortionParams> <drawFloorplanFlag> <fractionTrajToBeDrawn>
  if correctDistortion == 1:
    distortionParameters = map(float, distortionParameters.split(','))
  else:
    distortionParameters = []
  
  #various program options here, to be made as command line opts
  drawFloorplan = drawFloorplanFlag  #False #True
  trajColors = ['red','blue','orange','brown','purple']
  #Save trajectories in floorplan coords instead of world
  saveFloorplanCoords = False
  #fraction of trajectories to be included
  fractionTraj = float(fractionTrajToBeDrawn)  # 1
  
  verbose = False#True#
  
  #processDict = {'Camera1':['traj.csv']}
  processDict = loadProcessDict(processList) 
  
  try:
    os.makedirs(outputPath)
  except:
    pass

  #End program input

  p = calibrationParser()
  p.parse(calibrationFilename)
  cameraList = p.getCameraList()
  floorplan = p.getFloorplan()

  floorImg = None

  if drawFloorplan or saveFloorplanCoords:
    assert floorplan is not None
    if verbose:
      print floorplan
 

  for cameraName in processDict:
    #search for a camera with the same name
    cameraObj = None
    
    for camera in cameraList:
      if camera.getName() == cameraName:
        cameraObj = camera

    if cameraObj is None:
      sys.exit("ERROR: '%s' not found in %s" % (cameraName, calibrationFilename))

    if verbose: print cameraObj

    #Now process each camera accordingly
    for filename in processDict[cameraName]:
      outputFilename = os.path.join(outputPath,os.path.split(filename)[1])
      
      if verbose:
        print '%s: %s --> %s' % (cameraName, filename, outputFilename)

      trajList = []
      try:
        trajList = Trajectory.loadCSV(filename)
      except:
        trajList = Trajectory.loadXmlCsv(filename)

      convertToWorldView(trajList,cameraObj, correctDistortion, distortionParameters)
      if not saveFloorplanCoords:
        Trajectory.saveXmlCsv(outputFilename, trajList)

      if drawFloorplan:
        floorImg = Image.open(floorplan.getImageName())
      
      if drawFloorplan or saveFloorplanCoords:
        floorTransform = homography(floorplan.getWorldCoords(), floorplan.getLocalCoords())
        for traj in trajList:
          traj.transform(floorTransform)
          if drawFloorplan and random.random() < fractionTraj:
            traj.draw(floorImg, random.choice(trajColors))
            #traj.draw(floorImg, 'red')

      if saveFloorplanCoords:
        outputFilename, ext = outputFilename.split('.')
        outputFilename = outputFilename + '_FloorplanCoord.' + ext
        Trajectory.saveXmlCsv(outputFilename, trajList)
      
      if drawFloorplan:
        prependName = cameraName
        date = filename.split('\\')[-1].split('_')[-1].split('.')[0]
        floorplanOutputImage = outputPath + '\\' + prependName + '_' + date + '_' + str(random.randint(1,100000)) + '_floorplan_out.png'
	if verbose: 
          print "Saving image '%s'" % (floorplanOutputImage)
        while os.path.exists(floorplanOutputImage):
          floorplanOutputImage = prependName + '_' + date + '_' + str(random.randint(1,100000)) + '_floorplan_out.png'	
        floorImg.save(floorplanOutputImage)

"""
if __name__ == '__main__':

  if len(sys.argv) != 5 and len(sys.argv) != 6:
    sys.exit('Error: Usage: Calib <calibration file> <process file> <output folder> <distort 0/1> <distort parameters>')


  calibrationFilename = sys.argv[1]
  processFilename = sys.argv[2]
  outputPath = sys.argv[3]
  #Correct for lens distortion? True = yes
  correctDistortion = int(sys.argv[4])
  distortionParameters = sys.argv[5]

  Calib(calibrationFilename, processFilename, outputPath, correctDistortion, distortionParameters)
"""
