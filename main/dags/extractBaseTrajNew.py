# import pdb
import DBConnector
import datetime
import os
import sys
import csv
import collections
# import cProfile
import exceptions
import traceback
# import mainDict
import sqlite3
import extractCamInfoFromLive

class DBInfo:
    schema = 'vms'
    user = 'vmsuser'
    pwd = 'irrelev@nt'
    def __init__(self,host='',user=user,pwd=pwd,schema=schema):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.schema = schema

class DBInfoAWS:

    user = 'omniadmin'
    pwd = 'videoMININGLabs2016'
    host = 'production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com'
    def __init__(self,schema='',host=host,user=user,pwd=pwd):
        host = siteID.replace('-', '_')
        self.host = host
        self.user = user
        self.pwd = pwd
        self.schema = schema
        
class DBInfoAWS_Test:
    user = 'omniadmin'
    pwd = 'videoMININGlabs2015'
    host = 'vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com'
    def __init__(self,schema='',host=host,user=user,pwd=pwd):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.schema = schema
def _expandCamRange(camRangeStr):
    camList = []
    camRangeStr = camRangeStr.strip()
    argv = camRangeStr.split(' ')
    while len(argv) > 0:
        if argv[0] == 'R':
            start = int(argv[1])
            stop = int(argv[2])
            argv = argv[3:]
            camRange = range(start, stop + 1)
            camList.extend(["cam%03d"%camNum for camNum in camRange])
        elif argv[0] == 'A':
            cam = int(argv[1])
            argv = argv[2:]
            camList.append("cam%03d"%cam)
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,camRangeStr))
    return camList
    
def _expandDateRange(dateRangeStr):
    dateList = []
    timeDelta = datetime.timedelta(1,0,0)
    dateRangeStr = dateRangeStr.strip()
    argv = dateRangeStr.split(' ')
    while len(argv) > 0:
        if argv[0] == 'A':
            date = argv[1]
            argv = argv[2:]
            date = datetime.datetime(int(date[0:4]),int(date[4:6]), int(date[6:]), 0, 0, 0, 0)
            dateList.append(date)
        elif argv[0] == 'R':
            startDate = argv[1]
            stopDate = argv[2]
            argv = argv[3:]
            startDate = datetime.datetime(int(startDate[0:4]),int(startDate[4:6]), int(startDate[6:]), 0, 0, 0, 0)
            stopDate = datetime.datetime(int(stopDate[0:4]),int(stopDate[4:6]), int(stopDate[6:]), 0, 0, 0, 0)
            currDate = startDate
            while currDate <= stopDate:
                dateList.append(currDate)
                currDate = currDate + timeDelta
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,dateRangeStr))
    return dateList
def _createMissingEventTables(table_name,db):
    schemaName,tableName = table_name.split('.')

    baseQuery = '''
        CREATE TABLE IF NOT EXISTS <schemaName>.<tableName> (
            `start` datetime NOT NULL,
            `milliseconds` smallint(6) NOT NULL,
            `duration` float NOT NULL,
            `a4` longtext,
            KEY `<tableName>` (`start`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8;'''
    sqlQuery = baseQuery.replace("<tableName>",tableName).replace("<schemaName>",schemaName)
    print "Creating table since its missing"
    print sqlQuery
    db.execute(sqlQuery)


def _createOutputDirs(rootDir):
    if not os.path.exists(rootDir):
        try:
            os.makedirs(rootDir)
        except:
            print "Could NOT create out directory: %s\n"%(rootDir)
            pass

def _justify(st,num):
    while len(st) < num:
        st = '0'+ st
    return st

def _getStartTime(row):
    year = str(row[0].year)
    month = _justify(str(row[0].month),2)
    day = _justify(str(row[0].day),2)
    hour = _justify(str(row[0].hour),2)
    minutes = _justify(str(row[0].minute),2)
    second = _justify(str(row[0].second),2)
    microSec = _justify(str(row[1]),3)
    return year+'-'+month+'-'+day+'T'+hour+':'+minutes+':'+second+'.'+microSec

def _getStartTimeFromTraj(row):
    field = row[2].split('>')[0].split('=')[-1][1:][:-1]
    return field

def extractTrajectories(date, dbInfoObj, rootDir, siteID, camDict,project_type):
    db = DBConnector.DBConnector(host=dbInfoObj.host, user=dbInfoObj.user, password=dbInfoObj.pwd, database=dbInfoObj.schema) 

    outputFields = ['Start Time','Duration','Trajectory']
    if not os.path.exists(rootDir):
        os.mkdir(rootDir)

    dataSqlBase = """
    SELECT
    start_time,duration,a4 as track
    FROM
    <tablename>
    WHERE
    `start_time`
    BETWEEN '<date> 00:00:00' AND '<date> 23:59:59'
    ORDER BY `start_time`;"""
    
    
    for camName in camDict:
        temp_track_id = 0
        if project_type == 'old':
            dataSql = dataSqlBase.replace('<tablename>',camDict[camName])
            print "camName = %s\t table = %s"%(camName,camDict[camName])
        else:
            dataSql = dataSqlBase.replace('<tablename>', camName.lower()+'_vision')
            print "camName = %s\t table = %s"%(camName,camName.lower()+'_vision')


        rowNum = 0
        headers = {}
        dateStr = currDate.strftime("%Y-%m-%d")
        sql = dataSql.replace('<date>',dateStr)
        retVal = db.execute(sql)
        print "\tDate: %s\tnumTraj = %d" % (dateStr, len(retVal))

        fileName = camName + '_' + currDate.strftime("%Y%m%d") + ".csv"
        outFilename = os.path.join(rootDir, fileName)
        fOut = csv.DictWriter(open(outFilename, 'w'), fieldnames=outputFields, dialect='excel', lineterminator='\n')
        # write headers
        for field in outputFields:
            headers[field] = field
        fOut.writerow(headers)


        for row in retVal:
            startTime = _getStartTimeFromTraj(row)
            duration = row[1]
            traj = row[2]
            if traj == None:
                continue
            headers['Start Time'] = startTime
            headers['Duration'] = duration
            headers['Trajectory'] = traj
            if "trajectory id='-1'" in traj:
                temp_track_id += 1
                traj = traj.replace("trajectory id='-1'","trajectory id='%s'" %(str(temp_track_id)))
            fOut.writerow(headers)


def main(date,ignoreCams, outputDir, siteID, project_type = ''):
    # parse the project file to retrieve database id and host name
    
        if project_type == '':
            project_type = 'old'
        if '-' not in siteID:
            raise Exception("store id should be in format retailer-storeNum")

    # DBname, camDict[cam001]:(322_5632)
        camDict,host = extractCamInfoFromLive.extractCamInfo(siteID,date)

        finalCamDict = {}
        for cam in camDict:
            if cam not in ignoreCams:
                finalCamDict[cam] = camDict[cam]

        # create output dirs for each of the specified store
        _createOutputDirs(outputDir)
    

        if project_type == 'old':
            # DB Credentials to connect to videomining database
            dbInfoObj = DBInfo(host=host)

        else:
            # DB Credentials to connect to AWS
            dbInfoObj = DBInfoAWS(schema=host)


        extractTrajectories(date, dbInfoObj, outputDir, siteID, camDict, project_type)
    


if __name__ =='__main__':
    date = '20161201'
    ignoreCams = ['cam022']
    outputDir = r'~/airflow/swy-1751/Data/00_Base'
    siteID = 'swy-1751'
    project_type = 'old'
    main(date,ignoreCams, outputDir, siteID, project_type)
