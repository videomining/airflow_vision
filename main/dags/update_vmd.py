import datetime
import sys
import VmdDataLoader
# import xml.etree.ElementTree
import MySQLdb
import posseUtils
import os
import os.path
import traceback
import pdb
import GetshopUtils

class POSData:
    def __init__(self, schema_table):
        try:
            assert len(schema_table.split('.')) == 2
        except:
            raise Exception("\n\n'posSchema' should be of the form <schema>.<table>. Supplied value '%s' does not comply\n"%schema_table)    
        schema = schema_table.split('.')[0]
        posSettings = posseUtils.PoSSettings()
        self.db = MySQLdb.connect(host=posSettings.host,db='%s'%schema,user=posSettings.user,passwd=posSettings.password)
        self.cursor = self.db.cursor()
        baseQuery = "Select txn_dt, start_time, buyer from <posSchema> where node_id like '%s'"
        self.baseQuery = baseQuery.replace("<posSchema>",schema_table)

        baseQuerySingleDay = "Select txn_dt, start_time, buyer,node_id from <posSchema> where node_id in (<nodeidlist>) and txn_dt = '%s'"
        self.baseQuerySingleDay = baseQuerySingleDay.replace("<posSchema>",schema_table)

    def __del__(self):
        self.cursor.close()
        self.db.close()
        
    def getPOSData(self, storeId, categoryName):
        buyers = {}
        # db = MySQLdb.connect(host='vmdb_master',db='pos_swy',user="vmsuser",passwd="irrelev@nt")
        # cursor = db.cursor()
        # baseQuery = "Select txn_dt, start_time, buyer from pos_gsi.4_tran_node_catg_aisle where store_id=%d AND node_id = '%s'"
        
        
        posCategoryName = categoryName
        query = self.baseQuery % (posCategoryName)

        self.cursor.execute(query)
        results = self.cursor.fetchall()

        for row in results:
            year, month, day = map(int,str(row[0]).split('-'))
            hour = int(str(row[1]).split(':')[0])
            t = datetime.datetime(year, month, day, hour)
            buyers[t] =  row[2]
    ##    print buyers
        return buyers
    def getPOSDataPerDay(self, storeId, nodeList,date):
        buyers = {date:{}}
        # db = MySQLdb.connect(host='vmdb_master',db='pos_swy',user="vmsuser",passwd="irrelev@nt")
        # cursor = db.cursor()
        # baseQuery = "Select txn_dt, start_time, buyer from pos_gsi.4_tran_node_catg_aisle where store_id=%d AND node_id = '%s'"
        nodeStr = []
        for nodeId in nodeList:
            nodeStr.append("'" + nodeId + "'")

        query = self.baseQuerySingleDay % (date)
        query = query.replace('<nodeidlist>', ','.join(nodeStr))
        # query.replace('"','')
        self.cursor.execute(query)
        results = self.cursor.fetchall()

        for row in results:
            # year, month, day = map(int,str(row[0]).split('-'))
            # hour = int(str(row[1]).split(':')[0])
            # t = datetime.datetime(year, month, day, hour)
            node_id = row[3]
            if node_id not in buyers[date]:
                buyers[date][node_id] = {}
            buyers[date][node_id][str(row[1]).zfill(8)] =  row[2]
    ##    print buyers
        return buyers
def _CreateLogStructure(logFileDir,siteId, dateString):
    # convert dateString to a string of dates and construct a list of year-month 
    start = datetime.datetime(int(dateString[0][:4]), int(dateString[0][4:6]), int(dateString[0][6:]))
    stop = datetime.datetime(int(dateString[1][:4]), int(dateString[1][4:6]), int(dateString[1][6:]))
    delta = datetime.timedelta(1,0,0)
    # subdirs = set([])
    subdirToDateDict = {}
    dateTologFileDict = {}
    curr = start
    while curr <= stop:
        subdir = start.strftime("%Y-%m")
        # subdirs.update([subdir])
        if subdir not in subdirToDateDict:
            subdirToDateDict[subdir] = [curr.strftime("%Y%m%d")]
        else:
            subdirToDateDict[subdir].append(curr.strftime("%Y%m%d"))
        curr = curr + delta
    
    for subdir in subdirToDateDict:
        fullPath = os.sep.join([logFileDir,siteId,subdir])
        if not os.path.exists(fullPath):
            try:
                os.makedirs(fullPath)
            except Exception, e:
                print "Error %s" % (e)
                raise
            # pass
        dateStrList = subdirToDateDict[subdir]
        for date in dateStrList:
            logFile  =os.sep.join([fullPath,date+'.txt'])
            dateTologFileDict[date] = logFile
            # fp = open(logFile,'w')
            # fp.close()
    return dateTologFileDict
    

def main(databaseName, sitesDirectory, baseShopInfoDirectory, dateString, siteId, tableName, logFileDir, posSchema, cams, catNames=[]):
    '''
    the original main function that uploads cat + store data for a date range. The way it does the upload is
    it takes a category and uploads it for the entire date range and then moves onto the next category.
    
    This is being retained and probably will be removed at a later time when all work flows are stabilized.
    '''

    posObj = POSData(posSchema)
    dateToLogFileDict = _CreateLogStructure(logFileDir,siteId, dateString)

    segments = [None]
    numSegmentCategories = 3

    shopInfoDirectory = os.path.join(sitesDirectory, siteId, baseShopInfoDirectory)

    l = VmdDataLoader.VMDDataLoader(shopInfoDirectory, databaseName, siteId)

    l.setTimeRange(*dateString)
    l.createTable(tableName)

    print siteId

    l.setSegment(None, False)

    l.loadStore(id = siteId, dateToLogFileDict=dateToLogFileDict, cams=cams, scaleFactor = 1, segments=segments, numSegmentCategories= numSegmentCategories, getPOSData=posObj.getPOSData)
    

    # 'wholestore' is the only siteRegion used
    siteRegionId = "wholestore"
    print "\t%s"%siteRegionId
    l.setStoreRegionId(siteRegionId)
    productCategoryData = []

    # generate productCategoryData
    catgList = sorted(l.getCategoryList(dateToLogFileDict))

    if len(catNames) != 0:
        catSet = set(catNames)
        catgSet = set(catgList)
        errorCats = list(catSet - catgSet)
        print "The following cats are NOT present in passer info files for the specified date range\n"
        for errorCat in errorCats:
            print "%s\n"%errorCat
        catNames = list(catSet.intersection(catgSet))
    
    if len(catgList) != 0:
        for categoryId in catgList:
            if len(catNames) == 0 or categoryId in catNames:
                print "\t\t%s"%categoryId
                l.setSegment(None, False)
                productCategory = l.loadProductCategory(id=categoryId, dateToLogFileDict=dateToLogFileDict, trafficCountSF=1, shopperCountSF=1, segments=segments, numSegmentCategories= numSegmentCategories, getPOSData=posObj.getPOSData)
                if productCategory:
                    productCategoryData.append(productCategory)
                # else:
                    # fp = open(outputErrorFile,'a')
                    # fp.write(categoryId)
                    # fp.write("\n")
                    # fp.close()
                l.commit()
    else:
        print "There is NO data in the get shop folders to be uploaded!!"

    
    # upload everything
    l.setSegment(None, False)

    if siteRegionId.replace(' ','').lower() != 'entirestore':
        l.loadStoreRegion(id=siteRegionId,dateToLogFileDict=dateToLogFileDict, regionId=siteRegionId, trafficCountSF=1, shopperCountSF=1, segments=segments, numSegmentCategories= numSegmentCategories, productCategories = productCategoryData)

    l.commit()

def getShopDictToNodeList(getShopDict):
    nodeList = []
    for aisle in getShopDict:
        if 'endcap' in aisle.lower() or 'perimeter' in aisle.lower() or 'racetrack' in aisle.lower():
            print ''
        else:
            nodeList.append(getShopDict[aisle]['node_id'])
        for node_id in getShopDict[aisle]['category']:
            if node_id != '':
                nodeList.append(node_id)
    return nodeList
def main_Mongo(databaseName, bucket_name, mongoCollection, temp_out_path, dateString, siteId, tableName, logFileDir,project_type, posSchema,table_suffix):
    try:
        print "Enters update_vmd"
        try:
            os.mkdir(temp_out_path)
        except:
            pass
        posObj = POSData(posSchema)
        dateToLogFileDict = _CreateLogStructure(logFileDir,siteId, dateString)
        dateList = sorted(dateToLogFileDict.keys())
        start_date = dateList[0]
        end_date = dateList[-1]
        l = VmdDataLoader.VMDDataLoader(temp_out_path, databaseName, siteId)
        # dateString is a tuple (startDateOfWeek, stopDateOfWeek)
        l.setTimeRange(*dateString)
        l.createTable(tableName)
        # print siteId
        l.setSegment(None, False)
        print "-------------------------------\n"
        print "Loading store data: %s for date range: %s - %s"%(siteId, start_date, end_date)
        l.loadStoreMongo(id = siteId, bucket_name = bucket_name, dateToLogFileDict=dateToLogFileDict, getPOSData=posObj.getPOSData)
        l.commit()
        print "\tDone!\n"
        print "-------------------------------\n"
        #loading the categories which need to be uploaded
        gsuObj = GetshopUtils.getShop()
        getshopDict = gsuObj.fetchGetShopInfo(siteId,project_type)
        if len(getshopDict)!= 0:
            print "-------------------------------\n"
            print "Loading category data: %s for date range: %s - %s"%(siteId, start_date, end_date)


            for dt in dateList:
                getShopFound = False
                for dateRange in getshopDict:
                    start,end = dateRange.split('_')
                    if dt >= start and dt <= end:
                        nodeList = getShopDictToNodeList(getshopDict[dateRange])
                        # nodeList = ['swy#0413#2095#0']
                        if table_suffix:
                            mongoCollectionFinal = mongoCollection +'_' + dt[:6]
                        else:
                            mongoCollectionFinal = mongoCollection
                        l.loadProductCategoryPerDayMongo(dt = dt,logFile=dateToLogFileDict[dt],nodeList = nodeList,project_type=project_type,mongoColl = mongoCollectionFinal,getPOSData=posObj.getPOSDataPerDay)
                        l.commit()
                        getShopFound = True
                if not getShopFound:
                    raise Exception('No getshop files found the date %r' %dt)
            print "-------------------------------\n"
        else:
            raise Exception('No getshop files found in daterange')
    except:
        exinfo = sys.exc_info()
        stack_trace = ''.join([str(exinfo[1]) + '\n', ''.join(traceback.format_tb(exinfo[2]))])
        print stack_trace
        return stack_trace
    
def main2(databaseName, sitesDirectory, baseShopInfoDirectory, dateString, siteId, tableName, logFileDir, posSchema, cams,catNames=[] ):
    '''
    this is the main entry point for the updated way of uploading - upload one date at a time and all categories
    that are present for that date. This list of categories is fetched for every date to be uploaded.
    '''
    try:
        print "Enters update_vmd"
        posObj = POSData(posSchema)
        dateToLogFileDict = _CreateLogStructure(logFileDir,siteId, dateString)
        dateList = sorted(dateToLogFileDict.keys())
        start_date = dateList[0]
        end_date = dateList[-1]
        
        segments = [None]
        numSegmentCategories = 3

        shopInfoDirectory = os.path.join(sitesDirectory, siteId, baseShopInfoDirectory)

        l = VmdDataLoader.VMDDataLoader(shopInfoDirectory, databaseName, siteId)

        # dateString is a tuple (startDateOfWeek, stopDateOfWeek)
        l.setTimeRange(*dateString)
        l.createTable(tableName)
        #create navtime table if not exists
        # l.createNavTable(navTableName)
        # print siteId

        l.setSegment(None, False)
        print "-------------------------------\n"
        print "Loading store data: %s for date range: %s - %s"%(siteId, start_date, end_date)    
        l.loadStore(id = siteId, dateToLogFileDict=dateToLogFileDict, cams=cams, scaleFactor = 1, segments=segments, numSegmentCategories= numSegmentCategories, getPOSData=posObj.getPOSData)
        l.commit()
        print "\tDone!\n"
        print "-------------------------------\n"
        print "Loading nav time data: %s for date range: %s - %s"%(siteId, start_date, end_date)
        nav_time_found = l.loadNavTimeFromCsv(dateToLogFileDict=dateToLogFileDict)
        print "\t\tDone!\n\t-------------------------------\n"
        print "\n\t-------------------------------\n\tLoading Category data\n"
        #siteRegionId = "wholestore"

        # 'wholestore' is the only siteRegion used
        siteRegionId = "wholestore"
        print "\n\n-------------------------------\n"
        print "Loading category data for date range: %s - %s"%(start_date, end_date)
        print "-------------------------------\n"
        l.setStoreRegionId(siteRegionId)
        productCategoryData = []
        prod_data = {}


        if len(catNames) != 0:
            catSet = set(catNames)
            catgList = l.getCategoryListInDateRange(dateToLogFileDict.keys())
            catgSet = set(catgList)
            errorCats = list(catSet - catgSet)
            print "\tThe following cats are NOT present in passer info files for the specified date range\n"
            for errorCat in errorCats:
                print "\t\t%s\n"%errorCat
            catNames = list(catSet.intersection(catgSet))
            print "\tThe following cats were found and will be uploaded:\n"
            for cat in catNames:
                print "\t\t%s\n"%cat
            

        for dt in dateList:
            if len(catNames) == 0:
                catList = sorted(l.getCategoryList_SingleDay(dt))
            else:
                catList = catNames



            if len(catList)> 0:
                print "\n\n<============================================================>\n\n"
                print "\tFound %d categories for date: %s\n"%(len(catList),dt)
                print "\n\t-------------------------------\n\tLoading Passer info\t%s\n"%dt
                traffic_counts = l.loadPasserInfo(dt, dateToLogFileDict[dt], trafficCountSF=1, segments=segments, numSegmentCategories= numSegmentCategories)
                print "\t\tDone!\n\t-------------------------------\n"

                print "\tDate: %s\n"%dt
                for idx, categoryId in enumerate(catList):
                    print "\t\t%d) %s - %s"%(idx+1, categoryId, dt)
                    l.setSegment(None, False)
                    try:
                        tc = traffic_counts[categoryId]
                    except KeyError:
                        tc = {}

                    productCategory = l.loadProductCategoryPerDay(id=categoryId, dt=dt, logFile=dateToLogFileDict[dt], trafficCountSF=1, shopperCountSF=1,nav_time_found=nav_time_found, segments=segments, numSegmentCategories= numSegmentCategories, getPOSData=posObj.getPOSData, trafficCounts = tc)
                    l.commit()

                    if productCategory:
                        if categoryId not in prod_data:
                            prod_data[categoryId] = productCategory
                        else:
                            prod_data[categoryId].update(productCategory)

                print "\t\tDone!\n\t-------------------------------\n"
            
        # upload everything
        l.setSegment(None, False)

        # structure of productCategoryData is as follows:
        # list of dictionaries
        # each dictionary contains 24* #days keys and category data as values. This is not organized by category name as it is not required
        # for computing stats of site_region_<store>_wholestore which is basically data of the whole store as a region. It basically adds up
        # all the traffic, shoppers etc for all categories
        for categoryId, productCategory in prod_data.iteritems():
            productCategoryData.append(productCategory)
        
        # if siteRegionId.replace(' ','').lower() != 'entirestore':
        #     l.loadStoreRegion(id=siteRegionId,dateToLogFileDict=dateToLogFileDict, regionId=siteRegionId, trafficCountSF=1, shopperCountSF=1, segments=segments, numSegmentCategories= numSegmentCategories, productCategories = productCategoryData)

        l.commit()            
    except:
        exinfo = sys.exc_info()
        stack_trace = ''.join([str(exinfo[1]) + '\n', ''.join(traceback.format_tb(exinfo[2]))])
        print stack_trace
        return stack_trace



    else:
        return None


if __name__ == '__main__':
    if len(sys.argv) < 10:
        sys.exit('Usage: %s <databaseName> <Sites Directory> <Base ShopInfo Directory> [date] tbl_<tableName> site_<siteID1> cat_<categoryName1> cat_<categoryName2>..logDir_<logDir> posSchema_')
# update_vmd.py c_safeway_phase2categories \\VMVASE\data\Dashboard\Data\Weekly\20120418_20120424 UploadData 20120418 20120424 tbl_gsi_2_data_fll site_566 logDir_\\vmdhari\work\temp\upload posSchema_pos_gsi.4_tran_node_catg_aisle
# update_vmd.py krg503 \\vmvase\Data\Dashboard\Data\Weekly\20140806_20140812 UploadData 20140806 20140812 tbl_swy_daily_upload_unscaled_hour_test_dbupload2 site_503 logDir_\\vmdhari\work\temp\upload posSchema_pos_gsi.4_tran_node_catg camList_cam056_cam057

    databaseName = sys.argv[1]
    sitesDirectory = sys.argv[2]
    baseShopInfoDirectory = sys.argv[3]

    if len(sys.argv) > 4:
        startDate = sys.argv[4]
        endDate = sys.argv[5]
    print startDate, endDate
    
    siteIDs = [x.split('site_')[-1] for x in sys.argv if x.find('site_') != -1]
    siteId = siteIDs[0]
    catNames = [x.split('cat_')[-1].lower() for x in sys.argv if x.find('cat_') != -1]
    
    # camList_cam056_cam057
    camList = [x.split('_')[1:] for x in sys.argv if x.find('camList_') == 0][0]
    
   
    tableName = [x.split('tbl_')[-1] for x in sys.argv if x.find('tbl_') != -1]
    logFileDir = [x.split('logDir_')[-1] for x in sys.argv if x.find('logDir_') != -1]
    if len(logFileDir) != 1:
        raise Exception("\n\nONLY one instance of logDir_ is to be supplied.\nNum instances in the arg list supplied: %d\n"%len(logFileDir))
    logFileDir = logFileDir[0]
    posSchema = [x.split('posSchema_')[-1] for x in sys.argv if x.find('posSchema_') != -1]
    if len(posSchema) != 1:
        raise Exception("\n\nONLY one instance of posSchema is to be supplied.\nNum instances in the arg list supplied: %d\n"%len(posSchema))
    posSchema = posSchema[0]

    
    if tableName == []:
        tableName = 'stats_1h'
    else:
        tableName = tableName[0]

    pdb.set_trace()
    now1 = datetime.datetime.now()
    main2(databaseName, sitesDirectory, baseShopInfoDirectory, (startDate, endDate), siteId, tableName, logFileDir, posSchema, camList, catNames)
    duration = datetime.datetime.now() - now1
    
    print "Total script running time %.2f\n"%(duration.seconds/60.0)
    



