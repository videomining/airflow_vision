from psycopg2._psycopg import cursor

import getXMLdata
import posseUtils
import getLiveCams
from io import BytesIO
import xml.etree.cElementTree as ET
import datetime
import MySQLdb
import os
import TeemoUtils
import sys
import traceback
import utilsAmazonS3
import os
import shutil

def _isFloorFixture(fixture_element):
    flag = 0
    if int(float(fixture_element.find('z0').text)) == 0:
        flag = 1
    return flag

def connectToDB():
    db = MySQLdb.connect(host = 'vmvisiondb.videomining.com',user = 'vmdatasvc',passwd = 'irrelev@nt',db = 'calibration')
    cursor = db.cursor()
    return cursor
def closeDB(cursor):
    try:
        cursor.close()
    except:
        pass


def getfurnitureFiles(storeName,dateList,outputPath):
    start_date = datetime.datetime.strptime(dateList[0], "%Y%m%d").date()
    end_date = datetime.datetime.strptime(dateList[-1], "%Y%m%d").date()

    query = '''
       SELECT project_info.name,project.version, project.data,project.start_date, project.end_date
        from calibration.project_info
        INNER JOIN calibration.project
        ON project_info.project_id=project.id
        where name like '<store_name_calib>%';'''


    sql = query.replace('<store_name_calib>', storeName)
    cursor = connectToDB()
    cursor.execute(sql)
    fetchall= cursor.fetchall()
    furnitureDict = {}
    for element in fetchall:
        begin = element[3]
        end = element[4]
        date = datetime.datetime.strftime(element[3],'%Y%m%d') +'_'+ datetime.datetime.strftime(element[4],'%Y%m%d')
        if begin > end_date or end < start_date:
            continue
        else:
            if date not in furnitureDict:
                furnitureDict[date] = {}

            strcamxml = element[2]

            tree = ET.ElementTree(ET.fromstring(strcamxml))
            treeall = tree.findall('camera_views/camera_view')
            lineset = []
            for camview in treeall:
                #camera_name = camview.find('name').text
                type_label = camview.find('type_label').text
                if cmp(type_label, 'Floorplan') == 0 :
                    fixture_info = camview.find('floorplan_info/fixtures', camview)
                    for fixture_element in fixture_info :
                        flag = _isFloorFixture(fixture_element)
                        if flag :
                            fixture_points = fixture_element.findall('points')
                            fixtures = map(lambda x : x.text, fixture_points)
                            for fixture_element in fixtures :
                                list_coords = fixture_element.strip('\n').split(',')
                                list_coords = map(lambda x : x.split('.')[0], list_coords)
                                no_points = len(list_coords)/2
                                for index in range(no_points-1) :
                                    line = list_coords[2*index] + ',' + list_coords[2*index+1] + ',' + list_coords[2*index+2] + ',' + list_coords[2*index+3]
                                    lineset.append(line)
                                last_line = list_coords[-2] + ',' + list_coords[-1] + ',' + list_coords[0] + ',' + list_coords[1]
                                lineset.append(last_line)
                                furnitureDict[date] = lineset
    closeDB(cursor)
    #outFilename = os.sep.join([outputPath, storeName,'FurnitureFullStore_date.txt'])
    outFilename = os.sep.join([outputPath,'FurnitureFullStore_date.txt'])
    writeFile(outFilename,furnitureDict)
    # for dateList in furnitureDict:
    #     outFilename = os.sep.join([outputPath,storeName+'_Furniture_'+dateList+'.txt'])
    #     with open(outFilename,'w') as fp:
    #         for val in furnitureDict[dateList]:
    #             fp.write(val)
    #             fp.write('\n')
    #         fp.close()
    
def writeFile(filename,dictVal):
    for dateVal in dictVal:
        outName = filename.replace('date',dateVal)
        first = True
        with open(outName, 'w') as fp:
            for val in dictVal[dateVal]:
                if not first:
                    fp.write('\n')
                first = False
                fp.write(val)

            fp.close()

def generateSetsGroups(storeName,dateList,outputPath):
    setDict, groupDict, joinDict = TeemoUtils.getCameraJoinFiles(storeName, dateList[0], dateList[-1])
    outSetFilename = os.sep.join([outputPath,'CameraSets_date.txt'])
    writeFile(outSetFilename, setDict)
    outGroupFilename = os.sep.join([outputPath,'CameraGroups_date.txt'])
    writeFile(outGroupFilename, groupDict)
    outJoinFilename = os.sep.join([outputPath,'CameraGroupsEntireStore_date.txt'])
    writeFile(outJoinFilename, joinDict)

    #outSetFilename = os.sep.join([outputPath,storeName,'CameraSets_date.txt'])
    #writeFile(outSetFilename, setDict)
    #outGroupFilename = os.sep.join([outputPath, storeName,'CameraGroups_date.txt'])
    #writeFile(outGroupFilename, groupDict)
    #outJoinFilename = os.sep.join([outputPath, storeName,'CameraGroupsEntireStore_date.txt'])
    #writeFile(outJoinFilename, joinDict)


        
def createCalibXml(storeName,XMLDict,outputPath):
    for dateRange in XMLDict:
        root = ET.Element("root")
        floorplan = ET.SubElement(root, "floorplan",name = 'floorplan.png')
        worldVal= ''
        for val in XMLDict[dateRange]['floorplan']['world']:
            worldVal += '('+str(val[0])+','+str(val[1])+'),'
        worldVal = worldVal.strip(',').replace('),',';').replace(')','').replace('(','').replace(' ','')
        localVal = ','.join(str(x) for x in XMLDict[dateRange]['floorplan']['local']).replace('),',';').replace(')','').replace('(','').replace(' ','')
        ET.SubElement(floorplan, "coord_mapping", local=localVal, world=worldVal)
        for camNum in XMLDict[dateRange]['camera']:
            camName = XMLDict[dateRange]['camera'][camNum]['name']
            location = ','.join(XMLDict[dateRange]['camera'][camNum]['location'])
            orientation = XMLDict[dateRange]['camera'][camNum]['orientation']
            focalLength = XMLDict[dateRange]['camera'][camNum]['focalLength']
            ccdSize = ','.join(str(x) for x in XMLDict[dateRange]['camera'][camNum]['ccdSize'])
            imageSize  = ','.join(str(x) for x in XMLDict[dateRange]['camera'][camNum]['imageSize'])
            camera = ET.SubElement(root, "camera", name=camName, type=XMLDict[dateRange]['camera'][camNum]['type'])
            ET.SubElement(camera, "calibration",
                          location=location, orientation=orientation, focalLength =focalLength,
                          ccdSize=ccdSize,imageSize= imageSize)

        tree = ET.ElementTree(root)
        #finalOutDir = os.sep.join([outputPath, storeName])
        #try:
        #    os.mkdir(finalOutDir)
        #except:
        #    pass
        filename = os.sep.join([outputPath,'Calibration_'+str(dateRange).replace('-', '')+'.xml'])
        # f = BytesIO()
        tree.write(filename,encoding='utf-8',xml_declaration=True)
        
def main(dateList,storeName,documentDir,bucket_name,file_type,fileList):
    try:
        XMLDict = getXMLdata.getXMLdata(storeName,dateList)
        print 'generating calibration file'
        createCalibXml(storeName,XMLDict,documentDir)
        print 'generating furniture file'
        getfurnitureFiles(storeName,dateList,documentDir)
        print 'generating Join files'
        generateSetsGroups(storeName,dateList,documentDir)
        print 'copying camera coverage xml'
        src = 'camera_coverage.xml'
        shutil.copyfile(src,documentDir+'/'+src)
        for filename in fileList:
            os.remove(filename)
            utilsAmazonS3.downloadFilesFromS3(bucket_name,file_type,filename)
    except:
	    exinfo = sys.exc_info()
	    stack_trace = ''.join([str(exinfo[1])+'\n',''.join(traceback.format_tb(exinfo[2]))])
	    print stack_trace
	    return stack_trace
    else:
        return None

if __name__ == '__main__':
    #optsFile = r'\\vmnode5\ProjectData\giant-6072\Documents\optsFiles\6072_optsFile_omni.yml'
    #optsDict = posseUtils._ParseOptsFile(optsFile)
    #storeName = optsDict['siteId']
    storeName = 'swy-1751'
    dateRange = 'A 20161201'
    documentDir = r'~/airflow/swy-1751/Documents'
    #dateList = posseUtils._ParseDateRange(optsDict["dateRange"])
    dateList = posseUtils._ParseDateRange(dateRange)
    XMLDict = getXMLdata.getXMLdata(storeName,dateList)
    print 'generating calibration file'
    #createCalibXml(storeName,XMLDict,optsDict["documentDir"])
    createCalibXml(storeName,XMLDict,documentDir)
    print 'generating furniture file'
    #getfurnitureFiles(storeName,dateList,optsDict["documentDir"])
    getfurnitureFiles(storeName,dateList,documentDir)
    print 'generating Join files'
    #generateSetsGroups(storeName,dateList,optsDict["documentDir"])
    generateSetsGroups(storeName,dateList,documentDir)
