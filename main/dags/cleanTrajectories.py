import glob
import sys
import Trajectory
import TrajectoryFilters
import math
import time
import os

def trajsOverlap(traj1, traj2):
    return traj1.endTime() >= traj2.startTime() and traj2.endTime() >= traj1.startTime()


def trajOverlapsTime(traj, startTime, endTime):
    return traj.endTime() >= startTime and endTime >= traj.startTime()


def fullyContainedInTime(traj1, traj2):
    """Determines if traj2 starts and ends within the time that traj1 exists"""
    if traj1.startTime() <= traj2.startTime() and traj1.endTime() >= traj2.endTime():
        return True
    
    return False

def containedInSpace(traj1, traj2, neighborhoodDistance, threshold):
    numNear = 0
    totalNum = 0.0
    traj1Index = -1
    traj1Time = 0
    traj2Index = 0
    traj2Time = 0
    
    distances = []
    startTime = max(traj1.startTime(), traj2.startTime())
    endTime = min(traj1.endTime(), traj2.endTime())

    while traj1Time < startTime:
        traj1Index += 1
        traj1Time = traj1.nodes[traj1Index][0]
        traj1X = traj1.nodes[traj1Index][1]
        traj1Y = traj1.nodes[traj1Index][2]
    while traj2Time < startTime:
        traj2Index += 1
        traj2Time = traj2.nodes[traj2Index][0]
        traj2X = traj2.nodes[traj2Index][1]
        traj2Y = traj2.nodes[traj2Index][2]
    
    while (traj1Time < endTime and traj2Time <= endTime) or (traj1Time <= endTime and traj2Time < endTime):
        direction1_X = -1
        direction1_Y = -1
        direction2_X = -1
        direction2_Y = -1
        angle1 = math.pi
        angle2 = math.pi
        flag = True

        #print traj1X, traj1Y
        #print traj2X, traj2Y
        
        if traj1Time < traj2Time:
            nextNode = traj1.nodes[traj1Index+1]
            nextTime = nextNode[0]
            nextX = nextNode[1]
            nextY = nextNode[2]

            try :
            
                totalTimeDiff = nextTime - traj1Time
                timeDiff = traj2Time - traj1Time
                try :
                    timePercentage = timeDiff/totalTimeDiff
                except :
                    timePercentage = 0
                    
                traj1X = traj1X * (timePercentage) + nextX * (1-timePercentage)
                traj1Y = traj1Y * (timePercentage) + nextY * (1-timePercentage)
                
            except :                
                pass
            
         
        elif traj2Time < traj1Time:
            nextNode = traj2.nodes[traj2Index+1]
            nextTime = nextNode[0]
            nextX = nextNode[1]
            nextY = nextNode[2]

            try :
                totalTimeDiff = nextTime - traj2Time
                timeDiff = traj1Time - traj2Time
                try:
                    timePercentage = timeDiff/totalTimeDiff
                except:
                    timePercentage = 0

                traj2X = traj2X * (timePercentage) + nextX * (1-timePercentage)
                traj2Y = traj2Y * (timePercentage) + nextY * (1-timePercentage)
            except :
                pass


        
        distance = math.sqrt((traj1X-traj2X)**2 + (traj1Y-traj2Y)**2)
        
        if distance < neighborhoodDistance:
            #if flag :
##            print math.degrees(angle1), math.degrees(angle2)
##            time.sleep(0.1)
##            if ((angle1 - angle2) >= -2*math.pi/2) and ((angle1 - angle2) <= 2*math.pi/2):
            numNear += 1
        
        totalNum += 1
        
        if traj1Time < traj2Time:
            traj1Index += 1
            traj1Time = traj1.nodes[traj1Index][0]
            traj1X = traj1.nodes[traj1Index][1]
            traj1Y = traj1.nodes[traj1Index][2]
        elif traj2Time < traj1Time:
            traj2Index += 1
            traj2Time = traj2.nodes[traj2Index][0]
            traj2X = traj2.nodes[traj2Index][1]
            traj2Y = traj2.nodes[traj2Index][2]
        else:
            traj1Index += 1
            traj1Time = traj1.nodes[traj1Index][0]
            traj1X = traj1.nodes[traj1Index][1]
            traj1Y = traj1.nodes[traj1Index][2]
            traj2Index += 1
            traj2Time = traj2.nodes[traj2Index][0]
            traj2X = traj2.nodes[traj2Index][1]
            traj2Y = traj2.nodes[traj2Index][2]
            
    
    if totalNum == 0 or numNear/totalNum < threshold:
        return False
    else:
        return True
    
def getTrajTimeGroups(trajList):
    trajGroups = []
    trajList.sort(Trajectory.sortTrajByStart)
    
    trajGroup = []
    startTime = 0
    endTime = 0
    
    #print trajList
    trajGroup = [trajList[0]]
    startTime = trajList[0].startTime()
    endTime = trajList[0].endTime()
    
    for index in range(1, len(trajList)):
        currTraj = trajList[index]
        if trajOverlapsTime(currTraj, startTime, endTime):
            trajGroup.append(currTraj)
            if currTraj.endTime() > endTime:
                endTime = currTraj.endTime()
        else:
            trajGroups.append(trajGroup)
            trajGroup = [currTraj]
            startTime = currTraj.startTime()
            endTime = currTraj.endTime()
    
    trajGroups.append(trajGroup)
    return trajGroups

def splitTimeGroupTrajs(timeGroup, neighborhoodDistance, acceptanceThreshold):

    timeGroupCopy = list(timeGroup)
    spatialGroups = []
    currentGroup = []
    newlyAdded = []

    # pop elements from timeGroupCopy until none remain
    while len(timeGroupCopy) > 0:

        # start a new group if no new trajs were found last iteration
        if newlyAdded == []:
            if currentGroup != []:
                spatialGroups.append(currentGroup)
                currentGroup = []
            newlyAdded.append(timeGroupCopy.pop(0))

        # get the neighbors of all newly added trajs
        # iterate backwards so we can pop trajs safely
        neighbors = []

        for traj in newlyAdded:
            for i in range(len(timeGroupCopy) - 1, -1, -1):        
                if trajsOverlap(timeGroupCopy[i], traj) and \
                   containedInSpace(timeGroupCopy[i], traj, neighborhoodDistance, acceptanceThreshold):
                    neighbors.append(timeGroupCopy.pop(i))

        currentGroup += newlyAdded
        newlyAdded = neighbors

    # newlyAdded and currentGroup will still contain some trajectories
    # after the final iteration
    currentGroup += newlyAdded
    if currentGroup != []:
        spatialGroups.append(currentGroup)
    

    for groupIndex in range(len(spatialGroups)):
        group = spatialGroups[groupIndex]
        group = list(group)
        group.sort(Trajectory.sortTrajByStart)
        spatialGroups[groupIndex] = group
    
    return spatialGroups


def mergeTrajsInGroup(trajGroup):
    baseTraj = trajGroup[0]
    for traj in trajGroup[1:]:
        baseTraj = Trajectory.Trajectory.combine(baseTraj, traj)

    return baseTraj


def cleanTrajectories(trajectoryFilename, neighborhoodDistance, spatialThreshold, outputDirectory):
# """"Usage: cleanTrajectories.py <Trajectory Filename> <Neighborhood Distance> <Spatial Threshold> <Output Directory>"""
    try:
        os.makedirs(outputDirectory)
    except:
        pass
    print "################Processing input file: %s"%(os.path.basename(trajectoryFilename))
    outputFilename = '%s/%s' % (outputDirectory, os.path.basename(trajectoryFilename))
    print "Processing file: %s"%(outputFilename)
    trajList = Trajectory.Trajectory.loadXmlCsv(trajectoryFilename)
    print "# trajList (after loading from traj file) = %d"%len(trajList)
    trajList = TrajectoryFilters.filterMinDuration(trajList, 2)
    print "# trajList (after filtering) = %d"%len(trajList)
    if len(trajList)==0:
        Trajectory.Trajectory.saveXmlCsv(outputFilename, trajList)
    else:
        trajGroups = getTrajTimeGroups(trajList)
        spaceGroups = []
        
        for group in trajGroups:
            splitGroups = splitTimeGroupTrajs(group, neighborhoodDistance, spatialThreshold)
            spaceGroups += splitGroups

        trajList = []
        for group in spaceGroups:
            trajList.append(mergeTrajsInGroup(group))
        
        Trajectory.Trajectory.saveXmlCsv(outputFilename, trajList)
