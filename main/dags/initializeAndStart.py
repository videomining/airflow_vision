import sys,os
import subprocess

def initializeDb():
    os.chdir(r'/home/ubuntu/airflow/')
    initialize = subprocess.Popen(["airflow","initdb"],
                                      stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    initialize.wait()
    processResp, err = initialize.communicate()
    return processResp,err

def startWebserver():
    os.chdir(r'/home/ubuntu/airflow/')
    startWeb = subprocess.Popen(["airflow","webserver","-p","8080","-D"],
                                      stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    startWeb.wait()
    processResp, err = startWeb.communicate()
    return processResp,err

def startScheduler():
    os.chdir(r'/home/ubuntu/airflow/')
    startSchedule = subprocess.Popen(["airflow","scheduler","-D"],
                                      stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    initialize.wait()
    processResp, err = initialize.communicate()
    return processResp,err

if __name__ == '__main__':
    processResp,err = initializeDb()
    if processResp == '':
        resp1,err1 = startWebserver()
        if resp1 == '':
            resp2,err2 = startScheduler()
            if resp2 != '':
                raise Exception('Error while starting scheduler')
        else:
            raise Exception('Error while starting webserver')
    else:
        raise Exception('Error while initializing database')

