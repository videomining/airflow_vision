import cleanTrajectories
import mergeTrajectories
import getXMLdata
import os.path
import BatchConvertTrajToFloorplanCoord
from airflow.operators import PythonOperator
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import posseUtils
import extractBaseTrajNew
import generateInputFiles 
import glob
import runVisionPostprocessor
import BatchFilterJoinedTraj
import os
import sys
import getShopInfoInRegionNew
import FaceProcessor
import extractCamInfoFromLive
import update_vmd
import utilsAmazonS3
import os
def addToPath(dirName):
    sys.path.append(os.sep.join([os.path.dirname(os.path.abspath(__file__)),dirName]))


bucket_name = 'vml-binaries'
file_type = 'branches/ec2/origin/master/build/bin'
    
extractTaskName = {}
dowloadInputFilesTaskName = {}
runVisionPPTaskName = {}
runFilterTaskName = {}
runGetshopTaskName = {}
runUploadTaskName = {}
joinBrokenTask = {}
def createDir(dirPath):
    if not os.path.isdir(dirPath):
        os.mkdir(dirPath)
    
def parseConfig(optsFile):
    optsDict = posseUtils._ParseOptsFile(optsFile)
    return optsDict

# dag can be initialized with some default arguments
def initDag():
    default_args = {
	    'owner': 'airflow',
	    'depends_on_past': False,
	    'start_date': datetime(2017, 5, 31),
	    'email': ['dataservices@videomining.com'],
	    'email_on_failure': True,
	    'retries': 1,
	    'retry_delay': timedelta(minutes=5),
	}
    #bashtab command used to set the time when the dag is to run
    dag_posse = DAG('posse', default_args=default_args,schedule_interval='@* 6 * * *')
    return dag_posse


def extractBaseTrajWrapper(date, ignoreCams, outputDir, siteID,project_type):
    extractBaseTrajNew.main(date, ignoreCams, outputDir, siteID,project_type)

def generateInputFilesWrapper(dateList,storeName,documentDir,bucket_name,file_type,fileList):
    generateInputFiles.main(dateList,storeName,documentDir,bucket_name,file_type,fileList)
    
def generateVisionPPWrapper(resolution,date,join1_val,join2_val,join3_val,location):
    runVisionPostprocessor.runVisionPP(resolution,date,join1_val,join2_val,join3_val,location)


def generateFilterTaskWrapper(dateStr, bucket_name, inputPath, outputPath, filterlongFlag, store, getShopType):
    fileType = 'Filter'
    filterFlag = False
    BatchFilterJoinedTraj.BatchFilterJoinedTraj(dateStr, bucket_name, inputPath, outputPath, fileType, filterFlag, store)
    if filterlongFlag:
        fileType = 'FilterLong'
        filterFlag = True
        BatchFilterJoinedTraj.BatchFilterJoinedTraj(dateStr, bucket_name, inputPath, outputPath, fileType, filterFlag, store)    
    else:
        BatchFilterJoinedTraj.getGSIDoorCount(store,dateStr,bucket_name,inputPath,outputPath,getShopType)

def generateGetShopTaskWrapper(tempFileDir, fileDate, bucket_name, mongoCollection, storeId,getShopType):
    getShopInfoInRegionNew.main(tempFileDir, fileDate, bucket_name, mongoCollection, storeId,getShopType)

def generateUploadTaskWrapper(databaseName, bucket_name, mongoCollection, temp_out_path, date, siteId, tableName, logFileDir,project_type, posSchema,table_suffix):
    dateString = (date,date)
    update_vmd.main_Mongo(databaseName, bucket_name, mongoCollection, temp_out_path, dateString, siteId, tableName, logFileDir,project_type, posSchema,table_suffix)
	
# def main_joinBrokenTracks(startDateHour, endDateHour, storeName, facCam, timeZone):
    # FaceProcessor.FacePostProcessorJbt(startDateHour, endDateHour, storeName, facCam, timeZone)
    
#load all the tasks by parsing the config file and add them to the dag scheduler
def loadAllTasks(optsFile,dag_posse):
    optsDict = posseUtils._ParseOptsFile(optsFile)
    if optsDict['ignoreCams'] == '' or optsDict['ignoreCams'] == None:
        ignoreCams = []
    else:
        camList = posseUtils._ParseCamRange(optsDict['ignoreCams'])
        ignoreCams = ['cam%03d'%val for val in camList]
    baseDir = os.sep.join([optsDict['rootDir'],optsDict['baseDir']])
    documentDir = os.sep.join([optsDict['rootDir'],optsDict['documentsDir']])
    store = optsDict['siteId']
    try:
        project_type = optsDict['projectType']
    except:
        project_type = ''
    dateList = posseUtils._ParseDateRange(optsDict['dateRange'])
    extractTaskName[store] = {}
    dowloadInputFilesTaskName[store] = {}
    runVisionPPTaskName[store] = {}
    runFilterTaskName[store] = {}
    runGetshopTaskName[store] = {}
    runUploadTaskName[store] = {}
    joinBrokenTask[store] = {}
    startDate = dateList[0]
    endDate = dateList[-1]
    dateRange = startDate+'_'+endDate
    vidResolution = optsDict['processingResolution']
    join1_val = optsDict['IJDistThresholdSets']
    join2_val = optsDict['IJDistThresholdGroups']
    join3_val = optsDict['IJDistThreshold']
    baseDir = optsDict['rootDir']
    bucket_name = 'vm-posse-'+store
    inputPath = os.sep.join([optsDict['rootDir'],optsDict['level3MergedGroupsDir']])
    tempOutputPath = os.sep.join([optsDict['rootDir'],'temp_out'])
    filterLongTracksFlag = False
    try:
        storeType = optsDict['storeType']
        if storeType.lower() == 'csi':
            filterLongTracksFlag = True
    except:
        pass
    getShopType = optsDict['getShopType']
        databaseName = optsDict['dbName']
    tableName = optsDict['dbTableNameUnscaled']
    posSchema = optsDict['posSchema']
    logFileBaseDir = os.sep.join([os.path.dirname(os.path.abspath(__file__)),'..','log_posse']) 
    logFileStoreDir = os.sep.join([logFileBaseDir,store])
    
   
    createDir(inputPath)
    createDir(tempOutputPath)
    createDir(optsDict['rootDir'])
    createDir(baseDir)
    createDir(documentDir)
    createDir(logFileBaseDir)
    createDir(logFileStoreDir)

    # VisionFilename = '/home/vmadmin/airflow/dags/Vision_Postprocessor'
    # os.remove(VisionFilename)
    # utilsAmazonS3.downloadFilesFromS3(bucket_name,file_type,VisionFilename)
    
    #operators: represent a job in airflow . Example: BashOperator, PythonOperator
    #we use python operator since our backend is python
    #assign python fuctions to the job and then assign the dag where the operator will be used
    
    dowloadInputFilesTaskName[store][dateRange] = PythonOperator(
	        task_id = 'download_files_%s_%s_%s'%(store,startDate,endDate),
	        python_callable = generateInputFilesWrapper,
	        op_kwargs = {'dateList':dateList,'storeName':store,'documentDir':documentDir,'bucket_name': bucket_name,'file_type': file_type,'fileList': [FaceFilename,VisionFilename]},
	        dag = dag_posse)

    for date in dateList:
        extractTaskName[store][date] = PythonOperator(
	            task_id = 'extract_%s_%s'%(store,date),
	            python_callable = extractBaseTrajWrapper,
	            op_kwargs = {'date':date,'ignoreCams':ignoreCams,'outputDir':baseDir,'siteID':store,'project_type':project_type},
	            dag = dag_posse)

        extractTaskName[store][date].set_upstream(dowloadInputFilesTaskName[store][dateRange])

    for date in dateList:
        runVisionPPTaskName[store][date] = PythonOperator(
			    task_id = 'postprocess_%s_%s'%(store,date),
			    python_callable = generateVisionPPWrapper,
			    op_kwargs = {'resolution':vidResolution,'date':date,'join1_val':join1_val,'join2_val':join2_val,'join3_val':join3_val,'location':baseDir},
			    dag = dag_posse)

        runVisionPPTaskName[store][date].set_upstream(extractTaskName[store][date])
    
    for date in dateList:
        runFilterTaskName[store][date] = PythonOperator(
			    task_id = 'filter_%s_%s'%(store,date),
			    python_callable = generateFilterTaskWrapper,
			    op_kwargs = {'dateStr':date, 'bucket_name':bucket_name, 'inputPath':inputPath, 'outputPath':tempOutputPath, 'filterlongFlag':filterLongTracksFlag, 'store':store, 'getShopType':getShopType},
			    dag = dag_posse)

        runFilterTaskName[store][date].set_upstream(runVisionPPTaskName[store][date])
    try:
        table_suffix = optsDict['tableSuffix']
    except:
         table_suffix = False

    for date in dateList:
        mongoCollection = 'raw_vision_' + store
        if table_suffix:
            mongoCollection = mongoCollection + '_' + date[:6]
        runGetshopTaskName[store][date] = PythonOperator(
			    task_id = 'filter_%s_%s'%(store,date),
			    python_callable = generateGetShopTaskWrapper,
			    op_kwargs = {'tempFileDir':tempOutputPath, 'fileDate':date, 'bucket_name':bucket_name, 'mongoCollection':mongoCollection, 'storeId':store,'getShopType':getShopType},
			    dag = dag_posse)

        runGetshopTaskName[store][date].set_upstream(runFilterTaskName[store][date])
    

    for date in dateList:
        mongoCollection = 'raw_vision_' + store
        runUploadTaskName[store][date] = PythonOperator(
			    task_id = 'filter_%s_%s'%(store,date),
			    python_callable = generateUploadTaskWrapper,
			    op_kwargs = {'databaseName':databaseName, 'bucket_name':bucket_name, 'mongoCollection':mongoCollection, 'temp_out_path':tempOutputPath, 'date':date, 'siteId':store, 'tableName':tableName, 'logFileDir':logFileStoreDir, 'project_type':getShopType, 'posSchema':posSchema, 'table_suffix': table_suffix },
			    dag = dag_posse)

        runUploadTaskName[store][date].set_upstream(runGetshopTaskName[store][date])

optsDir = '/home/vmadmin/airflow/dags/opts_files'
optsFiles = glob.glob(os.sep.join([optsDir,'*.yml']))
print optsFiles
#initialize the dag here
dag_posse = initDag()
for optsFile in optsFiles:
    #load all the tasks here
    loadAllTasks(optsFile,dag_posse)