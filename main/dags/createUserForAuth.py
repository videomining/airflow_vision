import airflow
from airflow import models, settings
from airflow.contrib.auth.backends.password_auth import PasswordUser
def createUser():
    session = settings.Session()
    try:
        user = PasswordUser(models.User())
        user.username = 'vmdatasvc'
        user.email = 'vision@videomining.com'
        user.password = 'irrelev@nt'
        session.add(user)
        session.commit()
        session.close()
    except:
        session.rollback()
        session.close()
        pass
if __name__ == '__main__':
    print 'Creating user'
    createUser()