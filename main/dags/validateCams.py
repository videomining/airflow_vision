from DBConnector import DBConnector as dbc
import xml.etree.cElementTree as ET
# from GenerateJobs import *
from posseUtils import _ParseCamRange
from posseUtils import _ParseDateRange
# from Calib import calibrationParser
import getXMLdata
from itertools import combinations
import os
import datetime

import pdb

sqls = {\
    'get_proj_id': """select project_id from vms.project_info where name = '%s';""" ,\
    'get_latest_proj_xml': """select data from vms.project where id = %d and version = (select max(version) from vms.project where id = %d);""",\
    'get_proj_xml': """select data from vms.project where id = %d and version = %d;""",\
    'get_proj_xml_for_versions': """select data from vms.project where id = %d and version IN %s;""",\
    'get_all_versions_in_date_range': """SELECT project_id, max(version), date(modification_time) 
                                        FROM vms.version_history 
                                        WHERE project_id = %d
                                        AND date(modification_time) BETWEEN %s and %s
                                        GROUP BY date(modification_time);""",\
    'get_active_version_on_date': """SELECT project_id, max(version), date(modification_time) 
                                        FROM vms.version_history 
                                        WHERE project_id = %d 
                                        AND date(modification_time) <= %s
                                        GROUP BY date(modification_time)
                                        ORDER BY date(modification_time) DESC
                                        LIMIT 1;"""
    }

def _get_active_versions(optsDict, dbObj, project_id):
    '''
    for the specified date range(s), get all the versions of the project that were active.
    '''
    version_list = []
    # get date range
    argv = optsDict['dateRange'].split()
    while len(argv) > 0:
        if argv[0] == 'A':
            dt = argv[1]
            argv = argv[2:]
            sql = sqls['get_active_version_on_date']%(project_id, dt)
            ret_val = dbObj.execute(sql)
            version_list.append(int(ret_val[0][1]))
        elif argv[0] == 'R':
            start_date = argv[1]
            stop_date = argv[2]
            argv = argv[3:]
            # get active version between start and stop dates
            sql = sqls['get_all_versions_in_date_range']%(project_id, start_date, stop_date)
            ret_val = dbObj.execute(sql)
            version_list.extend([int(ver[1]) for ver in ret_val])
            # since a version might already be active on the start date, get that active version by looking a little back from the start date
            sql = sqls['get_active_version_on_date']%(project_id, start_date)
            ret_val = dbObj.execute(sql)
            version_list.append(int(ret_val[0][1]))
    return version_list
    
    
def _get_active_cam_list_proj_versions(dbObj, project_id, version_list):
    live_cams = set([])
    if len(version_list) == 1:
        version_list.append(version_list[0])
    sql = sqls['get_proj_xml_for_versions']%(project_id, str(tuple(version_list)))
    ret_val = dbObj.execute(sql)
    for xml_str in ret_val:
        root = ET.XML(xml_str[0])
        views = root.findall("camera_views/camera_view")
        for cam_obj in views:
            # find all event channels defined for this camera
            evntChannels = cam_obj.findall('event_channels/event_channel')
            # if there are none, this camera is not generating any vision data and we can skip it
            if len(evntChannels) == 0:
                continue
            cam_name = cam_obj.find('name').text
            # ignore any cams that are not named 'camxxx'. this includes cams like fac001 and cam015_highres etc
            # since high res cameras are usually created by copying existing cameras, we can't filter them out by a criterion like "camera has no event channels"
            if cam_name.lower().find('cam') != 0 or cam_name.lower().find('res') != -1 or len(cam_name.split('_')) > 1:
                continue
            live_cams.update([cam_name])
    return list(live_cams)
    
def _get_live_cam_list(host,user,password,proj_name):
    # setup db connection
    dbObj = dbc(host=host, user=user, password=password, database='vms')
    
    # retrieve project id 
    project_id = int(dbObj.execute(sqls['get_proj_id']%proj_name)[0][0])
    print project_id
    # retrieve list of all active versions of project in the specified date range
    version_list = _get_active_versions(optsDict, dbObj, project_id)
    print version_list
    if not version_list:
        live_cam_list = []
        return live_cam_list
    # retrieve a union of cams of all active versions. The assumption is that every single of these cameras have event tables created in the
    # vms schema. Querying an existing table (basically a table becomes a proxy for a camera) for a date when this camera was not active will
    # result in an empty file. There can be many versions per day, by only taking final versions we are ensuring that we are using the live
    # version. Using the live version means that there is an event table and querying it will not cause the system to crash. The idea is to
    # avoid a camera that was only added but not made live (meaning avoiding looking for an event table that does not exist.
    # live_cam_list = ['cam001','cam002','cam003' etc]
    live_cam_list = _get_active_cam_list_proj_versions(dbObj, project_id, version_list)
    return live_cam_list
    
def splitDateRange(optsDict,dateRange=None):
    dateRange = optsDict['dateRange']
    dateList = _ParseDateRange(dateRange)
    XMLDict = getXMLdata.getXMLdata(optsDict['calibFile'],dateList)
    dateSplitByCalib = {}
    something = {}
    daySets = {}
    count = 0
    for date in dateList:
        dateInFormat = datetime.datetime.strptime(date,'%Y%m%d')
        for vers in XMLDict.keys():
            startSet = datetime.datetime.strptime(vers.split('_')[0],'%Y-%m-%d')
            endSet = datetime.datetime.strptime(vers.split('_')[1],'%Y-%m-%d')
            if dateInFormat >= startSet and dateInFormat <= endSet:
                dateSplitByCalib.setdefault(startSet,[]).append(date)
    for groups in dateSplitByCalib.keys():
        dateAsList = []
        daySets = {}    
        dateAsList = dateSplitByCalib[groups]
        count = 0
        for index, obj in enumerate(dateAsList):
            # print daySets
            dayOneFormat = datetime.datetime.strptime(obj,'%Y%m%d')
            if index == 0:
                daySets.setdefault(count,[]).append(dayOneFormat)
                continue
            dayPrevFormat = datetime.datetime.strptime(dateAsList[index-1],'%Y%m%d')
            if abs((dayOneFormat - dayPrevFormat).days) != 1:
                count += 1
            daySets.setdefault(count,[]).append(dayOneFormat)
        rangeString = ''
        for ds in daySets.keys():
            if ds == 0:
                if len (daySets[ds]) == 1:
                    rangeString = " ".join(['A',daySets[ds][0].strftime('%Y%m%d')])
                else:
                    rangeString = " ".join(['R',daySets[ds][0].strftime('%Y%m%d'),daySets[ds][-1].strftime('%Y%m%d')])
            else:   
                if len (daySets[ds]) == 1:
                    rangeString = " ".join([rangeString,'A',daySets[ds][0].strftime('%Y%m%d')])
                else:
                    rangeString = " ".join([rangeString,'R',daySets[ds][0].strftime('%Y%m%d'),daySets[ds][-1].strftime('%Y%m%d')])
                
        something[groups] = rangeString
    return something
                

def validateCams(ignoreCams,camRange,dateRange,calibFile,host,user,password,proj_name):
    err = {}
    
    # get list of cameras to ignore
    if ignoreCams == '*':
        err["Opts file param: ignoreCams cannot be '*'. Please change and re-run"] = ''
        return err, []
    ignore_cam_nums = _ParseCamRange(ignoreCams)
    ignore_cam_list = ['cam%03d'%cam for cam in ignore_cam_nums]    
    
    # get active cam list in date range
    live_cam_list = _get_live_cam_list(host,user,password,proj_name)
    if not live_cam_list :
        err["No live project found for date range"] = ''
        return err, []
    live_cam_nums = sorted([int(cam.split('cam')[-1]) for cam in live_cam_list])
    # from list of live cam nums, remove ignore cams
    live_cam_nums = list(set(live_cam_nums).difference(ignore_cam_nums))
    # make dict out of live cams nums (note that ignore cams have already been removed)
    live_proj_cam_dict = {'Live Cams': set(['cam%03d'%cam for cam in live_cam_nums]), 'key': 'Live Cams'}
    
    # get camera range from the opts file
    if camRange != '*':
        # expand range specified in the opts file
        opts_cam_nums = _ParseCamRange(camRange)
        # remove ignore cams from list of cams
        cam_nums = set(opts_cam_nums).difference(ignore_cam_nums)
        # check these cam_nums to make sure they are in the live cam range. If not, return an error. note that ignore cams have been removed from both opts cam nums
        # as well as live cam nums
        diff = cam_nums.intersection(live_cam_nums)
        if diff != cam_nums:
            err["Cam range in opts file NOT entirely present in live project"] = sorted(list(diff))
            return err, list(cam_nums)
        else:
            cam_nums = list(cam_nums)
            opts_cam_list = ['cam%03d'%cam for cam in cam_nums]
    else:
        # note that live_cam_nums has already been stripped of ignore cams
        cam_nums = live_cam_nums
        opts_cam_list = ['cam%03d'%cam for cam in cam_nums]
    opts_cam_dict = {'Opts File Cams': set(opts_cam_list), 'key': 'Opts File Cams'}


    dateList = _ParseDateRange(dateRange)
    XMLDict = getXMLdata.getXMLdata(calibFile,dateList)
    camlist = []
    for vers in XMLDict.keys():
        camnums = XMLDict[vers]['camera'].keys() 
        for cam in camnums:
            cam = 'cam' + str(cam).zfill(3)
            if cam not in camlist:
                camlist.append(cam)
    calib_cam_dict = {'Calib Cams': camlist, 'key': 'Calib Cams'}   
    


    # find symmetric_differences between all three of them
    for combo in combinations([live_proj_cam_dict, opts_cam_dict, calib_cam_dict], 2):
        # ignore calib - <option> as calibration file is permitted to have entries for cameras that are not live or not being processed in opts
        key0 = combo[0]['key']
        key1 = combo[1]['key']
        if key0 == 'Calib Cams':
            continue
        elif key1 == 'Calib Cams':
            err["'%s' - '%s'"%(key0, key1)] = sorted(list((combo[0][key0].difference(combo[1][key1])).difference(ignore_cam_list)))
        # if key1 is Opts File Cams then make sure that the intersection of Opts File Cams with live cams is equal to Opts File Cams. This will cover the case
        # where either a range is entered in the GUI -OR- a range is entered in the opts file (instead of '*')
        elif key0 == 'Opts File Cams': #or key2 == 'Opts File Cams':
            diff = combo[0][key0].difference(combo[1][key1])
            if diff != combo[0][key0]:
                err["'%s' - '%s'"%(key0, key1)] = sorted(diff)
        elif key1 == 'Opts File Cams':
            diff = combo[1][key1].difference(combo[0][key0])
            if diff != combo[1][key1]:
                err["'%s' - '%s'"%(key1, key0)] = sorted(diff)
        else:
            err["'%s' --diff-- '%s'"%(key0, key1)] = sorted(list((combo[0][key0].symmetric_difference(combo[1][key1])).difference(ignore_cam_list)))

    
    for key in err.keys():
        if len(err[key]) == 0:
            del err[key]
    return err, cam_nums
