#Convert from world to local(usually floorplan image) coordinates

from Calib import *
import os
import os.path
import glob



def WorldToLocal(calibrationFilename, inputFiles, outputFolder):

  if inputFiles.find('*') is not -1:
    inputFiles = glob.glob(inputFiles)
  if type(inputFiles) is str:
    inputFiles = [inputFiles]

  if len(inputFiles) == 0:
    raise BaseException("WorldToLocal input file not found")
  

  try:
    os.makedirs(outputFolder)
  except:
    pass

  p = calibrationParser()
  p.parse(calibrationFilename)
  floorplan = p.getFloorplan()

  m = homography(floorplan.getWorldCoords(), floorplan.getLocalCoords())

  for filename in inputFiles:
    outputFile = os.path.join(outputFolder, os.path.split(filename)[1])
    trajList = Trajectory.loadXmlCsv(filename)

    print '%s --> %s' % (filename, outputFile)

    for traj in trajList:
      traj.transform(m)

    Trajectory.saveXmlCsv(outputFile, trajList)

  
if __name__ == '__main__':
  if len(sys.argv) != 4:
    sys.exit('Error: Usage: WorldToLocal <calibration file> <input files> <output folder> ')
    
  calibrationFilename = sys.argv[1]
  inputFiles = sys.argv[2]
  outputFolder = sys.argv[3]

  WorldToLocal(calibrationFilename, inputFiles, outputFolder)
  
