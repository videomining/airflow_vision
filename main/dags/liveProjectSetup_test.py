#############################################################################################################
"""
This script is for setting up the initial project file (live project file)/update live project file etc.
Its features include :
    1. Insert the calibration images into the individual folders
    2. Insert Video files into the video sources of the individual camera (to be taken care)
    3. Extract tracking regions into text file/update the project file
    4. Cropping the live images based on the tracking regions and 
       perform histogram analysis and set the background segmentation threshold setting
    5. Updating the project file based on a secondary file


Note : Some of the functions have '_' as the first character. What this means is that the scope of this 
function is limited to this python file and cannot be imported into another program.

"""
#############################################################################################################

import xml.etree.ElementTree as ET
import sys, copy, glob, shutil, os
import pdb


class ProjectParserOptions:
    def __init__(self, fileName = None):
        if fileName != None :
            self.fileName = open(fileName,'r')
            self.projectXml = ET.parse(self.fileName)
    
    def getPathElements(self, path, elementInst = None):
        if elementInst is not None :
            return elementInst.findall(path)
        else :    
            return self.projectXml.findall(path)
        
    def writeProjFile(self, outputFileName, XMLElement):
        elementTreeInst = ET.ElementTree(XMLElement)
        elementTreeInst.write(open(outputFileName,'w'))   

    def getIDCamNameDict(self, camera_views_element_list):
        idCamNameDict = {}
        for camera_view in camera_views_element_list :
            idCamNameDict[camera_view.attrib['id']] = camera_view.find('name').text
            
        return idCamNameDict
        
def _writeData(dataDict, outputFileName):
    ### writes dictionary data into an output file
    outFile = open(outputFileName,'w')
    for key in sorted(dataDict.keys()) :
        line = key + ':' + str(dataDict[key]) + '\n'
        outFile.write(line)
    # for key,val in dataDict.iteritems() :
        # line = key + ':' + str(val) + '\n'
        # outFile.write(line)
    outFile.flush()
    outFile.close()

def insertImagesIntoFolders(projectFileName, imagesPath):
    ### This function reads in a project file and then inserts the images into their corresponding
    ### folders for perforing Calibration
    ### The imagesPath is where the images are saved i.e. the calibration images which are 
    ### going to be used (the only constraint for now is that the images are to be saved by the names corresponding 
    ### to the camera names in the project file i.e. if your naming scheme is cam001 in project file then the images 
    ### are to be with the same name as a jpg file)
    
    projectInst = ProjectParserOptions(projectFileName)
    camera_views_element_list = projectInst.getPathElements('camera_views/camera_view')
    camNameDict = projectInst.getIDCamNameDict(camera_views_element_list)
    projectFolder = projectFileName.split('.')[0] + ' Files'
    for key,cameraName in camNameDict.iteritems():
        copyImagePath = imagesPath + '\\' + cameraName + '*.jpg'
        copyImages = glob.glob(copyImagePath)
        
        if len(copyImages) != 0:
            projectFolderPath = projectFolder + '\\camera' + key
            shutil.copy(copyImages[0], projectFolderPath)
            try :
                destPath = projectFolderPath + '\\LiveThumbnail.jpg'
                os.remove(destPath)
            except :
                print "No File to DEL"
                pass
            
            try :
                srcPath = projectFolderPath + '\\' + copyImages[0].split('\\')[-1]
                destPath = projectFolderPath + '\\LiveThumbnail.jpg'
                os.rename(srcPath, destPath)
                print camNameDict[key]
            except :
                print cameraName
                print "Exception related to removing or renaming. Ignore plzzz !!! only for debugging purpose"
                
        else :
            print "%s calibration image not available or check the images path" %(camNameDict[key])
    return 0

def extractTrackingRegionFromProjFile(projectFileName, isKillRegionDrawn, killRegionExpandValue = None):
    ### This function reads in a project file and then extracts the tracking region. It then writes into another project file or/and 
    ### also writes into a text file depending on the user inputs. 
    ### isKillRegionDrawn is a boolean value i.e. 0 or 1
    ### The killRegionExpandValue is an integer (scale in pixels) that expands the 'Start Region' by the specified value.
    ### The use of writing into a text file is because later this file can be used to visualize the tracking regions.
    ### One constraint is that if the project file has a camera_view element associated with it, it should then also have a 'Start Region'
    ### manual event channel type associated with it and a polygon associated with it.
    
    #### based on the inputs specifying the contents of the killRegionExpandValue ####
    if killRegionExpandValue is not None :
        killRegionExpandValue = int(killRegionExpandValue)
    else :
        killRegionExpandValue = 5
    
    startTrackingRegionDict, killTrackingRegionDict = {}, {}
    
    projectInst = ProjectParserOptions(projectFileName)
    camera_views_element_list = projectInst.getPathElements('camera_views/camera_view')
    count = 0
    
    for camera_view in camera_views_element_list :
        camera_name = camera_view.find('name').text
        event_channel_type = projectInst.getPathElements('event_channels/event_channel/label', camera_view)
        print camera_name
        
        try :
            ##### extracting the start region polygon drawn from the project file ####
            polygon = _trackPolyExtractorElementTree(camera_view, event_channel_type, 'Start Region', camera_name)
            startRegionStr = _genPolyStr(polygon)
            ##### extracting the kill region polygon based on the expand val specified ####
            if isKillRegionDrawn :
                polygon = _trackPolyExtractorElementTree(camera_view, event_channel_type, 'Kill Region', camera_name)
                killRegionStr = _genPolyStr(polygon)
                killTrackingRegionDict[camera_name] = killRegionStr
            else :
                # pdb.set_trace()
                try :
                    killRegionStr = _extendStartTrackRegion(startRegionStr, killRegionExpandValue)
                except ZeroDivisionError :
                    print "%s : The polygon defined has three points in a straight line and the VmsUtils expand cannot handle this (bug)" %(camera_name)
                    killRegionStr = startRegionStr
                    # sys.exit(0)
                    count += 1
                    
                killTrackingRegionDict[camera_name] = killRegionStr

            startTrackingRegionDict[camera_name] = startRegionStr
            
        except :
            # Detailed explanation of the various errors is already being printed out so ignoring it
            pass
            
    if count > 0 :
        # This is a work around for the way sys.exit works especially if it is an inner loop of a try, except statement
        sys.exit(0)
                
    return startTrackingRegionDict, killTrackingRegionDict

def _extendStartTrackRegion(startRegionStr, killRegionExpandValue):
    ### Extends a given polygon by a certain number of pixels using the function defined in VmsUtils
    import VmsUtils
    polyStr = VmsUtils.polyFromString(startRegionStr)
    kill_track_region = polyStr.expand(int(killRegionExpandValue))
    to_extend = str(kill_track_region).split(',')
    killRegionStr = '(' + str(kill_track_region) + str(to_extend[0]) + ',' + str(to_extend[1]) + ')'

    return killRegionStr

def _trackPolyExtractorElementTree(camera_view, event_channel_type, event_channel_name, camera_name):
    ### This function is a support to the main function extractTrackingRegionFromProjFile. This returns the 'polygon' 
    ### drawn on the vmproj file i.e. either the 'Start Region' or the 'Kill Region'. 
    try :
        start_region_label_index = map(lambda x : x.text, event_channel_type).index(event_channel_name)
        Start_Region_Polygon = camera_view.findall('event_channels/event_channel/visualization')
        polygon = Start_Region_Polygon[start_region_label_index].find('object/points').text        
        return polygon
    except :
        print "camera_name : %s" %(camera_name)
        print "There is no '%s' associated with the camera_view. Please check for the name since it is case sensitive. OR " % (event_channel_name)
        print "There is no 'xml settings' for the event channel associated with the camera. If it is face camera you can ignore it otherwise there is an error."
        print "\n\n\n"

def _trackPolyExtractor(projectInst, camera_view, polygonType):
    xml_settings = projectInst.getPathElements('event_channels/event_channel/xml_vision_settings/folder/param', camera_view)
    xml_settings_name_list = map(lambda x : x.attrib['name'], xml_settings)
    index_no = xml_settings_name_list.index(polygonType)
    
    return index_no, xml_settings
    
def __paramExtractor(projectInst, camera_view, parameter):
    xml_settings = projectInst.getPathElements('event_channels/event_channel/xml_vision_settings/folder/folder/folder/param', camera_view)
    xml_settings_name_list = map(lambda x : x.attrib['name'], xml_settings)
    index_no = xml_settings_name_list.index(parameter)
    return index_no, xml_settings
    
def _genPolyStr(polygon):
    ### creates a polygon string from the polygon list. This function becomes absolete 
    to_extend = polygon.strip('\n').split(',')
    polyStr = '(' + polygon + ',' + str(to_extend[0]) + ',' + str(to_extend[1]) + ')'
    return polyStr
    
def writeProjInst(projectInst, outputFileName):
    ### writes the projectInst element tree into a file
    treeIterator = projectInst.projectXml.getiterator()
    projectInst.writeProjFile(outputFileName,treeIterator[0])
    
def genBaseImageDirToExtractROI():
    ### This method creates the images that can be used later for calibration or
    ### to extract ROI from tracking regions and perform the histogram analysis.
    ### There are two ways to do it, one of them is through the admin portal (web page)
    ### or by specifying the folder where the images are stored e.g. \\servvms001
    
    # To be finished later
    return 0
    
def extract_ROI_from_Image(trackingRegionsInput, baseImageDir, outputImageDir):
    #### This function reads in a trackingRegionsFile and then based on the region crops the corresponding image.
    #### Then calculates the histogram of the cropped image. 
    #### constraint : naming of the camera image has to be same as in the project file and image type is a jpg
    
    from PIL import Image
    import os, VmsUtils
    
    camPolyRelation = _genDictFromInput(trackingRegionsInput)
    
    try :
        os.makedirs(outputImageDir)
    except :
        pass
    
    camHistRel = {}
    for camera_name, poly in camPolyRelation.iteritems() :
        image_name = baseImageDir + '\\' + camera_name + '.jpg'
        im = Image.open(image_name)
        outputImageName = outputImageDir + '\\' + camera_name + '_out' + '.bmp'
        (width, height) = im.size
        gray_scale_image = im.convert("L")
        pix_data = gray_scale_image.load()
        for x in range(width):
            for y in range(height):
                polyStr = VmsUtils.polyFromString(poly)
                if polyStr.isPointInside([x,y]) :
                    pass
                else :
                    pix_data[x,y] = 0
        print "Finished : %s" % (camera_name)
        
        gray_scale_image.save(outputImageName)
        hist_values = gray_scale_image.histogram()
        camHistRel[camera_name] = hist_values
        
    return camHistRel

def _genDictFromInput(inputFormat):
    if type(inputFormat) == dict :
        return inputFormat
    else :
        f = open(inputFormat).readlines()
        dataList = map(lambda x : x.strip('\n').split(':'), f)
        dataDict = {}
        for camera_name, val in dataList:
            dataDict[camera_name] = val
            
        return dataDict    
        
def analyzingHistVal(camHistRelInput, outputStatFile):
    #### This function reads in the camHistRel i.e. camera name and the histogram of the cropped image.
    #### From the input it computes a set of basic statistics like mean, mode, std etc. and later uses the
    #### mean to determine what the Background Segmentation Threshold (trajectory event xml settings) to be.
    
    import operator, math
    
    camHistRel = _genDictFromInput(camHistRelInput)
    outFile = open(outputStatFile, 'w')
    outFile.write('camera_name, median, mode, weighted_average, std\n')
    camBackgrdValRel = {}
    # pdb.set_trace()
    x_axis = range(0,256)
    for camera_name, hist in camHistRel.iteritems() :
        hist_values = hist.strip('\n').split('[')[1].split(']')[0].split(',')
        hist_values = map(int, hist_values)
        hist_values[0] = 0
        iterator, percent = 0, 0
        while percent <= 50:
            percent = (float(sum(hist_values[0:iterator]))/float(sum(hist_values)))*100
            iterator += 1
        median = iterator
        weighted_average = sum(map(operator.mul, hist_values, x_axis))/sum(hist_values)
        mode = hist_values.index(max(hist_values))
        std = map(lambda x : (x-weighted_average)*(x-weighted_average), hist_values)
        std = math.sqrt(sum(std))/len(std)
        string = camera_name + ',' + str(median) + ',' + str(mode) + ',' + str(weighted_average) + ',' + str(std) + '\n'
        outFile.write(string)
        value = _analyzingMeanVal(weighted_average)
        camBackgrdValRel[camera_name] = value
    
    return camBackgrdValRel
        
def _analyzingMeanVal(weighted_average):
    #### The heuristic values based on the mean. The heuristic values were determined initially for one particular
    #### project and then later became a standard. In the future it might become absolete.
    if weighted_average <= 135 :
        val = 30
    elif weighted_average > 135 and weighted_average <= 150:
        val = 35
    elif weighted_average > 150 and weighted_average <= 180 :
        val = 40
    elif weighted_average > 180 :
        val = 45

    return val

def updateTrackingRegion(projectFileName, startTrackingRegionDictInput, killTrackingRegionDictInput, height, distortParameters):
    
    startTrackingRegionDict = _genDictFromInput(startTrackingRegionDictInput)
    killTrackingRegionDict = _genDictFromInput(killTrackingRegionDictInput)

    projectInst = ProjectParserOptions(projectFileName)
    camera_views_element_list = projectInst.getPathElements('camera_views/camera_view')
    for camera_view in camera_views_element_list :
        try :
            camera_name = camera_view.find('name').text
            distortIndexNo, xml_settings = __paramExtractor(projectInst, camera_view, 'distortParameters')
            heightIndexNo, xml_settings = __paramExtractor(projectInst, camera_view, 'height')
            xml_settings[distortIndexNo].attrib['value'] = distortParameters
            xml_settings[heightIndexNo].attrib['value'] = height
            
            startRegionIndexNo, xml_settings = _trackPolyExtractor(projectInst, camera_view, 'startTrackPolygon')
            killRegionIndexNo, xml_settings = _trackPolyExtractor(projectInst, camera_view, 'killTrackPolygon')
            personTrackRegionIndexNo, xml_settings = _trackPolyExtractor(projectInst, camera_view, 'personTrackPolygon')
            goodTrackRegionIndexNo, xml_settings = _trackPolyExtractor(projectInst, camera_view, 'goodTrackRegionPolygon')
            xml_settings[startRegionIndexNo].attrib['value'] = startTrackingRegionDict[camera_name]
            xml_settings[killRegionIndexNo].attrib['value'] = killTrackingRegionDict[camera_name]
            xml_settings[personTrackRegionIndexNo].attrib['value'] = '(5,5,5,235,315,235,315,5,5,5)'
            xml_settings[goodTrackRegionIndexNo].attrib['value'] = '(5,5,5,235,315,235,315,5,5,5)'
        # except KeyError:
        except :
            # The main project might contain additional set of cameras that might not be useful like face cameras etc.
            pdb.set_trace()
            print "In Update Function"
            print camera_name
            pass
    return projectInst
    
def updateBackgrdHistVal(projectFileName, camBackgrdValRelInput):
    camBackgrdValRel = _genDictFromInput(camBackgrdValRelInput)

    projectInst = ProjectParserOptions(projectFileName)
    camera_views_element_list = projectInst.getPathElements('camera_views/camera_view')
    for camera_view in camera_views_element_list :
        camera_name = camera_view.find('name').text
        
        try :
            hist_value = camBackgrdValRel[camera_name]
            xml_settings = projectInst.getPathElements('event_channels/event_channel/xml_vision_settings/folder/folder', camera_view)

            xml_settings_name_list = map(lambda x : x.attrib['name'], xml_settings)
            index_no = xml_settings_name_list.index('BackgroundLearner')
            histbckgrnd_Model = projectInst.getPathElements('folder', xml_settings[index_no])
            
            histbckgrnd_xml_settings = map(lambda x : x.attrib['name'], histbckgrnd_Model)
            index_no_hist_mdl = histbckgrnd_xml_settings.index('HistogramBackgroundModel')
            histmargin = projectInst.getPathElements('param', histbckgrnd_Model[index_no_hist_mdl])

            histmargin_list = map(lambda x : x.attrib['name'], histmargin)
            hist_index_no = histmargin_list.index('histogramMargin [0..255]')
            histmargin[hist_index_no].attrib['value'] = hist_value
        
        except :
            pass
            
    return projectInst

def extractFurnitureFile(calibrationProjectFileName, outputFileName):
    outputFile = open(outputFileName, 'w')
    projectInst = ProjectParserOptions(calibrationProjectFileName)
    camera_views_element_list = projectInst.getPathElements('camera_views/camera_view')
    
    for camera_view in camera_views_element_list :
        camera_name = camera_view.find('name').text
        type_label = camera_view.find('type_label').text
        if cmp(type_label, 'Floorplan') == 0 :
            # fixture_info = camera_view.findall('floorplan_info/fixtures/fixture/points')
            fixture_info = projectInst.getPathElements('floorplan_info/fixtures/fixture', camera_view)
            for fixture_element in fixture_info :
                flag = _isFloorFixture(fixture_element)
                if flag :
                    fixture_points = fixture_element.findall('points')        
                    fixtures = map(lambda x : x.text, fixture_points)
                    for fixture_element in fixtures :
                        list_coords = fixture_element.strip('\n').split(',')
                        list_coords = map(lambda x : x.split('.')[0], list_coords)
                        no_points = len(list_coords)/2
                        for index in range(no_points-1) :
                            line = list_coords[2*index] + ',' + list_coords[2*index+1] + ',' + list_coords[2*index+2] + ',' + list_coords[2*index+3] + '\n'
                            outputFile.write(line)
                        last_line = list_coords[-2] + ',' + list_coords[-1] + ',' + list_coords[0] + ',' + list_coords[1] + '\n'
                        outputFile.write(last_line)
    
    return 0
    
def _isFloorFixture(fixture_element):
    flag = 0
    if int(float(fixture_element.find('z0').text)) == 0:
        flag = 1
    return flag

def CONTROLLER(inputProjectFileName, updateProjectFileName, outputProjectFileName):
    
    ceiling_height = 10.3 # Ceiling height in terms of feet
    ceiling_height = str(ceiling_height*0.3048) # Ceiling height in terms of metres
    distort_parameters = '0.66,0,0.0,0,0.3,0.24,0.96,0'
    
    # extractFurnitureFile(r'F:\work\CSI-3\Common\VMS Projects\Calibration Projects\CSI3_0327_Calibration.vmproj',r'F:\work\CSI-3\327_Pittsburgh\POSSE\Documents\327_Furniture_FullStore.txt')
    # calibrationProjectFileName = r'F:\work\GSI\Calibration Projects\Calibration-2890-Chicago-Safeway.vmproj'
    # extractFurnitureFile(calibrationProjectFileName, r'F:\work\GSI\POSSE\JoinVerification_Chicago_Dominicks_2890\Documents\Furniture_Chicago_Dominicks_2890.txt')
    
    pdb.set_trace()
    startTrackingRegionDict, killTrackingRegionDict = extractTrackingRegionFromProjFile(inputProjectFileName, 0, 1)
    _writeData(startTrackingRegionDict, r'F:\work\CSI-4\Common\Vision Settings\startTrackingRegionsFile_82376_bp.txt')
    _writeData(killTrackingRegionDict, r'F:\work\CSI-4\Common\Vision Settings\killTrackingRegionsFile_82376_bp.txt')
    camHistRel = extract_ROI_from_Image(r'F:\work\CSI-4\Common\Vision Settings\startTrackingRegionsFile_82376_bp.txt', \
                                        r'F:\work\CSI-4\Common\Calibration Projects\Calibration Images 82376' , \
                                        r'F:\work\CSI-4\Common\Vision Settings\OutputImages_82376_bp')
    pdb.set_trace()
    _writeData(camHistRel, r'F:\work\CSI-4\Common\Vision Settings\camHistRel_82376_bp.txt')
    camBackgrdValRel = analyzingHistVal(r'F:\work\CSI-4\Common\Vision Settings\camHistRel_82376_bp.txt', r'F:\work\CSI-4\Common\Vision Settings\Stats_82376_bp.csv')
    _writeData(camBackgrdValRel,r'F:\work\CSI-4\Common\Vision Settings\camBackgrdValRel_82376_bp.txt')
    pdb.set_trace()
    projectInst = updateTrackingRegion(r'F:\work\CSI-4\Common\Vision Settings\CSI4_82376_Base.vmproj', \
                                       r'F:\work\CSI-4\Common\Vision Settings\startTrackingRegionsFile_82376_bp.txt', \
                                       r'F:\work\CSI-4\Common\Vision Settings\killTrackingRegionsFile_82376_bp.txt', ceiling_height, distort_parameters)
    writeProjInst(projectInst, r'F:\work\CSI-4\Common\Vision Settings\VisionSettings_Base_82376_Temp1.vmproj')
    projectInst = updateBackgrdHistVal(r'F:\work\CSI-4\Common\Vision Settings\VisionSettings_Base_82376_Temp1.vmproj', r'F:\work\CSI-4\Common\Vision Settings\camBackgrdValRel_82376_bp.txt')
    writeProjInst(projectInst, r'F:\work\CSI-4\Common\Vision Settings\VisionSettings_Base_82376_Temp2.vmproj')
    
    # projectInst = updateTrackingRegion(r'F:\work\CSI-4\Common\Vision Settings\schnucks_191_stlouis_Embedded.vmproj', \
                                         # r'F:\work\CSI-4\Common\Vision Settings\startTrackingRegionsFile_327.txt', \
                                         # r'F:\work\CSI-4\Common\Vision Settings\startTrackingRegionsFile_327.txt', ceiling_height, distort_parameters)
    # writeProjInst(projectInst, r'F:\work\GSI\Live Project Setup\schnucks_191_stlouis_Embedded_Temp1.vmproj')
    # projectInst = updateBackgrdHistVal(r'F:\work\GSI\Live Project Setup\schnucks_191_stlouis_Embedded_Temp1.vmproj', r'F:\work\GSI\Live Project Setup\camBackgrdValRel_191_StLouis_Schnucks_COS.txt')
    # writeProjInst(projectInst, r'F:\work\GSI\Live Project Setup\sch-191.vmproj')
    
    
if __name__ == "__main__":
    # if len(sys.argv) != 2:
        # sys.exit("Usage : %s inputProjectFileName" %(__file__))
    

    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5_Thornton_57_Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_57_Thortons_FurnitureFile.txt')

    # pdb.set_trace()

    extractFurnitureFile(r'\\vmvase\vmvase_raid\Calibration Projects\krg-501\Calibration-Kroger-501.vmproj',r'\\vmnode8\ProjectData1\kroger-501\Documents\Furniture.txt')
    # extractFurnitureFile(r'D:\Sandbox\Projects\Target American Greetings\Calibration\0880_Calibration.vmproj',r'D:\Sandbox\Projects\Target American Greetings\Data\Furniture Files\0880_Furniture.txt')
    # extractFurnitureFile(r'D:\Sandbox\Projects\Target American Greetings\Calibration\1481_Calibration.vmproj',r'D:\Sandbox\Projects\Target American Greetings\Data\Furniture Files\1481_Furniture.txt')
    # extractFurnitureFile(r'D:\Sandbox\Projects\Target American Greetings\Calibration\1510_Calibration.vmproj',r'D:\Sandbox\Projects\Target American Greetings\Data\Furniture Files\1510_Furniture.txt')
    # extractFurnitureFile(r'D:\Sandbox\Projects\Target American Greetings\Calibration\1819_Calibration.vmproj',r'D:\Sandbox\Projects\Target American Greetings\Data\Furniture Files\1819_Furniture.txt')
    
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5_Thornton_57_Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_57_Thortons_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_Thortons_160_Louisville.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_160_Thortons_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_163_Boston_CF.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_163_CF_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_174_Racetrack_Dallas.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_174_Racetrak_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5_0327_Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_327_Sheetz_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-Sheetz-338-Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_338_Sheetz_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5_Calibration_Holliday-379-Minneapolis.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_379_Holiday_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_RaceTrac_618_Atlanta.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_618_Racetrak_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_GetGo_3280_Pittsburgh.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_3280_Getgo_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-3378-CK-Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_3378_CircleK_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_GetGo_3514_Columbus.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_3514_Getgo_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-CF-6682-calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_6682_CF_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-CK-8652-Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_8652_CircleK_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_9433_Hess_Clearwater.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_9433_Hess_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_9588_Hess_Tampa.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_9588_Hess_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-CK-9980-Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_9980_CircleK_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_64096.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_64096_BP_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-64666_AMPM_Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_64666_BP_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration-82376-Sac-AMPM.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_82376_BP_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\CSI5-AMPM-83100-Calibration.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_83100_BP_FurnitureFile.txt')
    # extractFurnitureFile(r'F:\work\CSI-5\Common\Calibration Projects\Calibration_370416.vmproj',r'F:\work\CSI-5\Common\FurnitureFiles\CSI4_370416_BP_FurnitureFile.txt')
    
    # calibrationbaseProjFileName = r'F:\work\GSI\Calibration Projects\Calibration-097-Hershey.vmproj'
    # calibrationbaseProjFileName = r'F:\work\GSI\Calibration Projects\Calibration_Schnucks-191.vmproj'
    
    # calibrationbaseProjFileName = r'F:\work\GSI\Calibration Projects\Calibration-2890-Chicago-Safeway.vmproj'
    # insertImagesIntoFolders(calibrationbaseProjFileName, r'F:\work\GSI\Calibration Projects\Calibration Images Chicago')
    
    # calibrationbaseProjFileName = r'F:\work\GSI\Calibration Projects\Calibration-097-Giant-Hershey-FullStore.vmproj'
    # insertImagesIntoFolders(calibrationbaseProjFileName, r'F:\work\GSI\Calibration Projects\Calibration Images Hershey Giant Full Store')
    
    
    # pdb.set_trace()
    # inputProjectFileName = r'F:\work\CSI-4\Common\Vision Settings\CSI4_82376_Base.vmproj'
    # outputProjectFileName = r'F:\work\GSI\Live Project Setup\VisionSettings_Base_6501_Temp1.vmproj'
    # updateProjectFileName = r'F:\work\GSI\Live Project Setup\VisionSettings_Base_6501_Temp2.vmproj'
    # CONTROLLER(inputProjectFileName, updateProjectFileName, outputProjectFileName)
    
    