from Trajectory import *
import random
import os
import os.path
import glob
import VmsUtils, TimeUtils
import csv
import pdb
import numpy

class DwellTraj(Trajectory):
# class to represent the dwell points of a track
# contents are start time of the original track,total duration of the track
# and node list where every node represents a dwell point storing its start 
# time, midpoint position and duration
# CSV format written to file is as follows:
# "'2007-02-07T23:00:00',15.00,"'2007-02-07T23:00:01',35.0,19.0,4.5;'2007-02-07T23:00:07',20.0,1.0,3.5"
# The above contains two dwell stops. The first one started at '2007-02-07T23:00:01'
# was 4.5 seconds long with the avg. position being (35,19). The second one started
# at '2007-02-07T23:00:07'
  def __init__(self,id=-1):
    Trajectory.__init__(self,id)
    
    self.totalTime = -1 # duration of original track
    #self.dwellNodeList = [] #(starttime,x,y,duration) for each node
    self._startTime = -1
  def startTime(self):
    return self._startTime

  def addDwell(self,dwellNode):
    #print "DWELLNODE=",dwellNode
    self.nodes.append(dwellNode)
    
  def getCsvString(self):
    csvString = ""
    csvString += "%s," % secsToDate(self.startTime()).strftime('%Y-%m-%dT%H:%M:%S')
    csvString += "%f," % self.totalTime
    nodesString = '"'
    for node in self.nodes:
      dwellDt = secsToDate(node[0])
      dwellStartTime = dwellDt.strftime('%Y-%m-%dT%H:%M:%S')+ ("%f" % (dwellDt.microsecond/1000000.0))[1:]
      nodesString += "%s,%f,%f,%f,%s;" % (dwellStartTime,node[1],node[2],node[3],node[4])
    
    nodesString.strip(';')
    nodesString += '"'
    csvString += nodesString
    return csvString
  
  def duration(self):
    return self.totalTime
  
  def endTime(self):
    return self.starttime + self.duration()
  
  def readRow(self,row):
  #input is the list returned by csv reader represent one dwellTraj
  # row obtained by
    self.starttime = TimeUtils.DateStringToSecs(row[0])
    self.totalTime = float(row[1])
    nodeStrList = row[2].split(';')
    for nodeStr in nodeStrList:
      nodeElements = nodeStr.split(',')
      dwellStartTime = DateStringToSecs(nodeElements[0])
      posX = float(nodeElements[1])
      posY = float(nodeElements[2])
      duration = float(nodeElements[3])
      side = nodeElements[4]
      self.addDwell((dwellStartTime,posX,posY,duration))
  
  #def 
  #def def
  
  def hasDwell(self):
    return self.nodes != []
  
  def insidePoly(self,dwellStartTime,posX,posY,duration):
    for dwellStartTime,posX,posY,duration in self.nodes:
      if VmsUtils.Polygon.isPointInside((posX,posY)):
        return True
    
    return False

def readDwellTraj(filename):
  dwellTrajList = []
  # csvObj = csv.reader(open(filename))
  csvObj = csv.reader(VmsUtils.OpenRobustly(filename))
  for row in csvObj:
    dwellTraj = DwellTraj()
    dwellTrajList.append(dwellTraj.readRow(row))
  
  return dwellTrajList

def saveDwellTraj(dwellTrajList,filename):
  # fileObj = open(filename,'w')
  fileObj = VmsUtils.OpenRobustly(filename,'w')
  for dwellTraj in dwellTrajList:
    csvString = dwellTraj.getCsvString()
    fileObj.write("%s\n" % csvString)
  
  fileObj.close()
  
  
def GetDwellPoint(subNodeList):
    xTotal = sum(node[1] for node in subNodeList)
    yTotal = sum(node[2] for node in subNodeList)
    dwellPoint = (xTotal / len(subNodeList), yTotal / len(subNodeList))
    return dwellPoint
  
  
def FindShoppedPolygon(dwellPoint, polygonList):
    for polygon in polygonList:
        if polygon.isPointInside(dwellPoint):
            return polygon
    return None
    
    
# def GetSubNodeList(nodeList, maxDisp):
    # subNodeList = [nodeList.pop(0)]
        
    # while nodeList and dist((subNodeList[0][1], subNodeList[0][2]), (nodeList[0][1], nodeList[0][2])) < maxDisp:
        # subNodeList.append(nodeList.pop(0))
            
    # return subNodeList
  
# def GetNextDwellNode(nodeList, minTime, maxDisp, polygonList):
    # subNodeList = GetSubNodeList(nodeList, maxDisp)
        
    # # check duration
    # dwellDuration = subNodeList[-1][0] - subNodeList[0][0]
    # if dwellDuration < minTime:
        # return None
        
    # # find geographical center of subNode collection
    # dwellPoint = GetDwellPoint(subNodeList)

    # # find which polygon was shopped
    # shoppedPolygon = FindShoppedPolygon(dwellPoint, polygonList)
    
    # dwellNode = (subNodeList[0][0], dwellPoint[0], dwellPoint[1], dwellDuration)
    # return dwellNode
    

# def getDwellTraj(traj, minTime, maxDisp, polygonList):
    # dwellTraj = DwellTraj()
    # dwellTraj._startTime = traj.startTime()
    # dwellTraj.totalTime = traj.duration()

    # if traj.duration() < minTime:
        # return dwellTraj

    # totalNodes = len(traj.nodes)

    # nodeList = list(traj.nodes)
    
    # while nodeList:
        # dwellNode = GetNextDwellNode(nodeList, minTime, maxDisp, polygonList)
        # if dwellNode:
            # dwellTraj.addDwell(dwellNode)

    # return dwellTraj

def getDwellTrajNew(traj, minDuration, maxDisp):
    dwellTraj = DwellTraj()
    dwellTraj._startTime = traj.startTime()
    dwellTraj.totalTime = traj.duration()

    if traj.duration() < minDuration:
        return dwellTraj

    totalNodes = len(traj.nodes)

    index = 0
    while index < totalNodes:
        firstNode = traj.nodes[index]
        medianXList = [firstNode[1]]
        medianYList = [firstNode[2]]
        # totalX = firstNode[1]
        # totalY = firstNode[2]
        subNodeCount = 1
        index += 1

        while index < totalNodes and dist((firstNode[1], firstNode[2]),(traj.nodes[index][1],traj.nodes[index][2])) < maxDisp:
            # totalX += traj.nodes[index][1]
            # totalY += traj.nodes[index][2]
            medianXList.append(traj.nodes[index][1])
            medianYList.append(traj.nodes[index][2])
            subNodeCount += 1
            index += 1

        dwellDuration = traj.nodes[min(index-1,totalNodes-1)][0] - firstNode[0]

        if dwellDuration >= minDuration:
            midPoint = (numpy.median(medianXList),numpy.median(medianYList))
            #midPoint = ((totalX)/subNodeCount, (totalY)/subNodeCount)
            dwellTraj.addDwell((firstNode[0], midPoint[0], midPoint[1], dwellDuration))

    return dwellTraj

def getDwellTraj(traj, minDuration, maxDisp, polygonList):
    dwellTraj = DwellTraj()
    dwellTraj._startTime = traj.startTime()
    dwellTraj.totalTime = traj.duration()

    if traj.duration() < minDuration:
        return dwellTraj

    totalNodes = len(traj.nodes)

    index = 0
    while index < totalNodes:
        firstNode = traj.nodes[index]
        totalX = firstNode[1]
        totalY = firstNode[2]
        subNodeCount = 1
        index += 1
        
        while index < totalNodes and dist((firstNode[1], firstNode[2]),(traj.nodes[index][1],traj.nodes[index][2])) < maxDisp:
            totalX += traj.nodes[index][1]
            totalY += traj.nodes[index][2]
            subNodeCount += 1
            index += 1

        dwellDuration = traj.nodes[min(index-1,totalNodes-1)][0] - firstNode[0]
        
        if dwellDuration >= minDuration:
            midPoint = ((totalX)/subNodeCount, (totalY)/subNodeCount)
            dwellTraj.addDwell((firstNode[0], midPoint[0], midPoint[1], dwellDuration))

    return dwellTraj
  
  
def getTrajListWithDwell(trajList,minTime,maxDisp, polygonList):
  '''
  from the input list of trajectories, this function returns ONLY those trajectories that have dwell nodes in the supplied list of polygons
  '''
  dwellTrajList = []
  for traj in trajList:
    dwellTraj = getDwellTraj(traj,minTime,maxDisp, polygonList)
    if dwellTraj.hasDwell():
      dwellTrajList.append(traj)
  return dwellTrajList
  
def getDwellTrajList(trajList,minTime,maxDisp, polygonList=[]):
  '''
  from the input list of trajectories, this function returns the actual dwell trajectories (that have dwell nodes in the supplied list of polygons). This is
  different from getTrajListWithDwell in the sense that this function returns the dwell traj while getTrajListWithDwell returns the input traj that
  has dwell nodes in poly list
  '''
  dwellTrajList = []
  for traj in trajList:
    dwellTraj = getDwellTraj(traj,minTime,maxDisp, polygonList)
    if dwellTraj.hasDwell():
      dwellTrajList.append(dwellTraj)
    # else:      
      # print "No dwell for traj %d" % int(traj.id)
  return dwellTrajList

def loadPolygonList(filename):
  polygonList = []
  # for line in open(filename):
  for line in VmsUtils.OpenRobustly(filename):
    polygon = VmsUtils.polyFromString(line.strip('\n'))
    polygonList.append(polygon)
  return polygonList


    
    
