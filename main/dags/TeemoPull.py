import sys
import MySQLdb
import pdb
import os
from datetime import datetime


class TeemoPull(object):
    def __init__(self,storeName,startdate):
        if '-' in storeName:
            self.storeId = storeName.split('-')[1]
            self.platform = storeName.split('-')[0]
        else:
            self.storeId = storeName
            self.platform = ''
        self.startdate = str(startdate)
        
        
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def getDateList(self,enddate):
        connection= MySQLdb.connect(host = "vmvisiondb.videomining.com", user = "vmdatasvc", passwd = "irrelev@nt", db = "setsgroup")

        cur= connection.cursor()

        # element_name0="start_date"
        # element_name1="end_date"
        # element_name2="camera_set"    #a BLOB
        # element_name3="camera_group"
        # element_name4="camera_join"
        # element_name5="platform"
        # element_name6= 'storeId'
        table_name="setsgroup.project_info"

        if self.platform != '':
            query='''
            SELECT
                *
            FROM
                table_name

            WHERE
                platform = '<platform>' and store_id = '<storeId>' and  (start_date<='<S_Date>' and end_date>='<S_Date>' or start_date >= '<S_Date>' and start_date <= '<E_Date>' )
            '''

            # sql=query.replace('element_name0',str(element_name0)).replace('element_name1',str(element_name1)).replace('element_name2',str(element_name2)).replace('element_name3',str(element_name3)).replace('element_name4',str(element_name4)).replace('element_name5',str(element_name5)).replace('table_name',str(table_name))

            sql = query.replace('<platform>',str(self.platform)).replace('<storeId>', str(self.storeId)).replace('table_name',str(table_name) ).replace('<S_Date>',str(self.startdate)).replace('<E_Date>',str(enddate))
        else:
            query = '''
                SELECT
                    *
                FROM
                    table_name

                WHERE
                    store_id = '<storeId>' and  (start_date<='<S_Date>' and end_date>='<S_Date>' or start_date >= '<S_Date>' and start_date <= '<E_Date>' )
                '''

            # sql=query.replace('element_name0',str(element_name0)).replace('element_name1',str(element_name1)).replace('element_name2',str(element_name2)).replace('element_name3',str(element_name3)).replace('element_name4',str(element_name4)).replace('element_name5',str(element_name5)).replace('table_name',str(table_name))

            sql = query.replace('<storeId>', str(self.storeId)).replace(
                'table_name', str(table_name)).replace('<S_Date>', str(self.startdate)).replace('<E_Date>',
                                                                                                str(enddate))

        cur.execute(sql)
        dateList=cur.fetchall()             
        
        finalDateList = []
        for dtVal in dateList:
            finalDateList.append((datetime.combine(dtVal[2],datetime.min.time()),datetime.combine(dtVal[3],datetime.min.time()),dtVal[4],dtVal[5],dtVal[6],dtVal[0],dtVal[1]))
        return finalDateList


    def Pull(self,FileDestination,enddate):
        dtList=self.getDateList(FileDestination,enddate)
        
        
        try:

            self.startdate = datetime.strptime(self.startdate, '%Y%m%d')  #input start date-->datetime
            if enddate!= None:
                strenddate = str(enddate)                    # if c is NOT empty
                enddate = datetime.strptime(strenddate, '%Y%m%d')  #input end date-->datetime
                print "The date range you enter is -->%r-%r" %(datetime.strftime(self.startdate,'%Y%m%d'),datetime.strftime(enddate,'%Y%m%d')) #Return datetime back into string 
            else:
                dt_end = ''                             # if dt_end is empty, therefore only one input
                print "The date you enter is--->    %r" %(datetime.strftime(self.startdate,'%Y%m%d')) #Return datetime back into string 
            
            
        except ValueError:                            # if the input is not in the format of %Y%m%d then exit out
            print "Incorrect  input format"
            sys.exit("Incorrect  input format")
        
        
        if len==0:
            print 'The store/start date entered does not exist in the database'
            sys.exit()
            
         
        dt_S=self.startdate    #input Start Date
        dt_E=enddate     #input End Date
        File_Folder=FileDestination
        
        if dt_E != '':
                if dt_S>dt_E:
                    print "Error, the start date should not be later than the end date"
                    sys.exit()
        
         
        foundDateRangeFlag = False 
        for i in range(len(dtList)):      #list range from DB
            DbStart= dtList[i][0]
            DbEnd= dtList[i][1]
            Set_Blob=dtList[i][2]
            Group_Blob=dtList[i][3]
            Join_Blob=dtList[i][4]
            platform=dtList[i][5]
            store_id=dtList[i][6]
            
    #~~"SET"------SAVE THE FILE IN THE LOCAL DRIVE~~

            DbStart= datetime.strftime(DbStart,'%Y%m%d')
            DbEnd=datetime.strftime(DbEnd,'%Y%m%d')
            Store_Name = platform + '-' + store_id

            save_path=File_Folder                                                      # <-------designated folder location
            txt_name=os.path.join(save_path, Store_Name+"_"+"Camera_Set"+"_"+DbStart+"_"+DbEnd+".txt")        # <-------create a txt file name
            with open(txt_name,"w") as out_file:                                                         # w use to write file (create a file)
                outBlob=""                                                                               # <-------create a empty file first
                out_file.write(outBlob)

            with open(txt_name,"r+") as out_file:                                                        #r+ read and write files
                outSetBlob=""                                                                            #r+ can't use to create a file
                outSetBlob+=Set_Blob
                outSetBlob+="\n"
                out_file.write(outSetBlob)




    #~~"GROUP"------SAVE THE FILE IN THE LOCAL DRIVE~~
                                                
            txt_name=os.path.join(save_path, Store_Name+"_"+"Camera_Group"+"_"+DbStart+"_"+DbEnd+".txt")                # <-------create a txt file name
            with open(txt_name,"w") as out_file:                                                         # w use to write file (create a file)
                outBlob=""                                                                               # <-------create a empty file first
                out_file.write(outBlob)

            with open(txt_name,"r+") as out_file:                                              #r+ read and write files
                outSetBlob=""                                                                  #r+ can't use to create a file
                outSetBlob+=Group_Blob
                outSetBlob+="\n"
                out_file.write(outSetBlob)
                
    #~~"Join"------SAVE THE FILE IN THE LOCAL DRIVE~~
                                                
            txt_name=os.path.join(save_path, Store_Name+"_"+"Camera_Join"+"_"+DbStart+"_"+DbEnd+".txt")      # <-------create a txt file name
            with open(txt_name,"w") as out_file:                                               # w use to write file (create a file)
                outBlob=""                                                                     # <-------create a empty file first
                out_file.write(outBlob)

            with open(txt_name,"r+") as out_file:                                              #r+ read and write files
                outSetBlob=""                                                                  #r+ can't use to create a file
                outSetBlob+=Join_Blob
                outSetBlob+="\n"
                out_file.write(outSetBlob)
                                
          
            
            print Store_Name+" "+DbStart+" "+DbEnd+" "+"has been selected and stored in the folder" 
       