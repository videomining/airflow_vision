import collections
import sys
import sys
import os.path
from datetime import datetime
import DBConnector
import MySQLdb


# platformKeywords = {"safeway":"swy",\
#             "foodlion":"fll",\
#             "giant":"gnt",
#             "ahold":"ahld",
#             "giant eagle":"gte",
#             "kroger":"krg",
#             "schnucks":"sch",
#             "meijer":"mir",
#             "walgreens":"wlg",
#             "mac":"mac",
#             "savemart":"svmt",
#             "petsmart":"psm"}
#
# damnedCharacters = set(['&', '%', ',', '.'])
#
#
# dbSettings = collections.namedtuple('dbSettings',['host','user','password','nodeTable','getShopTable','scalingFactorTable'])


class getShop(object):
    def __init__(self):
        self.db = MySQLdb.connect(host = 'vmvisiondb',user = 'vmdatasvc',passwd = 'irrelev@nt',db = 'region_info')
        self.cursor = self.db.cursor()
        self.getShopTable = 'getshop_region_info'

    def selectsql(self,storeId,nodeType,projecttype):
        if len(storeId.split('-')) ==1:
            selectaisle = '''
                select start_date, end_date, node_id, name, side, points,parent from <table>
                where store_id = '<storeId>'
                and project_type = '<project_type>'
                and node_type = '<nodeType>'
                '''

            sql = selectaisle.replace('<table>', self.getShopTable).replace('<storeId>',storeId).replace('<project_type>', projecttype).replace('<nodeType>', nodeType)
        else:
            selectaisle = '''
            select start_date, end_date, node_id, name, side, points,parent from <table>
            where store_id = '<storeId>'
            and platform = '<retailer>'
            and project_type = '<project_type>'
            and node_type = '<nodeType>'
            '''

            sql = selectaisle.replace('<table>',self.getShopTable).replace('<retailer>',storeId.split('-')[0]).replace('<storeId>',storeId.split('-')[-1]).replace('<project_type>',projecttype).replace('<nodeType>',nodeType)
        self.cursor.execute(sql)
        fetchall =  self.cursor.fetchall()
        return fetchall

    def selectdoe(self,storeId,node_type,projecttype):
        if len(storeId.split('-')) == 1:
            selectcat = '''
            select start_date, end_date, node_id, name, node_type, parent, side, points
            from <table>
            where store_id = '<storeId>'
            and project_type = '<project_type>'
            and node_type = '<node_type>'
            '''
            sql = selectcat.replace('<table>',self.getShopTable).replace('<storeId>',storeId).replace('<project_type>',projecttype).replace('<node_type>',node_type)
        else:
            selectcat = '''
                select start_date, end_date, node_id, name, node_type, parent, side, points
                from <table>
                where store_id = '<storeId>'
                and platform = '<retailer>'
                and project_type = '<project_type>'
                and node_type = '<node_type>'
                '''
            sql = selectcat.replace('<table>', self.getShopTable).replace('<retailer>', storeId.split('-')[0]).replace(
                '<storeId>', storeId.split('-')[-1]).replace('<project_type>', projecttype).replace('<node_type>',
                                                                                                    node_type)
        print sql
        self.cursor.execute(sql)
        fetchall = self.cursor.fetchall()
        return fetchall


    def fetchGetShopInfo(self,storeId,projecttype):
        fetchall = self.selectsql(storeId,'region',projecttype)
        getshopDictaisle = {}
        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in getshopDictaisle:
                getshopDictaisle[date] = {}
            aisle = element[3]
            region = element[5]
            node_id = element[2]

            getshopDictaisle[date][aisle] = {'region':region,'node_id':node_id,'category':{}}

        fetchall = self.selectsql(storeId,'category',projecttype)

        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            catName = element[3]
            catPoints = element[5]
            node_id = element[2]
            parent = element[6]
            side = element[4]
            if date not in getshopDictaisle:
                raise Exception('date range has some errors')
            if parent not in getshopDictaisle[date]:
                raise  Exception('parent region not found %r' %parent)
            if node_id not in getshopDictaisle[date][parent]['category']:
                getshopDictaisle[date][parent]['category'][node_id] = [[catPoints,side,catName]]
            else:
                getshopDictaisle[date][parent]['category'][node_id].append([catPoints,side,catName])

        fetchall = self.selectsql(storeId,'endcap',projecttype)

        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            node_id = element[2]
            endName = element[3]
            endPoints = element[5]
            parent = element[6]
            if date not in getshopDictaisle:
                raise Exception('date range has some errors')
            if parent not in getshopDictaisle[date]:
                raise  Exception('parent region not found %r' %parent)
            if node_id not in getshopDictaisle[date][parent]['category']:
                getshopDictaisle[date][parent]['category'][node_id] = [[endPoints,'',endName]]
            else:
                getshopDictaisle[date][parent]['category'][node_id].append([endPoints,'',endName])

        return getshopDictaisle

    def fetchDOEInfo(self,storeId,projecttype):
        fetchall = self.selectdoe(storeId,'doe',projecttype)
        doeDict = {}
        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in doeDict:
                doeDict[date] = {}
            aisle = element[5]
            doeDict[date][aisle] = {}

        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in doeDict:
                doeDict[date] = {}
            aisle = element[5]
            doePoints = element[7]
            name = element[3]
            parent = element[5]
            if date not in doeDict:
                raise Exception('date range has some errors')
            if parent not in doeDict[date]:
                raise  Exception('parent region not found %r' %parent)
            doeDict[date][aisle][name] = doePoints
        return doeDict

    def fetchEntranceInfo(self,storeId,projecttype):
        fetchall = self.selectdoe(storeId,'entrance',projecttype)
        entranceDict = {}
        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in entranceDict:
                entranceDict[date] = {}
            entrance = element[5]
            entranceDict[date][entrance] = {'name':{}}

        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in entranceDict:
                entranceDict[date] = {}
            name = element[3]
            entrancePoints = element[7]
            if date not in entranceDict:
                raise Exception('date range has some errors')
            entranceDict[date][entrance]['name'][name] = {'region':entrancePoints}

        return entranceDict


class getShopWithoutProject(object):
    def __init__(self):
        self.db = MySQLdb.connect(host = 'vmvisiondb',user = 'vmdatasvc',passwd = 'irrelev@nt',db = 'region_info')
        self.cursor = self.db.cursor()
        self.getShopTable = 'getshop_region_info'

    def selectsql(self, storeId, nodeType, projecttype):
        if len(storeId.split('-')) == 1:
            selectaisle = '''
                select start_date, end_date, node_id, name, side, points,parent from <table>
                where store_id like '%<storeId>%'
                and project_type = '<project_type>'
                and node_type = '<nodeType>'
                '''

            sql = selectaisle.replace('<table>', self.getShopTable).replace('<storeId>', storeId).replace(
                '<project_type>', projecttype).replace('<nodeType>', nodeType)
        else:
            selectaisle = '''
            select start_date, end_date, node_id, name, side, points,parent from <table>
            where store_id like '%<storeId>%'
            and platform = '<retailer>'
            and project_type = '<project_type>'
            and node_type = '<nodeType>'
            '''

            sql = selectaisle.replace('<table>', self.getShopTable).replace('<retailer>',
                                                                            storeId.split('-')[0]).replace('<storeId>',
                                                                                                           storeId.split(
                                                                                                               '-')[
                                                                                                               -1]).replace(
                '<project_type>', projecttype).replace('<nodeType>', nodeType)
        self.cursor.execute(sql)
        fetchall = self.cursor.fetchall()
        return fetchall



    def fetchGetShopInfo(self,storeId,projecttype):
        fetchall = self.selectsql(storeId,'category',projecttype)
        getshopDictCatDict = {}
        for element in fetchall:
            startdt = datetime.strftime( element[0], '%Y%m%d')
            enddt = datetime.strftime( element[1], '%Y%m%d')
            date = startdt + '_' + enddt
            if date not in getshopDictCatDict:
                getshopDictCatDict[date] = {}
            catName = element[3]
            catPoints = element[5]
            node_id = element[2]

            getshopDictCatDict[date][catName] = {'region':catPoints,'node_id':node_id,'category':{}}
        return getshopDictCatDict


if __name__ == "__main__":

    # db = MySQLdb.connect(host = 'vmvisiondb',user = 'vmdatasvc',passwd = 'irrelev@nt',db = 'region_info')
    # db_obj = db.cursor()
    # storeId = '0413'
    # platform = 'safeway'
    # projecttype = 'gsi'
    # getShopTable = 'getshop_region_info'
    # # node_type = 'region'
    # # dbdbInfo = dbSettings(optsDict['host'],optsDict['user'],optsDict['password'],nodeTable, getShopTable, sfTable)

    # getshopfiles(platform,storeId, projecttype,getShopTable,db_obj)
    # doefiles(platform,storeId, projecttype,getShopTable,db_obj)
    getShopObj = getShop()
    storeId = '6097'
    projecttype = 'gsi_sf_channel_phase1'
    getShopObj.fetchGetShopInfo(storeId,projecttype)
    # getshopfiles
    # getentrancefiles(storeId, projecttype,getShopTable,db)