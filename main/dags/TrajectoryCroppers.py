from Trajectory import *
import VmsUtils
import copy
from getDwellPointsInRegion_New import *
import pdb
import bisect

def cropBeforeTime(trajList, cropTime):
    beforeTimeTrajList = []
    afterTimeTrajList = []

    for traj in trajList:
        beforeTimeTraj = copy.copy(traj)
        beforeTimeTraj.nodes = []
        afterTimeTraj = copy.copy(traj)
        afterTimeTraj.nodes = []
        
        for time,x,y in traj.nodes:
            if time <= cropTime:
                beforeTimeTraj.nodes.append((time,x,y))
            else:
                afterTimeTraj.nodes.append((time,x,y))

        beforeTimeTrajList.append(beforeTimeTraj)
        afterTimeTrajList.append(afterTimeTraj)

    return beforeTimeTrajList, afterTimeTrajList

    
def bisect_right(a, x, lo=0, hi=None):
    """Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e <= x, and all e in
    a[i:] have e > x.  So if x already appears in the list, a.insert(x) will
    insert just after the rightmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    """

    if lo < 0:
        raise ValueError('lo must be non-negative')
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo+hi)//2
        if x < a[mid][0]: hi = mid
        else: lo = mid+1
    return lo
    
    
def bisect_left(a, x, lo=0, hi=None):
    """Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e < x, and all e in
    a[i:] have e >= x.  So if x already appears in the list, a.insert(x) will
    insert just before the leftmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    """

    if lo < 0:
        raise ValueError('lo must be non-negative')
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo+hi)//2
        if a[mid][0] < x: lo = mid+1
        else: hi = mid
    return lo
    
    
def cropWithinTimeRange(traj, startTime, endTime):
    croppedTraj = Trajectory()
        
    startIndex = bisect_left(traj.nodes, startTime)
    endIndex = bisect_right(traj.nodes, endTime)
    
    croppedTraj.nodes = traj.nodes[startIndex:endIndex]
    
    return croppedTraj
    
    
# def cropWithinTimeRange(traj, startTime, endTime):
    # croppedTraj = Trajectory()
    
    # croppedTraj.nodes = [node for node in traj.nodes if node[0] >= startTime and node[0] <= endTime]
    
    # return croppedTraj
    
    
# def cropWithinTimeRange(traj, startTime, endTime):
    # croppedTraj = Trajectory()
    
    # for node in traj.nodes:
        # if node[0] >= startTime and node[0] <= endTime:
            # croppedTraj.nodes.append(node)
            
    # return croppedTraj
    
# def cropWithinTimeRange(traj, startTime, endTime):

    # croppedTraj = Trajectory()
    
    # index = 0
    
    # while traj.nodes[index][0] < startTime:
        # index += 1
        
    # while index < len(traj.nodes) and traj.nodes[index][0] <= endTime:
        # croppedTraj.nodes.append(traj.nodes[index])
        # index += 1
        
    # if len(croppedTraj.nodes) == 0:
        # print traj
        # print startTime, endTime
        # sys.exit()

    # return croppedTraj
    
def findMultipleEnterExitPolyIndices(traj, polygon):
    '''
    returns the list of indices of the nodes that enter the polygon (not all nodes but just the first node, unless a trajectory
    enters the polygon multiple times, the index list returns only one) and also the list of indices 
    of the node that exit the polygon (only if exits the polygon multiple times.
    If traj doesn't enter the polygon at all, it returns an index value that is equal to the 
    length of the node list in the traj.
    '''
    
    enterPolyAllIndicesList = []
    
    #### enterPolyAllIndicesList returns all the indices of the traj.nodes where traj.node is inside the polygon
    for index, node in enumerate(traj.nodes) :
        try :
            x = node[1]
            y = node[2]
            if polygon.isPointInside((x,y)) :
                enterPolyAllIndicesList.append(index)
        except :
            print "Failint in Trajectory Croppers"
            pdb.set_trace()
            
    
    if len(enterPolyAllIndicesList) == 0:
        return [len(traj.nodes)],[len(traj.nodes)],[len(traj.nodes)]
            
            
            
    #### Returns the first entry and first exit traj.nodes indices. If the trajectory enters multiple times then 
    #### enter and exit poly index list will be of length > 1.
    #### enterPolyAllIndicesList = [1,2,3,4,5,9,13,26,27,28,29,30,32,36,37,38,39] is this is the list then the output would be
    #### exterPolyIndexList = [1, 9, 13, 26, 32, 36], exitPolyIndexList = [5, 9, 13, 30, 32, 39]
    
    try:
        enterPolyIndexList, exitPolyIndexList = [enterPolyAllIndicesList[0]], []
    except IndexError:
        pdb.set_trace()
        
    for counter in range(len(enterPolyAllIndicesList)-1):
        diff = enterPolyAllIndicesList[counter + 1] - enterPolyAllIndicesList[counter]
        if  diff == 1 :
            continue
        else :
            enterPolyIndexList.append(enterPolyAllIndicesList[counter + 1])
            exitPolyIndexList.append(enterPolyAllIndicesList[counter])

    exitPolyIndexList.append(enterPolyAllIndicesList[-1])
    # enterPolyIndexList.remove(enterPolyIndexList[-1])
    # print enterPolyAllIndicesList, '\n' ,enterPolyIndexList, '\n' ,exitPolyIndexList
    # print len(traj.nodes)
    
    if len(enterPolyIndexList) != len(exitPolyIndexList) :
        #### This should never happen. Only there for debugging purposes.
        print "Something golmal"
        pdb.set_trace()
    else :
        if len(enterPolyIndexList) == 0 :
            enterPolyIndexList, exitPolyIndexList = [len(traj.nodes)], [len(traj.nodes)]
    
    return enterPolyAllIndicesList, enterPolyIndexList, exitPolyIndexList    


def findFirstExitPolyIndex(traj,polygon):
    '''if the trajectory has not entered the poly then the return will be equal to len(traj.nodes)
    If the trajectory ended within the poly then the last point will be point where the traj ended
    '''
    exitIdx = len(traj.nodes)
    firstEntry = False
    for index, node in enumerate(traj.nodes):
        x = node[1]
        y = node[2]
        if polygon.isPointInside((x,y)):
            exitIdx = index
            if  not firstEntry:
                firstEntry = True
        elif firstEntry:
            return exitIdx,firstEntry
    return exitIdx,firstEntry


def findEnterPolyIndex(traj, polygon):
    '''
    returns the index of the first node to be inside the supplied polygon. If traj doesn't enter the polygon at all, it returns an index value that is equal to the
    length of the node list in the traj.
    '''
    for index, node in enumerate(traj.nodes):
        x = node[1]
        y = node[2]
        if polygon.isPointInside((x, y)):
            return index
    else:
        return index + 1

def findExitPolyIndex(traj,polygon):
    '''
    finds the index of the node in the traj that is the last node that is inside the polygon. 
    return values:
    1. if the traj dies inside the polygon => the last node in the traj is ALSO the last node inside the polygon, ret value = len(traj.nodes) - 1
    2. if the traj NEVER enters the polygon, ret value = len(traj.nodes)
    '''
    exitIdx = len(traj.nodes)
    for index, node in enumerate(traj.nodes):
        x = node[1]
        y = node[2]
        if polygon.isPointInside((x,y)):
            exitIdx = index
    return exitIdx
    
    
def cropBeforeEnterPoly(trajList, polygon):
    """Splits each trajectory into two pieces. The piece from the start of
    the trajectory until they first enter the polygon and the rest of the
    trajectory. If the trajectory does not enter the polygon then the entire
    trajectory is considered before entering the polygon."""
    beforePolyTrajList = []
    afterPolyTrajList = []

    for traj in trajList:
        beforePolyTraj = Trajectory(id = traj.id)
        afterPolyTraj = Trajectory(id = traj.id)
                
        index = findEnterPolyIndex(traj, polygon)
                
        beforePolyTraj.nodes = traj.nodes[:index]
        afterPolyTraj.nodes = traj.nodes[index:]

        if len(beforePolyTraj.nodes) != 0:
            beforePolyTrajList.append(beforePolyTraj)
        if len(afterPolyTraj.nodes) != 0:
            afterPolyTrajList.append(afterPolyTraj)
            
    return beforePolyTrajList, afterPolyTrajList

# def cropBeforeEnterPoly(trajList, polygon):
    # """Splits each trajectory into two pieces. The piece from the start of
    # the trajectory until they first enter the polygon and the rest of the
    # trajectory. If the trajectory does not enter the polygon then the entire
    # trajectory is considered before entering the polygon."""
    # beforePolyTrajList = []
    # afterPolyTrajList = []

    # for traj in trajList:
        # foundPointInside = False
        # beforePolyTraj = copy.copy(traj)
        # beforePolyTraj.nodes = []
        # afterPolyTraj = copy.copy(traj)
        # afterPolyTraj.nodes = []
        # for time,x,y in traj.nodes:
            # if not foundPointInside:
                # if polygon.isPointInside((x,y)):
                    # foundPointInside = True
                    # print "found point inside: ", (time, x, y)
                    # afterPolyTraj.nodes.append((time,x,y))
                # else:
                    # beforePolyTraj.nodes.append((time,x,y))
            # else:
                # afterPolyTraj.nodes.append((time,x,y))

        # if len(beforePolyTraj.nodes) != 0:
            # beforePolyTrajList.append(beforePolyTraj)
        # if len(afterPolyTraj.nodes) != 0:
            # afterPolyTrajList.append(afterPolyTraj)
            
        # pdb.set_trace()

    # return beforePolyTrajList, afterPolyTrajList

def cropBeforeShopInPoly(trajList, polygons):
    """Splits each trajectory into two pieces. The piece from the start of
    the trajectory until they first shop inside the polygon and the rest of the
    trajectory. If the trajectory does not enter the polygon then the entire
    trajectory is considered before entering the polygon."""
    beforePolyTrajList = []
    afterPolyTrajList = []

    for traj in trajList:
        shoppedPoly = False
        beforePolyTraj = copy.copy(traj)
        beforePolyTraj.nodes = []
        afterPolyTraj = copy.copy(traj)
        afterPolyTraj.nodes = []
        
        dwellTraj = getDwellTraj(traj,5,30,polygons)
        if len(dwellTraj.nodes) > 0:
            cropTime = 99999999999999999
            for time, x, y, duration in dwellTraj.nodes:
                for poly in polygons:
                    if not shoppedPoly and poly.isPointInside((x, y)):
                        cropTime = time
            
            for time,x,y in traj.nodes:
                if time < cropTime:
                    beforePolyTraj.nodes.append((time,x,y))
                else:
                    afterPolyTraj.nodes.append((time,x,y))
        else:
            beforePolyTraj.nodes = traj.nodes
        
        if len(beforePolyTraj.nodes) != 0:
            beforePolyTrajList.append(beforePolyTraj)
        if len(afterPolyTraj.nodes) != 0:
            afterPolyTrajList.append(afterPolyTraj)

    
    return beforePolyTrajList, afterPolyTrajList

def cropAfterLeavePoly(trajList, polygon):
    beforeLeaveTrajList = []
    afterLeaveTrajList = []

    for traj in trajList:
        foundPointInside = False

        beforeLeaveTraj = Trajectory(id=traj.id)
        afterLeaveTraj = Trajectory(id=traj.id)

        for index, node in enumerate(traj.nodes):
            t, x, y = node
            
            if polygon.isPointInside((x, y)):
                foundPointInside = True
            elif foundPointInside:
                beforeLeaveTraj.nodes = traj.nodes[:index]
                afterLeaveTraj.nodes = traj.nodes[index:]
                break

        if len(beforeLeaveTraj.nodes) != 0:
            beforeLeaveTrajList.append(beforeLeaveTraj)
        if len(afterLeaveTraj.nodes) != 0:
            afterLeaveTrajList.append(afterLeaveTraj)

    return beforeLeaveTrajList, afterLeaveTrajList
    
    
# def cropAfterLeavePoly(trajList, polygon):
    # beforeLeaveTrajList = []
    # afterLeaveTrajList = []

    # for traj in trajList:
        # foundPointInside = False
        # leftPoly = False
        # beforeLeaveTraj = copy.copy(traj)
        # beforeLeaveTraj.nodes = []
        # afterLeaveTraj = copy.copy(traj)
        # afterLeaveTraj.nodes = []
        # for time,x,y in traj.nodes:
            # if not foundPointInside and not polygon.isPointInside((x,y)):
                # beforeLeaveTraj.nodes.append((time,x,y))
            # elif not foundPointInside:
                # foundPointInside = True
                # beforeLeaveTraj.nodes.append((time,x,y))
            # elif leftPoly or not polygon.isPointInside((x,y)):
                # leftPoly = True
                # afterLeaveTraj.nodes.append((time,x,y))
            # else:
                # beforeLeaveTraj.nodes.append((time,x,y))

        # if len(beforeLeaveTraj.nodes) != 0:
            # beforeLeaveTrajList.append(beforeLeaveTraj)
        # if len(afterLeaveTraj.nodes) != 0:
            # afterLeaveTrajList.append(afterLeaveTraj)

    # return beforeLeaveTrajList, afterLeaveTrajList
    
    
if __name__ == "__main__":
    import pickle
    
    traj = pickle.load(open("badTraj.pickle"))
    # print traj.nodes[813]
    # print len(traj.nodes)
    traj.nodes.sort()
    
    startTime = 12911385219.690001
    endTime = 12911385255.094
    
    for index in range(len(traj.nodes) - 1):
        if traj.nodes[index][0] > traj.nodes[index + 1][0] + 0.2:
            print "unsorted traj, index", index
            print traj.nodes[index]
            print traj.nodes[index + 1]
    
    startIndex = bisect_left(traj.nodes, startTime)
    endIndex = bisect_right(traj.nodes, endTime)

    print "startTime =", startTime
    print "startIndex =", startIndex
    print "startNodes =", traj.nodes[startIndex]
    print "endIndex =", endIndex
    print "croppedTraj =", traj.nodes[startIndex:endIndex]
    
    correctTraj = cropWithinTimeRange(traj, startTime, endTime)
    print correctTraj.nodes
    print traj.nodes.index(correctTraj.nodes[0]), traj.nodes.index(correctTraj.nodes[-1])
