import os
from bs4 import BeautifulSoup as bs
import CalibXML as Calib
import xml.etree.ElementTree as ET
import copy
import sys
import collections
import DBConnector as dbConn
import xml.etree.cElementTree as ET

dbSettings = collections.namedtuple('dbSettings',['host','user','password','schema','table'])

class extractTrackingRegion:
    def __init__(self,optsDict,dbInfo):
        self.cameraType = {}
        self.final_regions = {}
        self.op = optsDict
        self.dbObj = dbConn.DBConnector(host=optsDict['liveProjectSchema'], user=dbInfo.user, password=dbInfo.password, database='vms')
        self.fetchVSFile()



    def fetchVSFile(self):
        self.__get_proj_id()
        self.__get_proj_version()
        self.__get_proj_soup()
        self.__extractTrackingRegion()
    def createOutput()
        self.__output()
        
    def returnDict()
        return self.track_region_dict

    def getRegions(self):
        return self.final_regions

    def __extractTrackingRegion(self):
        self.track_region_dict = {}
        unknownCamList = []
        camera_views = self.vision_settings_file.find_all('camera_view')
        defaultCamType = ''
        for cam in camera_views:

            if cam.find('type_label').text == 'Camera View' and 'cam' in cam.find('name').string and 'hires' not in cam.find('name').string.lower() and 'highres' not in cam.find('name').string.lower():

                camName = str(cam.find('name').string)

                find_all_event_channel = cam.find_all('event_channel')
                for template in find_all_event_channel:
                    if 'trajectory_generator2' in template.find('template'):
                        start_track_region = str(cam.find('xml_vision_settings').find('param', {"name": 'startTrackPolygon'})['value']).replace(')','').replace('(','')
                #start_track_region = str(cam.find('xml_vision_settings').find('param', {"name": 'startTrackPolygon'})['value']).replace(')','').replace('(','')
                    elif 'OmniTrajectory' in template.find('template'):
                        start_track_region = str(cam.find('xml_vision_settings').find('param', {"name": 'TrackPolygon'})['value']).replace(')','').replace('(', '')

                self.track_region_dict[str(cam.find('name').string)] = start_track_region.split(',')


    #loading camera calibration
    def loadCameraCalibration(self):
        cameraObjects = {}
        calibrationTree = ET.parse(self.calibration_file)
        xmlCamerasList = calibrationTree.findall('camera')

    #    for camera in stitchedCameraPositions.keys():
        for xmlCamera in xmlCamerasList:
            cameraObject = Calib.Camera(xmlCamera.attrib['name'])
            xmlCamera = xmlCamera.find('calibration')
            cameraObject.location = map(float,(xmlCamera.attrib['location'].strip('()').split(',')))
            cameraObject.orientation = float(xmlCamera.attrib['orientation'])
            cameraObject.focalLength = float(xmlCamera.attrib['focalLength'])
            cameraObject.ccdSize = map(float,(xmlCamera.attrib['ccdSize'].strip('()').split(',')))
            cameraObject.imageSize = map(int,(xmlCamera.attrib['imageSize'].strip('()').split(',')))
            cameraObjects[cameraObject.name] = cameraObject
        return cameraObjects
    #Get proj_id from db
    def __get_proj_id(self):
        base_sql = '''
        SELECT
            project_id
        FROM
            vms.project_info
        WHERE
            name = '<proj_name>';
        '''
        sql = base_sql.replace('<proj_name>', self.op['proj_name'])
        retval = self.dbObj.execute(sql)
        if len(retval) == 0:
            raise Exception("There arent any projects in %s with name %s" %(self.op['liveProjectSchema'], self.op['proj_name']))
            #raise Exception("There arent any projects in %s with name %s" %(self.op['db_server'], self.op['proj_name']))
        elif len(retval) > 1:
            raise Exception("There is more than one project in %s with name %s" %(self.op['liveProjectSchema'], self.op['proj_name']))
            #raise Exception("There is more than one project in %s with name %s" %(self.op['db_server'], self.op['proj_name']))
        else:
            self.proj_id = int(retval[0][0])

    #Get the project version associated with the date given in the optsFile
    def __get_proj_version(self):
        base_sql = '''
        SELECT max(version)
        FROM vms.version_history
        where project_id = <proj_id>
        and DATE(modification_time) <= '<date>';
        '''

        sql = base_sql.replace('<proj_id>', str(self.proj_id)).replace('<date>', self.op['startDate'])
        retval = self.dbObj.execute(sql)[0][0]
        if retval:
            self.version = retval
        else:
            #raise Exception('Could not find a version containing the given date.')
            raise Exception('Could not find a version containing the given date.')

    #Get project blob file'''
    def __get_proj_soup(self):
        base_sql = '''
        SELECT
            data
        FROM
            vms.project
        WHERE
            id = <proj_id>
            and version = <version>;
        '''

        sql = base_sql.replace('<proj_id>', str(self.proj_id)).replace('<version>', str(self.version))

        retval = self.dbObj.execute(sql)
        if len(retval) == 0:
            #raise Exception("No project found for ID %d" % self.proj_id)
            raise Exception("No project found for ID %d" % self.proj_id)
        else:
            self.vision_settings_file = bs(str(retval[0][0]), 'xml')

        self.vision_settings_file.event_storage_type.string = 'embedded'

    def __output(self):
        finalOutDir = os.path.dirname(os.path.realpath(__file__)) + '\\' + projFileName + '_' + 'trackingRegion'+ '.xml'
        self.writeFile(finalOutDir, self.track_region_dict)

    def writeFile(self, filename,dictVal):
        root = ET.Element("root")
        camera = ET.SubElement(root, "camera")
        for cam in dictVal:
            ET.SubElement(camera, cam, trackingRegion=','.join(dictVal[cam]))
        tree = ET.ElementTree(root)
        tree.write(filename,encoding='utf-8',xml_declaration=True)
        # with open(filename, 'w') as fp:

            # for cam in dictVal:
                # point = ','.join(dictVal[cam])
                # fp.write(cam + ':'+ str(point)+ '\n')

        # fp.close()

def extractProjectFileLocInfo(projFileName, dbInfo):
    dbObj = dbConn.DBConnector(host=dbInfo.host, user=dbInfo.user, password=dbInfo.password, database=dbInfo.schema)
    projLocSql = """
    SELECT host_name from
        <table>
    where name = '<proj_file_name>'
    """

    sql = projLocSql.replace('<table>', dbInfo.table).replace('<proj_file_name>', projFileName)
    retval = dbObj.execute(sql)
    try:
        proj_host = retval[0][0]
    except:
        print 'No project file found for the storeID specified. Please check and retry'
        sys.exit()

    return proj_host






def main(storeName,startDate,outputFileFlag):
    dbInfo = dbSettings('vmdb_master', 'vmsuser', 'irrelev@nt', 'vms', 'all_project_info_fed')
    retDict = {}

    if 'live' not in storeName:
        projFileName = storeName + '_live'
    else:
        projFileName = storeName
    proj_host = extractProjectFileLocInfo(projFileName, dbInfo)
    optsDict = {'liveProjectSchema': proj_host, 'user': dbInfo.user, 'password': dbInfo.password,
                'proj_name': projFileName, 'startDate': startDate, 'calibration': storeName}
    extractObj = extractTrackingRegion(optsDict,dbInfo)
    if outputFileFlag
        extractObj.createOutput()
    else:
        retDict = extractObj.returnDict()
        
    return retDict
    
    
if __name__=='__main__':
    if len(sys.argv) != 3:
        print '<usage> --> store name, Start date'
        sys.exit()

    storeName = sys.argv[1]
    startDate = sys.argv[2]

    dbInfo = dbSettings('vmdb_master', 'vmsuser', 'irrelev@nt', 'vms', 'all_project_info_fed')

    if 'live' not in storeName:
        projFileName = storeName + '_live'
    else:
        projFileName = storeName
    proj_host = extractProjectFileLocInfo(projFileName, dbInfo)
    optsDict = {'liveProjectSchema': proj_host, 'user': dbInfo.user, 'password': dbInfo.password,
                'proj_name': projFileName, 'startDate': startDate, 'calibration': storeName}

    extractObj = extractTrackingRegion(optsDict,dbInfo)
