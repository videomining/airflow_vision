import copy
from Trajectory import *
import VmsUtils
from getDwellPointsInRegion_New import *
import sys
import glob
import os, os.path
#from statlib import stats
import TrajectoryCroppers
import TimeUtils
from MongoUtils import MongoUtils
import VmdDataLoader
import utilsAmazonS3
import GetshopUtils

def getShopDictToNodeList(getShopDict):
    nodeList = []
    for aisle in getShopDict:
        if 'endcap' in aisle.lower() or 'perimeter' in aisle.lower() or 'racetrack' in aisle.lower():
            print ''
        else:
            nodeList.append(getShopDict[aisle]['node_id'])
        for node_id in getShopDict[aisle]['category']:
            if node_id != '':
                nodeList.append(node_id)
    return nodeList
    
class PasserInfo:
    index = 0
    header = ''
    miscData = []

    def setHeader(self, header):
        self.header = header

    def getHeader(self):
        return self.header

    def __init__(self):
        self.trajId = PasserInfo.index
        PasserInfo.index += 1
        self.duration = 0 # traj Duration
        self.startTime = None
        self.entryPolyId = ''
        self.exitPolyId = ''
        self.regionPassFlagList = []

    def getCsvRow(self):
        csvString = ""
        csvString += secsToDate(self.startTime).strftime('%Y-%m-%dT%H:%M:%S.%f')
        csvString += ",%.3f" % self.duration #to allow import from VMS
        csvString += ",%d" % self.trajId
        if self.entryPolyId != None:
            csvString += ',"%s"' % self.entryPolyId
        else:
            csvString += ","

        if self.exitPolyId != None:
            csvString += ',"%s"' % self.exitPolyId
        else:
            csvString += ","
        for regionPassFlag in self.regionPassFlagList:
            csvString += ",%d" % regionPassFlag

        for item in self.miscData:
            csvString += ',%s' % (item)
        return csvString

#NOTE: very few diff. with getShopInfo.py ... need to consolidate
class ShopInfo:
    index = 0
    maxShopStops = 10
    header = ''
    miscData = []

    @staticmethod
    def setHeader(header):
        ShopInfo.header = header

    def getHeader(self):
        return ShopInfo.header

    @staticmethod
    def setNumStops(numStops):
        ShopInfo.maxShopStops = numStops

    def __init__(self):
        self.trajId = ShopInfo.index
        self.duration = 0
        ShopInfo.index += 1 # keeps unique ids
        self.startTime = None
        self.entryPolyId = ''
        self.exitPolyId = ''
        self.aisleEntryPolyId = ''
        self.aisleExitPolyId = ''
        self.entireShopDuration = 0
        self.lastShopPolyId = ''
        self.lastShopDuration = ''
        self.numberOfStops = 0
        self.shopPolyIdList = []
        self.shopDurationList = []

    def getCsvRow(self):
        csvString = ""
        if self.duration == 0 and self.startTime == None and self.entireShopDuration == 0 and self.numberOfStops == 0:
            return csvString
        csvString += secsToDate(self.startTime).strftime('%Y-%m-%dT%H:%M:%S.%f')
        csvString += ",%.3f" % self.duration #to allow import from VMS
        csvString += ",%d" % self.trajId

        if self.entryPolyId != None:
            csvString += ',"%s"' % str(self.entryPolyId)
        else:
            csvString += ","

        if self.exitPolyId != None:
            csvString += ',"%s"' % str(self.exitPolyId)
        else:
            csvString += ","

        if self.aisleEntryPolyId != None:
            csvString += ',"%s"' % str(self.aisleEntryPolyId)
        else:
            csvString += ","

        if self.aisleExitPolyId != None:
            csvString += ',"%s"' % str(self.aisleExitPolyId)
        else:
            csvString += ","

        csvString += ",%d" % self.numberOfStops
        csvString += ",%s" % str(self.entireShopDuration)

        if self.lastShopPolyId != None:
            csvString += ',"%s"' % str(self.lastShopPolyId)
        else:
            csvString += ","

        csvString += ",%s" % str(self.lastShopDuration)

        assert len(self.shopPolyIdList) == len(self.shopDurationList)
        index = 0
        while index < len(self.shopPolyIdList):
            csvString += ',"%s",%s' % (str(self.shopPolyIdList[index]),str(self.shopDurationList[index]))
            index += 1
        for item in self.miscData:
            csvString += ',%s' % (item)
        return csvString


    def saveShopInfo(shopInfoList,filename, append = False):
      if append:
        # fileObj = open(filename,'a')
        fileObj = VmsUtils.OpenRobustly(filename,'a')
        for shopInfo in shopInfoList[1:]:
            fileObj.write(shopInfo.getCsvRow() + "\n")
      else:
        # fileObj = open(filename,'w')
        fileObj = VmsUtils.OpenRobustly(filename,'w')
        if len(shopInfoList) == 0:
            tmpShopInfo = ShopInfo()
        else:
            tmpShopInfo = shopInfoList[0]
        fileObj.write(tmpShopInfo.getHeader() + "\n")
        for shopInfo in shopInfoList:
            fileObj.write(shopInfo.getCsvRow() + "\n")
      fileObj.close()
  
def getEntryPolygonId(traj,polygonDict, shoppingRegion):
    beforePolyTrajList, afterPolyTrajList = TrajectoryCroppers.cropBeforeEnterPoly([traj], shoppingRegion)
    
    # look for the last node in a polygon before entering the shopping region
    if len(beforePolyTrajList) < 1:
        return None
    
    traj = beforePolyTrajList[0]

    polygonList = polygonDict.items()
    
    for t, x, y in reversed(traj.nodes):
        for polyId, polygon in polygonList:
            if polygon.isPointInside((x, y)):
                return polyId
                
    # look for the first node in a polygon after entering the shopping region
    if len(afterPolyTrajList) < 1:
        return None
        
    traj = afterPolyTrajList[0]
    for t, x, y in traj.nodes:
        if not shoppingRegion.isPointInside((x, y)):
            return None
        for polyId, polygon in polygonList:
            if polygon.isPointInside((x, y)):
                return polyId


def getExitPolygonId(traj,polygonDict, shoppingRegion):
# single point search to see if it exists in the polygon
    trash, traj = TrajectoryCroppers.cropAfterLeavePoly([traj], shoppingRegion)
    if len(traj) <= 0:
        return None
        
    traj = traj[0]
    polygonList = polygonDict.items()

    for t, x, y in traj.nodes:
        for polyId, polygon in polygonList:
            if polygon.isPointInside((x, y)):
                return polyId
    isInside = True
    if len(trash) > 0:
        traj = trash[0]
        nodeIndex = 0
        while isInside and nodeIndex in range(len(traj.nodes)-1,-1,-1):
            t, x, y = traj.nodes[nodeIndex]
            if shoppingRegion.isPointInside((x, y)):
                for polyId, polygon in polygonList:
                    if polygon.isPointInside((x, y)):
                        return polyId
            else:
                isInside = False
            nodeIndex -= 1

def getDwellTimeAndNumStops(dwellTraj,polygon):
    # dwellWithinAisle = DwellTraj()
    firstDwellInAisleIndex = -1
    duration = 0
    totalStops = 0
    shop_time_list = []
    first_dest_flag = False
    for index in range(len(dwellTraj.nodes)):
        node = dwellTraj.nodes[index]
        if polygon.isPointInside((node[1],node[2])):
            if firstDwellInAisleIndex == -1:
                firstDwellInAisleIndex = index
            # dwellWithinAisle.addDwell(dwellTraj.nodes[index])
            shop_time_list.append(node[0])
            duration += node[3]
            totalStops += 1
            if index == 0:
                first_dest_flag = True
    return duration,totalStops,shop_time_list,first_dest_flag,firstDwellInAisleIndex

#for category we need to check the stopping side to assign the shoppers
def getDwellTimeAndNumStopsCat(dwellTraj,polygon,firstDwellInAisleIndex):
    duration = 0
    totalStops = 0
    shop_time_list = []
    extraTime = 0
    first_dest_flag = False
    for index in range(len(dwellTraj.nodes)):
        try:
            time,x,y,dur, sideShopped = dwellTraj.nodes[index]
        except:
            time,x,y,dur = dwellTraj.nodes[index]
            sideShopped = None
        if polygon.isPointInside((x,y)) and (sideShopped == polygon.getActiveSide() or polygon.getActiveSide() is None):
            shop_time_list.append(time)
            duration += dur
            totalStops += 1
            if index == firstDwellInAisleIndex:
                first_dest_flag = True
        else:
            if polygon.isPointInside((x, y)):
                extraTime += dur
    return duration,extraTime,totalStops,shop_time_list,first_dest_flag


def getDwellTime(dwellTraj,polygonList):
    duration = 0
    for node in dwellTraj.nodes:
        addedDuration = False
        for polygon in polygonList:
            if not addedDuration:
                duration += node[3]
                addedDuration = True

    return duration

def getLastShopPolyIdAndDuration(dwellTraj,polygonList):
    lastShopPolyId = ''
    lastShopDuration = ''
    for i in range(len(dwellTraj.nodes)-1,-1,-1):
        time,x,y,duration,sideShopped = dwellTraj.nodes[i]
        for polygon in polygonList:
            if polygon.isPointInside((x,y)) and (sideShopped == polygon.getActiveSide() or polygon.getActiveSide() is None):
                lastShopPolyId = polygon.getRegionId()
                lastShopDuration = duration
                return lastShopPolyId,lastShopDuration

    return lastShopPolyId,lastShopDuration


def getShopPolyIdAndDurationList(dwellTraj,polygonList,entireShoppingRegion,emptyVal=''):
    shopPolyIdList = []
    shopDurationList = []

    for i in range(0,ShopInfo.maxShopStops):
        shopPolyIdList.append('')
        shopDurationList.append(emptyVal)


    stopsFound = 0 #Number of stops belonging to at least one of the polygons
    index = 0
    while index < len(dwellTraj.nodes) and stopsFound < ShopInfo.maxShopStops:
        time,x,y,duration, sideShopped = dwellTraj.nodes[index]
        stoppedInside = False
        polysInside = []
        for polygon in polygonList:
            if polygon.isPointInside((x,y)) and (sideShopped == polygon.getActiveSide() or polygon.getActiveSide() is None):
                polysInside.append(polygon)

        if len(polysInside) != 0:
            shopPolyIdList[stopsFound] = polysInside[0].getRegionId()#polygonList.index(polysInside[0]) + 1
            shopDurationList[stopsFound] = duration
            stopsFound += 1

        if not stoppedInside and stopsFound == 0 and entireShoppingRegion.isPointInside((x,y)):
            if stopsFound < ShopInfo.maxShopStops:
                shopPolyIdList[stopsFound] = 0
                shopDurationList[stopsFound] = duration
            stopsFound += 1
        index += 1

    return (shopPolyIdList,shopDurationList,stopsFound)

def loadPolygonList(filename):
    polygonList = []
    # for line in open(filename):
    for line in VmsUtils.OpenRobustly(filename):
        polygon = VmsUtils.polyFromString(line.strip('\n'))
        polygonList.append(polygon)
    return polygonList


class RegionEventInfo:
    maxBeforeRegions = 10
    maxAfterRegions = 10
    header = ''
    miscData = []
    @staticmethod
    def setHeader(header):
        RegionEventInfo.header = header

    def getHeader(self):
        return RegionEventInfo.header

    @staticmethod
    def setNumStops(numStops):
        RegionEventInfo.maxBeforeRegions = numStops
        RegionEventInfo.maxAfterRegions = numStops

    def __init__(self):
        self.polyId = -1
        self.trajId = -1
        self.startTime = None
        self.entryPolyId = ""
        self.exitPolyId = ""
        self.aisleEntryPolyId = ""
        self.aisleExitPolyId = ""
        self.beforeStops = 0
        self.afterStops = 0
        self.shopDuration = ''
        self.beforeStopPolygonIdList = []
        self.beforeStopDurationList = []
        self.afterStopPolygonIdList = []
        self.afterStopDurationList = []

        for index in range(RegionEventInfo.maxBeforeRegions):
            self.beforeStopPolygonIdList.append("")
            self.beforeStopDurationList.append('')

        for index in range(RegionEventInfo.maxAfterRegions):
            self.afterStopPolygonIdList.append("")
            self.afterStopDurationList.append('')
            

    
    def getCsvRow(self):
        csvString = ""
        if self.startTime == None and self.polyId == -1 and self.shopDuration == '' and self.beforeStops == 0 and self.afterStops == 0:
            return csvString
        csvString += secsToDate(self.startTime).strftime('%Y-%m-%dT%H:%M:%S.%f')
        csvString += ',%.3f' % (self.duration)
        csvString += ',%d' % self.trajId
        csvString += ',"%s"' % (self.entryPolyId)
        csvString += ',"%s"' % (self.exitPolyId)
        csvString += ',"%s"' % (self.aisleEntryPolyId)
        csvString += ',"%s"' % (self.aisleExitPolyId)
        csvString += ',%d' % self.beforeStops
        csvString += ',%d' % self.afterStops
        csvString += ',%f' % self.shopDuration

        for index in range(0,len(self.beforeStopPolygonIdList)):
            csvString += ',"%s"' % (self.beforeStopPolygonIdList[index])
            csvString += ',%s' % str(self.beforeStopDurationList[index])

        for index in range(0,len(self.afterStopPolygonIdList)):
            csvString += ',"%s"' % (self.afterStopPolygonIdList[index])
            csvString += ',%s' % str(self.afterStopDurationList[index])

        for item in self.miscData:
            csvString += ',%s' % (item)
        return csvString


def getRegionInfo(shopInfo):
    regionEventInfoList = []
    index = 0
    uniquePolyIdList = []
    while index < len(shopInfo.shopPolyIdList):
        if shopInfo.shopPolyIdList[index] not in uniquePolyIdList:
            uniquePolyIdList.append(shopInfo.shopPolyIdList[index])
            regEventInfo = RegionEventInfo()
            regEventInfo.polyId = shopInfo.shopPolyIdList[index]
            regEventInfo.trajId = shopInfo.trajId
            regEventInfo.duration = shopInfo.duration
            regEventInfo.entryPolyId = shopInfo.entryPolyId
            regEventInfo.exitPolyId = shopInfo.exitPolyId
            regEventInfo.aisleEntryPolyId = shopInfo.aisleEntryPolyId
            regEventInfo.aisleExitPolyId = shopInfo.aisleExitPolyId
            regEventInfo.startTime = shopInfo.startTime
            regEventInfo.beforeStops = index
            regEventInfo.miscData = shopInfo.miscData
            if '' in shopInfo.shopPolyIdList:
                regEventInfo.afterStops = shopInfo.shopPolyIdList.index('') - index - 1
            else:
                regEventInfo.afterStops = len(shopInfo.shopPolyIdList) - index - 1

            regEventInfo.shopDuration = shopInfo.shopDurationList[index]

            #before shopping regions
            for shopIndex in range(0,index):
                beforeStopIndex = len(regEventInfo.beforeStopPolygonIdList) - index + shopIndex
                regEventInfo.beforeStopPolygonIdList[beforeStopIndex] = shopInfo.shopPolyIdList[shopIndex]
                regEventInfo.beforeStopDurationList[beforeStopIndex] = shopInfo.shopDurationList[shopIndex]

            #after shopping regions
            for shopIndex in range(index+1,len(shopInfo.shopPolyIdList)):
                afterStopIndex = shopIndex - index - 1
                regEventInfo.afterStopPolygonIdList[afterStopIndex] = shopInfo.shopPolyIdList[shopIndex]
                regEventInfo.afterStopDurationList[afterStopIndex] = shopInfo.shopDurationList[shopIndex]

            regionEventInfoList.append(regEventInfo)

        index += 1

    return regionEventInfoList

class ShopInfoRegion:
    regionId = None
    regionPolys = None
    doePolys = None
    regionName = None
    activeSides = None
    activePoly = None


    # Currently can be set to one of four values from (left, right, top, bottom)
    activeSide = None

    def __init__(self, id, polygons = None, doePolygons = None):
        self.regionId = id

        if polygons is not None:
            self.regionPolys = polygons
        else:
            self.regionPolys = []

        if doePolygons is not None:
            self.doePolys = doePolygons
        else:
            self.doePolys = {}

        self.sideShoppedPolys = []
        self.activeSides = {}

    def getRegionId(self):
        return self.regionId

    def setRegionName(self, name):
        self.regionName = name

    def getRegionName(self):
        return self.regionName

    def addRegionPoly(self, polygon):
        self.regionPolys.append(polygon)

    def addRegionPolyFromStr(self, polygonStr):
        polygonTokens = polygonStr.split('-')
        regionPoly = VmsUtils.polyFromString(polygonTokens[0])

        polyExists = False
        for poly in self.regionPolys:
            if poly.verts == regionPoly.verts:
                polyExists = True

        if not polyExists:
            self.regionPolys.append(regionPoly)
            if len(polygonTokens) == 2 and (polygonTokens[1].lower() == 'left' or polygonTokens[1].lower() == 'right' or polygonTokens[1].lower() == 'top' or polygonTokens[1].lower() == 'bottom'):
                self.activeSides[regionPoly] = polygonTokens[1].lower()
            else:
                self.activeSides[regionPoly] = None

    def addDOEPoly(self, doeName, polygon):
        self.doePolys[doeName] = polygon

    def addDOEPolyFromStr(self, doeName, polygonStr):
        self.doePolys[doeName] = (VmsUtils.polyFromString(polygonStr))

    def isPointInside(self, point):
        isInside = False
        for poly in self.regionPolys:
            if poly.isPointInside(point):
                self.activePoly = poly
                isInside = True
        return isInside

    def setActiveSide(self, side):
        self.activeSides[self.activePoly] = side.lower()

    def getActiveSide(self):
        return self.activeSides[self.activePoly]

    def __str__(self):
        outputString = '%s\n' % (self.regionId)
        outputString += 'Region Polygons\n-----------------------------\n'
        for poly in self.regionPolys:
            outputString += str(poly)
        outputString += '\n\nDOE Polygons\n-----------------------------\n'
        for poly in self.doePolys:
            outputString += poly

        return outputString

    def getRegionPolys(self):
        return self.regionPolys
    def getDOEPolys(self):
        return self.doePolys

class ShopInfoMainRegion(ShopInfoRegion):
    subRegions = None

    def __init__(self, id, polygons = None, doePolygons = None, subRegions = None):
        ShopInfoRegion.__init__(self, id, polygons, doePolygons)

        if subRegions is not None:
            self.subRegions = subRegions
        else:
            self.subRegions = []

    def addSubRegion(self, subRegion):
        self.subRegions.append(subRegion)

    def getSubRegions(self):
        return self.subRegions

    def calculateShopInfo(self, traj, mainShoppingRegion, demographicsData):        
        minDwellTime = 3
        maxDisplacement = 30
        shopInfo = ShopInfo()
        shopInfo.startTime = traj.startTime()
        shopInfo.duration = traj.duration()
        shopInfo.trajId = int(traj.id)
        shopInfo.miscData = demographicsData

        # Fix getEntryPolygonId and getExitPolygonId to assign the ID of the last polygon entered before going in the aisle (first entered after leaving aisle)
        shopInfo.entryPolyId = getEntryPolygonId(traj, self.doePolys, mainShoppingRegion)
        shopInfo.exitPolyId =  getExitPolygonId(traj, self.doePolys, mainShoppingRegion)
        
        # Looks like we can get rid of EntryPolyId since it's not being used anyway
        shopInfo.aisleEntryPolyId = shopInfo.entryPolyId
        shopInfo.aisleExitPolyId = shopInfo.exitPolyId
        
        if shopInfo.entryPolyId == None:
            shopInfo.entryPolyId = ""
        if shopInfo.exitPolyId == None:
            shopInfo.exitPolyId = ""

        if shopInfo.aisleEntryPolyId == None:
            shopInfo.aisleEntryPolyId = ""
        if shopInfo.aisleExitPolyId == None:
            shopInfo.aisleExitPolyId = ""
        
        # Update getDwellTime to support a main region that is multiple polygons - Done
##        shopInfo.entireShopDuration = getDwellTime(traj, self.regionPolys)
        
        if shopInfo.entireShopDuration == 0:
            shopInfo.entireShopDuration = ''

        # Update getDwellTraj to support overlapping polygons - A lot of work.
        # Needs to decide the side shopped internally so there's a lot of bookkeeping to do this properly
        dwellTraj = getDwellTraj(traj,minDwellTime,maxDisplacement,self.subRegions)
        shopInfo.entireShopDuration = getDwellTime(dwellTraj, self.regionPolys)
        
        dwellTraj = addSidesShopped(traj, dwellTraj, mainShoppingRegion)
        # Update getLastShopPolyIdAndDuration to support overlapping polygons - Done
        shopInfo.lastShopPolyId,shopInfo.lastShopDuration = \
                                            getLastShopPolyIdAndDuration(dwellTraj, self.subRegions)

        if shopInfo.lastShopDuration == 0:
            shopInfo.lastShopDuration = ''
        
        # Update  getShopPolyIdAndDurationList to support overlapping polygons - Done
        shopInfo.shopPolyIdList,shopInfo.shopDurationList,shopInfo.numberOfStops = \
                        getShopPolyIdAndDurationList(dwellTraj,self.subRegions, mainShoppingRegion)
        
        return shopInfo
        
    def __str__(self):
        outputString = '************************************************************\n'
        outputString += '%s\n' % (self.regionId)
        outputString += 'Region Polygons\n-----------------------------\n'
        for poly in self.regionPolys:
            outputString += str(poly)
        outputString += '\n\nDOE Polygons\n-----------------------------\n'
        for poly in self.doePolys:
            outputString += poly
        for subRegion in self.subRegions:
            outputString += '\n\n%s' % (subRegion)
        return outputString

class ShopInfoController:
    mainRegions = None
    shopInfoList = None
    regionInfoList = None
    mongoAisleDict = None
    mongoCategoryDict = None

    def __init__(self, doeDict, storeId, getShopDict):
        self.mainRegions = []
        self.shopInfoList = {}
        self.regionInfoList = {}
        self.mongoAisleDict = {}
        self.mongoCategoryDict = {}
        self.regionToNodeDict = {}
        self.outTrajList = []
        self.loadMainRegionsFromDatabase(getShopDict)
        self.loadDOERegionFromDatabase(doeDict)
        # for filename in regionFilenames:
        #     self.loadMainRegionsFromFile(filename)
        #loading NodeId and Name for all regions
        # self.loadNodeInfo(storeId)
        # for filename in doeFilenames:
        #     self.loadDOERegionsFromFile(filename)

    def __del__(self):
        for key in self.shopInfoList.keys():
            del self.shopInfoList[key]
        for key in self.regionToNodeDict.keys():
            del self.regionToNodeDict[key]
        for key in self.regionInfoList.keys():
            del self.regionInfoList[key]

        for region in self.mainRegions:
            del region

        del self.mainRegions
        del self.shopInfoList
        del self.regionInfoList

    
    def clear(self):
        self.mongoAisleDict = {}
        self.mongoCategoryDict = {}
        for key in self.shopInfoList.keys():
            del self.shopInfoList[key]
        
        for key in self.regionInfoList.keys():
            del self.regionInfoList[key]

    def loadMainRegionsFromDatabase(self,dataDict):
        # for region in
        print 'hi'
        for mainVal in dataDict:
            mainRegionId = dataDict[mainVal]['node_id']+':'+mainVal
            mainRegion = ShopInfoMainRegion(mainRegionId)
            #adding the polyStr
            mainRegion.addRegionPolyFromStr(dataDict[mainVal]['region'])
            for nodeId in dataDict[mainVal]['category']:
                subRegion = None
                appendSubRegion = True
                subRegionId = nodeId+':'+dataDict[mainVal]['category'][nodeId][0][-1]
                #
                # for region in mainRegion.getSubRegions():
                #     regionId = region.getRegionId()
                #     if regionId == subRegionId:
                #         appendSubRegion = False
                #         subRegion = region
                #
                if subRegion is None:
                    subRegion = ShopInfoRegion(subRegionId)

                for values in dataDict[mainVal]['category'][nodeId]:
                    if values[1] != '':
                        subRegion.addRegionPolyFromStr(values[0]+'-'+values[1])
                    else:
                        subRegion.addRegionPolyFromStr(values[0])
                if appendSubRegion:
                    mainRegion.addSubRegion(subRegion)
            self.mainRegions.append(mainRegion)
    def loadMainRegionsFromFile(self, filename):
        # lines = map(str.strip, open(filename).readlines())
        lines = map(str.strip, VmsUtils.OpenRobustly(filename).readlines())

        line = lines[0]
        tokens = line.split('|')
        mainRegionId = ''.join(tokens[0].split()).replace('&','_').replace('/','_').lower()
        mainRegion = None
        appendMainRegion = True

        for region in self.mainRegions:
            regionId = region.getRegionId()
            if regionId == mainRegionId:
                mainRegion = region
                appendMainRegion = False

        if mainRegion is None:
            mainRegion = ShopInfoMainRegion(mainRegionId)

        for token in tokens[1:]:
            mainRegion.addRegionPolyFromStr(token)

        for line in lines[1:]:
            subRegion = None
            appendSubRegion = True
            tokens = line.split('|')
            subRegionId = ''.join(tokens[0].split()).replace('&','_').replace('/','_').lower()

            for region in mainRegion.getSubRegions():
                regionId = region.getRegionId()
                if regionId == subRegionId:
                    appendSubRegion = False
                    subRegion = region

            if subRegion is None:
                subRegion = ShopInfoRegion(subRegionId)

            for token in tokens[1:]:
                subRegion.addRegionPolyFromStr(token)

            if appendSubRegion:
                mainRegion.addSubRegion(subRegion)

        if appendMainRegion:
            self.mainRegions.append(mainRegion)

    # def loadNodeInfo(self,storeId):
    #     databaseName = 'region_info'
    #
    #     VDL = VmdDataLoader.VMDDataLoader('', databaseName, storeId)
    #     for region in self.mainRegions:
    #         rgnName =region.regionId
    #         nodeId,name = VDL._getDatabaseNodeIdAndName(storeId,rgnName)
    #         self.regionToNodeDict[rgnName] = [nodeId,name]
    #         for categoryRegion in region.getSubRegions():
    #             catRgnName = categoryRegion.regionId
    #             catnodeId,catname = VDL._getDatabaseNodeIdAndName(storeId,catRgnName)
    #             self.regionToNodeDict[catRgnName] = [catnodeId,catname]

            # if rgnName not in self.regionToNodeDict:

    def loadDOERegionFromDatabase(self,doeDict):

        for aisle in doeDict:
            applicableRegions = []
            for region in self.mainRegions:
                if region.regionId.split(':')[-1] == aisle:
                    applicableRegions.append(region)
            for  doe in doeDict[aisle]:
                for region in applicableRegions:
                    region.addDOEPolyFromStr(doe,doeDict[aisle][doe])
    def loadDOERegionsFromFile(self, filename):
        # lines = map(str.strip, open(filename).readlines())
        lines = map(str.strip, VmsUtils.OpenRobustly(filename).readlines())

        applicableRegions = []
        for line in lines:
            tokens = line.split('|')

            if len(tokens) == 1:
                applicableRegions = []
                regionId = ''.join(tokens[0].split()).replace('&','_').replace('/','_').lower()
                for region in self.mainRegions:
                    if region.regionId == regionId:
                        applicableRegions.append(region)
            else:
                doeName = tokens[0]
                for region in applicableRegions:
                    region.addDOEPolyFromStr(doeName, tokens[1])

    def _getStopTS(self, trajExitNode, dwellTraj, dwellNodeIndices):
        maxTS = trajExitNode[0]
        for idx in dwellNodeIndices:
            lastSwallowedTime = dwellTraj.nodes[idx][0] + dwellTraj.nodes[idx][3]
            if lastSwallowedTime > maxTS:
                maxTS = lastSwallowedTime
        return maxTS

    def _getStartTS(self, trajEntryNode, dwellTraj, dwellNodeIndices):
        minTS = trajEntryNode[0]
        for idx in dwellNodeIndices:
            if dwellTraj.nodes[idx][0] < minTS:
                minTS = dwellTraj.nodes[idx][0]
        return minTS

    def _getTrajIdx(self, traj, trajEntryIndices, trajExitIndices, dwellNode):
        for idx in range(len(trajEntryIndices)):
            if dwellNode[0] >= traj.nodes[trajEntryIndices[idx]][0] and dwellNode[0] <= traj.nodes[trajExitIndices[idx]][0]:
                return idx
        return -1

    def _getRegionTime(self,region,traj,dwellTraj):
        regionTime = 0
        '''
        two cases:
        1. dwell lies inside (because its present in the shop info files) while the traj doesn't
        2. dwell doesn't lie (because of the averaging method calculating dwell nodes) while the traj does

        in case 1, the func findMultipleEnterExitPolyIndices, start time is equal to the dwell time.
        in case 2, do not consider this track
        '''
        trajEntryPolyAllIndicesList, trajEntryIndices, trajExitIndices = TrajectoryCroppers.findMultipleEnterExitPolyIndices(
            traj, region)
        dwellEntryPolyAllIndicesList, dwellEntryIndices, dwellExitIndices = TrajectoryCroppers.findMultipleEnterExitPolyIndices(
            dwellTraj, region)
        if dwellEntryIndices[0] == len(dwellTraj.nodes):
            regionTime = 0
            return regionTime
        # dwellDurationList = map(lambda x: round(dwellTraj.nodes[x][3], 3),dwellEntryPolyAllIndicesList)

        #### Since the length of trajEntryIndices and trajExitIndices are the same you dont have to check if the length of trajExitIndices is 1
        if (len(trajEntryIndices) == 1 and trajEntryIndices[0] == len(traj.nodes)):
            startIdx = dwellEntryIndices[0]
            start_ts = dwellTraj.nodes[startIdx][0]
            exitIdx = dwellExitIndices[-1]
            stop_ts = dwellTraj.nodes[exitIdx][0] + dwellTraj.nodes[exitIdx][3]
            regionTime += stop_ts - start_ts
        else:
            trajIdxToDwellNodeIdxDict = {}
            inbetweenDwellNodeIndices = []
            # dwellIndices = list(set(dwellEntryIndices).union(dwellExitIndices))
            for dwellIdx in dwellEntryPolyAllIndicesList:
                idx_trajIdx = self._getTrajIdx(traj, trajEntryIndices, trajExitIndices,
                                               dwellTraj.nodes[dwellIdx])
                if idx_trajIdx != -1:
                    try:
                        trajIdxToDwellNodeIdxDict[idx_trajIdx].append(dwellIdx)
                    except:
                        trajIdxToDwellNodeIdxDict[idx_trajIdx] = [dwellIdx]
                else:
                    inbetweenDwellNodeIndices.append(dwellIdx)
            for idx in trajIdxToDwellNodeIdxDict:
                start_ts = self._getStartTS(traj.nodes[trajEntryIndices[idx]],
                                            dwellTraj,
                                            trajIdxToDwellNodeIdxDict[idx])
                stop_ts = self._getStopTS(traj.nodes[trajExitIndices[idx]], dwellTraj,
                                          trajIdxToDwellNodeIdxDict[idx])
                regionTime += (stop_ts - start_ts)
            trajNodeIndices = list(set(trajEntryIndices).union(trajExitIndices))
            trajNodeIndices.sort()
            for dwellIdx in inbetweenDwellNodeIndices:
                diff = -1
                # find the closest traj start or traj end
                maxDist = 10000
                minDist = maxDist
                minIdx = len(traj.nodes)
                for trajIdx in trajNodeIndices:
                    d = abs(dwellTraj.nodes[dwellIdx][0] - traj.nodes[trajIdx][0])
                    if d < minDist:
                        minDist = d
                        minIdx = trajIdx
                nearestTrajNode = traj.nodes[minIdx]
                dwellNode = dwellTraj.nodes[dwellIdx]
                if minIdx in trajExitIndices:
                    # if the order is <trajExitNode>-------<dwellNode>
                    start_ts = nearestTrajNode[0]
                    stop_ts = dwellNode[0] + dwellNode[3]
                    diff = stop_ts - start_ts
                if minIdx in trajEntryIndices:
                    # if the order is <dwellNode>----------<trajStartNode>
                    lastSwallowedTime = dwellNode[0] + dwellNode[3]
                    if nearestTrajNode[0] > lastSwallowedTime:
                        stop_ts = nearestTrajNode[0]
                    else:
                        stop_ts = lastSwallowedTime
                    start_ts = dwellNode[0]
                    if diff == -1:
                        diff = (stop_ts - start_ts)
                    elif diff < (stop_ts - start_ts):
                        diff = (stop_ts - start_ts)
                regionTime += diff

        return regionTime
    def calculateShopInfo(self, trajList, demographicsData,project_type):
        minDwellTime = 3
        maxDisplacement = 30
        trajIndex = 0
        trajCount = 0
        aisleDictCounter = 0
        categoryDictCounter = 0
        for traj in trajList:

            regionForSequence = {}
            aisleForTrafficSeq = {}
            catForTrafficSeq = {}
            trajCount += 1
            # print 'traj num %r' %trajCount
            dwellTraj = getDwellTrajNew(traj,minDwellTime,maxDisplacement)
            trajDate = datetime.strftime(TimeUtils.secsToDate(traj.startTime()),'%Y%m%d')
            trajHour = datetime.strftime(TimeUtils.secsToDate(traj.startTime()),'%H')
            for region in self.mainRegions:
                for nodeIndex in range(len(traj.nodes)):
                    t, x, y = traj.nodes[nodeIndex]
                    #first check if there is any point of the track inside the polygon
                    if region.isPointInside((x,y)):
                        #get the last nodeIndex so we can get the total_time in aisle
                        rgnName = region.regionId
                        #get total time in  aisle polygon. if 0 then ignore the aisle
                        # totalTimeInPoly = TrajectoryCroppers.timeInPoly(traj,region)
                        totalTimeInPoly = self._getRegionTime(region,traj,dwellTraj)
                        if totalTimeInPoly > 0:

                            # print 'region found %r'%rgnName
                            #we calculate data only for aisles and not for endcap aisles and perimeter and racetrack
                            if 'perimeter' not in rgnName.lower() and 'endcap' not in rgnName.lower():

                                #getting aisle level data
                                self.mongoAisleDict[aisleDictCounter] = {'node_name':rgnName.split(':')[-1],'node_id':rgnName.split(':')[0],'traj_id':traj.id,'node_type':'Aisle','start_date':trajDate,
                                                                         'start_hour':trajHour,'shop_time_list':[],'nav_time':0,'shop_time':0,'num_of_shops':0,'D0E':'','D0X':'',
                                                                         'traf_sequence_num':[],'parent_region':rgnName.split(':')[-1],'first_dest_flag':False,'project_type': project_type}
                                #doe we calculate only for main aisles
                                doePolys = region.getDOEPolys()
                                entryPolyId = getEntryPolygonId(traj, doePolys, region)
                                exitPolyId = getExitPolygonId(traj, doePolys, region)
                                if entryPolyId != None:
                                    self.mongoAisleDict[aisleDictCounter]['D0E']= entryPolyId
                                    #exitPolyId =  getExitPolygonId(traj, doePolys, region)
                                if exitPolyId != None:
                                    self.mongoAisleDict[aisleDictCounter]['D0X'] = exitPolyId
                                shopDuration,totalStops,shop_time_list,first_dest_flag,firstDwellInAisleIndex = getDwellTimeAndNumStops(dwellTraj,region)
                                if totalStops > 0:
                                    shop_list = [TimeUtils.secsToDate(timeVal) for timeVal in shop_time_list]
                                    self.mongoAisleDict[aisleDictCounter] ['shop_time_list'] = shop_list
                                    self.mongoAisleDict[aisleDictCounter]['shop_time'] = shopDuration
                                    self.mongoAisleDict[aisleDictCounter]['num_of_shops'] = totalStops
                                    self.mongoAisleDict[aisleDictCounter]['nav_time'] = totalTimeInPoly-shopDuration
                                    if first_dest_flag:
                                        self.mongoAisleDict[aisleDictCounter]['first_dest_flag'] = True
                                aisleForTrafficSeq[aisleDictCounter]=region
                                aisleDictCounter += 1


                                #getting category level data
                                dwellTraj = addSidesShopped(traj, dwellTraj, region)
                                #we will get the category level information here
                                #aisle part ends here  now lets calculate the category information
                                for categoryRegion in region.getSubRegions():
                                    # print categoryRegion.regionId

                                    for nodeIndex1 in range(nodeIndex,len(traj.nodes),1):
                                        t, x, y = traj.nodes[nodeIndex1]
                                        #first check if there is any point of the track inside the polygon
                                        if categoryRegion.isPointInside((x,y)):

                                            catrgnName = categoryRegion.regionId
                                            self.mongoCategoryDict[categoryDictCounter] = {'node_name':catrgnName.split(':')[-1],'node_id':catrgnName.split(':')[0],
                                                                                           'traj_id':traj.id,'node_type':'Category','start_date':trajDate,'start_hour':trajHour,
                                                                                           'shop_time_list':[],'nav_time':0,'shop_time':0,'num_of_shops':0,'first_dest_flag':False,'shop_sequence_num':[],
                                                                                           'traf_sequence_num':[],'parent_region':rgnName.split(':')[-1],'project_type': project_type}
                                            # totalTimeInPoly1 = TrajectoryCroppers.timeInPoly(traj,categoryRegion)
                                            totalTimeInPoly1 = self._getRegionTime(categoryRegion,traj,dwellTraj)
                                            shopDuration1,extraTime1,totalStops1,shop_time_list1,first_dest_flag = getDwellTimeAndNumStopsCat(dwellTraj,categoryRegion,firstDwellInAisleIndex)
                                            if totalStops1 > 0:
                                                shop_list = [TimeUtils.secsToDate(timeVal) for timeVal in shop_time_list1]
                                                self.mongoCategoryDict[categoryDictCounter] ['shop_time_list'] = shop_list
                                                self.mongoCategoryDict[categoryDictCounter]['shop_time'] = shopDuration1
                                                self.mongoCategoryDict[categoryDictCounter]['num_of_shops'] = totalStops1
                                                if first_dest_flag:
                                                    self.mongoCategoryDict[categoryDictCounter]['first_dest_flag'] = True
                                                #if the track is shopper for the category we will include it in a seperate list used to calculate the sequence_num
                                                regionForSequence[categoryDictCounter]= categoryRegion
                                                self.mongoCategoryDict[categoryDictCounter]['nav_time'] = totalTimeInPoly1-extraTime1-shopDuration1
                                            catForTrafficSeq[categoryDictCounter] = categoryRegion
                                            categoryDictCounter+= 1
                                            break
                            else:
                                #if the region is one of the above then no need to calculate first dest
                                #getting category level data
                                # dwellTraj = addSidesShopped(traj, dwellTraj, region)
                                #we will get the category level information here
                                #aisle part ends here  now lets calculate the category information
                                for categoryRegion in region.getSubRegions():
                                    for nodeIndex1 in range(nodeIndex,len(traj.nodes),1):
                                        t, x, y = traj.nodes[nodeIndex1]
                                        #first check if there is any point of the track inside the polygon
                                        if categoryRegion.isPointInside((x,y)):
                                            catrgnName = categoryRegion.regionId
                                            self.mongoCategoryDict[categoryDictCounter] = {'node_name':catrgnName.split(':')[-1],'node_id':catrgnName.split(':')[0],
                                                                                           'traj_id':traj.id,'node_type':'Category','start_date':trajDate,'start_hour':trajHour,
                                                                                           'shop_time_list':[],'nav_time':0,'shop_time':0,'num_of_shops':0,'shop_sequence_num':[],
                                                                                           'traf_sequence_num':[],'parent_region':rgnName.split(':')[-1],'project_type': project_type}
                                            # totalTimeInPoly1 = TrajectoryCroppers.timeInPoly(traj,region)
                                            totalTimeInPoly1 = self._getRegionTime(categoryRegion,traj,dwellTraj)
                                            shopDuration1, extraTime1, totalStops1, shop_time_list1, first_dest_flag = getDwellTimeAndNumStopsCat(dwellTraj,categoryRegion,-1)
                                            # shopDuration1,totalStops1,shop_time_list1,first_dest_flag = getDwellTimeAndNumStopsCat(dwellTraj,categoryRegion,-1)
                                            if totalStops1 > 0:
                                                shop_list = [TimeUtils.secsToDate(timeVal) for timeVal in shop_time_list1]
                                                self.mongoCategoryDict[categoryDictCounter] ['shop_time_list'] = shop_list
                                                self.mongoCategoryDict[categoryDictCounter]['shop_time'] = shopDuration1
                                                self.mongoCategoryDict[categoryDictCounter]['num_of_shops'] = totalStops1
                                                # if first_dest_flag:
                                                #     self.mongoCategoryDict[categoryDictCounter]['first_dest_flag'] = True
                                                #if the track is shopper for the category we will include it in a seperate list used to calculate the sequence_num
                                                regionForSequence[categoryDictCounter]= categoryRegion
                                                self.mongoCategoryDict[categoryDictCounter]['nav_time'] = totalTimeInPoly1-extraTime1-shopDuration1
                                            catForTrafficSeq[categoryDictCounter] = categoryRegion
                                            categoryDictCounter+= 1
                                            break
                        break
            #we will check the  shop sequence number here
            if len(regionForSequence) == 1:
                self.mongoCategoryDict[regionForSequence.keys()[0]]['shop_sequence_num'].append(1)
            if len(regionForSequence) > 1:
                seq_num = 1
                for index in range(len(dwellTraj.nodes)):
                    numAddedFlag = False
                    try:
                        time,x,y,dur, sideShopped = dwellTraj.nodes[index]
                    except:
                        time,x,y,dur = dwellTraj.nodes[index]
                        sideShopped = None
                    for key in regionForSequence:
                        polygon = regionForSequence[key]
                        if polygon.isPointInside((x,y)) and (sideShopped == polygon.getActiveSide() or polygon.getActiveSide() is None):
                            if seq_num-1 not in self.mongoCategoryDict[key]['shop_sequence_num']:
                                self.mongoCategoryDict[key]['shop_sequence_num'].append(seq_num)
                                numAddedFlag = True
                    if numAddedFlag:
                        seq_num += 1
            if len(aisleForTrafficSeq) == 1:
                self.mongoAisleDict[aisleForTrafficSeq.keys()[0]]['traf_sequence_num'].append(1)
            if len(aisleForTrafficSeq) > 1:
                seq_num = 1
                nodeIndex = 0
                while (nodeIndex<len(traj.nodes)):
                # for nodeIndex in range(len(traj.nodes)):
                    numAddedFlag = False
                    t, x, y = traj.nodes[nodeIndex]
                    for key in aisleForTrafficSeq:
                        polygon = aisleForTrafficSeq[key]
                        if polygon.isPointInside((x,y)):
                            if seq_num-1 not in self.mongoAisleDict[key]['traf_sequence_num']:
                                self.mongoAisleDict[key]['traf_sequence_num'].append(seq_num)
                                numAddedFlag = True
                                # we will increment index until it reaches the first exit point
                                while polygon.isPointInside((x,y)):
                                    nodeIndex += 1
                                    if nodeIndex+1 <len(traj.nodes):
                                        t, x, y = traj.nodes[nodeIndex]
                                    else:
                                        break

                    if numAddedFlag:
                        seq_num += 1
                    else:
                        nodeIndex+= 1
            if len(catForTrafficSeq) == 1:
                self.mongoCategoryDict[catForTrafficSeq.keys()[0]]['traf_sequence_num'].append(1)
            if len(catForTrafficSeq) > 1:
                seq_num = 1
                nodeIndex = 0
                while (nodeIndex<len(traj.nodes)):
                # for nodeIndex in range(len(traj.nodes)):
                    numAddedFlag = False
                    t, x, y = traj.nodes[nodeIndex]
                    for key in catForTrafficSeq:
                        polygon = catForTrafficSeq[key]
                        if polygon.isPointInside((x,y)):
                            if seq_num-1 not in self.mongoCategoryDict[key]['traf_sequence_num']:
                                self.mongoCategoryDict[key]['traf_sequence_num'].append(seq_num)
                                numAddedFlag = True
                    if numAddedFlag:
                        seq_num += 1
                    nodeIndex+= 1
    def calculateShopInfo_test(self, trajList, demographicsData,project_type):
        minDwellTime = 3
        maxDisplacement = 30
        trajIndex = 0
        trajCount = 0
        aisleDictCounter = 0
        categoryDictCounter = 0
        counter = 0
        self.outTrajList = []
        for traj in trajList:
            counter += 1
            regionForSequence = {}
            aisleForTrafficSeq = {}
            catForTrafficSeq = {}
            trajCount += 1
            # print 'traj num %r' %trajCount
            dwellTraj = getDwellTrajNew(traj,minDwellTime,maxDisplacement)
            trajDate = datetime.strftime(TimeUtils.secsToDate(traj.startTime()),'%Y%m%d')
            trajHour = datetime.strftime(TimeUtils.secsToDate(traj.startTime()),'%H')
            trajDateFull = datetime.strftime(TimeUtils.secsToDate(traj.startTime()),'%Y%m%dT%H%M%S')
            self.mongoAisleDict[counter] = {}
            self.mongoAisleDict[counter][trajDateFull] = []
            for nodeIndex in range(len(traj.nodes)):
                t, x, y = traj.nodes[nodeIndex]
                for region in self.mainRegions:
                    #first check if there is any point of the track inside the polygon
                    if region.isPointInside((x,y)):
                        for categoryRegion in region.getSubRegions():
                                catrgnName = categoryRegion.regionId.split(':')[-1]
                                if  catrgnName != 'veterinarian (mars)':
                                    continue
                                # first check if there is any point of the track inside the polygon
                                if categoryRegion.isPointInside((x, y)):
                                    self.outTrajList.append(traj)
                                        
                                    # data = catrgnName
                                    # if len (self.mongoAisleDict[counter][trajDateFull]) == 0:
                                        # self.mongoAisleDict[counter][trajDateFull].append(data)
                                    # elif self.mongoAisleDict[counter][trajDateFull][-1] != data:
                                        # self.mongoAisleDict[counter][trajDateFull].append(data)


        print 'done'
    def saveMainShopInfo(self, baseOutputDirectory, outputFilename, append = False):
        filenameList = []
        for region in self.mainRegions:
            outputDirectory = os.path.join(baseOutputDirectory, region.getRegionId())

            try:
                os.makedirs(outputDirectory)
            except:
                pass

            filename = os.path.join(outputDirectory, outputFilename)
            filenameList.append(filename)
            if len(self.shopInfoList[region]) == 0:
                tmpShopInfo = ShopInfo()
                VmsUtils.saveCsvInfo([tmpShopInfo], filename, append)
            else:    
                VmsUtils.saveCsvInfo(self.shopInfoList[region], filename, append)
        return filenameList

    def __str__(self):
        outputStr = ''
        for mainRegion in self.mainRegions:
            outputStr += '%s\n' % (mainRegion)

        return outputStr


    def calculatePasserInfo(self, trajList, demographicsData):
        for region in self.mainRegions:
            passerInfoHeader = "Start Time,Duration,ID,StartPoly,EndPoly"
            regionsToCheck = []#[region]
            self.passerInfoList[region] = []
            passerInfoHeader += ',"%s"' % (region.getRegionId())
##            regionsToCheck.append(region)
            for subRegion in region.getSubRegions():
                passerInfoHeader += ',"%s"' % (subRegion.getRegionId())
                regionsToCheck.append(subRegion)
            passerInfoHeader += ',Gender,Age,Ethnicity'
        
            trajIndex = 0
            for traj in trajList:
                enteredMain = False
                passerInfo = PasserInfo()
                passerInfo.setHeader(passerInfoHeader)
                passerInfo.duration = traj.duration()
                passerInfo.startTime = traj.startTime()
                passerInfo.trajId = int(traj.id)
    ##                passerInfo.trajId = traj.id
                passerInfo.entryPolyId = getEntryPolygonId(traj, region.getDOEPolys(), region)
                passerInfo.exitPolyId = getExitPolygonId(traj, region.getDOEPolys(), region)
                passerInfo.miscData = demographicsData[trajIndex]
                
                checkableRegions = copy.copy(regionsToCheck)
                for time, x, y in traj.nodes:
                    removeIndexes = []
                    if region.isPointInside((x,y)):
                        enteredMain = True
                        for regionIndex in range(len(checkableRegions)):
                            checkRegion = checkableRegions[regionIndex]
                            if checkRegion.isPointInside((x,y)):
                                removeIndexes.append(regionIndex)
                        alreadyRemoved = 0
                        for index in removeIndexes:
                            del checkableRegions[index - alreadyRemoved]
                            alreadyRemoved += 1
                
                if enteredMain:
                    entryFlagList = [1]
                else:
                    entryFlagList = [0]
                for index in range(len(regionsToCheck)):
                    entryFlagList.append(1)
                for checkRegion in checkableRegions:
                    regionIndex = regionsToCheck.index(checkRegion)
                    entryFlagList[regionIndex+1] = 0
                passerInfo.regionPassFlagList = entryFlagList
                self.passerInfoList[region].append(passerInfo)
                trajIndex += 1


def addSidesShopped(traj, dwellTraj, mainShoppingRegion):
    timeBuffer = 10
    shoppingPolys = mainShoppingRegion.getRegionPolys()
    newDwellTraj = DwellTraj()

    for dwellNode in dwellTraj.nodes:
        sideShopped = None
        startTime = dwellNode[0]
        endTime = dwellNode[0] + dwellNode[3]

        croppedTraj = TrajectoryCroppers.cropWithinTimeRange(traj, startTime - timeBuffer, endTime + timeBuffer)
        
        for poly in reversed(shoppingPolys):
            if poly.isPointInside((dwellNode[1], dwellNode[2])):
                sideShopped = analyzeSideShopped(croppedTraj, poly)
                break
        newNode = (dwellNode[0], dwellNode[1], dwellNode[2], dwellNode[3], sideShopped)
        newDwellTraj.addDwell(newNode)
    return newDwellTraj

def analyzeSideShopped(shoppingTraj, shoppingPoly):
    sideShopped = None
    minX = 100000000#Integer.MaxValue
    maxX = 0
    minY = 100000000#Integer.MaxValue
    maxY = 0

    for vert in shoppingPoly.verts:
        if vert[0] < minX:
            minX = vert[0]
        if vert[0] > maxX:
            maxX = vert[0]

        if vert[1] < minY:
            minY = vert[1]
        if vert[1] > maxY:
            maxY = vert[1]

    # This is done so we don't assume anything about the orientation of the main
    # shopping region. Assuming the shopping poly is an eccentric rectangle at
    # a 90 degree orientation this will work

    if maxX - minX > maxY - minY:
        shortIndex = 2
        shortMin = minY
        shortMax = maxY
        shortMiddle = (maxY + minY)/2
    else:
        shortIndex = 1
        shortMin = minX
        shortMax = maxX
        shortMiddle = (maxX + minX)/2

    browsing_level_sum = 0
    browsing_count = 0
    shopping_level_sum = 0
    shopping_count = 0
    shortDisplacementList = []
    shortDisplacementListShopping = []
    shortDisplacementListBrowsing = []
    numLeftShopping = 0
    numRightShopping = 0
        
    time0, xPos0, yPos0 = shoppingTraj.nodes[0]

    for x in range(len(shoppingTraj.nodes)):
        time = shoppingTraj.nodes[x][0]
        shortLocation = shoppingTraj.nodes[x][shortIndex]

        duration = shoppingTraj.duration()
        relative_time = time - shoppingTraj.startTime()
        shortDisplacementList.append(shortLocation)

        # Browsing is the first 6 secs and shopping is after that
        if (6 < relative_time):# and (relative_time < duration - 6):
            shopping_count = shopping_count + 1
            shopping_level_sum = shopping_level_sum + shortLocation
            shortDisplacementListShopping.append(shortLocation)
            if (shortLocation <= shortMiddle):
                numLeftShopping = numLeftShopping + 1
            else:
                numRightShopping = numRightShopping + 1
        else:
            browsing_count = browsing_count + 1
            browsing_level_sum = browsing_level_sum + shortLocation
            shortDisplacementListBrowsing.append(shortLocation)

    # Begin histogram computation
    hist_limits = [shortMin, shortMax]
    numbin = 40
    hist_base = range(int(shortMin), int(shortMax), int(shortMax/numbin))
    (bins, lowerreallimit, binsize, extrapoints)= stats.lhistogram(shortDisplacementList, numbin, hist_limits, 0)
    first_nonzero_index = 0
    for i in range(0, numbin):
        if bins[i] > 0:
            first_nonzero_index = i
            break
    totalsum = sum(bins)
    binsum = 0
    quartile = 0
    for i in range(0, numbin):
        binsum = binsum + bins[i]
        if(binsum) > 0.25*totalsum:
            quartile = i
            break
    Lquartile = quartile - first_nonzero_index + 1

    bins.reverse()

    last_nonzero_index = 0
    for i in range(0, numbin):
        if bins[i] > 0:
            last_nonzero_index = i
            break
    totalsum = sum(bins)
    binsum = 0
    quartile = 0
    for i in range(0, numbin):
        binsum = binsum + bins[i]
        if(binsum) > 0.25*totalsum:
            quartile = i
            break
    Rquartile = quartile - last_nonzero_index + 1

    histogram_width = ((numbin - last_nonzero_index) - first_nonzero_index)/numbin
    # End of histogram computation

    if shopping_count == 0:
        shopping_count = 1
    leftShoppingProportion = float(numLeftShopping) / shopping_count
    rightShoppingProportion = float(numRightShopping) / shopping_count

    if (shopping_count > 0) and (browsing_count > 0):
        shopping_level = shopping_level_sum / shopping_count
        browsing_level = browsing_level_sum / browsing_count
        # First, if a majority (0.9) of track points belong to one of the LR sides, then choose that side.
        if (leftShoppingProportion > 0.9):
            label = 'left'
        elif (rightShoppingProportion > 0.9):
            label = 'right'
        # Otherwise, compare the shopping level against browsing level (at the entry)
        else:
            if shopping_level < browsing_level:
                label = 'left'
            else:
                label = 'right'
        # Whenever the histogram is tight (less than 30% of the total possible width),
        # make a decision based on the shopping level
        if (histogram_width < .3):
            if (shopping_level < shortMiddle):
                label = 'left'
            else:
                label = 'right'

        if label == 'left' and shortMax == maxY:
            label = 'top'
        elif label == 'right' and shortMax == maxY:
            label = 'bottom'

    return label


def main(tempFileDir, fileDate, bucket_name, mongoCollection, storeId,project_type,floorPoints=None):


    filenameList = []
    try:
        os.mkdir(tempFileDir)
    except:
        pass
    fileDirForStore = os.sep.join([tempFileDir,storeId])
    try:
        os.mkdir(fileDirForStore)
    except:
        pass
    #first download the file from S3
    print fileDirForStore
    trajFilenameString = os.sep.join([fileDirForStore,fileDate+'.csv'])
    # mongoCollection = mongoCollection+'
    # utilsAmazonS3.downloadFilterFromS3(bucket_name,trajFilenameString)
    trajFilenames = glob.glob(trajFilenameString)
    print trajFilenames
    if not trajFilenames:
        if '-' in storeId:
            bucket_name = 'vm-posse-' +storeId.split('-')[-1]
            print 'trying in bucket %r' %bucket_name
            # utilsAmazonS3.downloadFilterFromS3(bucket_name, trajFilenameString)
            trajFilenames = glob.glob(trajFilenameString)
            if not trajFilenames:
                raise BaseException("Trajectory file not found.")
        else:
            raise BaseException("Trajectory file not found.")
    print trajFilenames
    #connect to mongo
    muObj = MongoUtils()
    muObj.connectToMongo('vision')
    # for runNumber in filenameSets.keys():
    muObj.deleteDocuments(mongoCollection,fileDate,project_type)
    #getting regions from db
    if project_type != 'csi':
        gsuObj = GetshopUtils.getShop()
        getshopDict = gsuObj.fetchGetShopInfo(storeId,project_type)
        doeDict = gsuObj.fetchDOEInfo(storeId,project_type)
        reqGetShop = {}
        getShopFound = False
        for dateRange in getshopDict:
            start,end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqGetShop = getshopDict[dateRange]
                getShopFound = True
                break
        reqDOE = {}
        doeFound = False
        # req1 = {}
        # req1['Aisle14'] =reqGetShop['Aisle14']
        for dateRange in doeDict:
            start,end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqDOE = doeDict[dateRange]
                doeFound = True
                break
        if not doeFound:
            print 'DOE not found.'
        if not getShopFound:
            raise Exception('Getshop details not found for date %r'%fileDate)
    if 'csi' in project_type:
        if floorPoints == None:
            raise Exception('Floorplan points not found')
        gsuObj = GetshopUtils.getShop()
        getshopDict = gsuObj.fetchGetShopInfoCSI(storeId, project_type,floorPoints)
        reqGetShop = {}
        getShopFound = False
        for dateRange in getshopDict:
            start, end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqGetShop = getshopDict[dateRange]
                getShopFound = True
                break
        reqDOE = {}
        doeFound = False


    nodeList = getShopDictToNodeList(reqGetShop)
    controller = ShopInfoController(reqDOE, storeId, reqGetShop)
    # controller = ShopInfoController(regionFilenames, doeFilenames, storeId,reqGetShop)
    for trajFilename in trajFilenames:
        trajList, demographicsData = Trajectory.loadXmlCsvGetShopInfo(trajFilename,1000)
        while trajList is not None:
            controller.calculateShopInfo(trajList, demographicsData, project_type)
            muObj.insertDocument(mongoCollection,controller.mongoAisleDict)
            muObj.insertDocument(mongoCollection,controller.mongoCategoryDict)
            controller.clear()
            for element in demographicsData:
                del element
            del demographicsData
            trajList, demographicsData = Trajectory.nextLines(1000)
    # for trajFile in trajFilenames:
    #     os.remove(trajFile)

def main2(tempFileDir, fileDate, bucket_name, mongoCollection, storeId,project_type,floorPoints=None):

    filenameList = []
    try:
        os.mkdir(tempFileDir)
    except:
        pass
    fileDirForStore = os.sep.join([tempFileDir,storeId])
    try:
        os.mkdir(fileDirForStore)
    except:
        pass
    #first download the file from S3
    print fileDirForStore
    trajFilenameString = os.sep.join([fileDirForStore,fileDate+'.csv'])
    # mongoCollection = mongoCollection+'
    # utilsAmazonS3.downloadFilterFromS3(bucket_name,trajFilenameString)
    trajFilenames = glob.glob(trajFilenameString)
    print trajFilenames
    if not trajFilenames:
        if '-' in storeId:
            bucket_name = 'vm-posse-' +storeId.split('-')[-1]
            print 'trying in bucket %r' %bucket_name
            utilsAmazonS3.downloadFilterFromS3(bucket_name, trajFilenameString)
            trajFilenames = glob.glob(trajFilenameString)
            if not trajFilenames:
                raise BaseException("Trajectory file not found.")
        else:
            raise BaseException("Trajectory file not found.")
    print trajFilenames
    #connect to mongo
    muObj = MongoUtils()
    muObj.connectToMongo('vision')
    # for runNumber in filenameSets.keys():
    muObj.deleteDocuments(mongoCollection,fileDate,project_type)
    #getting regions from db
    if project_type != 'csi':
        gsuObj = GetshopUtils.getShop()
        getshopDict = gsuObj.fetchGetShopInfo(storeId,project_type)
        doeDict = gsuObj.fetchDOEInfo(storeId,project_type)
        reqGetShop = {}
        getShopFound = False
        for dateRange in getshopDict:
            start,end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqGetShop = getshopDict[dateRange]
                getShopFound = True
                break
        reqDOE = {}
        doeFound = False
        # req1 = {}
        # req1['Aisle14'] =reqGetShop['Aisle14']
        for dateRange in doeDict:
            start,end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqDOE = doeDict[dateRange]
                doeFound = True
                break
        if not doeFound:
            print 'DOE not found.'
        if not getShopFound:
            raise Exception('Getshop details not found for date %r'%fileDate)
    if 'csi' in project_type:
        if floorPoints == None:
            raise Exception('Floorplan points not found')
        gsuObj = GetshopUtils.getShop()
        getshopDict = gsuObj.fetchGetShopInfoCSI(storeId, project_type,floorPoints)
        reqGetShop = {}
        getShopFound = False
        for dateRange in getshopDict:
            start, end = dateRange.split('_')
            if fileDate >= start and fileDate <= end:
                reqGetShop = getshopDict[dateRange]
                getShopFound = True
                break
        reqDOE = {}
        doeFound = False


    # print reqGetShop
    controller = ShopInfoController(reqDOE, storeId, reqGetShop)
    # controller = ShopInfoController(regionFilenames, doeFilenames, storeId,reqGetShop)
    for trajFilename in trajFilenames:
        trajList, demographicsData = Trajectory.loadXmlCsvGetShopInfo(trajFilename,10000)
        while trajList is not None:
            controller.calculateShopInfo_test(trajList, demographicsData, project_type)
            # muObj.insertDocument(mongoCollection,controller.mongoAisleDict)
            # muObj.insertDocument(mongoCollection,controller.mongoCategoryDict)
            # controller.clear()
            for element in demographicsData:
                del element
            del demographicsData
            trajList, demographicsData = Trajectory.nextLines(10000)
        outtrajFilename = trajFilename.replace('.csv','_output.csv')

        Trajectory.saveXmlCsv(outtrajFilename,controller.outTrajList)
        # longest = 0
        # finalDict = controller.mongoAisleDict
        # for index in finalDict:
            # for date in finalDict[index]:
                # if len(finalDict[index][date]) > longest:
                    # longest = len(finalDict[index][date])
        # print longest
        # csvheader = ['Date']
        # for i in range(1, longest + 1):
            # csvheader.append('Region' + str(i))
        # with open(outtrajFilename, 'wb') as fp:
            # print 'writing into file %r' % outtrajFilename
            # newFile = csv.writer(fp)
            # newFile.writerow(csvheader)
            # for index in finalDict:
                # for date in finalDict[index]:
                    # if len(finalDict[index][date]) == 0:
                        # continue
                    # new_row = [date]
                    # for region in finalDict[index][date]:
                        # new_row.append(region)

                    # newFile.writerow(new_row)
            # fp.close()
    # for trajFile in trajFilenames:
    #     os.remove(trajFile)

if __name__ == "__main__":

    if not len(sys.argv) >= 5 and not len(sys.argv) <= 7:
        sys.exit("Usage: %s <traj files> <shop Polygon Files> <DOE Files> <Base Output Directory> [Maximum Number of Stops] [doPasserInfo]" % (sys.argv[0].rsplit('\\',1)[-1]))

    trajFilenameString = sys.argv[1]
    regionFilenameString = sys.argv[2]
    doeFilenameString = sys.argv[3]
    baseOutputDirectory = sys.argv[4]

    doPasserInfo = True
    if len(sys.argv) == 7 and sys.argv[6] == 'False':
        doPasserInfo = False

    main(trajFilenameString, regionFilenameString, doeFilenameString, baseOutputDirectory, doPasserInfo)
