# import pdb
import DBConnector
import datetime
import os
import sys
import elementtree.ElementTree
import csv
import collections
# import cProfile
import exceptions
import traceback
import pdb
class DBInfo:
    schema = 'vms'
    user = 'vmsuser'
    pwd = 'irrelev@nt'
    def __init__(self,host='',user=user,pwd=pwd,schema=schema):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.schema = schema

def _expandCamRange(camRangeStr):
    camList = []
    camRangeStr = camRangeStr.strip()
    argv = camRangeStr.split(' ')
    while len(argv) > 0:
        if argv[0] == 'R':
            start = int(argv[1])
            stop = int(argv[2])
            argv = argv[3:]
            camRange = range(start, stop + 1)
            camList.extend(["cam%03d"%camNum for camNum in camRange])
        elif argv[0] == 'A':
            cam = int(argv[1])
            argv = argv[2:]
            camList.append("cam%03d"%cam)
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,camRangeStr))
    return camList
    
def _expandDateRange(dateRangeStr):
    dateList = []
    timeDelta = datetime.timedelta(1,0,0)
    dateRangeStr = dateRangeStr.strip()
    argv = dateRangeStr.split(' ')
    while len(argv) > 0:
        if argv[0] == 'A':
            date = argv[1]
            argv = argv[2:]
            date = datetime.datetime(int(date[0:4]),int(date[4:6]), int(date[6:]), 0, 0, 0, 0)
            dateList.append(date)
        elif argv[0] == 'R':
            startDate = argv[1]
            stopDate = argv[2]
            argv = argv[3:]
            startDate = datetime.datetime(int(startDate[0:4]),int(startDate[4:6]), int(startDate[6:]), 0, 0, 0, 0)
            stopDate = datetime.datetime(int(stopDate[0:4]),int(stopDate[4:6]), int(stopDate[6:]), 0, 0, 0, 0)
            currDate = startDate
            while currDate <= stopDate:
                dateList.append(currDate)
                currDate = currDate + timeDelta
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,dateRangeStr))
    return dateList
def _createMissingEventTables(table_name,db):
    schemaName,tableName = table_name.split('.')

    baseQuery = '''
        CREATE TABLE IF NOT EXISTS <schemaName>.<tableName> (
            `start` datetime NOT NULL,
            `milliseconds` smallint(6) NOT NULL,
            `duration` float NOT NULL,
            `a4` longtext,
            KEY `<tableName>` (`start`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8;'''
    sqlQuery = baseQuery.replace("<tableName>",tableName).replace("<schemaName>",schemaName)
    print "Creating table since its missing"
    print sqlQuery
    db.execute(sqlQuery)

def _updatevmProjLUT(vmProjLUT, projID, db):	
    sqlQuery = 'SELECT * FROM vms.project_info WHERE project_id = %d;'%(int(projID))
    returnVal = db.execute(sqlQuery)
    if len(returnVal) == 0:
        raise Exception("\n\nProject id: %s not found in vms.project_info\n\n"%projID)
    vmProjLUT[projID] = returnVal[0][2]
    return vmProjLUT



def _updateCamLUTNew(camLUT, camIdEvntChnlDict, ID, db):
    sqlQuery = "SELECT * FROM vms.project WHERE id = %d;"%(int(ID))
    retVal = db.execute(sqlQuery)
    maxIdx = 0
    maxVer = retVal[maxIdx][1]
    for idx in range(len(retVal)):
        currVer = retVal[idx][1]
        if currVer > maxVer:
            maxVer = currVer
            maxIdx = idx
    projxml = retVal[maxIdx][2]
    print "version = %d\n"%retVal[maxIdx][1]
    root = elementtree.ElementTree.XML(projxml)
    # find all camera objects
    views = root.findall("camera_views/camera_view")

    # cases NOT handled: camera having multiple trajectory generator event channels
    # pdb.set_trace()
    for i in range(len(views)):
        camid = views[i].get("id")
        camname = views[i].find("name").text
        # filter out those that do not have a 'trajectory_generator2' event channel defined
        # if a camera doesn't have a 'trajectory_generator2' event channel then it is completely ignored
        evntChannels = views[i].findall('event_channels/event_channel')
        
        for evntChannel in evntChannels:
            template = evntChannel.find('template')
            # print camname
            # print template.text
            if template.text == 'OmniTrajectory':
                # names[id] = views[i].find("name").text
                camIdEvntChnlDict[camid] = evntChannel.find('id').text
                #this ensures that if there are more than one trajectory generator channels, only the first is used
                camLUT[camid] = camname
                camLUT[camname] = camid
                break 
    
    return camLUT, camIdEvntChnlDict
def _updateCamLUT(camLUT, camIdEvntChnlDict, ID, db):
    sqlQuery = "SELECT * FROM vms.project WHERE id = %d;"%(int(ID))
    retVal = db.execute(sqlQuery)
    maxIdx = 0
    maxVer = retVal[maxIdx][1]
    for idx in range(len(retVal)):
        currVer = retVal[idx][1]
        if currVer > maxVer:
            maxVer = currVer
            maxIdx = idx
    projxml = retVal[maxIdx][2]
    print "version = %d\n"%retVal[maxIdx][1]
    root = elementtree.ElementTree.XML(projxml)
    # find all camera objects
    views = root.findall("camera_views/camera_view")

    # cases NOT handled: camera having multiple trajectory generator event channels
    for i in range(len(views)):
        camid = views[i].get("id")
        camname = views[i].find("name").text
        # filter out those that do not have a 'trajectory_generator2' event channel defined
        # if a camera doesn't have a 'trajectory_generator2' event channel then it is completely ignored
        evntChannels = views[i].findall('event_channels/event_channel')
        for evntChannel in evntChannels:
            template = evntChannel.find('template')
            if template.text == 'trajectory_generator2':
                # names[id] = views[i].find("name").text
                camIdEvntChnlDict[camid] = evntChannel.find('id').text
                #this ensures that if there are more than one trajectory generator channels, only the first is used
                camLUT[camid] = camname
                camLUT[camname] = camid
                break 

    return camLUT, camIdEvntChnlDict


def _createOutputDirs(rootDir):
    if not os.path.exists(rootDir):
        try:
            os.makedirs(rootDir)
        except:
            print "Could NOT create out directory: %s\n"%(rootDir)
            pass

def _justify(st,num):
    while len(st) < num:
        st = '0'+ st
    return st

def _getStartTime(row):
    year = str(row[0].year)
    month = _justify(str(row[0].month),2)
    day = _justify(str(row[0].day),2)
    hour = _justify(str(row[0].hour),2)
    minutes = _justify(str(row[0].minute),2)
    second = _justify(str(row[0].second),2)
    microSec = _justify(str(row[1]),3)
    return year+'-'+month+'-'+day+'T'+hour+':'+minutes+':'+second+'.'+microSec
            
def _parseProj(projName, projDict):
    try:
        fp = open(projName,'r')
    except IOError:
        print "\n\nProject file: %s probably does not exist. Please check the path\n\n"%projName
        raise
    tree = elementtree.ElementTree.parse(fp)
    root = tree.getroot()
    fp.close()
    print "\n\nContents of %s"%projName
    for child in root:
        print "\t%s: %s"%(child.tag, child.text)
        projDict[child.tag] = child.text
    return projDict

def extractTrajectories(camList, camLUT, camIdEvntChnlDict, dateList, dbId, dbInfoObj, rootDir):
    db = DBConnector.DBConnector(host=dbInfoObj.host, user=dbInfoObj.user, password=dbInfoObj.pwd, database=dbInfoObj.schema) 

    outputFields = ['Start Time','Duration','Trajectory']
    
    tableNameBase = "vms.event_<projId>_<camId>_<eventId>"

    dataSqlBase = """
    SELECT
        * 
    FROM 
        <tableName> 
    WHERE 
        `start`
    BETWEEN '<date> 00:00:00' AND '<date> 23:59:59'
    ORDER BY `start`;"""
    
    tableNameTemp = tableNameBase.replace('<projId>',dbId)

    for camName in camList:
        camId = camLUT[camName]
        if camName.find("cam") != 0:
            print "camName = %s\tcamId = %s <======= SKIPPING"%(camName,camId)
            continue
        tableName = tableNameTemp.replace('<camId>',camId)
        tableName = tableName.replace('<eventId>',camIdEvntChnlDict[camId])

        dataSql = dataSqlBase.replace('<tableName>',tableName)
        print "camName = %s\ttablename = %s"%(camName,tableName)
        for currDate in dateList:

            headers = {}
            dateStr = currDate.strftime("%Y-%m-%d")
            fileName = camName + '_' + currDate.strftime("%Y%m%d") + ".csv"
            outFilename = os.path.join(rootDir, fileName)
            sql = dataSql.replace('<date>',dateStr)
            try:
                retVal = db.execute(sql)
            except:
                # print "creating missing tables"
                _createMissingEventTables(tableName,db)
                continue
                #print "\n\n%s\n\n"%sql
                #raise
            print "\tDate: %s\tnumTraj = %d"%(dateStr,len(retVal))
            fOut = csv.DictWriter(open(outFilename,'w'),fieldnames=outputFields,dialect='excel',lineterminator='\n')
            # write headers
            for field in outputFields:
                headers[field] = field
            fOut.writerow(headers)
            for row in retVal:
                startTime = _getStartTime(row)
                duration = row[2]
                traj = row[3]
                if traj == None:
                    continue
                # if '2016-11-13T17' in startTime or '2016-11-13T18' in startTime:
                headers['Start Time'] = startTime
                headers['Duration'] = duration
                headers['Trajectory'] = traj
                fOut.writerow(headers)
    
    
def process(projDict, camRangeStr, date, outputDir,defaultCamType):
    # (key:value) = (dbID: projName)
    vmProjLUT = {}
    # (key:value) = (camID: camName))
    camLUT = {}
    # (key: value) = (camID: event channel id)
    camIdEvntChnlDict = {}
    
    # read relevant values from the project dictionary
    dbId = projDict['database_id']
    host = projDict['database_server']
    dbInfoObj = DBInfo(host=host)
    # instantiate connection to host
    db = DBConnector.DBConnector(host=dbInfoObj.host, user=dbInfoObj.user, password=dbInfoObj.pwd, database=dbInfoObj.schema)    
    
    _updatevmProjLUT(vmProjLUT, dbId, db)
    # pdb.set_trace()
    if 'omni_4x3' == defaultCamType:
        _updateCamLUTNew(camLUT, camIdEvntChnlDict, dbId, db)
    else:
    # update the camera look up for each store
        _updateCamLUT(camLUT, camIdEvntChnlDict, dbId, db)
    
    # ascertain the cam range for which trajectory extraction is to take place
    # if the range is supplied, expand it out to cam names and verify if all names are present in project file -ELSE- assume that the entire cam range is to be extracted
    if camRangeStr == '':
        camIdList = camIdEvntChnlDict.keys()
        camList = [camLUT[camId] for camId in camIdList]
    else:
        # camList = _expandCamRange(camRangeStr)
        camList = ["cam%03d"%cam_num for cam_num in camRangeStr]
    # pdb.set_trace()
    try:
        camSet = set(camList)
        assert camSet == camSet.intersection(camLUT.keys())
    except:
        missingCams = list(camSet - camSet.intersection(camLUT.keys()))
        print "camList = "
        print camList
        raise Exception("\n\nsupplied cam range '%s' not entirely found in project. Please adjust accordingly\n\nMissing cams: %s"%(camRangeStr,missingCams))
    print camList
    del db
    dateRangeStr = 'A %s'%date
    dateList = _expandDateRange(dateRangeStr)
    print dateList
    
    # locals = {'camList':camList,
              # 'camLUT':camLUT,
              # 'camIdEvntChnlDict':camIdEvntChnlDict,
              # 'dateList':dateList,
              # 'dbId':dbId,
              # 'dbInfoObj':dbInfoObj,
              # 'outputDir':outputDir}
    # cProfile.runctx('extractTrajectories(camList, camLUT, camIdEvntChnlDict, startDate, stopDate, dbId, dbInfoObj, outputDir)',globals(),locals,'extractTraj_mod.profile')
    extractTrajectories(camList, camLUT, camIdEvntChnlDict, dateList, dbId, dbInfoObj, outputDir)
    
    


def main(projName, camRangeStr, date, outputDir,defaultCamType):
    # parse the project file to retrieve database id and host name
    try:
        projDict = {}
        projDict = _parseProj(projName, projDict)
        dbId = projDict['database_id']
        host = projDict['database_server']

        # create output dirs for each of the specified projects
        _createOutputDirs(outputDir)

        # process(projDict, camRangeStr, dateRangeStr, outputDir)
        process(projDict, camRangeStr, date, outputDir,defaultCamType)
    except:
        exinfo = sys.exc_info()
        stack_trace = ''.join([str(exinfo[1]) + '\n', ''.join(traceback.format_tb(exinfo[2]))])
        return stack_trace
if __name__ == "__main__":
    usageStr = "\n\nUsage: %s <vmprojFile> <rootOutputDir> -dateRange R date1 date2 A date3 <optional: -camRange R cam1 cam2 A cam3 R cam6 cam10...>\n\n"%(__file__)
    if len(sys.argv) < 6:
            sys.exit(usageStr)
    try:
        dateIdx = sys.argv.index('-dateRange')
    except ValueError:
        raise Exception("\n\n-dateRange is an expected input switch\n%s\n\n"%usageStr)

    try:
        camIdx = sys.argv.index('-camRange')
    except ValueError:
        camIdx = -1
    dateRangeStr = ''
    camRangeStr = ''
    if dateIdx < camIdx: # camidx can never be -1 when in this block
        assert camIdx != -1
        argv = sys.argv[dateIdx+1:camIdx]
        for arg in argv:
            dateRangeStr += ' %s'%(arg)            
        camArgv = sys.argv[camIdx+1:]
        for arg in camArgv:
            camRangeStr += ' %s'%(arg)
    else: # dateIdx > camIdx. camIdx could be -1
        if camIdx == -1:
            argv = sys.argv[dateIdx+1:]
            for arg in argv:
                dateRangeStr += ' %s'%(arg)
        else:
            camArgv = sys.argv[camIdx+1:dateIdx]
            for arg in camArgv:
                camRangeStr += ' %s'%(arg)
            dateArgv = sys.argv[dateIdx+1:]
            for arg in dateArgv:
                dateRangeStr += ' %s'%(arg)

    
    projName = sys.argv[1]
    outputDir = sys.argv[2]
    main(projName, camRangeStr, dateRangeStr, outputDir)