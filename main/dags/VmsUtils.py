#******************************************************
# This library is supposed to contain miscelleneous
# useful programs
# If this get too big then it will be broken down into 
# sublibraries
# TODO: need to provide good exception handling
#******************************************************

import copy
import math
import os
import time
import random
import pdb
import sys
import errno



def OpenRobustly(filename, mode="r", numAttempts=10, maxWaitTime=20):
    for i in range(numAttempts):
        try:
            f = open(filename, mode)
        except IOError as e:
            if e.errno == errno.ENOENT and mode == 'r':
                raise IOError("File not found %s" %filename)
            time.sleep(random.randint(1, maxWaitTime))
        else:
            return f
    raise IOError("Could not open '%s' after %d attempts" % (filename, numAttempts))

# Rectangle class
# TODO: implement class for drawing rectangle
class Rect:

    def __init__(self,x1,y1,x2,y2):
        self.xMin = min(float(x1), float(x2))
        self.xMax = max(float(x1), float(x2))

        self.yMin = min(float(y1), float(y2))
        self.yMax = max(float(y1), float(y2))

    def isPointInside(self, p1):
        x,y = p1

        if x >= self.xMin and x <= self.xMax and y >= self.yMin and y <= self.yMax:
            return True
        else:
            return False

    def __str__(self):
        return '(%.2f,%.2f,%.2f,%.2f)' % (self.xMin,self.yMin,self.xMax,self.yMax)

    def coords(self):
        return [self.xMin,self.yMin,self.xMax,self.yMax]

    def readFromString(rectString):
    #FORMAT: x1,y1,x2,y2 or (x1,y1),(x2,y2)
        pass

    def intersect(rect):
    #TODO: complete this function
        pass
    
    def draw(img):
        #TODO: complete this function
        pass

    def intersects(self, rect):
        """Note that this function only works for axis aligned rectangles"""
        if self.xMin >= rect.xMax or self.xMax <= rect.xMin or self.yMin >= rect.yMax or self.yMax <= rect.yMin:
            return False
        else:
            return True


def rectsFromString(rectString):
    l = rectString.split(',')
    l = map(float,l)

    assert len(l) % 4 == 0

    rectList = []

    for x in range(0,len(l),4):
        rectList.append(Rect(l[x],l[x+1],l[x+2],l[x+3]))

    return rectList

# Polygon class
# TODO: implement class for drawing polygon
class Polygon:
    def __init__(self,verts):
    #pointList is a list of tuples [(x1,y1),(x2,y2),...]
        self.verts = verts
        
        xList = [vert[0] for vert in verts]
        yList = [vert[1] for vert in verts]
        
        self.minX = min(xList)
        self.maxX = max(xList)
        self.minY = min(yList)
        self.maxY = max(yList)

    def getBoundingBox(self):
            minX = sys.maxint
            maxX  = -sys.maxint-1
            minY = sys.maxint
            maxY  = -sys.maxint-1
            for i in range(len(self.verts)-1):
                    if self.verts[i][0] < minX:
                            minX = self.verts[i][0]
                    if self.verts[i][0] > maxX:
                            maxX = self.verts[i][0]
                    if self.verts[i][1] < minY:
                            minY = self.verts[i][1]
                    if self.verts[i][1] > maxY:
                            maxY = self.verts[i][1]
            return Rect(minX,minY,maxX,maxY)

    
    def isPointInside(self,p):
        # speed hack: check if p is in the bounding box
        if p[0] < self.minX or p[0] > self.maxX or p[1] < self.minY or p[1] > self.maxY:
                return False
    
        isInside = False
        
        if len(self.verts) < 3:
                return False
                
        xold, yold = self.verts[-1]
        
        for vertex in self.verts:
                xnew, ynew = vertex
                if xnew > xold:
                        x1 = xold
                        x2 = xnew
                        y1 = yold
                        y2 = ynew
                else:
                        x1 = xnew
                        x2 = xold
                        y1 = ynew
                        y2 = yold
                        
                if (xnew < p[0]) == (p[0] <= xold) and \
                        (p[1] - y1)*(x2 - x1) < (y2 - y1) * (p[0] - x1):
                                isInside = not isInside
                
                xold, yold = xnew, ynew
                                
        return isInside

    def isPointInsideSlow(self,p):
        #TODO: more thouroughly test this function
            for x in range(len(self.verts) - 1):
                if p[0] == self.verts[x][0] and p[1] == self.verts[x][1]:
                    return True

            intersections = 0

            p1 = self.verts[0]

            vertCount = len(self.verts)

            for x in range(1,vertCount+1):
                p2 = self.verts[x % vertCount]
                if p[1] > min(p1[1],p2[1]) and p[1] <= max(p1[1],p2[1]):
                    if p[0] <= max(p1[0],p2[0]):
                        if p1[1] != p2[1]:
                            xinters = (p[1]-p1[1])*(p2[0]-p1[0])/(p2[1]-p1[1])+p1[0]
                            if p1[0] == p2[0] or p[0] <= xinters:
                                intersections += 1
                p1 = p2

            if intersections % 2 == 0:
                return False
            else:
                return True
        

        
    def draw(img):
    #TODO: complete this function
        pass

    def __str__(self):
        vertStr = ''
        for x in range(len(self.verts)):
            vertStr += ('%f,%f' % (self.verts[x][0],self.verts[x][1]))
            vertStr += ','

        return vertStr

    def coords(self):
        return self.verts

    # Returns a polygon that has been expanded by a specified distance.
    def expand(self, distance):
            if self.verts[0] == self.verts[-1]:
                    self.verts = self.verts[:-1]
            expandedPoly = copy.deepcopy(self)

            if distance != 0:
                    numVerts = len(expandedPoly.verts)
                            
                    for vertIndex in range(numVerts):
                            pdb.set_trace()
                            point1 = self.verts[vertIndex-1]
                            point2 = self.verts[(vertIndex)]
                            point3 = self.verts[(vertIndex+1) % numVerts]

                            vector1x = point2[0] - point1[0]
                            vector1y = point2[1] - point1[1]
                            vector1Distance = math.sqrt(vector1x**2 + vector1y**2)
                            vector1 = (vector1x/vector1Distance, vector1y/vector1Distance)

                            vector2x = point2[0] - point3[0]
                            vector2y = point2[1] - point3[1]
                            vector2Distance = math.sqrt(vector2x**2 + vector2y**2)
                            vector2 = (vector2x/vector2Distance, vector2y/vector2Distance)

                            directionx = (vector1[0] + vector2[0])/2
                            directiony = (vector1[1] + vector2[1])/2
                            directionDistance = math.sqrt(directionx**2 + directiony**2)

                            try:
                                directionVector = (directionx/directionDistance, directiony/directionDistance)
                            except:
                                pdb.set_trace()

                            testPoint = (point2[0] + directionVector[0], point2[1] + directionVector[1])

                            if distance > 0 and self.isPointInside(testPoint)\
                                 or distance < 0 and not self.isPointInside(testPoint):
                                directionVector = (directionVector[0]*-1, directionVector[1]*-1)

                            #print point2, directionVector
                            #if vector1[0] == 0 or vector1[1] == 0:
                            #  vector1 = vector2
                            perpVector1 = (-vector1[1], vector1[0])
                            numVecs = (perpVector1[0]*directionVector[1] - perpVector1[1]*directionVector[0])/(perpVector1[0]*vector1[1] - perpVector1[1]*vector1[0])#(dirVec1 - dirVec0) / (perpVector1[0] * vector1[1] - vector1[0])

                            if perpVector1[1] == 0:
                                numPerps = (directionVector[0] - numVecs * vector1[0]) / perpVector1[0]
                            else:
                                numPerps = (directionVector[1] - numVecs * vector1[1]) / perpVector1[1]

                            vector1 = (distance * vector1[0], distance * vector1[1])
                            perpVector1 = (-vector1[1], vector1[0])

                            numVecs = numVecs/abs(numPerps)
                            numPerps = numPerps/abs(numPerps)

                            deltaX = numVecs * vector1[0] + numPerps * perpVector1[0]
                            deltaY = numVecs * vector1[1] + numPerps * perpVector1[1]

                            if distance < 0:
                                deltaX *= -1
                                deltaY *= -1

                            moveVector = (deltaX, deltaY)
                            expandedPoly.verts[vertIndex] = (int(point2[0] + moveVector[0]), int(point2[1] + moveVector[1]))

            return expandedPoly
                    

def polyFromString(polygonStr):
# eg of input (1,2,34,23,1,2)
    polygonStr = polygonStr.strip(')').strip('(')

    splitList = polygonStr.split(',')
    splitList = map(float, splitList)

    #print splitList  
    assert len(splitList) % 2 == 0

    pointList = []

    for x in range(0, len(splitList), 2):
        pointList.append((splitList[x],splitList[x+1]))

    return Polygon(pointList)

def loadPolygonList(filename):
#This function loads multiple polygons from a file that has one per polygon
# lines starting with # are ignored. This should allow python style comments
    polygonList = []
    for line in open(filename):
        if line.startswith('#'):
            continue

        polygon = polyFromString(line.strip('\n'))
        polygonList.append(polygon)
    return polygonList

def saveCsvInfo(infoList,filename, append = False):
# assumes getCsvRow has been defined for all object that are present in infoList
# assumes getHeader is defined for the object
# header of the first element is written on the first line
    if append:
        fileObj = open(filename,'a')
        for info in infoList[1:]:
            fileObj.write(info.getCsvRow() + "\n")
    else:
        fileObj = open(filename,'w')
        if infoList != []:
            fileObj.write(infoList[0].getHeader() + "\n")
            fileObj.write(infoList[0].getCsvRow() + "\n")

        for info in infoList[1:]:
            fileObj.write(info.getCsvRow() + "\n")
    fileObj.close()

class Line:
#line class. 
#The line is of fixed length from startPoint to endPoint
    def __init__(self,startPoint,endPoint):
        self.startPoint = (float(startPoint[0]),float(startPoint[1]))
        self.endPoint = (float(endPoint[0]),float(endPoint[1]))

    def __str__(self):
        return "(%f,%f),(%f,%f)" % (self.startPoint[0],self.startPoint[1],self.endPoint[0],self.endPoint[1])

    def slope(self):
        if self.startPoint[0] == self.endPoint[0]:
            raise Exception("Slope is undefined")

        return (self.startPoint[1]- self.endPoint[1]) / (self.startPoint[0] - self.endPoint[0])

    def getIntersection(self, line):
        firstVertical = False
        secondVertical = False

        if self.startPoint[0] == self.endPoint[0]:
            firstVertical = True
        
        if line.startPoint[0] == line.endPoint[0]:
            secondVertical = True

        #check if both are vertical and return false:
        if firstVertical and secondVertical:
            return None
        else:
            if firstVertical:
                m2 = line.slope()
                c2 = line.endPoint[1] - m2*self.endPoint[0]
                x = self.endPoint[0]
                y = m2*x+c2
            elif secondVertical:
                m1 = self.slope()
                c1 = self.endPoint[1] - m1*self.endPoint[0]
                x = line.endPoint[0]
                y = m1*x+c1
            elif self.slope() == line.slope(): #Parallel line do not intersect
                return None
            else:
                m1 = self.slope()
                m2 = line.slope()
                c1 = self.endPoint[1] - m1*self.endPoint[0]
                c2 = line.endPoint[1] - m2*line.endPoint[0]
     
                x = - (c1 - c2)/(m1-m2)
                y = c1 + m1*x
            #print ((self.startPoint[0] - x)*(x - self.endPoint[0])),(line.startPoint[0] - x)*(x - line.endPoint[0])
            #print (self.startPoint[1] - y)*(y - self.endPoint[1]),(line.startPoint[1] - y)*(y - line.endPoint[1])
            if (self.startPoint[0] - x)*(x - self.endPoint[0]) >= 0 and \
    (line.startPoint[0] - x)*(x - line.endPoint[0]) >= 0 and \
    (self.startPoint[1] - y)*(y - self.endPoint[1]) >= 0 and \
                (line.startPoint[1] - y)*(y - line.endPoint[1]) >= 0:

                return (x,y)
            else:
                return None

    def ccw(self, A,B,C):
        return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])  

    def intersects(self, line):

        A = self.startPoint
        B = self.endPoint
        C = line.startPoint
        D = line.endPoint

        if A == C or A == D or B == C or B == D:
                return True
        
        return self.ccw(A,C,D) != self.ccw(B,C,D) and self.ccw(A,B,C) != self.ccw(A,B,D)


def loadLineFromString(line):
# Example of a line is '12,354,24,45' represents a line from point (12,342) to point (24,45)
    pList = line.split(',')
    assert len(pList) == 4
    return Line((float(pList[0]),float(pList[1])),(float(pList[2]),float(pList[3])))

#function to run an executable with a set of arglist
def runExec(execFile,argList):
    os.spawnl(os.P_WAIT, execFile, execFile, *argList)
