import os,sys
import tempfile


def readDateFile(configLoc,store):
    processDate = ''
    fileName = os.sep.join([configLoc,'dateRange_'+store+'.txt'])
    inputFile =  open(fileName,'r')
    for line in inputFile:
        processDate= line.split(',')[0]

    processDate = processDate.rstrip('\n')
    return processDate

def updateDateFile(configLoc,store,processDate):
    fileName = os.sep.join([configLoc,'dateRange_'+store+'.txt'])
    t = tempfile.NamedTemporaryFile(mode="r+")
    inputFile =  open(fileName,'r')
    for line in inputFile:
        outline = line.strip(processDate).lstrip(',')
        t.write(outline.rstrip() + "\n")
    inputFile.close()
    t.seek(0)
    outFile = open(fileName,'w')
    for line in t:
        outFile.write(line)
    outFile.close()

    return processDate
if __name__ == '__main__':
    processDate = readDateFile(r'D:\work\scripts\AWSTest\airflow_demo\main\dags\opts_files', '1751')

    updateDateFile(r'D:\work\scripts\AWSTest\airflow_demo\main\dags\opts_files', '1751',processDate)