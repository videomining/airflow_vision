import validateCams
import pdb

def getLiveCams(ignoreCams,camRange,dateRange,calibFile,host,user,password,proj_name):
    msg = ''
    # validate cam ranges between the live project, calibration file and the opts file
    error_dict, cam_nums = validateCams.validateCams(ignoreCams,camRange,dateRange,calibFile,host,user,password,proj_name)

    if len(error_dict) > 0:
        msg = "%s\n%s\n"%("Discrepancies in camera counts was detected!","Please fix the following. Also check 'cam_errors.txt' in the POSSE dir\n\n")
        for key, cam_list in error_dict.iteritems():
            msg += "%s: %s\n\n"%(key, cam_list)
        with open("cam_errors.txt","w") as fp:
            fp.writelines(msg)
    
    return msg, cam_nums
        
