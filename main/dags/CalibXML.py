import sys
import pdb
from numpy import *
from numpy import linalg

import xml.parsers.expat

from math import radians

import random

from Trajectory import *

import os.path
import glob

import random
import VmsUtils

def translate(x = 0.0,y = 0.0,z = 0.0):
  x = float(x)
  y = float(y)
  z = float(z)

  m = identity(4, float32)

  m[0,3] = x
  m[1,3] = y
  m[2,3] = z

  return m

def rotate(x = 0.0, y = 0.0, z = 0.0, theta = 0.0):
  u = float(x)
  v = float(y)
  w = float(z)
  theta = radians(float(theta))

  Lsquared = u*u + v*v + w*w
  L = sqrt(Lsquared)

  m = identity(4, float32)

  m[0,0] = (u**2 + (v**2 + w**2)*cos(theta)) / Lsquared
  m[0,1] = (u*v*(1 - cos(theta)) - w*L*sin(theta)) / Lsquared
  m[0,2] = (u*w*(1 - cos(theta)) + v*L*sin(theta)) / Lsquared  
  
  m[1,0] = (u*v*(1 - cos(theta)) + w*L*sin(theta)) / Lsquared
  m[1,1] = (v**2 + (u**2 + w**2)*cos(theta)) / Lsquared
  m[1,2] = (v*w*(1 - cos(theta)) - u*L*sin(theta)) / Lsquared

  m[2,0] = (u*w*(1 - cos(theta)) - v*L*sin(theta)) / Lsquared
  m[2,1] = (v*w*(1 - cos(theta)) + u*L*sin(theta)) / Lsquared
  m[2,2] = (w**2 + (u**2 + v**2)*cos(theta)) / Lsquared
  return m

def scale(x = 0.0, y = 0.0, z = 0.0):
  m = identity(4,float32)

  m[0,0] = float(x)
  m[1,1] = float(y)
  m[2,2] = float(z)  

  return m

def homography(s, t):

  #generate homography matrix
  x1 = float(s[0][0])
  x2 = float(s[1][0])
  x3 = float(s[2][0])
  x4 = float(s[3][0])

  y1 = float(s[0][1])
  y2 = float(s[1][1])
  y3 = float(s[2][1])
  y4 = float(s[3][1])

  u1 = float(t[0][0])
  u2 = float(t[1][0])
  u3 = float(t[2][0])
  u4 = float(t[3][0])

  v1 = float(t[0][1])
  v2 = float(t[1][1])
  v3 = float(t[2][1])
  v4 = float(t[3][1]) 
  
  m = zeros((8,8), float32)  

  m[0,0] = x1
  m[0,1] = y1
  m[0,2] = 1.0
  m[0,6] = -u1*x1
  m[0,7] = -u1*y1

  m[1,3] = x1
  m[1,4] = y1
  m[1,5] = 1.0
  m[1,6] = -v1*x1
  m[1,7] = -v1*y1

  m[2,0] = x2
  m[2,1] = y2
  m[2,2] = 1.0
  m[2,6] = -u2*x2
  m[2,7] = -u2*y2

  m[3,3] = x2
  m[3,4] = y2
  m[3,5] = 1.0
  m[3,6] = -v2*x2
  m[3,7] = -v2*y2

  m[4,0] = x3
  m[4,1] = y3
  m[4,2] = 1.0
  m[4,6] = -u3*x3
  m[4,7] = -u3*y3

  m[5,3] = x3
  m[5,4] = y3
  m[5,5] = 1.0
  m[5,6] = -v3*x3
  m[5,7] = -v3*y3

  m[6,0] = x4
  m[6,1] = y4
  m[6,2] = 1.0
  m[6,6] = -u4*x4
  m[6,7] = -u4*y4

  m[7,3] = x4
  m[7,4] = y4
  m[7,5] = 1.0
  m[7,6] = -v4*x4
  m[7,7] = -v4*y4  

  #return a useful tranform

  p = array([u1,v1,u2,v2,u3,v3,u4,v4], float32)

  # Mh = p, so h = (M^-1)P

  h = dot(linalg.inv(m),p)
  
  transform = zeros((3,3), float32)

  transform[0,0] = h[0]
  transform[0,1] = h[1]
  transform[0,2] = h[2]  
  transform[1,0] = h[3]
  transform[1,1] = h[4]
  transform[1,2] = h[5]
  transform[2,0] = h[6]
  transform[2,1] = h[7]
  transform[2,2] = 1.0

  return transform

class Camera:
  def __init__(self, name):
    self.id = None
    self.name = name

    self.location = None
    self.orientation = None
    self.focalLength = None
    self.ccdSize = None
    self.imageSize = None

  def __str__(self):
    s = '%s:\n' % (self.name)

    s+= '-Location: %s\n' % (self.location)
    s+= '-Orientation: %s\n' % (self.orientation)
    s+= '-Focal Length: %s\n' % (self.focalLength)
    s+= '-CCD Size: %s\n' % (str(self.ccdSize))
    s+= '-Image Size: %s\n' % (str(self.imageSize))    

    return s   

  def getName(self):
    return self.name 

  def getOrientation(self):
    return self.orientation

  def getLocation(self):
    return self.location


  def getImageSize(self):
    return self.imageSize


    xmlString = xmlString % (self.name,self.location[0],self.location[1],self.location[2],\
		self.orientation,self.focalLength,self.ccdSize[0],self.ccdSize[1],\
		self.imageSize[0],self.imageSize[1])

    return xmlString
    

class Floorplan:
  def __init__(self):
    self.local = None
    self.world = None
    self.image = None

  def __str__(self):
    s = 'Floorplan:\n'
    s+= '-Local: %s\n' % (str(self.local))
    s+= '-World: %s\n' % (str(self.world))
    return s

  def getImageName(self):
    return self.image

  def getLocalCoords(self):
    return self.local

  def getWorldCoords(self):
    return self.world

  def getXmlStr(self):
    xmlStr = '<floorplan image="%s">' % (self.image)
    localStr = ""
    for x,y in self.local:
      localStr += "(%f,%f)," % (x,y)
    localStr.rstrip(",")
    
    worldStr = ""
    for x,y in self.local:
      worldStr += "(%f,%f)," % (x,y)
    worldStr.rstrip(",")

    xmlStr += '\n<coord_mapping local="%s" world="%s" />\n</floorplan>' % (localStr,worldStr)


def convertToWorldViewNew(trajList, camXMLtoSend, correctDistortion,camType, distortionParameters):
  """Convert all trajectories in trajList into the world view,
  based on calibration information within camera"""
  # if correctDistortion:
  # print "CorrectDistortion"
  ### TODO: HANDLE LENS DISTORTION
  ### TODO: HANDLE OFFSET FOR FOOT LOCATION VS BLOB CENTER

  # if '70' in camXMLtoSend['name']:
  # print camXMLtoSend['imageSize']
  # print camXMLtoSend['ccdSize']
  # print camXMLtoSend['location'][0]
  # print camXMLtoSend['location'][1]
  # print camXMLtoSend['location'][2]
  # print camXMLtoSend['focalLength']

  xOff = double(camXMLtoSend['imageSize'][0]) / 2
  yOff = double(camXMLtoSend['imageSize'][1]) / 2

  # Translate to center of image coords
  t = translate(-xOff, -yOff)

  # print t
  s = scale(1 / 40.0, 1 / 40.0)
  # print s
  # Scale according to camera settings
  ccdW, ccdH = camXMLtoSend['ccdSize']
  dist = double(camXMLtoSend['location'][2])
  f = double(camXMLtoSend['focalLength'])
  # print ccdW
  # print ccdH
  # print dist
  # print f
  s = scale(ccdW * (1.0 + dist / f) / double(camXMLtoSend['imageSize'][0]),
            ccdH * (1.0 + dist / f) / double(camXMLtoSend['imageSize'][1]), dist)

  # print s
  temp = dot(s, t)

  # Two negatives shoudl be positive
  num_dashes = camXMLtoSend['orientation'].count('-')
  if num_dashes == 2:
    camXMLtoSend['orientation'] = camXMLtoSend['orientation'].replace('-', '')

  temp = dot(rotate(z=1.0, theta=double(camXMLtoSend['orientation'])), temp)

  xPos = double(camXMLtoSend['location'][0])
  yPos = double(camXMLtoSend['location'][1])

  m = dot(translate(xPos, yPos), temp)

  for traj in trajList:
    if correctDistortion:
      if '_' in camType:
        # print 'using the new undistortion'
        traj.unDistort(distortionParameters, camXMLtoSend['imageSize'])
      else:
        traj.distort(distortionParameters, camXMLtoSend['imageSize'])
    traj.transform(m)



def convertToWorldView(trajList, camXMLtoSend, correctDistortion, distortionParameters):
  """Convert all trajectories in trajList into the world view,
  based on calibration information within camera"""
  # if correctDistortion:
    # print "CorrectDistortion"
  ### TODO: HANDLE LENS DISTORTION
  ### TODO: HANDLE OFFSET FOR FOOT LOCATION VS BLOB CENTER
  
  # if '70' in camXMLtoSend['name']:
    # print camXMLtoSend['imageSize']
    # print camXMLtoSend['ccdSize']
    # print camXMLtoSend['location'][0]
    # print camXMLtoSend['location'][1]
    # print camXMLtoSend['location'][2]
    # print camXMLtoSend['focalLength']

  xOff = double(camXMLtoSend['imageSize'][0])/2
  yOff = double(camXMLtoSend['imageSize'][1])/2

  #Translate to center of image coords
  t = translate(-xOff,-yOff)

  # print t
  s = scale(1/40.0,1/40.0)
  # print s
  #Scale according to camera settings
  ccdW, ccdH = camXMLtoSend['ccdSize']
  dist = double(camXMLtoSend['location'][2])
  f = double(camXMLtoSend['focalLength'])
  # print ccdW
  # print ccdH
  # print dist
  # print f
  s = scale(ccdW * (1.0 + dist/f)/double(camXMLtoSend['imageSize'][0]), ccdH * (1.0 + dist/f)/double(camXMLtoSend['imageSize'][1]),dist)
 
  # print s
  temp = dot(s,t)

  # Two negatives shoudl be positive
  num_dashes = camXMLtoSend['orientation'].count('-')
  if num_dashes == 2:
    camXMLtoSend['orientation'] = camXMLtoSend['orientation'].replace('-','')

  temp = dot(rotate(z=1.0, theta = double(camXMLtoSend['orientation'])), temp)

  xPos = double(camXMLtoSend['location'][0])
  yPos = double(camXMLtoSend['location'][1])

  m = dot(translate(xPos,yPos), temp)

  for traj in trajList:
    if correctDistortion:
      traj.distort(distortionParameters,camXMLtoSend['imageSize'])
    traj.transform(m)

def loadProcessDict(filelist):
  d = {}
  completeFileList = []
  for line in filelist:
    camera, filelist = line.split(':')
    # filelist = filelist.split(',')
    completeFileList = []
    keys = d.keys()
    if camera not in d:
      d[camera] = []
    completeFileList.extend(glob.glob(filelist))
    d[camera].extend(completeFileList)

  return d


def CalibXMLNew(camXMLtoSend, processList, outputPath, correctDistortion, distortionParameters,camType):
  # usage: <calibFilename> <processList> <outputPath> <correctDistortion> <distortionParams> <camType>
  if correctDistortion == 1:
    distortionParameters = map(float, distortionParameters.split(','))
  else:
    distortionParameters = []

  verbose = True  # True#

  # processDict = {'Camera1':['traj.csv']}
  processDict = loadProcessDict(processList)
  print processList
  print processDict
  try:
    os.makedirs(outputPath)
  except:
    pass
  floorImg = None


  for cameraName in processDict:
    for filename in processDict[cameraName]:
      outputFilename = os.path.join(outputPath, os.path.split(filename)[1])

      if verbose:
        print '%s: %s --> %s' % (cameraName, filename, outputFilename)

      trajList = []
      try:
        trajList = Trajectory.loadCSV(filename)
      except:
        trajList = Trajectory.loadXmlCsv(filename)
      convertToWorldViewNew(trajList, camXMLtoSend, correctDistortion, camType, distortionParameters)
      Trajectory.saveXmlCsv(outputFilename,trajList)

"""
if __name__ == '__main__':

  if len(sys.argv) != 5 and len(sys.argv) != 6:
    sys.exit('Error: Usage: Calib <calibration file> <process file> <output folder> <distort 0/1> <distort parameters>')


  calibrationFilename = sys.argv[1]
  processFilename = sys.argv[2]
  outputPath = sys.argv[3]
  #Correct for lens distortion? True = yes
  correctDistortion = int(sys.argv[4])
  distortionParameters = sys.argv[5]

  Calib(calibrationFilename, processFilename, outputPath, correctDistortion, distortionParameters)
"""
