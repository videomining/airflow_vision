import sys
import os
import posseUtils
import TeemoPull
from datetime import datetime, timedelta


def parseSetsFile(setFileLoc):
    cam_set = open(setFileLoc, 'r')
    camList = []
    setAisleList = []
    cam_set_read = cam_set.readlines()
    for line in cam_set_read:
        aisle, cam = [a.strip() for a in line.split(':')]
        setAisleList.append(aisle)
        cam = cam.strip('(').strip(')').split(',')
        for camName in cam:
            while len(camName) != 3:
                camName = '0' + camName
            if 'cam' not in camName:
                camName = 'cam' + camName
            camList.append(camName)
    return camList, setAisleList

def parseGroupFiles(groupFileLoc):
    cam_group = open(groupFileLoc, 'r')
    groupList = []
    groupAisleList = []
    cam_group_read = cam_group.readlines()
    for line in cam_group_read:
        group, aisle = [a.strip() for a in line.split(':')]
        groupList.append(group)
        aisle = aisle.strip('(').strip(')').split(',')
        for aisleName in aisle:
            groupAisleList.append(aisleName)
    return groupList, groupAisleList

def parseFinalFiles(finalFileLoc):
    open_join = open(finalFileLoc, 'r')
    entireGroupList = []
    join_read = open_join.readlines()
    for line in join_read:
        EntireStore, group = [a.strip() for a in line.split(':')]
        group = group.strip('(').strip(')').split(',')
        for groupName in group:
            entireGroupList.append(groupName)
    return entireGroupList




def checkPushFiles(livecamList,storeName, setFileLoc, groupFileLoc, finalFileLoc, startdate, endDate,ignoreCams):
    # checking sets file
    camList, setAisleList = parseSetsFile(setFileLoc)
    uniqueCamList = list(set(camList))
    # store ignoreCams into ignoreCamsList and compare with camList
    ignoreCams = GenerateJobs._ParseCamRange(ignoreCams)
    strignoreCams = list(str(cam) for cam in ignoreCams)
    ignoreCamsList = []
    for camName in strignoreCams:
            while len(camName) != 3:
                camName = '0' + camName
            if 'cam' not in camName:
                camName = 'cam' + camName
            ignoreCamsList.append(camName)
   #  compare ignoreCamsList with camList if cam in ignoreCamsList it will be ignore
    if len(uniqueCamList) != len(camList):
        for cam in uniqueCamList:
            camList.remove(cam)
        raise Exception('There are duplicate cameras in the set file -- >%r' % camList)
    for cam in livecamList:
        if cam not in camList:
            if cam in ignoreCamsList:
                continue
            else:
                raise Exception('%r is not in the set file! Please check the live project file for more information!'%cam)

    # checking groups file
    groupList, groupAisleList = parseGroupFiles(groupFileLoc)
    unigroupAisleList = list(set(groupAisleList))
    if len(unigroupAisleList) != len(groupAisleList):
        for aisle in unigroupAisleList:
            groupAisleList.remove(aisle)
        raise Exception('There are duplicate cameras in the group file -- >%r' % groupAisleList)
    if len(unigroupAisleList) != len(setAisleList):
        for aisle in setAisleList:
            if aisle not in unigroupAisleList:
                raise Exception('%r is missing in the group file' % aisle)

    # checking final file
    entireGroupList = parseFinalFiles(finalFileLoc)
    nuientireGroupList = list(set(entireGroupList))
    if len(nuientireGroupList) != len(entireGroupList):
        for group in nuientireGroupList:
            entireGroupList.remove(group)
        raise Exception('There are duplicate group in final file-->%r' % entireGroupList)
    if len(nuientireGroupList)!= len(groupList):
        for group in groupList:
            if group not in nuientireGroupList:
                raise Exception('%r is missing in the final file' % group)

def getCameraJoinFiles(storeName,startdate,enddate):
    teemoPullObj = TeemoPull.TeemoPull(storeName,startdate)
    finalDateList = teemoPullObj.getDateList(enddate)
    #creating a Dictionary for Set
    setDict= {}
    groupDict = {}
    joinDict = {}
    for element in finalDateList:
        startdt = datetime.strftime( element[0], '%Y%m%d')
        enddt = datetime.strftime( element[1], '%Y%m%d')
        date = startdt + '_' + enddt
        set = element[2]
        setlist = set.split('\n')
        setDict[date]= setlist
        group = element[3]
        groupList = group.split('\n')
        groupDict[date] = groupList
        join = element[4]
        joinlist = join.split('\n')
        joinDict[date] = joinlist
    return setDict,groupDict,joinDict




