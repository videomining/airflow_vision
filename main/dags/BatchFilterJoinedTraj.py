import glob
from Trajectory import *
import TimeUtils
import math
import TrajectoryFilters
import time
import sys
import datetime
import utilsAmazonS3
import os
import csv
import copy
from posseUtils import stringToTime
import pickle
import VmsUtils
#import getHeatMaps
import traceback
from GetshopUtils import getShop

def fetchEntranceCamRegions(store,projecttype,dateVal):
    reqEntvalues = {}
    getshopObj = getShop()
    entranceDict = getshopObj.fetchEntranceInfo(store,projecttype)
    for dateRange in entranceDict:
        start,stop = dateRange.split('_')
        if start <= dateVal <= stop:
            for key in entranceDict[dateRange]['StoreEntrance']['name']:
                reqEntvalues[key] = entranceDict[dateRange]['StoreEntrance']['name'][key]['region']
    
def getTimeDict(start,end,delta,defaultVal):
    dict = {}
    t = start
    while t < end:
        dict[t] = copy.deepcopy(defaultVal)
        t+=delta;
    return dict
def addColVal(dict, colIndex, time, value):
    try:
        hour = datetime.datetime(time.year,time.month,time.day,time.hour,0,0)
        row = dict[hour]
        row = row[0:colIndex] + [row[colIndex] + value] + row[colIndex+1:]
        dict[hour] = row
    except:
        # pdb.set_trace()
        pass
        
def calculateAndUploadDoorCount(bucket_name,entranceCamDict,dateStr):

    startTime = stringToTime(dateStr)
    doorCountDict = {}
    for cam in entranceCamDict:
            #loading the entrance cam dict

        doorCountDict[cam] = {}
        doorCountDict[cam].update(getTimeDict(startTime,startTime + datetime.timedelta(days=1),datetime.timedelta(hours=1),0.0))
        fname = entranceCamDict[cam]

        if os.path.exists(fname):
            firstLine = 1
            for row in csv.reader(open(fname)):
                if not firstLine:
                    if len(row) != 0:
                        eventTime = stringToTime(row[0])
                        hour = datetime.datetime(eventTime.year,eventTime.month,eventTime.day,eventTime.hour,0,0)
                        doorCountDict[cam][hour]+= 1
                    else:
                        print "Error 2: File is empty"
                else:
                    firstLine = 0
    print 'doorcount calculation completed for date %r'%dateStr
    #now we will upload to amazon s3 the pickle file
    #for cam in doorCountDict:
    outFileName = os.getcwd()+'\\'+dateStr+'.pkl'
    output = open(outFileName, 'wb')
    pickle.dump(doorCountDict, output)
    output.close()
    fileList = [outFileName]
    utilsAmazonS3.multipartUploadToS3(bucket_name,fileList,'doorcount')
    os.remove(outFileName)

def getGSIDoorCount(store,dateStr,bucket_name,input_path,output_path,projecttype):
    date = datetime.datetime(int(dateStr[0:4]),int(dateStr[4:6]), int(dateStr[6:]), 0, 0, 0, 0)
    entranceCamDict = {}
    reqEntvalues = fetchEntranceCamRegions(store,projecttype,dateStr)
    if len(reqEntvalues.keys()) == 0:
        raise Exception('No entrance camera points found')
    
    for cam in entranceCamDict.keys():
        inputFilename = os.sep.join([input_path,dateStr+'_'+cam+'.csv'])
        trajList = Trajectory.loadXmlCsv(inputFilename)
        foundEntrance = False
        for key in reqEntvalues:
            if key.lower() == entranceCamDict[cam]:
                foundEntrace = True
                startPoly = VmsUtils.polyFromString(reqEntvalues[key])
                endPoly = VmsUtils.polyFromString(reqEntvalues[key])
                polyPairs.append((startPoly, endPoly))
        if not foundEntrance:
            raise Exception('No match found for entrance camera in database')
            
        #filtering tracks which start from the store entrance
        finalTrajList = []
        for startPoly, endPoly in polyPairs:
            startTrajList = TrajectoryFilters.filterStartPoly(trajList, [startPoly])
            finalTrajList.extend(startTrajList)

        outputFilename = '%s/%s' %(output_path, dateStr+'_'+cam+'.csv')
        outFile = file(outputFilename,'w')
        print outputFilename
        outFile.write('Start Time,Duration,Trajectory\n')
        for traj in finalTrajList:
            startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
            outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
        outFile.close()
        entranceCamDict[cam] = outputFilename
    #once all files are loaded we will create the dict and load it to Amazon S3
    calculateAndUploadDoorCount(bucket_name,entranceCamDict,dateStr)
    #deleting the temporary csv
    for cam in entranceCamDict:
        os.remove(entranceCamDict[cam])

def BatchFilterJoinedTraj(dateStr, bucket_name, inputPath, outputPath, fileType, filterlongFlag, store):

    date = datetime.datetime(int(dateStr[0:4]),int(dateStr[4:6]), int(dateStr[6:]), 0, 0, 0, 0)
    filename = os.sep.join([inputPath,dateStr+'.csv'])
	filename = os.sep.join([inputPath,'20170701.csv'])
    trajList = Trajectory.loadXmlCsv(filename)
    print 'length of traj before filter %r'%len(trajList)
    outputFilename = os.sep.join([outputPath, dateStr+'.csv'])
    outFile = file(outputFilename,'w')
    outFile.write('Start Time,Duration,Trajectory\n')
    print "Processing %s"%(outputFilename)
    ctr = 1

    for traj in trajList:
        # apply filter
        if not filterlongFlag:
            if traj.duration() > 10 and traj.maxDisp() > 100:
                traj.id = ctr
                # write to output file
                startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
                outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
                ctr+=1
        else:
            if traj.duration() > 20 and traj.duration() < 900 and traj.maxDisp() > 100 and traj.length() > 150:
                traj.id = ctr
                # write to output file
                startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
                outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
                ctr+=1
    outFile.close()
    fileList = [outputFilename]

    utilsAmazonS3.multipartUploadToS3(bucket_name,fileList,fileType)
    #if the filterlongFlag is true then its a csi store so  use the filter long traj files to determine the hourly traffic count
    if filterlongFlag:
        entranceCamDict = {}
        entranceCamDict['fac001'] = outputFilename
        calculateAndUploadDoorCount(bucket_name,entranceCamDict,dateStr)
    os.remove(outputFilename)
    
#def BatchFilterJoinedTraj(inputDB, dateStr, bucket_name, temp_out_path,fileType, filterlongFlag,entranceCamList,heatMapsDir,xmlTemplateFilename,dayType,store):

#    date = datetime.datetime(int(dateStr[0:4]),int(dateStr[4:6]), int(dateStr[6:]), 0, 0, 0, 0)
#    inputTableName = 'final_join_'+dateStr
#    trajList = Trajectory.loadTrajFromDB_Filter(inputDB,date,inputTableName)
#    print 'length of traj before filter %r'%len(trajList)
#    outputFilename = '%s\\%s' % (temp_out_path, dateStr+'.csv')
#    outFile = file(outputFilename,'w')
#    outFile.write('Start Time,Duration,Trajectory\n')
#    print "Processing %s"%(outputFilename)
#    ctr = 1

#    for traj in trajList:
#         apply filter
#        if not filterlongFlag:
#            if traj.duration() > 10 and traj.maxDisp() > 100:
#                traj.id = ctr
#                 write to output file
#                startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
#                outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
#                ctr+=1
#        else:
#            if traj.duration() > 20 and traj.duration() < 900 and traj.maxDisp() > 100 and traj.length() > 150:
#                traj.id = ctr
#                 write to output file
#                startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
#                outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
#                ctr+=1
#    outFile.close()
#    fileList = [outputFilename]
#    trajFilesPath = temp_out_path
#    dateVal = dateStr
#    xmlTemplateFilename = xmlTemplateFilename
#    dayType = dayType


#    getHeatMaps.getHeatMaps(trajFilesPath,dateVal,xmlTemplateFilename,dayType)
#    utilsAmazonS3.multipartUploadToS3(bucket_name,fileList,fileType)
#    if the filterlongFlag is true then its a csi store so  use the filter long traj files to determine the hourly traffic count
#    if filterlongFlag:
#        entranceCamDict = {}
#        entranceCamDict[entranceCamList[0]] = outputFilename
#        calculateAndUploadDoorCount(bucket_name,entranceCamDict,dateStr)
#    os.remove(outputFilename)
