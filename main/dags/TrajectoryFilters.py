#Contains numerous functions for filtering a list of trajectories

import getDwellPoints
from Trajectory import *
import VmsUtils

def filterShopInPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do not dwell inside one of the polygons in polygon list"""
    
    filteredTrajList = []
    
    for traj in trajList:
        dwellTraj = getDwellPoints.getDwellTraj(traj,5,30)
        inPoly = False
        for time, x, y, duration in dwellTraj.nodes:
            for poly in polygonList:
                if poly.isPointInside((x,y)):
                    inPoly = True
        
        if inPoly:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)
    
    return filteredTrajList

def filterShopInPoly_distance(trajList, polygonList, shopDistance, removedTrajList = None):
    """Filters out trajectories that do not dwell inside one of the polygons in polygon list"""
    
    filteredTrajList = []
    for traj in trajList:
        dwellTraj = getDwellPoints.getDwellTraj(traj,5,shopDistance)
        inPoly = False
        for time, x, y, duration in dwellTraj.nodes:
            for poly in polygonList:
                if poly.isPointInside((x,y)):
                    inPoly = True
        
        if inPoly:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)
    
    return filteredTrajList

def filterStartPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do not start inside one of the polygons
    in polygonList."""
    
    filteredTrajList = []
    
    for traj in trajList:
        pointInside = False
        for poly in polygonList:
            if poly.isPointInside(traj.startPos()):
                filteredTrajList.append(traj)
                pointInside = True
                break
        if not pointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterEndPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do not end inside one of the polygons
    in polygonList"""
    filteredTrajList = []
    
    for traj in trajList:
        pointInside = False
        for poly in polygonList:
            if poly.isPointInside(traj.endPos()):
                filteredTrajList.append(traj)
                pointInside = True
                break
        if not pointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterStartOrEndPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do not start inside one of the polygons
    in polygonList."""
    
    filteredTrajList = []
    
    for traj in trajList:
        pointInside = False
        for poly in polygonList:
            if poly.isPointInside(traj.startPos()) or poly.isPointInside(traj.endPos()):
                filteredTrajList.append(traj)
                pointInside = True
                break
        if not pointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterEntersPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do have at least one point inside one
    of the polygons in polygonList"""
    filteredTrajList = []
    # enterTimes = []

    for traj in trajList:
        foundPointInside = False
        for time,x,y in traj.nodes:
            for poly in polygonList:
                if not foundPointInside and poly.isPointInside((x,y)):
                    filteredTrajList.append(traj)
                    enterTimes.append(time)
                    foundPointInside = True

        if not foundPointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList, enterTimes


def filterEntersPoly2(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do have at least one point inside one
    of the polygons in polygonList"""
    filteredTrajList = []
    
    for traj in trajList:
        foundPointInside = False
        for time,x,y in traj.nodes:
            for poly in polygonList:
                if not foundPointInside and poly.isPointInside((x,y)):
                    filteredTrajList.append(traj)
                    foundPointInside = True

        if not foundPointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList
    
    
def filterPointNotInPoly(trajList, polygonList, removedTrajList = None):
    """Filters out trajectories that do not have at least one point inside one
    of the polygons in polygonList"""
    filteredTrajList = []

    for traj in trajList:
        foundPointOutside = False
        for time,x,y in traj.nodes:
            for poly in polygonList:
                if not foundPointOutside and not poly.isPointInside((x,y)):
                    filteredTrajList.append(traj)
                    foundPointOutside = True

        if not foundPointOutside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterDurationInPoly(trajList, polygonList, duration, removedTrajList = None):
    """Filters out trajectories that do not spend at least duration seconds
    inside the polygons in polygonList"""
    filteredTrajList = []

    for traj in trajList:
        timeInside = 0

        for poly in polygonList:
            prevPointInside = False
            for time,x,y in traj.nodes:
                if prevPointInside:
                    timeInside += time - prevTime

                prevPointInside = False
                
                if poly.isPointInside((x,y)):
                    prevPointInside = True
                    prevTime = time;

        if timeInside >= duration:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterMaxDisplacement(trajList, maxDisplacement, removedTrajList = None):
    """Filters out trajectories that were never a distance of at least
    maxDisplacement from the start or end of the track"""
    filteredTrajList = []

    for traj in trajList:
        if traj.maxDisp() > maxDisplacement:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterLength(trajList, minLength, removedTrajList = None):
    """Filters out trajectories that do not travel a total distance of
    minLength or greater"""
    filteredTrajList = []

    for traj in trajList:
        if traj.length() > minLength:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterMinDuration(trajList, minDuration, removedTrajList = None):
    """Filters out trajectories that were not around for a duration of at least
    minDuration"""
    filteredTrajList = []
    
    for traj in trajList:
        if traj.duration() > minDuration:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList



def filterByTime(trajList, startHour, startMinute, endHour, endMinute, removedTrajList = None):
    """Filters out trajectories that did not start between the times of
    startHour:startMinute and endHour:endMinute"""
    filteredTrajList = []
    
    startTime = time(startHour, startMinute)
    endTime = time(endHour, endMinute)

    for traj in trajList:
        trajTime = secsToDate(traj.startTime())
        trajTime = time(trajTime.hour,trajTime.minute)

        if trajTime >= startTime and trajTime < endTime:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList

#def filterByTime(trajList, startTime, endTime, removedTrajList = None):
#    """Filters out trajectories that did not start between the times of
#    startHour:startMinute and endHour:endMinute"""
#    filteredTrajList = []

#    for traj in trajList:
#        trajTime = secsToDate(traj.startTime())
#        trajTime = time(trajTime.hour,trajTime.minute)

#        if trajTime >= startTime and trajTime < endTime:
#            filteredTrajList.append(traj)
#        elif removedTrajList is not None:
#            removedTrajList.append(traj)

    #return filteredTrajList



def filterCrossLineSegments(trajList, lineSegments, startSide = 'left', removedTrajList = None):
    """Filters out trajectories that do not cross one of the directed line
    segments defined in lineSegments heading in the direction of startSide to
    the opposite side. For example, if startSide is left then any trajectories
    that don't cross from left to right are removed"""
    filteredTrajList = []
    timesCrossed = []

    if type(lineSegments) != types.ListType:
        lineSegments = [lineSegments]

    if startSide == 'left':
        sideModifier = 1
    elif startSide == 'right':
        sideModifier = -1
    else:
        sideModifier = 0

    for traj in trajList:
        crossedLine = False
        for line in lineSegments:
            prevTrajPoint = []
            beginPoint = line[0]
            endPoint = line[1]
            for time,x,y in traj.nodes:
                trajPoint = (x,y)

                if prevTrajPoint:
                    area1 = triangleArea(prevTrajPoint, trajPoint, beginPoint)
                    area2 = triangleArea(prevTrajPoint, trajPoint, endPoint)

                    if area1*area2 <= 0:
                        area1 = triangleArea(beginPoint, endPoint, prevTrajPoint)
                        area2 = triangleArea(beginPoint, endPoint, trajPoint)
                        if area1*sideModifier <= 0 and area2*sideModifier >= 0 and area1*area2 <= 0:
                            crossedLine = True
                            timeCrossed = time

                prevTrajPoint = trajPoint

        if crossedLine:
            filteredTrajList.append(traj)
            timesCrossed.append(timeCrossed)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList, timesCrossed

def filterTimeToEnterPoly(trajList, polygonList, timeToEnter, removedTrajList = None):
    """Filters out trajectories that take longer than timeToEnter seconds to
    enter a polygon inside polygonList. If the trajectory never enters a polygon
    in polygonList then it is also filtered out."""
    filteredTrajList = []

    for traj in trajList:
        foundPointInside = False
        for time,x,y in traj.nodes:
            for poly in polygonList:
                if not foundPointInside and poly.isPointInside((x,y))\
                   and time - traj.startTime() <= timeToEnter:
                    filteredTrajList.append(traj)
                    foundPointInside = True

        if not foundPointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList

def filterEnterPolyBeforeTime(trajList, polygonList, timesToEnter, removedTrajList = None):
    """Filters out trajectories that take longer than timeToEnter seconds to
    enter a polygon inside polygonList. If the trajectory never enters a polygon
    in polygonList then it is also filtered out."""
    filteredTrajList = []

    for traj in trajList:
        foundPointInside = False
        timeToEnter = timesToEnter[trajList.index(traj)]
        for time,x,y in traj.nodes:
            for poly in polygonList:
                if not foundPointInside and poly.isPointInside((x,y))\
                   and time <= timeToEnter:
                    filteredTrajList.append(traj)
                    foundPointInside = True

        if not foundPointInside and removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList


def filterCrossLinesBeforePoly(trajList, lineSegments, polygonList, startSide = 'left', removedTrajList = None):
    """Filters out trajectories that cross one of the directed line
    segments defined in lineSegments heading in the direction of startSide to
    the opposite side before entering one of the polygons in polygonList. For
    example, if startSide is left then any trajectories that cross from left
    to right are removed if the crossing happen before entering one of the
    polygons."""
    filteredTrajList = []
    timesCrossed = []

    if type(lineSegments) != types.ListType:
        lineSegments = [lineSegments]

    if startSide == 'left':
        sideModifier = 1
    elif startSide == 'right':
        sideModifier = -1
    else:
        sideModifier = 0

    for traj in trajList:
        crossedBeforePoly = False
        enteredPoly = False;
        prevTrajPoint = ()
        for time,x,y in traj.nodes:
            if not enteredPoly and not crossedBeforePoly:
                trajPoint = (x,y)
                for poly in polygonList:
                    if poly.isPointInside(trajPoint):
                        enteredPoly = True
                for line in lineSegments:
                    beginPoint = line[0]
                    endPoint = line[1]

                    if prevTrajPoint:
                        area1 = triangleArea(prevTrajPoint, trajPoint, beginPoint)
                        area2 = triangleArea(prevTrajPoint, trajPoint, endPoint)

                        if area1*area2 <= 0:
                            area1 = triangleArea(beginPoint, endPoint, prevTrajPoint)
                            area2 = triangleArea(beginPoint, endPoint, trajPoint)
                            if area1*sideModifier <= 0 and area2*sideModifier >= 0 and area1*area2 <= 0:
                                crossedBeforePoly = True
                                timesCrossed.append(time)

                prevTrajPoint = trajPoint

        if crossedBeforePoly:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)

    return filteredTrajList, timesCrossed



def filterIdenticalTrajectories(trajList):
    trajList.sort(sortTrajByStartAndId)

    if len(trajList) == 0:
        return []
    
    prevTraj = trajList[0]
    filteredTrajList = [prevTraj]
    for traj in trajList:
        if prevTraj.startTime() != traj.startTime() or prevTraj.getId() != traj.getId() or abs(prevTraj.duration() - traj.duration()) > .1:
            filteredTrajList.append(traj)
        prevTraj = traj

    return filteredTrajList



def filterVectorDirection(trajList, poly, vectorDirection, errorAllowed, removedTrajList = None):
    filteredTrajList = []
    
    for traj in trajList:
        enteredPoly = False
        exitedPoly = False
        for time,x,y in traj.nodes:
            trajPoint = (x,y)
            if not enteredPoly:
                if poly.isPointInside(trajPoint):
                    startPos = trajPoint
                    enteredPoly = True
            elif not exitedPoly:
                if not poly.isPointInside(trajPoint):
                    endPos = trajPoint
                    exitedPoly = True
            elif enteredPoly and exitedPoly:
                if poly.isPointInside(trajPoint):
                    exitedPoly = False

        if not enteredPoly:
            startPos = traj.startPos()
            endPos = traj.endPos()
        elif not exitedPoly:
            endPos = traj.endPos()
        trajVector = [endPos[0] - startPos[0], endPos[1] - startPos[1]]
        trajDirection = atan2(trajVector[1], trajVector[0])*180/pi

        if(trajDirection < 0):
            trajDirection = 360 + trajDirection

        circularDistance = min(abs(trajDirection - vectorDirection),abs(360 + trajDirection - vectorDirection), abs(trajDirection - vectorDirection - 360))
        
        if circularDistance <= errorAllowed:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            #print trajDirection, circularDistance
            removedTrajList.append(traj)
            
    return filteredTrajList

def filterVisitKPolys(trajList, k, polygons, removedTrajList = None, visitedPolys = None):
    filteredTrajList = []
    if visitedPolys is None:
        visitedPolys = {}
    
    for traj in trajList:
        visitedPolys[traj] = []
        for poly in polygons:
            visitedPolys[traj].append(False)
        nodeIndex = 0
        while nodeIndex < len(traj.nodes):# and visitedPolys[traj].count(True) < k:
            time, x, y = traj.nodes[nodeIndex]
            point = (x,y)
            for polyIndex in range(len(polygons)):
                poly = polygons[polyIndex]
                if not visitedPolys[traj][polyIndex] and poly.isPointInside(point):
                    visitedPolys[traj][polyIndex] = True
            
            nodeIndex += 1
        
        if visitedPolys[traj].count(True) >= k:
            filteredTrajList.append(traj)
        elif removedTrajList is not None:
            removedTrajList.append(traj)
    
    return filteredTrajList

def movingAverage(trajList, pointDistance, averageInterval):
    smoothedTrajList = []
    for traj in trajList:
        prevTime = 0
        smoothedTraj = copy.copy(traj)
        smoothedTraj.nodes = []
        
        midTime = ceil(traj.startTime() / pointDistance) * pointDistance
        startTime = midTime - averageInterval
        endTime = midTime + averageInterval

        for i in range(0, len(traj.nodes) - 1):
            checkTime = True

            while checkTime:
                checkTime = False
                time, x, y = traj.nodes[i]
                if time >= startTime and prevTime < startTime:
                    averagex = 0
                    averagey = 0
                    numNodes = 0
                    j = i
                    currTime = time
                    while currTime < endTime and j < len(traj.nodes):
                        currTime, currx, curry = traj.nodes[j]
                        averagex += currx
                        averagey += curry
                        numNodes += 1
                        j += 1

                    if numNodes > 0:
                        averagex /= numNodes
                        averagey /= numNodes
                    else:
                        averagex = x
                        averagey = y

                    smoothedTraj.nodes.append((midTime, averagex, averagey))
                if time >= endTime:
                    checkTime = True
                    midTime += pointDistance
                    startTime = midTime - averageInterval
                    endTime = midTime + averageInterval
            prevTime = time
        smoothedTrajList.append(smoothedTraj)
    return smoothedTrajList

def triangleArea(point1, point2, point3):
    return point1[0]*point2[1] - point1[1]*point2[0] + point1[1]*point3[0] -\
           point1[0]*point3[1] + point2[0]*point3[1] - point2[1]*point3[0]
