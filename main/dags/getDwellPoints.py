from Trajectory import *
import random
import os
import os.path
import glob
# from PIL import Image, ImageDraw,ImageFont
import VmsUtils, TimeUtils
import csv

class DwellTraj(Trajectory):
# class to represent the dwell points of a track
# contents are start time of the original track,total duration of the track
# and node list where every node represents a dwell point storing its start 
# time, midpoint position and duration
# CSV format written to file is as follows:
# "'2007-02-07T23:00:00',15.00,"'2007-02-07T23:00:01',35.0,19.0,4.5;'2007-02-07T23:00:07',20.0,1.0,3.5"
# The above contains two dwell stops. The first one started at '2007-02-07T23:00:01'
# was 4.5 seconds long with the avg. position being (35,19). The second one started
# at '2007-02-07T23:00:07'
  def __init__(self,id=-1):
    Trajectory.__init__(self,id)
    
    self.totalTime = -1 # duration of original track
    #self.dwellNodeList = [] #(starttime,x,y,duration) for each node
    self._startTime = -1
  def startTime(self):
    return self._startTime

  def addDwell(self,dwellNode):
    #print "DWELLNODE=",dwellNode
    self.nodes.append(dwellNode)
    
  def getCsvString(self):
    csvString = ""
    csvString += "%s," % secsToDate(self.startTime()).strftime('%Y-%m-%dT%H:%M:%S')
    csvString += "%f," % self.totalTime
    nodesString = '"'
    for node in self.nodes:
      dwellDt = secsToDate(node[0])
      dwellStartTime = dwellDt.strftime('%Y-%m-%dT%H:%M:%S')+ ("%f" % (dwellDt.microsecond/1000000.0))[1:]
      nodesString += "%s,%f,%f,%f;" % (dwellStartTime,node[1],node[2],node[3])
    
    nodesString.strip(';')
    nodesString += '"'
    csvString += nodesString
    return csvString
  
  def duration(self):
    return self.totalTime
  
  def endTime(self):
    return self.starttime + self.duration()
  
  def readRow(self,row):
  #input is the list returned by csv reader represent one dwellTraj
  # row obtained by
    self.starttime = TimeUtils.DateStringToSecs(row[0])
    self.totalTime = float(row[1])
    nodeStrList = row[2].split(';')
    for nodeStr in nodeStrList:
      nodeElements = nodeStr.split(',')
      dwellStartTime = DateStringToSecs(nodeElements[0])
      posX = float(nodeElements[1])
      posY = float(nodeElements[2])
      duration = float(nodeElements[3])
      self.addDwell((dwellStartTime,posX,posY,duration))
  
  #def 
  #def def
  
  def hasDwell(self):
    return self.nodes != []
  
  def insidePoly(self,dwellStartTime,posX,posY,duration):
    for dwellStartTime,posX,posY,duration in self.nodes:
      if VmsUtils.Polygon.isPointInside((posX,posY)):
        return True
    
    return False

def readDwellTraj(filename):
  dwellTrajList = []
  csvObj = csv.reader(VmsUtils.OpenRobustly(filename))
  # csvObj = csv.reader(open(filename))
  for row in csvObj:
    dwellTraj = DwellTraj()
    dwellTrajList.append(dwellTraj.readRow(row))
  
  return dwellTrajList

def saveDwellTraj(dwellTrajList,filename):
  fileObj = VmsUtils.OpenRobustly(filename,'w')
  # fileObj = open(filename,'w')
  for dwellTraj in dwellTrajList:
    csvString = dwellTraj.getCsvString()
    fileObj.write("%s\n" % csvString)
  
  fileObj.close()
    

def getDwellTraj(traj,minTime,maxDisp):

  dwellTraj = DwellTraj()
  dwellTraj._startTime = traj.startTime()
  dwellTraj.totalTime = traj.duration()
  
  if traj.duration() < minTime:
    return dwellTraj
  

  
  totalNodes = len(traj.nodes)

  x=0
  while x < totalNodes:
    firstPointIndex = x
    firstTime, firstxPos, firstyPos = traj.nodes[firstPointIndex]
    firstPoint = (firstxPos,firstyPos)
    distX = firstxPos
    distY = firstyPos
    dwellNodeCount=1
    lastNodeIndex=x
    while x < totalNodes and dist(firstPoint,(traj.nodes[x][1],traj.nodes[x][2])) < maxDisp:
      distX+=traj.nodes[x][1]
      distY+=traj.nodes[x][2]
      dwellNodeCount+=1
      x+=1

    if traj.nodes[min(x-1,totalNodes-1)][0] - firstTime >= minTime:
      midPoint = ((distX)/dwellNodeCount,(distY)/dwellNodeCount)
      #print "dwellNodeCount=%d" % dwellNodeCount
      #print "middlePoint=%f,%f" % (midPoint[0],midPoint[1])
      #print "firstPoint=%d,%d" % (firstPoint[0],firstPoint[1])
      #print "stopPos - 1=%d,%d" % (traj.nodes[max(0,x-2)][1],traj.nodes[max(0,x-2)][2])

      dwellDuration = traj.nodes[min(x-1,totalNodes-1)][0] - firstTime
      dwellTraj.addDwell((firstTime,midPoint[0],midPoint[1],dwellDuration))
 
  return dwellTraj
  
  
  
  
def getDwellTrajList(trajList,minTime,maxDisp):
  dwellTrajList = []
  for traj in trajList:
    dwellTraj = getDwellTraj(traj,minTime,maxDisp)
    if dwellTraj.hasDwell():
      dwellTrajList.append(dwellTraj)
    else:
      print "No dwell for traj %d" % traj.id
  
  return dwellTrajList

# if __name__ == '__main__':
  # if len(sys.argv) != 5:
    # sys.exit('Error in Usage. Use:  getDwellPoints <inputFile(s)> <minTime> <maxDisp> <outputFile>\n' + \
        # "Note this script will draw dwell trajectories on 'Floorplan.png and save it to 'allpoints.png'")
    
  # inputFiles = glob.glob(sys.argv[1])
  # minTime = float(sys.argv[2])
  # maxDisp = float(sys.argv[3])
  # outputFile = sys.argv[4]

  # outputImage = "Floorplan.png"
  
  # allDwellTrajList = []
  # for file in inputFiles:
    # trajList = Trajectory.loadXmlCsv(file)
    # dwellTrajList = getDwellTrajList(trajList,minTime,maxDisp)
    # allDwellTrajList.extend(dwellTrajList)

  # print "Writing to File",outputFile
  
  # saveDwellTraj(allDwellTrajList,outputFile)

  # font = ImageFont.truetype("Arial.ttf", 12)

  # bgImage = Image.open(outputImage)
  # colors = ['red','green','blue','orange','yellow','pink','purple']
  # #draw = ImageDraw.Draw(bgImage)

  # for dwellTraj in dwellTrajList:
    # dwellTraj.draw(bgImage,random.choice(colors))

  # bgImage.save("allPoints.png")
    
    
