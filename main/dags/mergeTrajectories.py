##### To be used only for filtering in Individual cameras and not across multiple cameras #######


import sys
import glob
from Trajectory import *
import TimeUtils
import math
import TrajectoryFilters
import time
import itertools

def dist(x1, y1, x2, y2) :
    return sqrt((x1 - x2)**2 + (y1-y2)**2)

def filterHangingTracks(trajList) :
    #outputfile = open('hangingTrajEvaluate.csv','w')
    #string = ''
    new_trajGroup = []
    for traj in trajList :
        #string = string + ',' + str(secsToDate(traj.startTime())) + ',' + str(traj.duration())
        count = 0
        for index in range(len(traj.nodes)-1) :
            node1Time = traj.nodes[index][0]
            node1X = traj.nodes[index][1]
            node1Y = traj.nodes[index][2]
            node2Time = traj.nodes[index+1][0]
            node2X = traj.nodes[index+1][1]
            node2Y = traj.nodes[index+1][2]

            if dist(node1X, node1Y, node2X, node2Y) <= 2 :
                count = count + 1
            #string = string + ',' + str(dist(node1X, node1Y, node2X, node2Y))
        percentage = (float(count)/float((len(traj.nodes)-1)))*100
        #print percentage
        if percentage < 45 :
            new_trajGroup.append(traj)
        #string = string + ',' + str(percentage) + '\n'
    #outputfile.write(string)
    return new_trajGroup


def getTrajGroups(trajList) :
    trajGroups = []
    trajGroup = [trajList[0]]
    startTime = trajList[0].startTime()
    endTime = trajList[0].endTime()

    for currTraj in trajList[1:] :
        if currTraj.startTime() <= endTime:
            trajGroup.append(currTraj)
            if currTraj.endTime() > endTime :
                endTime = currTraj.endTime()
        else :
            trajGroups.append(trajGroup)
            trajGroup = [currTraj]
            startTime = currTraj.startTime()
            endTime = currTraj.endTime()


    trajGroups.append(trajGroup)
    return trajGroups

def directionVector(x1, y1, x2, y2) :
    return (x1-x2, y1-y2)

# returns the index of the first node in traj.nodes whose time is at least startTime
def findFirstOverlapNodeIndex(traj, startTime):
    for index in range(len(traj.nodes)):
        node = traj.nodes[index]
        if node[0] >= startTime:
            return index
    raise ValueError("No node found with time >= startTime of %d" % startTime)


def getInterpolatedTrajectories(traj1, traj2):
    traj1_all = []
    traj2_all = []

    startTime = max(traj1.startTime(), traj2.startTime())
    endTime = min(traj1.endTime(), traj2.endTime())
    length = max(len(traj1.nodes)-1, len(traj2.nodes)-1)

    traj1Index = findFirstOverlapNodeIndex(traj1, startTime)
    traj1Node = traj1.nodes[traj1Index]
    traj1Time = traj1Node[0]
    traj1X = traj1Node[1]
    traj1Y = traj1Node[2]

    traj2Index = findFirstOverlapNodeIndex(traj2, startTime)
    traj2Node = traj2.nodes[traj2Index]
    traj2Time = traj2Node[0]
    traj2X = traj2Node[1]
    traj2Y = traj2Node[2]

    while (traj1Time < endTime and traj2Time <= endTime) or (traj1Time <= endTime and traj2Time < endTime):

        if traj1.nodes[traj1Index][0] > traj2.nodes[traj2Index][0] :
            previousNode = traj1.nodes[traj1Index-1]
            previousTime = previousNode[0]
            previousX = previousNode[1]
            previousY = previousNode[2]

            totalTimeDiff = traj1.nodes[traj1Index][0] - traj1.nodes[traj1Index-1][0]
            timeDiff = traj1.nodes[traj1Index][0] - traj2.nodes[traj2Index][0]
            if totalTimeDiff != 0:
                timePercentage = timeDiff/totalTimeDiff
            else:
                timePercentage = 0

            traj1X_interpolated = previousX * (1-timePercentage) + traj1.nodes[traj1Index][1]* timePercentage
            traj1Y_interpolated = previousY * (1-timePercentage) + traj1.nodes[traj1Index][2]* timePercentage


            traj1_all.append((traj2.nodes[traj2Index][0], traj1X_interpolated, traj1Y_interpolated))
            traj2_all.append((traj2.nodes[traj2Index][0], traj2.nodes[traj2Index][1], traj2.nodes[traj2Index][2]))

        elif traj2.nodes[traj2Index][0] > traj1.nodes[traj1Index][0] :
            previousNode = traj2.nodes[traj2Index-1]
            previousTime = previousNode[0]
            previousX = previousNode[1]
            previousY = previousNode[2]

            totalTimeDiff = traj2.nodes[traj2Index][0] - traj2.nodes[traj2Index-1][0]
            timeDiff = traj2.nodes[traj2Index][0] - traj1.nodes[traj1Index][0]
            if totalTimeDiff != 0:
                timePercentage = timeDiff/totalTimeDiff
            else:
                timePercentage = 0

            traj2X_interpolated = previousX * (1-timePercentage) + traj2.nodes[traj2Index][1]* timePercentage
            traj2Y_interpolated = previousY * (1-timePercentage) + traj2.nodes[traj2Index][2]* timePercentage


            traj2_all.append((traj1.nodes[traj1Index][0], traj2X_interpolated, traj2Y_interpolated))
            traj1_all.append((traj1.nodes[traj1Index][0], traj1.nodes[traj1Index][1], traj1.nodes[traj1Index][2]))

        if traj1.nodes[traj1Index][0] > traj2.nodes[traj2Index][0] :
            traj2Index += 1
            traj2Time = traj2.nodes[traj2Index][0]

        elif traj2.nodes[traj2Index][0] > traj1.nodes[traj1Index][0] :
            traj1Index += 1
            traj1Time = traj1.nodes[traj1Index][0]

        elif traj1.nodes[traj1Index][0] == traj2.nodes[traj2Index][0] :
            traj1Index += 1
            traj1Time = traj1.nodes[traj1Index][0]
            traj2Index += 1
            traj2Time = traj2.nodes[traj2Index][0]

    return traj1_all, traj2_all


def distOfTrajOverTime(traj) :
    dist_list = []

    for index in range(len(traj)-1) :
        time = traj[index][0]
        x1 = traj[index][1]
        y1 = traj[index][2]
        x2 = traj[index+1][1]
        y2 = traj[index+1][2]
        
        # we only use this to check whether distance = 0, so this is sufficient
        if x1 != x2 or y1 != y2:
            dist_list.append(1)
        else:
            dist_list.append(0)

    return dist_list

# returns the percentage of node pairs that are within a certain distance threshold
def evaluateTrajPair(traj1, traj2):
    traj1_all, traj2_all = getInterpolatedTrajectories(traj1, traj2)

    traj1Dist_list = distOfTrajOverTime(traj1_all)
    traj2Dist_list = distOfTrajOverTime(traj2_all)

    count = 0
    dist_all = []

    for index in range(len(traj1Dist_list)-1) :
        # handle the case where a shopper moves away from a cart temporarily
        if traj1Dist_list[index] == 0 and traj2Dist_list[index] > 0:
            continue

        x1 = traj1_all[index][1]
        y1 = traj1_all[index][2]
        x2 = traj2_all[index][1]
        y2 = traj2_all[index][2]
        temp = dist(x1, y1, x2, y2)
        if temp < 35 : # about 3 feet 8.8 * 3 = 27
            count = count + 1

        dist_all.append(temp)

    if float(len(dist_all)) == 0:
        percentage = 0
    else:
        percentage = (float(count)/float(len(dist_all)))*100

    return percentage


def evaluateTrajGroups(trajGroup):
    tomergeTrajs = []

    indexPairList = itertools.combinations(range(len(trajGroup)), 2)
    for first_traj_index, second_traj_index in indexPairList:
        traj1 = trajGroup[first_traj_index]
        traj2 = trajGroup[second_traj_index]

        if not traj1.overlapsTrajectory(traj2):
            continue
            
        percentage = evaluateTrajPair(traj1, traj2)

        if percentage >= 40:
            tomergeTrajs.append([first_traj_index, second_traj_index])

    return tomergeTrajs


def mergeTrajPairs(trajGroup, tomergeTrajs):
    trajGroupList = []
    finalTrajGroup = []

    if len(tomergeTrajs) <= 0:
        return trajGroup
    
    trajGroupList = [tomergeTrajs[0]]
    for index1, index2 in tomergeTrajs:
        inGroup = False
        for group in trajGroupList:
            if (index1 in group) or (index2 in group):
                inGroup = True
                group.append(index1)
                group.append(index2)
        if not inGroup:
            trajGroupList.append([index1, index2])

    trajGroupList = map(set, trajGroupList)
    trajGroupList = map(list, trajGroupList)
    trajGroupList = map(sorted, trajGroupList)
    
    temp = list(itertools.chain(*trajGroupList))

    for index in range(len(trajGroup)):
        if index not in temp:
            finalTrajGroup.append(trajGroup[index])

    for toCombine in trajGroupList:
        count = 1
        traj = trajGroup[toCombine[0]]
        while count < len(toCombine) :
            traj = Trajectory.combineAdvanced(traj, trajGroup[toCombine[count]])
            count = count + 1

        finalTrajGroup.append(traj)

    return finalTrajGroup



def unique(s):
    """Return a list of the elements in s, but without duplicates.

    For example, unique([1,2,3,1,2,3]) is some permutation of [1,2,3],
    unique("abcabc") some permutation of ["a", "b", "c"], and
    unique(([1, 2], [2, 3], [1, 2])) some permutation of
    [[2, 3], [1, 2]].

    For best speed, all sequence elements should be hashable.  Then
    unique() will usually work in linear time.

    If not possible, the sequence elements should enjoy a total
    ordering, and if list(s).sort() doesn't raise TypeError it's
    assumed that they do enjoy a total ordering.  Then unique() will
    usually work in O(N*log2(N)) time.

    If that's not possible either, the sequence elements must support
    equality-testing.  Then unique() will usually work in quadratic
    time.
    """

    n = len(s)
    if n == 0:
        return []

    # Try using a dict first, as that's the fastest and will usually
    # work.  If it doesn't work, it will usually fail quickly, so it
    # usually doesn't cost much to *try* it.  It requires that all the
    # sequence elements be hashable, and support equality comparison.
    u = {}
    try:
        for x in s:
            u[x] = 1
    except TypeError:
        del u  # move on to the next method
    else:
        return u.keys()

    # We can't hash all the elements.  Second fastest is to sort,
    # which brings the equal elements together; then duplicates are
    # easy to weed out in a single pass.
    # NOTE:  Python's list.sort() was designed to be efficient in the
    # presence of many duplicate elements.  This isn't true of all
    # sort functions in all languages or libraries, so this approach
    # is more effective in Python than it may be elsewhere.
    try:
        t = list(s)
        t.sort()
    except TypeError:
        del t  # move on to the next method
    else:
        assert n > 0
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]

    # Brute force is all that's left.
    u = []
    for x in s:
        if x not in u:
            u.append(x)
    return u


def combinations(iterable, r) :
    pool = tuple(iterable)
    n = len(pool)
    if r> n :
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True :
        for i in reversed(range(r)) :
            if indices[i] != i + n - r :
                break
        else :
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)



def mergeTrajectories(trajectoryFilename, outputDirectory):
# """"Usage: mergeTrajectories.py <Trajectory Files> <Output Directory>"""
    try :
        os.makedirs(outputDirectory)
    except :
        pass

    outputFilename = '%s\\%s' %(outputDirectory, trajectoryFilename.rsplit('\\', 1)[-1])
    print "Processing file: %s"%(outputFilename)
    trajList = Trajectory.loadXmlCsv(trajectoryFilename)
    trajList = TrajectoryFilters.filterMinDuration(trajList, 3)
    if len(trajList)==0:
        Trajectory.saveXmlCsv(outputFilename, trajList)
        return
    trajList.sort(sortTrajByStart)

    trajList = filterHangingTracks(trajList)
    if len(trajList)==0:
        Trajectory.saveXmlCsv(outputFilename, trajList)
        return    
    trajGroups = getTrajGroups(trajList)

    finalTrajList = []
    uniqFinalTrajList = []
    for trajGroup in trajGroups :
        if len(trajGroup) >= 2 :
            tomergeTrajs = evaluateTrajGroups(trajGroup)
            newTrajGroup = mergeTrajPairs(trajGroup, tomergeTrajs)

            for traj in newTrajGroup :
                finalTrajList.append(traj)
        else :
            finalTrajList.append(trajGroup[0])


    finalTrajList.sort(sortTrajByStart)

    temp = []

    for index in range(len(finalTrajList)) :
        temp.append(finalTrajList[index].startTime())
    tempVariable = unique(temp)
    #print len(temp), len(tempVariable)
    for time in tempVariable :
        uniqFinalTrajList.append(finalTrajList[temp.index(time)])


    Trajectory.saveXmlCsv(outputFilename, uniqFinalTrajList)
