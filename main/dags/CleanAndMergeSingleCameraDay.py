import sys
import cleanTrajectories
import mergeTrajectories
import traceback

def CleanAndMergeSingleCameraDay(inputFile, cleanedDir, mergedDir):
    try:
        # clean
        print "\n\nCLEANING TRAJECTORY FILE: %s" % (inputFile)
        cleanTrajectories.cleanTrajectories(inputFile, 35, 0.7, cleanedDir)

        camNum = inputFile.split('\\')[-1].split('_')[0]
        date = inputFile.split('\\')[-1].split('_')[-1].split('.')[0]
        inputFile = cleanedDir + "\\" + camNum + "_" + date + ".csv"

        # merge
        print "\n\nMERGING TRAJECTORY FILE: %s"%(inputFile)

        mergeTrajectories.mergeTrajectories(inputFile, mergedDir)


    except:
        exinfo = sys.exc_info()
        stack_trace = ''.join([str(exinfo[1]) + '\n', ''.join(traceback.format_tb(exinfo[2]))])
        return stack_trace

    else:
        return None