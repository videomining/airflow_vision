import csv
import numpy as np
from numpy import dot, float32, zeros, cos, sin
from math import radians, atan2, sqrt
import sys
import copy
from datetime import *
from TimeUtils import *

import xml.parsers.expat


import os.path
import time
import pdb
import VmsUtils

##################################################
def dist(p1,p2):
    return sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

def sortTrajByStart(t1, t2):
    if t1.startTime() < t2.startTime():
        return -1
    elif t1.startTime() == t2.startTime():
        return 0
    else:
        return 1

def sortTrajByStartAndId(t1, t2):
    if t1.startTime() < t2.startTime():
        return -1
    elif t1.startTime() == t2.startTime():
        if t1.getId() < t2.getId():
            return -1
        elif t1.getId() == t2.getId():
            return 0
        else:
            return 1
    else:
        return 1

def sortTrajByEnd(t1, t2):
    if t1.endTime() < t2.endTime():
        return -1
    elif t1.endTime() == t2.endTime():
        return 0
    else:
        return 1

def sortTrajByLength(t1,t2):
    if t1.length() < t2.length():
        return -1
    elif t1.length() == t2.length():
        return 0
    else:
        return 1

def sortTrajByDistDispRatio(t1,t2):
    d1 = dist(t1.startPos(),t1.endPos())
    d2 = dist(t2.startPos(),t2.endPos())
    if d1 == 0.0 or d2 == 0.0:
        if d1 == 0.0 and d2 == 0.0:
            return 0
        if d1 == 0.0:
            return 1
        else:
            return -1

    r1 = t1.length()/dist(t1.startPos(),t1.endPos())
    r2 = t2.length()/dist(t2.startPos(),t2.endPos())
    if r1 < r2:
        return -1
    elif r1 == r2:
        return 0
    else:
        return 1

def sortTrajByDistCovered(t1,t2):
    d1 = dist(t1.startPos(),t1.endPos())
    d2 = dist(t2.startPos(),t2.endPos())

    if d1 < d2:
        return -1
    elif d1 == d2:
        return 0
    else:
        return 1

def sortTrajByTimeDuration(t1,t2):
    time1 = t1.duration()
    time2 = t2.duration()
    if time1 < time2:
        return -1
    elif time1 == time2:
        return 0
    else:
        return 1

class TrajectoryHandler:
    def __init__(self):
        self.trajectory = Trajectory()
        self.startTime = 0.0
        self.stack = []

    def startElement(self, name, attrs):
        self.stack.insert(0,name)

        if self.stack[0] == 'trajectory':
            id = attrs['id']
            self.trajectory.setId(id)

            if self.startTime == 0.0:
        #Temporary workaround for incorrect attr error
                if 'start_time' in attrs:
                    self.startTime = DateStringToSecs(attrs['start_time'])
                else:
                    self.startTime = DateStringToSecs(attrs['startTime'])


    def charData(self,data):
        #e.g. 0.000,48.49,126.67;0.133,48.80,133.84;

        if len(self.stack) > 0:
            if self.stack[0] == 'trajectory':
                data = data.replace(';',',')
                splitline = data.split(',')
                #print splitline
                splitline = map(float, splitline)
                assert len(splitline) % 3 == 0

                i = 0
                for x in range(len(splitline) / 3):

                    time = self.startTime + splitline[i]
                    xPos = splitline[i+1]
                    yPos = splitline[i+2]
                    self.trajectory.nodes.append((time,xPos,yPos))
                    i += 3

    def endElement(self,name):
        self.stack.pop(0)

    def parse(self, data):
        p = xml.parsers.expat.ParserCreate()

        p.StartElementHandler = self.startElement
        p.EndElementHandler = self.endElement
        p.CharacterDataHandler = self.charData
        p.Parse(data,True)


class Trajectory:
    csvReader = None
    inputFile = None
    lastLineNum = 0
    completedFlag = 'False'

    def __init__(self, id =-1):
        self.nodes = []
        self.id = id
        self.attributes = {}

    def getId(self):
        return self.id

    def setId(self,val):
        self.id = val

    def __str__(self):
        s = 'Id: %s\n' % (self.id)
        for time,x,y in self.nodes:
            s += '%f %f %f\n' % (time,x,y)
        return s

    def isValid(self):
        if len(self.nodes) < 2:
            return True

        #if self.startTime() < self.endTime():
            #return True

        time1, xPos, yPos = self.nodes[0]

        for x in range(1,len(self.nodes)):
            time2, xPos, yPos = self.nodes[x]
            if time1 >= time2:
                return False
            time1 = time2

        return True

    def __del__(self):
        for node in self.nodes:
            del node
        del self.nodes
        del self.id

    def  loadXmlCsv(filename):
        trajList = []
        lineNum = 0
        inFile = open(filename)
        # inFile = VmsUtils.OpenRobustly(filename, "r")
	lineNum = 1
        for line in inFile:
	    lineNum+=1
            if line == '\n':
                continue
            # try:
            if line == 'Start Time,Duration,Trajectory\n' or line == 'Start Time,Duration,Trajectory\r\n':
                continue
            splitline = line.split(',',2)
            startTime = DateStringToSecs(splitline[0])
            xmlString = splitline[2].strip('"\n').strip('"\r')
            trajList.append(Trajectory.fromXmlString(xmlString,startTime))
            # except:
            # print "Error "+ filename #+ ": "+line

        return trajList

    loadXmlCsv = staticmethod(loadXmlCsv)


    def loadXmlCsvGetShopInfo(filename, numLines = -1):
        trajList = []
        demographicsData = []
        # Trajectory.inputFile = open(filename)
        try:
            Trajectory.inputFile = VmsUtils.OpenRobustly(filename, "r")
        except:
            raise IOError("Could not open '%s' after %d attempts")

        Trajectory.csvReader = csv.reader(Trajectory.inputFile)
        try:
            csv.field_size_limit(sys.maxsize)
        except:
            csv.field_size_limit(sys.maxint)
##    for line in inFile:
##      try:
##        if line == 'Start Time,Duration,Trajectory\n':
##          continue
####        splitline = line.split(',')
##        startTime = DateStringToSecs(line[0])
##        xmlString = line[2].strip('"\n')
##
##        trajList.append(Trajectory.fromXmlString(xmlString,startTime))
##      except:
##        print "Error "+ filename #+ ": "+line
        if numLines != -1:
            trajList,demographicsData = Trajectory.nextLines(numLines)
        else:
            extendTrajList,extendDemographicsData = Trajectory.nextLines(1000)
            while extendTrajList is not None:
                trajList.extend(extendTrajList)
                demographicsData.extend(extendDemographicsData)
                extendTrajList,extendDemographicsData = Trajectory.nextLines(1000)

        return trajList, demographicsData

    loadXmlCsvGetShopInfo = staticmethod(loadXmlCsvGetShopInfo)

    def nextLines(numLines):
        numLinesRead = 0
        trajList = None
        demographicsData = []
        try:
            if Trajectory.csvReader is None:
                if not Trajectory.completedFlag:
                    print "Csv reader has been reloaded since the file handler has gone bad"
                    Trajectory.csvReader = csv.reader(Trajectory.inputFile)
                    for i in range(Trajectory.lastLineNum):

                        Trajectory.csvReader.next()
                else:
                    return trajList, demographicsData
            if Trajectory.csvReader is not None:
                trajList = []
                demographicsData = []
                for i in range(numLines):
                    numLinesRead += 1
                    line = Trajectory.csvReader.next()
                    # if i == 0 or i==numLines-1:
                        # print "i = %d"%i
                        # print line[0:2]
                    if 'Start Time' in line:
                        continue
                    else:
                        startTime = DateStringToSecs(line[0])
                        xmlString = line[2].strip('"\n')
                        trajList.append(Trajectory.fromXmlString(xmlString,startTime))
                        if line[-3] == 'Male' or line[-3] == 'Female':
                            demographicsData.append(line[-3:])
                        else:
                            demographicsData.append(['','',''])
                Trajectory.lastLineNum += numLines
        except StopIteration:
            print "Completed"
            Trajectory.completedFlag =  True
            Trajectory.lastLineNum += numLinesRead
            print "Total Number of lines processed %r" %Trajectory.lastLineNum
            Trajectory.lastLineNum = 0
            Trajectory.inputFile.close()
            del Trajectory.inputFile
            del Trajectory.csvReader
            Trajectory.inputFile = None
            Trajectory.csvReader = None
        except:
            Trajectory.completedFlag =  True
            print "Unexpected error:", sys.exc_info()[0]
            raise IOError("Error while reading csv file. Check stack trace")

        return trajList, demographicsData
    nextLines = staticmethod(nextLines)


    def loadXmlCsvGenerator(filename):

        # inFile = open(filename)
        inFile = VmsUtils.OpenRobustly(filename, "r")
        inFile.readline()

        for line in inFile:
                # try:
            splitline = line.split(',',2)

            startTime = DateStringToSecs(splitline[0])
            xmlString = splitline[2].strip('"\n')
            traj = Trajectory.fromXmlString(xmlString,startTime)
            yield traj
            del traj

            # except:
                # print "Error "+ filename #+ ": "+line

        inFile.close()

    loadXmlCsvGenerator = staticmethod(loadXmlCsvGenerator)

    def saveXmlCsv(filename, trajList):

        outFile = file(filename,'w')
        outFile.write('Start Time,Duration,Trajectory\n')

        trajIndex = 0
        for traj in trajList:
            trajIndex += 1
            try:
                startTimeString = str(secsToDate(traj.startTime())).replace(' ','T')
                outFile.write('%s,%.3f,"%s"\n' % (startTimeString, traj.duration(), traj.toXmlString()))
            except:
                print trajIndex,traj

        outFile.close()

    saveXmlCsv = staticmethod(saveXmlCsv)

    def toXmlString(self):

        startTime = self.startTime()
        startTimeString = str(secsToDate(startTime)).replace(' ','T')
        startTag = "<trajectory id='%s' start_time='%s'>" % (self.id,startTimeString)

        bodyText = ''

        for (time, xPos, yPos) in self.nodes:
            bodyText += '%.3f,%.3f,%.3f,' % (time-startTime,xPos,yPos)
        bodyText = bodyText[:-1]
        endTag = '</trajectory>'

        return '%s%s%s' % (startTag,bodyText,endTag)

    def fromXmlString(xmlString, startTime = 0.0):
        h = TrajectoryHandler()
        h.startTime = float(startTime)
        h.parse(xmlString)
        return h.trajectory
    fromXmlString = staticmethod(fromXmlString)

    def maxDisp(self):
        if len(self.nodes) <= 1:
            return 0.0



        assert len(self.nodes) > 1

        maxVal = 0.0
        startPoint = (self.nodes[0][1],self.nodes[0][2])
        endPoint = (self.nodes[-1][1],self.nodes[-1][2])

        for x in range(1,len(self.nodes)):
            p1 = (self.nodes[x][1], self.nodes[x][2])
            d1 = dist(p1,startPoint)
            d2 = dist(p1,endPoint)
            maxVal = max(d1,d2,maxVal)


        return maxVal

    def maxDispXY(self):
        if len(self.nodes) <= 1:
            return 0.0



        assert len(self.nodes) > 1

        maxX = 0.0
        maxY = 0.0

        startPoint = (self.nodes[0][1],self.nodes[0][2])
        endPoint = (self.nodes[-1][1],self.nodes[-1][2])

        for time,x,y in self.nodes:
            dispX1 = math.fabs(x - startPoint[0])
            dispY1 = math.fabs(y - startPoint[1])
            dispX2 = math.fabs(x - endPoint[0])
            dispY2 = math.fabs(y - endPoint[1])

            maxX = max(dispX1,dispX2,maxX)
            maxY = max(dispY1,dispY2,maxY)


        return (maxX,maxY)

    def length(self):
        if len(self.nodes) <= 1:
            return 0.0

        assert len(self.nodes) > 1

        sum = 0.0

        for x in range(1,len(self.nodes)):
            p1 = (self.nodes[x-1][1], self.nodes[x-1][2])
            p2 = (self.nodes[x][1], self.nodes[x][2])
            sum += dist(p1,p2)
        return sum


    def combine(t1,t2):
        """Assumes t1 < t2 and combines them, ignoring any part of t2 that is before
        the end of t1"""
        newTraj = Trajectory(t1.getId())

        lastTime = t1.nodes[-1][0]

        foundJoinNode = False
        for x in range(len(t2.nodes)):
            if t2.nodes[x][0] > lastTime:
                foundJoinNode = True
                break

        newTraj.nodes.extend(t1.nodes)
        if foundJoinNode:
            newTraj.nodes.extend(t2.nodes[x:])

        return newTraj
    combine = staticmethod(combine)

    def isSorted(self):
        for index in range(len(self.nodes) - 1):
            if self.nodes[index][0] > self.nodes[index + 1][0]:
                return False
        return True
        
    
    def overlapsTrajectory(self, traj):
        return self.endTime() >= traj.startTime() and traj.endTime() >= self.startTime()


    @staticmethod
    def combineAdvanced(t1, t2):
        if not t1.overlapsTrajectory(t2):
            raise ValueError("combineAdvanced: trajectories do not overlap")
    
        newTraj = Trajectory(t1.getId())
        t1startTime = t1.nodes[0][0]
        t2startTime = t2.nodes[0][0]

        t1endTime = t1.nodes[-1][0]
        t2endTime = t2.nodes[-1][0]

        index = 0

        # add all nodes from t1 that come before t2 if they overlap
        if t1endTime > t2startTime:
            # while t1startTime < t2startTime:
            while t1.nodes[index][0] < t2startTime:
                currentNode = t1.nodes[index]
                t1startTime = currentNode[0]
                newTraj.nodes.append(currentNode)
                index = index + 1

        # if they don't overlap, all nodes from t1 before t2's start time are thrown away for some reason
        elif t1endTime <= t2startTime:
            pass

        # add all nodes from t2
        newTraj.nodes.extend(t2.nodes)

        # add all nodes from t1 that come after t2
        # while t1startTime < t2endTime and index < len(t1.nodes):
        while index < len(t1.nodes) and t1.nodes[index][0] < t2endTime:
            currentNode = t1.nodes[index]
            t1startTime = currentNode[0]
            index = index + 1
        newTraj.nodes.extend(t1.nodes[index:])

        return newTraj


    def append(self,t1):
        """Assumes self < t1 and combines them, ignoring any part of t1 that is before
        the end of self"""

        lastTime = self.nodes[-1][0]

        foundJoinNode = False
        for x in range(len(t1.nodes)):
            if t1.nodes[x][0] > lastTime:
                foundJoinNode = True
                break

        if foundJoinNode:
            self.nodes.extend(t1.nodes[x:])


    def startPos(self):
        return (self.nodes[0][1],self.nodes[0][2])

    def endPos(self):
        return (self.nodes[-1][1],self.nodes[-1][2])

    def startTime(self):
        return self.nodes[0][0]

    def endTime(self):
        return self.nodes[-1][0]

    def duration(self):
        return self.endTime() - self.startTime()


    def offsetTime(self,timeOff):
        for x in range(len(self.nodes)):
            time, xPos, yPos = self.nodes[x]
            time = time + timeOff
            self.nodes[x] = (time, xPos, yPos)

    #Transform each trajectory with the following function
    def transform(self, transform):
        for x in range(len(self.nodes)):
            time, xPos, yPos = self.nodes[x]

            mSize = transform.shape[0]
            pointMatrix = zeros(mSize,float32)
            pointMatrix[0] = float(xPos)
            pointMatrix[1] = float(yPos)
            pointMatrix[mSize-1] = 1.0


            point = dot(transform, pointMatrix)
            self.nodes[x] = (time, point[0], point[1])

    def unDistort(self, values, size):
        import cv2
        DistCoeffs_arr = values[0:6]
        fx = values[6]
        fy = values[7]
        calibImageSize_width = values[8]
        calibImageSize_height = values[9]
        LENS_OFFSET_ZOOM_COMPENSATION = values[10]
        UNDISTORT_SCALE_FACTOR = values[11]
        isFishEye = values[12]

        cx = size[0] * 0.5
        cy = size[1] * 0.5

        fx *= (size[0] / calibImageSize_width) * LENS_OFFSET_ZOOM_COMPENSATION
        fy *= (size[1] / calibImageSize_height) * LENS_OFFSET_ZOOM_COMPENSATION

        UNDISTORT_SCALE_FACTOR *= LENS_OFFSET_ZOOM_COMPENSATION

        # cameraMatrix = numpy.zeros(shape=(3,3))
        # cameraMatrix = np.zeros((3,3), dtype=np.float64)
        # cameraMatrix[0,0] = fx
        # cameraMatrix[1,1] = fy
        # cameraMatrix[0,2] = cx
        # cameraMatrix[1,2] = cy
        # cameraMatrix[2,2] = 1


        # cameraMatrix = np.array([[1.65e+02, 0., 1.6e+02], [0., 1.65e+02, 1.2e+02], [0., 0., 1.]], dtype=np.float32)
        cameraMatrix = np.array([[fx, 0., cx], [0., fy, cy], [0., 0., 1]], dtype=np.float64)
        # cameraMatrix = np.mat([[np.float64(fx), np.float64(0), np.float64(cx)], [np.float64(0), np.float64(fy), np.float64(cy)], [np.float64(0),np.float64(0), np.float64(1)]], dtype=np.float64)
        # cameraMatrix = np.mat([[np.float64(fx), np.float64(0), np.float64(cx)], [np.float64(0), np.float64(fy), np.float64(cy)], [np.float64(0),np.float64(0), np.float64(1)]], dtype=np.float64)

        if isFishEye:
            distCoeffs = np.zeros((4, 1), dtype=np.float64)
            distCoeffs[0, 0] = DistCoeffs_arr[0]
            distCoeffs[1, 0] = DistCoeffs_arr[1]
            distCoeffs[2, 0] = DistCoeffs_arr[2]
            distCoeffs[3, 0] = DistCoeffs_arr[3]
            # distCoeffs = np.array([DistCoeffs_arr[0:3]])
            # distCoeffs.shape = (4,1)
        else:
            # distCoeffs = np.zeros((8,1), dtype=np.float64)
            # distCoeffs[0,0] = DistCoeffs_arr[0]
            # distCoeffs[1,0] = DistCoeffs_arr[1]
            # distCoeffs[2,0] = DistCoeffs_arr[2]
            # distCoeffs[3,0] = DistCoeffs_arr[3]
            # distCoeffs[4,0] = DistCoeffs_arr[4]
            # distCoeffs[5,0] = DistCoeffs_arr[5]

            distCoeffs = np.array(
                [[DistCoeffs_arr[0]], [DistCoeffs_arr[1]], [DistCoeffs_arr[2]], [DistCoeffs_arr[3]],
                 [DistCoeffs_arr[4]], [DistCoeffs_arr[5]], [0], [0]], dtype=np.float64)
        newCameraMatrix = copy.deepcopy(cameraMatrix)
        newCameraMatrix[0, 0] *= UNDISTORT_SCALE_FACTOR  # UNDISTORT_SCALE_FACTOR = zoom
        newCameraMatrix[1, 1] *= UNDISTORT_SCALE_FACTOR

        src_norm_mat = np.zeros((1, 1, 2), dtype=np.float32)
        # src_norm_mat2 = np.zeros((1,1,2), dtype=np.float32)
        dst_norm_mat = np.zeros((1, 1, 2), dtype=np.float32)
        Mat = np.array([], dtype=np.float64)

        # nodeset1 = copy.deepcopy(self.nodes)
        for x in range(len(self.nodes)):
            time, xPos, yPos = self.nodes[x]
            src_norm_mat[0, 0, 0] = xPos
            src_norm_mat[0, 0, 1] = yPos
            if isFishEye:
                cv2.fisheye.undistortPoints(src_norm_mat, dst_norm_mat, cameraMatrix, distCoeffs, Mat,
                                            newCameraMatrix)
            else:
                cv2.undistortPoints(src_norm_mat, cameraMatrix, distCoeffs, dst_norm_mat, Mat, newCameraMatrix)

            # dtype = np.float32
            # cv2.distortPoints(dst_norm_mat, cameraMatrix, distCoeffs, src_norm_mat2, Mat, newCameraMatrix)
            self.nodes[x] = (time, dst_norm_mat[0, 0, 0], dst_norm_mat[0, 0, 1])

            # nodeset2 = copy.deepcopy(self.nodes)
            #
            # if isFishEye:
            #     cv2.fisheye.undistortPoints(src_norm_mat, dst_norm_mat, cameraMatrix, distCoeffs, Mat, newCameraMatrix)
            # else:
            #     map1,map2 = cv2.initUndistortRectifyMap(cameraMatrix, distCoeffs, None, newCameraMatrix, (int(size[0]),int(size[1])) , cv2.CV_32F)
            #
            # ognodes = []
            #
            # for x in range(len(self.nodes)):
            #     time, xPos, yPos = self.nodes[x]
            #     newxPos = map1[int(yPos),int(xPos)]
            #     newyPos = map2[int(yPos),int(xPos)]
            #     ognodes.append((time, newxPos, newyPos))
            # size = [320,240]
            # new_cam_matrix, valid_roi = cv2.getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, size, 1)
            # Distort this trajectory by the values a, b, and c.
            # Uses formula in polar coordinates: r' = a*r^7 + b*r^5 + c*r^3 + d*r
    #Distort this trajectory by the values a, b, and c.
    #Uses formula in polar coordinates: r' = a*r^7 + b*r^5 + c*r^3 + d*r
    def distort(self, values, size):
        #a,b,c,d = values

        #if size[0] != 320 or size[1] != 240:
        #  raise Exception("Invalid Image Size")

        for x in range(len(self.nodes)):
            time, xPos, yPos = self.nodes[x]

            #Normalize x,y values between [-1,1] so that
            # same a,b,c,d parameters can be used regardless of
            # resolution
            xPos = float(2.0*xPos/size[0] - 1.0)
            yPos = float(size[1])/size[0] - 2.0*yPos /size[0]

            theta = 0.0
            rs = 0.0
            #Check for degenerate case
            #TODO: may not be numerically sound for very small xPos?
            if xPos == 0.0 and yPos == 0.0:
                theta = 0.0
                rs = 0.0
            elif xPos == 0.0:
                if yPos > 0.0:
                    theta = radians(90.0)
                    rs = yPos
                else:
                    theta = radians(-90.0)
                    rs = -yPos
            else:
                theta = atan2(yPos,xPos)
                rs = sqrt(xPos**2 + yPos**2)

            rd = 0
            exponent = len(values)-1
            for coeff in values:
                rd += coeff * (rs**exponent)
                exponent -= 1

            self.nodes[x] = (time, (size[0]/2.0)*(1.0+rd*cos(theta)), (size[0]/2.0)*(float(size[1])/size[0]-rd*sin(theta)))

    #distort radially.
    def displaceRadial(self,(centerX,centerY),factor):

        for x in range(len(self.nodes)):
            time, xPos, yPos = self.nodes[x]
            r = sqrt((xPos-centerX)**2+(yPos-centerY)**2)
            theta = radians(90.0)
            if xPos - centerX != 0:
                theta = atan2(yPos-centerY,xPos-centerX)
            elif yPos - centerY < 0:
                theta = theta*-1.0

            newR = factor*r
            self.nodes[x] = (time,newR*cos(theta) + centerX,newR*sin(theta) + centerY)

    #assumes records in the file are grouped by trajectoryId
    def loadCSV(filename):
        inFile = open(filename)
        trajList = []

        for line in inFile:
            if line == 'TrajectoryId,Time,X,Y\n':
                continue
            splitLine = line.rstrip('\n').split(',')

            id = int(splitLine[0])

            if len(trajList) == 0 or trajList[-1].getId() != id:
                trajList.append(Trajectory(id))

            time = float(splitLine[1])
            xPos = float(splitLine[2])
            yPos = float(splitLine[3])

            trajList[-1].nodes.append((time,xPos,yPos))

        inFile.close()

        return trajList
    loadCSV = staticmethod(loadCSV)

    def saveCSV(filename, trajList):
        outFile = file(filename, 'w')
        outFile.write('TrajectoryId,Time,X,Y\n')

        for traj in trajList:
            id = traj.getId()

            for time,xpos,ypos in traj.nodes:
                outFile.write('%d,%f,%f,%f\n' % (id,time,xpos,ypos))

        outFile.close()
    saveCSV = staticmethod(saveCSV)
