import MySQLdb
import datetime
from datetime import datetime, timedelta
import sys
import xml.etree.ElementTree as ET

def connectToDB():
    db = MySQLdb.connect(host = 'vmvisiondb',user = 'vmdatasvc',passwd = 'irrelev@nt',db = 'calibration')
    storeName = 'ahld-6097_calib'
    cursor = db.cursor()

def _isFloorFixture(fixture_element):
    flag = 0
    if int(float(fixture_element.find('z0').text)) == 0:
        flag = 1
    return flag


def getfurnitureFiles(cursor,storeName):
    query = '''
       SELECT project_info.name,project.version, project.data,project.start_date, project.end_date
        from calibration.project_info
        INNER JOIN calibration.project
        ON project_info.project_id=project.id
        where name = '<store_name_calib>';'''

    sql = query.replace('<store_name_calib>', storeName)
    cursor.execute(sql)
    fetchall= cursor.fetchall()
    furnitureDict = {}
    for element in fetchall:
        startdate = datetime.strftime(element[3],'%Y%m%d')
        enddate = datetime.strftime(element[4],'%Y%m%d')
        date = startdate +'_'+ enddate
        if date not in furnitureDict:
            furnitureDict[date] = {}

        strcamxml = element[2]

        tree = ET.ElementTree(ET.fromstring(strcamxml))
        treeall = tree.findall('camera_views/camera_view')
        lineset = []
        for camview in treeall:
            #camera_name = camview.find('name').text
            type_label = camview.find('type_label').text
            if cmp(type_label, 'Floorplan') == 0 :
                fixture_info = camview.find('floorplan_info/fixtures', camview)
                for fixture_element in fixture_info :
                    flag = _isFloorFixture(fixture_element)
                    if flag :
                        fixture_points = fixture_element.findall('points')
                        fixtures = map(lambda x : x.text, fixture_points)
                        for fixture_element in fixtures :
                            list_coords = fixture_element.strip('\n').split(',')
                            list_coords = map(lambda x : x.split('.')[0], list_coords)
                            no_points = len(list_coords)/2
                            for index in range(no_points-1) :
                                line = list_coords[2*index] + ',' + list_coords[2*index+1] + ',' + list_coords[2*index+2] + ',' + list_coords[2*index+3]
                                lineset.append(line)
                            last_line = list_coords[-2] + ',' + list_coords[-1] + ',' + list_coords[0] + ',' + list_coords[1]
                            lineset.append(last_line)
                            furnitureDict[date] = lineset
    return furnitureDict
