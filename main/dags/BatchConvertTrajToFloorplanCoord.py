import os
import sys
import datetime
import pdb

import traceback


import CalibXML
import WorldToLocalXML

def _parseDateArgv(argv):
    List = []
    timeDelta = datetime.timedelta(1,0,0)
    while len(argv) > 0:
        if argv[0] == 'A':
            date = argv[1]
            argv = argv[2:]
            List.append(datetime.datetime(int(date[0:4]),int(date[4:6]), int(date[6:]), 0, 0, 0, 0))
        elif argv[0] == 'R':
            startDate = argv[1]
            stopDate = argv[2]
            argv = argv[3:]
            startDate = datetime.datetime(int(startDate[0:4]),int(startDate[4:6]), int(startDate[6:]), 0, 0, 0, 0)
            stopDate = datetime.datetime(int(stopDate[0:4]),int(stopDate[4:6]), int(stopDate[6:]), 0, 0, 0, 0)
            currDate = startDate
            while currDate <= stopDate:
                List.append(currDate)
                currDate = currDate + timeDelta
        else:
            sys.exit("Error: format unsupported. correct format: tag A <singleVal> /tag -OR- tag R <startVal> <stopVal> /tag")
    return List

def _parseCamArgv(argv):
    List = []
    while len(argv) > 0:
        if argv[0] == 'A':
            cam = argv[1]
            argv = argv[2:]
            List.append(cam)
            
        elif argv[0] == 'R':
            startCam = argv[1]
            stopCam = argv[2]
            argv = argv[3:]
            currCam = int(startCam)
            while currCam <= int(stopCam):
                List.append(str(currCam))
                currCam = int(currCam) + 1
        else:
            sys.exit("Error: format unsupported. correct format: tag A <singleVal> /tag -OR- tag R <startVal> <stopVal> /tag")
    return List

def main( mergedDir, worldDir, floorplanDir, cams, camType, floorXML, camXMLtoSend, dates):
    try:
        distort = 1
        distortionParams = "0.66,0,0.0,0,0.30,0.24,0.96,0"
        distortionParamsDict = {'212_4x3': ".2315890601951167,-0.01014790881235929,0,0,0.0003533324593994937,0.54676298923663,254.4,254.4,640,480,1.0,0.93,0,1.0",\
                                '212': "0.66,0,0.0,0,0.30,0.24,0.96,0",\
                                '3006': "0.03689,0,0.12335,0,0.11827,0.21814,0", \
                                'omni': "0.153372,0,0.264268,0,0.4133516,0,1.336574,0",\
                                'omni_4x3': "0.2462086051644309,-0.01518149992622307,0,0,0.0007750245892998894,0.6051077649755596,321.422,321.422,640,480,1.1,1.5,0,1.7",\
                                # '6500': "0.1256,0,.20109,0,0.41509,0,1.101568,0",\
                                '6500':"0.0916,0,.20109,0,0.33509,0,.821568,0"}
                                # '3006': "0.07378,0,0.2467,0,0.23654,0,0.43628,0"}
        distortionParams = distortionParamsDict[camType]

        cams = _parseCamArgv(cams)
        dateList = _parseDateArgv(dates)

        fNames = []
        fList = []
        for cam in cams :
            while len(cam) < 3 :
                cam = '0' + cam
            inputDir = mergedDir
            camName = "cam%s" % cam

            for date in dateList:
                date = date.strftime('%Y%m%d')
                fNames.append("/cam" + cam + "_" + date + ".csv")
                inputFile = inputDir + "/cam" + cam + "_" + date + ".csv"
                fList.append(camName + ":" + inputFile)
        print("\n\nConvert merged trajectories to world coordinates")
        outputDir = worldDir

        CalibXML.CalibXMLNew(camXMLtoSend, fList, outputDir, int(distort), distortionParams,camType)


        print("\n\nConvert merged trajectories from world coordinates to floorplan coordinates")
        outputDir = floorplanDir
        for file in fNames:
            WorldToLocalXML.WorldToLocalXML(floorXML, worldDir+file, outputDir)

    except:
        exinfo = sys.exc_info()
        stack_trace = ''.join([str(exinfo[1]) + '\n', ''.join(traceback.format_tb(exinfo[2]))])
        return stack_trace

    else:
        return None




if __name__ == "__main__":
    if len(sys.argv) < 15:
        sys.exit('Usage: %s <calibFile> <mergedDir> <worldDir> <floorplanDir> cam R <camStart> <camStop> A <cam1> /cam date R <startDate> <stopDate> A <date> /date drawTrajOnFloorplan fractionTrajToBeDrawn'%(__file__))
    calibrationFile = sys.argv[1]
    mergedDir = sys.argv[2]
    worldDir = sys.argv[3]
    floorplanDir = sys.argv[4]

    idxStart = sys.argv.index('cam')
    idxStop = sys.argv.index('/cam')
    cams = sys.argv[idxStart+1:idxStop]

    idxStart = sys.argv.index('date')
    idxStop = sys.argv.index('/date')
    dates = sys.argv[idxStart+1:idxStop]

    drawTraj = sys.argv[idxStop + 1]
    fractionTraj = sys.argv[idxStop + 2]

    main(calibrationFile, mergedDir, worldDir, floorplanDir, cams, dates, drawTraj, fractionTraj)
