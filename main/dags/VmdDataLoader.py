import copy
import random
import sys

import csv, time, datetime, os, string
import MySQLdb
import posseUtils
import pdb
import exceptions
import errno
import utilsAmazonS3
import pickle

from MongoUtils import MongoUtils
def stringToTime(string):
    # print string
    if isinstance(string, datetime.datetime):
        return string
    l = len(string) 
    if l == 8:
        return datetime.datetime(*(time.strptime(string, '%Y%m%d')[0:6]))
    elif l == 10:
        return datetime.datetime(*(time.strptime(string, '%Y-%m-%d')[0:6]))
    dt = datetime.datetime(int(string[0:4]),int(string[5:7]),int(string[8:10]),
        int(string[11:13]),int(string[14:16]),int(string[17:19]))
    return dt
  
def dateToFileName(d):

    if type(d) == type('some string'):
        return d
    else:
        return time.strftime('%Y%m%d',d.timetuple())

def dateToString(d):
    return time.strftime('%Y-%m-%dT%H:%M:%S',d.timetuple())
    
def datetimeToSql(d):
    return time.strftime('%Y-%m-%d %H:%M:%S',d.timetuple())

def dateToSql(d):
    return time.strftime('%Y-%m-%d',d.timetuple())
    
def sqlDiv(x,y):
    if y == 0:
        return "NULL"
    return "%.2f" % (x/y)
    
def sqlNullIfZero(x):
    if x == 0:
        return "NULL"
    return "%.2f" % x

def countOccurences(findItem, itemList):
    occurences = 0
    for item in itemList:
        if item == findItem:
            occurences += 1
            
    return occurences

def mapRound(itemList):
    newItemList = []
    for item in itemList:
        try:
            newItemList.append(int(round(item)))
        except:
            newItemList.append(None)

    return newItemList

def min_gt(seq, val):
    return min([v for v in seq if v > val])

def mapScale(itemList):
    newItemList = []
    minValue = min_gt(itemList, 0)
    if minValue > 0:
        scaleFactor = 1.0/minValue
    else:
        scaleFactor = 0
    for item in itemList:
        try:
            if item != 0 and scaleFactor != 0 and int(round(item*scaleFactor)) == 0:
                print item, scaleFactor, item*scaleFactor, int(round(item*scaleFactor))
            newItemList.append(int(round(item*scaleFactor)))
        except:
            newItemList.append(None)
    if addList(itemList) > 0 and addList(newItemList) == 0:
        print itemList, minValue, scaleFactor, newItemList, '\n'
    return newItemList

def addList(addList):
    total = 0

    for item in addList:
        try:
            total += item
        except:
            pass
    return total

def adjustCountSegments(counts, valueIndexes):
    newCounts = {}
    
    segmentCategories = [['male','female'], ['child','youngadult','adult','senior'], ['africanamerican','caucasian','hispanic','oriental']]
    for segment in counts.keys():
        newCounts[segment] = counts[segment]
    
    for category in segmentCategories:
        for valueIndex in valueIndexes:
            for date in counts[None].keys():
                targetValue = counts[None][date][valueIndex]
                valueList = []
                for segment in category:
                    valueList.append(counts[segment][date][valueIndex])
                
                adjustedValues = adjustCountSegmentValues(targetValue, valueList)
##                print targetValue, valueList, adjustedValues
                printStr = '%d,' % (targetValue)
                for segmentIndex in range(len(adjustedValues)):
                    newCounts[category[segmentIndex]][date][valueIndex] = adjustedValues[segmentIndex]
                    printStr += ',%d' % newCounts[category[segmentIndex]][date][valueIndex]
##                print adjustedValues
##                if targetValue != 0:
##                    print printStr
    return newCounts
    
def adjustCountSegmentValues(target, values):
    roundedVals = copy.deepcopy(values)
    total = addList(roundedVals)
    difference = target-total
    
    if total > 0:
        # ratios = map(lambda x : x/total, roundedVals)
        ratios = [val / total for val in roundedVals]
        # changes = map(lambda x : x * difference, ratios)
        changes = [ratio * difference for ratio in ratios]
        changes = mapRound(changes)

        for i in range(len(roundedVals)):
            roundedVals[i] += changes[i]

        prevValue = addList(roundedVals)
        
        # mapScale returns a list of values normalized such that 1 unit represents the minimum value in the
        # original list e.g. [0.5, 1, 1.5] -> [1, 2, 3]
        roundedVals = mapScale(roundedVals)
        
        
        difference = target - addList(roundedVals)
        if difference < 0:
            change = -1
        else:
            change = 1
            
        # add change to each bin until it makes up the difference
        if difference != 0:
            changes = map(lambda x : abs(x * difference), ratios)
            sortedChanges = copy.deepcopy(changes)
            sortedChanges.sort()
            
            indexDiff = 1
            changesMade = 0
            i = 0
            while changesMade < (int(abs(difference))):
                roundedVals[i] = int(roundedVals[i]) + change
                if roundedVals[i] < 0:
                    roundedVals[i] -= change
                else:
                    changesMade += 1
                i = (i+1) % len(sortedChanges)
    roundedVals = mapRound(roundedVals)
    return roundedVals
    

def adjustValues(target, values):
    roundedVals = copy.deepcopy(values)
    total = addList(roundedVals)
    difference = target-total
    
    if total > 0:
        # ratios = map(lambda x : x/total, roundedVals)
        ratios = [val / total for val in roundedVals]
        # changes = map(lambda x : x * difference, ratios)
        changes = [ratio * difference for ratio in ratios]
        changes = mapRound(changes)

        for i in range(len(roundedVals)):
            roundedVals[i] += changes[i]
            
        timRoundedVals = [ratio * target for ratio in ratios]
        
        # if roundedVals != timRoundedVals:
            # print roundedVals
            # print timRoundedVals
            # pdb.set_trace()

        prevValue = addList(roundedVals)
        
        # mapScale returns a list of values normalized such that 1 unit represents the minimum value in the
        # original list e.g. [0.5, 1, 1.5] -> [1, 2, 3]
        roundedVals = mapScale(roundedVals)
        
        
        difference = target - addList(roundedVals)
        if difference < 0:
            change = -1
        else:
            change = 1
            
        # add change to each bin until it makes up the difference
        if difference != 0:
            changes = map(lambda x : abs(x * difference), ratios)
            sortedChanges = copy.deepcopy(changes)
            sortedChanges.sort()
            
            indexDiff = 1
            changesMade = 0
            i = 0
            while changesMade < (int(abs(difference))):
                roundedVals[i] = int(roundedVals[i]) + change
                if roundedVals[i] < 0:
                    roundedVals[i] -= change
                else:
                    changesMade += 1
                i = (i+1) % len(sortedChanges)
    roundedVals = mapRound(roundedVals)
    return roundedVals

def validShopTime(storeId, regionId, shopTime):
    isValid = True
    
    if regionId == 'candy' and shopTime >= 90:
        isValid = False
    if regionId == 'canprepared' and shopTime >= 100:
        isValid = False
    if 'frozenss' in regionId and shopTime >= 120:
        isValid = False
    
    return isValid

class VMDDataLoader:

    # db = None
    # dataDir = None
    # startTime = stringToTime("2010-06-30")
    # endTime = stringToTime("2010-07-14")
    # storeId = None
    # storeRegionId = None
    # segmentId = None
    # segmentString = ''
    # tableString = ''
    # tableName = ''
    debug = 0

    def __init__(self, dataDir, databaseName,siteId):
        self.settings = posseUtils.visionDBSettings()
        self.db = MySQLdb.connect(host=self.settings.Host,\
                                  user=self.settings.User,\
                                  passwd=self.settings.Password,\
                                  db=databaseName)
        self.dataDir = dataDir
        self.schema = databaseName
        self.navTimeDict = {}
        self.storeId = siteId
        self.errorFile = 'missed_files_%s.txt'%(siteId)
    
    def __del__(self):
        c = self.db.cursor()
        c.close()
        self.db.close()
    
    def setTimeRange(self,start,end):
        self.startTime = stringToTime(start)
        self.endTime = stringToTime(end)
        print self.startTime
        print self.endTime
        # pdb.set_trace()
    
    def _execute(self,sql):
        # print sql
        if self.debug:
            return
        c = self.db.cursor()
        c.execute(sql)
    
    def commit(self):
        self.db.commit()

    def createTable(self, tableName):
        if tableName == 'stats_1h_new':
            self.tableName = tableName
            return
        self.tableName = tableName
        sqlCmd = "CREATE TABLE IF NOT EXISTS %s LIKE c_safeway_phase2categories.stats_1h_new"%(tableName)
        c = self.db.cursor()
        c.execute(sqlCmd)


    def _getDbScalar(self,sql):
        c = self.db.cursor()
        c.execute(sql)
        rows = c.fetchall()
        if len(rows) != 1:
            raise Exception("Result not found for: " + sql)
        return rows[0][0]
    def _getDbMultiple(self,sql):
        c = self.db.cursor()
        c.execute(sql)
        rows = c.fetchall()
        if len(rows) != 1:
            raise Exception("Result not found for: " + sql)
        return rows[0][0],rows[0][1]

    def _getDbRows(self,sql):
        c = self.db.cursor()
        c.execute(sql)
        rows = c.fetchall()
        return rows

    def _getTimeDict(self,start,end,delta,defaultVal):
        dict = {}
        t = start
        while t < end:
            dict[t] = copy.deepcopy(defaultVal)
            t+=delta;
        return dict

    def _addVal(self,dict, time, value):
        hour = datetime.datetime(time.year,time.month,time.day,time.hour,0,0)
        dict[hour] = dict[hour] + value

    def _addColVal(self, dict, colIndex, time, value):
        try:
            hour = datetime.datetime(time.year,time.month,time.day,time.hour,0,0)
            row = dict[hour]
            row = row[0:colIndex] + [row[colIndex] + value] + row[colIndex+1:]
            dict[hour] = row
        except:
            # pdb.set_trace()
            pass
            
        
    def _shopTimeSlot(self, time):
        if time < 10:
            return 1
        if time < 20:
            return 2
        if time < 30:
            return 3
        if time < 40:
            return 4
        return 5

    def _shopTime2Slot(self, time):
        if time >=40 and time < 50:
            return 1
        if time >=50 and time < 60:
            return 2
        if time >= 60 and time < 70:
            return 3
        if time >= 70 and time < 80:
            return 4
        if time >= 80 and time < 90:
            return 5
        if time >= 90:
##        if time <100:
            return 6
            
        return 7
##        if time < 150:
##            return 7
##        if time < 180:
##            return 8
##        
##        return 9
    
    def _getDatabaseNodeId(self, storeId):
        nodeId = int(self._getDbScalar("SELECT node_id FROM node WHERE store_id=%s"%(storeId)))
        return nodeId
        
    '''    
    def _getDatabaseNodeIdNew(self, platform, siteId, categoryId):
        # pdb.set_trace()
        if categoryId.find('aisle') == -1:
            mappingId, locId, regionId, locDesc, name = categoryId.split("#")
            nodeId = "%s#%s#%s#%s" % (platform, siteId, regionId, locId)
        else:
            # begin fugly hack for aisles. this is because currently the aisles extracted into getshop regions do not have the names of the regions in them. just the name of the
            # floorplan
            nodeId = "%s#%s#%s"%(platform, siteId, categoryId)
        return nodeId
    '''
    def _getDatabaseNodeIdNew(self, siteId, categoryId):
        mappingId, locId, regionId, locDesc, name = categoryId.split("#")
        getNodeIdSql = """
                        SELECT 
                            node_id 
                        FROM 
                            region_info.node
                        WHERE 
                            store_id = '%s' AND
                            region_id = '%s' AND
                            loc_id = '%s'
                        """%(siteId, regionId, locId)
        try:
            nodeId = self._getDbScalar(getNodeIdSql)
        except:
            print getNodeIdSql
        # print nodeId
        return nodeId
        
    def _getDatabaseNodeIdAndName(self, siteId, categoryId):
        mappingId, locId, regionId, locDesc, name = categoryId.split("#")
        getNodeIdSql = """
                        SELECT
                            node_id,name
                        FROM
                            region_info.node
                        WHERE
                            store_id = '%s' AND
                            region_id = '%s' AND
                            loc_id = '%s'
                        """%(siteId, regionId, locId)
        try:
            nodeId,name = self._getDbMultiple(getNodeIdSql)
        except:
            raise Exception('node_id not found for store-->%r and category %r' %(siteId,categoryId))
        # print nodeId
        return nodeId,name
    def setSegment(self, segment, useTempTables = False):
        self.segmentId = segment
        if segment != None:
            self.segmentString = "_" + segment
            self.tableString = "_" + segment
        else:
            self.segmentString = ''
            self.tableString = ''

        if useTempTables == True:
            self.tableString += '_temp'
            self.createTempTables()

    def createTempTables(self):
        sql = 'DROP TABLE IF EXISTS %s%s' % (self.tableName,self.tableString)
        self._execute(sql)
        sql = 'CREATE TABLE IF NOT EXISTS %s%s LIKE stats_1h' % (self.tableName,self.tableString)
        self._execute(sql)

        sql = 'DROP TABLE IF EXISTS stats_1d%s' % self.tableString
        self._execute(sql)
        sql = 'CREATE TABLE IF NOT EXISTS stats_1d%s LIKE stats_1d' % self.tableString
        self._execute(sql)

    def removeExistingStats(self, filterExpression=None):
        filter = ""
        self.filterExpression = filterExpression
        if filterExpression != None:
            filter = "and node_id IN (SELECT database_id FROM node WHERE id LIKE '" + filterExpression + "')"
            sql = "DELETE FROM %s"%(self.tableName) + self.tableString + " WHERE start_time >= '" + datetimeToSql(self.startTime) + "' AND start_time <= '" + datetimeToSql(self.endTime) + "' " + filter
            self._execute(sql)

    # def loadNavTimeFromCsv(self,dateToLogFileDict):
    #     navTimeDir = os.path.join(self.dataDir,'nav_time')
    #     t = self.startTime
    #     filesFound = False
    #     while t <= self.endTime:
    #         dateStr = dateToFileName(t)
    #         if dateStr not in self.navTimeDict:
    #             self.navTimeDict[dateStr] = {}
    #         fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
    #         fpLog.write("----------------------\n")
    #         fpLog.write("Loading nav time\n----------------------\n")
    #         fname = os.path.join(navTimeDir, ('%s.csv' % (dateToFileName(t).strip())))
    #         fpLog.write("nav_time :%s\n"%(fname))
    #         try:
    #             csvReader = csv.reader(open(fname))
    #             filesFound = True
    #         except IOError as e:
    #              if e.errno == errno.ENOENT:
    #                 print "File not found: %r" %fname
    #                 fpLog.write("File not found: %r. Ignoring navtime" %fname)
    #                 t += datetime.timedelta(days=1)
    #                 continue
    #         try:
    #             header = csvReader.next()
    #         except:
    #             pass
    #         try:
    #             for row in csvReader:
    #                 hour = row[2]
    #                 node_id = row[0]
    #                 if hour not in self.navTimeDict[dateStr]:
    #                     self.navTimeDict[dateStr][hour] = {}
    #                 if node_id not in self.navTimeDict[dateStr][hour]:
    #                     self.navTimeDict[dateStr][hour][node_id] = row[3]
    #                 else:
    #                     raise Exception("Single node_id has 2 entries for same date and hour in nav time file for date:" + dateStr)
    #
    #         except:
    #             raise Exception("Error while parsing nav time file for date: " + dateStr)
    #         fpLog.close()
    #         t += datetime.timedelta(days=1)
    #     return filesFound
    def loadStoreMongo(self, id, bucket_name, dateToLogFileDict, getPOSData=None):
        dbId = 'site_' + str(id.split('-')[-1])+'%'
        self.storeId = id

        if getPOSData is not None:
            buyers = getPOSData(self.storeId, dbId)
        else:
            buyers = {}
        #here we will download the doorcount picke file uploaded in S3
        fnameList = []
        t = self.startTime
        while t <= self.endTime:
            fpLog = open(dateToLogFileDict[dateToFileName(t)],'w')
            fpLog.write("----------------------\n")
            fpLog.write("Loading Store data\n----------------------\n")
            fname = self.dataDir + '\\'+ datetime.datetime.strftime(t,'%Y%m%d')+'.pkl'
            utilsAmazonS3.downloadFilesFromS3(bucket_name,'doorcount',fname)
            if not os.path.exists(fname):           
                newBucketName = 'vm-posse-' + bucket_name.split('-')[-1]
                print 'hello %s'%newBucketName
                utilsAmazonS3.downloadFilesFromS3(newBucketName,'doorcount',fname)
            fpLog.write("DoorCount filename: %s\n"%(fname))
            fnameList.append(fname)
            t += datetime.timedelta(days=1)
        t = self.startTime
        fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')

        for fname in fnameList:
            if os.path.exists(fname):
                 #unpickle to read the dict
                doorcountDict = pickle.load(open(fname,'rb'))
                #first lets calculate the fullstore
                siteId = dbId.strip('%')
                fullStoreDict = {siteId:{}}

                for cam in doorcountDict:
                    for date in doorcountDict[cam]:
                        if date not in fullStoreDict[siteId]:
                            fullStoreDict[siteId][date] = doorcountDict[cam][date]
                        else:
                            fullStoreDict[siteId][date]+=doorcountDict[cam][date]
                #next we will load the fullstore
                sql = "REPLACE INTO %s.%s (node_id,start_date,start_time,traffic_count,buyer_count) VALUES "%(self.schema,self.tableName)
                firstrow = 1
                for siteId in fullStoreDict:
                    for t1 in fullStoreDict[siteId]:
                        if t1 in buyers.keys():
                            buyer = int(buyers[t1])
                        else:
                            buyer = 0
                        if firstrow:
                            sql += "('%s','%s','%s',%d,%d)" % (
                                    siteId,
                                    datetimeToSql(t1).split(' ')[0],
                                    datetimeToSql(t1).split(' ')[1],
                                    fullStoreDict[siteId][t1],
                                    buyer
                                    )
                            firstrow = 0
                        else:
                            sql += ",('%s','%s','%s',%d,%d)" % (
                                    siteId,
                                    datetimeToSql(t1).split(' ')[0],
                                    datetimeToSql(t1).split(' ')[1],
                                    fullStoreDict[siteId][t1],
                                    buyer
                                    )

                for cam in doorcountDict:
                    nodeId = siteId +'_'+cam
                    dateStr = datetime.datetime.strftime(doorcountDict[cam].keys()[0],'%Y%m%d')
                    fpLog.write("\nUploading store data: %s and %s and %s\n"%(str(self.storeId),dateStr, cam))
                    for t1 in doorcountDict[cam]:
                        sql += ",('%s','%s','%s',%d,%d)" % (
                                nodeId,
                                datetimeToSql(t1).split(' ')[0],
                                datetimeToSql(t1).split(' ')[1],
                                doorcountDict[cam][t1],
                                0
                                )
                sql += ";"
                self._execute(sql)
                self.commit()

            else:
                fpLog.close()
                raise Exception('Doorcount file not found %r' %fname)
        fpLog.close()
        print 'Deleting the doorcount files'
        for fname in fnameList:
            os.remove(fname)


    # def loadCategoriesMongo(self, id,categoryList, dateToLogFileDict, getPOSData=None):
    #     dbId = 'site_' + str(id)+'%'
    #     self.storeId = id
    #
    #     if getPOSData is not None:
    #         buyers = getPOSData(self.storeId, dbId)
    #     else:
    #         buyers = {}
    #     #here we will download the doorcount picke file uploaded in S3
    #     fnameList = []
    #     t = self.startTime
    #     while t <= self.endTime:
    #         fpLog = open(dateToLogFileDict[dateToFileName(t)],'w')
    #         fpLog.write("----------------------\n")
    #         fpLog.write("Loading Store data\n----------------------\n")
    #         fname = self.dataDir + '\\'+ datetime.datetime.strftime(t,'%Y%m%d')+'.pkl'
    #         #utilsAmazonS3.downloadFilesFromS3(bucket_name,'doorcount',fname)
    #         fpLog.write("DoorCount filename: %s\n"%(fname))
    #         fnameList.append(fname)
    #         t += datetime.timedelta(days=1)
    #     t = self.startTime
    #     fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
    #
    #     for fname in fnameList:
    #         if os.path.exists(fname):
    #              #unpickle to read the dict
    #             doorcountDict = pickle.load(open(fname,'rb'))
    #             #first lets calculate the fullstore
    #             siteId = dbId.strip('%')
    #             fullStoreDict = {siteId:{}}
    #
    #             for cam in doorcountDict:
    #                 for date in doorcountDict[cam]:
    #                     if date not in fullStoreDict[siteId]:
    #                         fullStoreDict[siteId][date] = doorcountDict[cam][date]
    #                     else:
    #                         fullStoreDict[siteId][date]+=doorcountDict[cam][date]
    #             #next we will load the fullstore
    #             sql = "REPLACE INTO %s.%s (node_id,start_date,start_time,traffic_count,buyer_count) VALUES "%(self.schema,self.tableName)
    #             firstrow = 1
    #             for siteId in fullStoreDict:
    #                 for t1 in fullStoreDict[siteId]:
    #                     if t1 in buyers.keys():
    #                         buyer = int(buyers[t1])
    #                     else:
    #                         buyer = 0
    #                     if firstrow:
    #                         sql += "('%s','%s','%s',%d,%d)" % (
    #                                 siteId,
    #                                 datetimeToSql(t1).split(' ')[0],
    #                                 datetimeToSql(t1).split(' ')[1],
    #                                 fullStoreDict[siteId][t1],
    #                                 buyer
    #                                 )
    #                         firstrow = 0
    #                     else:
    #                         sql += ",('%s','%s','%s',%d,%d)" % (
    #                                 siteId,
    #                                 datetimeToSql(t1).split(' ')[0],
    #                                 datetimeToSql(t1).split(' ')[1],
    #                                 fullStoreDict[siteId][t1],
    #                                 buyer
    #                                 )
    #
    #             for cam in doorcountDict:
    #                 nodeId = siteId +'_'+cam
    #                 dateStr = datetime.datetime.strftime(doorcountDict[cam].keys()[0],'%Y%m%d')
    #                 fpLog.write("\nUploading store data: %s and %s and %s\n"%(str(self.storeId),dateStr, cam))
    #                 for t1 in doorcountDict[cam]:
    #                     sql += ",('%s','%s','%s',%d,%d)" % (
    #                             nodeId,
    #                             datetimeToSql(t1).split(' ')[0],
    #                             datetimeToSql(t1).split(' ')[1],
    #                             doorcountDict[cam][t1],
    #                             0
    #                             )
    #             sql += ";"
    #             self._execute(sql)
    #             self.commit()
    #
    #         else:
    #             fpLog.close()
    #             raise Exception('Doorcount file not found %r' %fname)
            fpLog.close()
    def loadStore(self, id, dateToLogFileDict, cams = [], scaleFactor = 1, segment=None, segments=[None], numSegmentCategories=0, getPOSData=None):
        dbId = 'site_' + str(id)+'%'
        self.storeId = id
        if getPOSData is not None:
            buyers = getPOSData(self.storeId, dbId)
        else:
            buyers = {}
        # print dateToLogFileDict

        counts = {}
        for cam in cams:
            counts[cam] = {}
            for segment in segments:
                counts[cam][segment] = {}

        t = self.startTime
        while t <= self.endTime:
            fpLog = open(dateToLogFileDict[dateToFileName(t)],'w')
            fpLog.write("----------------------\n")
            fpLog.write("Computing Store data\n----------------------\n")
            # Get counts
            for cam in cams:
                for segment in segments:
                    # print t, self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0])
                    counts[cam][segment].update(self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0]))
                    
                fname = os.path.join(self.dataDir, 'DoorCounts', ('%s_%s.csv' % (cam, dateToFileName(t).strip())))
                fpLog.write("DoorCount: %s\n\t%s\n"%(cam,fname))

                if os.path.exists(fname):
                    firstLine = 1
                    for row in csv.reader(open(fname)):
                        if not firstLine:
                            if len(row) != 0:
                                eventTime = stringToTime(row[0])

                                self._addColVal(counts[cam][None],0,eventTime,scaleFactor);

                                for i in range(0, numSegmentCategories):
                                    segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                    # print counts.keys()
                                    if segmentName in counts[cam].keys():
                                        self._addColVal(counts[cam][segmentName],0,eventTime,scaleFactor);
                            else:
                                print "Error 1: File is empty"
                                fpLog.write("\t\tFile is empty\n")
                        else:
                            firstLine = 0
                            
                fname = os.path.join(self.dataDir, 'ShopInfo-Overall', 'wholestore',  '%s.csv' % dateToFileName(t)).strip()
                fpLog.write("ShopInfo-Overall-wholestore: %s\n\t%s\n"%(cam,fname))
                if os.path.exists(fname):
                    firstLine = 1
                    for row in csv.reader(open(fname)):
                        if not firstLine:
                            if len(row) != 0:
                                eventTime = stringToTime(row[0])
                                self._addColVal(counts[cam][None],3,eventTime,float(row[1]));
                            else:
                                print "Error 2: File is empty"
                                fpLog.write("\t\tFile is empty\n")
                        else:
                            firstLine = 0
                fpLog.write("------\n")
            fpLog.write("\n\nDone!\n----------------------\n")
            fpLog.close()
            t += datetime.timedelta(days=1)

        storeFlag = False
        # pdb.set_trace()
        for cam in cams:
            t = self.startTime
            while t <= self.endTime:
                fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
                fpLog.write("\nUploading store data: %s and %s\n"%(str(self.storeId), cam))
                for segment in segments:
                    times = counts[cam][segment].keys()
                    times.sort()
                    # print times
                    if segment is None:
                        sql = "REPLACE INTO %s.%s (node_id,start_date,start_time,traffic_count,buyer_count) VALUES "%(self.schema,self.tableName)
                        segmentId = "site_" + id
                    else:
                        sql = "REPLACE INTO %s_%s" % (self.tableName,segment) + "(node_id,start_date,start_time,traffic_count,buyer_count) VALUES "
                        segmentId = "site_" + id + '_' +  segment

                    dbId = 'site_' + id + '_' + cam
                    storeSiteId = 'site_'+id
                    firstRow = 1
                    for t1 in times:
                        # try:
                        #    percentage = counts[cam][segment][t1][0] / sum([counts[i][segment][t1][0] for i in counts])
                        # except:
                        #    percentage = 1
                        #percentage = 1
                        if t1 in buyers.keys():
                            buyer = int(buyers[t1])
                        else:
                            buyer = 0

                        if t1.hour < self.settings.StartTimeOfDay or t1.hour >= self.settings.EndTimeOfDay:
                            continue
                        if not firstRow: sql += ",";
                        row = counts[cam][segment][t1]

                        sql += "('%s','%s','%s',%d,%d)" % (
                            dbId,
                            datetimeToSql(t1).split(' ')[0],
                            datetimeToSql(t1).split(' ')[1],
                            int(round(row[0])),
                            0
                            )
                        #This part is to add the store buyers to store nodeId: "site_'id'
                        if not storeFlag:
                            sql += ",('%s','%s','%s',%d,%d)" % (
                                storeSiteId,
                                datetimeToSql(t1).split(' ')[0],
                                datetimeToSql(t1).split(' ')[1],
                                sum([counts[i][segment][t1][0] for i in counts]),
                                buyer
                                )
                        firstRow = 0
                        #print sql
                    sql += ";"
                    self._execute(sql)

                self.commit()    
                fpLog.close()
                t += datetime.timedelta(days=1)
                # pdb.set_trace()
            storeFlag = True
    def loadStoreRegion(self,id,dateToLogFileDict, regionId,trafficCountSF=1.2,shopperCountSF=2.5, segments=[None], numSegmentCategories=0, productCategories = []):
        self.storeRegionId = id
        colTotalTraffic = 0
        colNumShoppers = 1
        counts = {}
        t = self.startTime
        
        while t <= self.endTime:
            fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
            fpLog.write("\n\n\n========================================================\n")
            fpLog.write("Loading Store Region - {0:^30}\n".format(id))
            fpLog.write("========================================================\n")
            for segment in segments:
                counts[segment] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0])
            
            dirName = os.path.join(self.dataDir,"PasserInfo",regionId)
            #pdb.set_trace()
            if not os.path.exists(dirName):
                print dirName
                raise Exception("Directory not found: " + dirName)
            fname = dirName + "\\" + dateToFileName(t) + ".csv"
            fpLog.write("[Info]: Reading Passer Info file: %s\n"%(fname))
            if os.path.exists(fname):
                firstLine = 1
                for row in csv.reader(open(fname)):
                    numCols = len(row)
                    if not firstLine:
                        if len(row) != 0:
                            eventTime = stringToTime(row[0])
                            #for catRegionId in categoryRegionIds:
                            if numCols > categoryIndex and row[categoryIndex] == '1':
                                self._addColVal(counts[None],0,eventTime,trafficCountSF);
                                for i in range(0, numSegmentCategories):
                                    segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                    if segmentName in counts.keys():
                                        self._addColVal(counts[segmentName],0,eventTime,trafficCountSF);
                    else:
                        categoryIndex = row.index(regionId)
                        firstLine = 0
                fpLog.write("\tDone\n")
            else:
                fpLog.write("\t[Warning]: No file found\n")
                            
            dirName = os.path.join(self.dataDir,"shopInfo-Overall",regionId)
            if not os.path.exists(dirName):
                raise Exception("Directory not found: " + dirName)
            fname = dirName + "\\" + dateToFileName(t) + ".csv"
            fpLog.write("\n------------------------\n")
            fpLog.write("[Info]: Reading shopInfo-Overall file: %s\n"%fname)
            #pdb.set_trace()
            startIndex = 12
            endIndex = 30
            if os.path.exists(fname):
                firstLine = 1
                for row in csv.reader(open(fname)):
                    for i in xrange(len(row)-1,-1,-1):
                        if 'newagebeverage(' in row[i]:
                            row[i] = row[i]+row[i+1]+row[i+2]
                            del row[i+2]
                            del row[i+1]
                    numCols = len(row)
                    if not firstLine:
                        if len(row) != 0:
                            eventTime = stringToTime(row[0])
    ####                        Hack for dealing with demographics segments --replace
                            if int(row[7]) > 0:
                                shopTime = 0
                                self._addColVal(counts[None],1,eventTime,shopperCountSF);
                                self._addColVal(counts[None],2,eventTime,int(row[7])*shopperCountSF);
                                
                                for index in range(startIndex, endIndex+1, 2):
                                    if len(row[index]) != 0:
                                        shopTime += float(row[index])
                                self._addColVal(counts[None],3,eventTime,shopTime*shopperCountSF)
                                
                                for i in range(0, numSegmentCategories):
                                    segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                    if segmentName in counts.keys():
                                        shopTime = 0
                                        self._addColVal(counts[segmentName],1,eventTime,shopperCountSF);
                                        self._addColVal(counts[segmentName],2,eventTime,int(row[7])*shopperCountSF);
                                        
                                        for index in range(startIndex, endIndex+1, 2):
                                            if len(row[index]) != 0:
                                                shopTime += float(row[index])
                                        self._addColVal(counts[segmentName],3,eventTime,shopTime*shopperCountSF)
                    else:
                        firstLine = 0
                fpLog.write("\t[Info]: Done\n")
            else:
                fpLog.write("\t[Warning]: file is missing\n")
            
##            counts = adjustCountSegments(counts, [0,1])
            fpLog.write("\n------------------------\n")
            fpLog.write("[Info]: Uploading data\n")
            for segment in segments:
                times = counts[segment].keys()
                times.sort()
                if segment is None:
                    sql = "REPLACE INTO %s.%s (node_id, start_time,traffic_count,shopper_count,shopping_points_avg,shopping_time_avg) VALUES "%(self.schema, self.tableName)
                    nodeId = "site_region_" + self.storeId + "_" + id
                    #pdb.set_trace()
                else:
                    sql = "REPLACE INTO %s_%s" % (self.tableName,segment) + "(node_id,start_time,traffic_count,shopper_count,shopping_points_avg,shopping_time_avg) VALUES "
                    nodeId = "site_region_" + self.storeId + "_" + id + "_" + segment
                try:
                    dbId = self._getDatabaseNodeId(self.storeRegionId);
                except:
                    # dbId = 'site_' + self.storeId
                    dbId = nodeId
                    
                
                firstRow = 1
                for t1 in times:
                    if t1.hour < self.settings.StartTimeOfDay or t1.hour >= self.settings.EndTimeOfDay:
                        continue
                    if not firstRow: sql += ",";
                    row = counts[segment][t1]
                    try:
                        for category in productCategories:
                            if category[t1][colTotalTraffic] > int(round(row[colTotalTraffic])):
                                row[colTotalTraffic] = category[t1][colTotalTraffic]
                            if category[t1][colNumShoppers] > int(round(row[colNumShoppers])):
                                row[colNumShoppers] = category[t1][colNumShoppers]
                    except KeyError:
                        pdb.set_trace()
                    if 'site_' in dbId:
                        qual = "'%s'"
                    else:
                        qual = "%d"
                    sql += "(" + qual%dbId + ","
                    # pdb.set_trace()
                    sql += "'%s',%d,%d,%s,%s)" % (
                        datetimeToSql(t1),
                        int(round(row[colTotalTraffic])),
                        int(round(row[colNumShoppers])),
                        sqlDiv(row[2],row[1]),
                        sqlDiv(row[3],row[1])
                        )
                    firstRow = 0
                sql += ";"
                try:
                    self._execute(sql)
                except:
                    pdb.set_trace()
                fpLog.write("\t[Info]: Done\n")
            fpLog.close()
            t += datetime.timedelta(days=1)
        
    def getCategoryListInDateRange(self, dateList):
        '''
        returns a union of all categories found in passerinfo files for the dates in dateList
        '''
       
        categoryList = set([])
        
        dirName = os.path.join(self.dataDir,'PasserInfo', 'wholestore')
        delta = datetime.timedelta(1,0,0)
        categoryList = set([])
        for dt in dateList:
            fname = dirName + "\\" + dateToFileName(dt) + ".csv"
            fileSize = os.path.getsize(fname)
            if fileSize != 0:
                row = csv.reader(open(fname)).next()
                categoryList.update(row[row.index('wholestore')+1:row.index('Gender')])
        categoryList.difference_update(set([""]))
        return list(categoryList)


    def loadPasserInfo(self, dt, logFile, trafficCountSF, segments=[None], numSegmentCategories=0):
        '''
        loads traffic data for all categories for the specified date
        '''
        colTotalTraffic = 0
        
        t = stringToTime(dt)
        counts = {}
        trafficCounts = {}
        if len(self.navTimeDict[dt]) >0:
            fpLog = open(logFile,'a')
            fpLog.write("\n\n\n========================================================\n")
            fpLog.write("Loading passerinfo data - %s\n"%dt)
            fpLog.write("========================================================\n")


            # pdb.set_trace()
            for segment in segments:
                counts[segment] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
            # trafficCounts = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])

            dirName = os.path.join(self.dataDir,'PasserInfo', 'wholestore')
            if not os.path.exists(dirName):
                fpLog.write("Passerinfo dir: %s not found!\n"%dirName)
                raise Exception("Directory not found: " + dirName)

            fname = dirName + "\\" + dateToFileName(t) + ".csv"
            fpLog.write("[Info]: Reading passer info file: %s\n"%fname)

            if os.path.exists(fname):
                catList = self.getCategoryList_SingleDay(dt)
                with open(fname) as fp:
                    csvObj = csv.reader(fp)
                    header = csvObj.next()
                    for row in csvObj:
                        numCols = len(row)
                        for id in catList:
                            categoryIndex = header.index(id)
                            if numCols > categoryIndex and row[categoryIndex] == '1':
                                eventTime = stringToTime(row[0])
                                evt_time = datetime.datetime(eventTime.year,
                                    eventTime.month,
                                    eventTime.day,
                                    eventTime.hour,
                                    0, 0)
                                self._addColVal(counts[None],colTotalTraffic,eventTime,trafficCountSF)
                                # pdb.set_trace()
                                if id not in trafficCounts:
                                    # trafficCounts[id] = {evt_time: 1}
                                    trafficCounts[id] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),0)
                                    trafficCounts[id][evt_time] = 1
                                else:
                                    if evt_time in trafficCounts[id]:
                                        trafficCounts[id][evt_time] += 1
                                    else:
                                        trafficCounts[id][evt_time] = 1
                                for i in range(0, numSegmentCategories):
                                    segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                    if segmentName in counts.keys():
                                        self._addColVal(counts[segmentName],colTotalTraffic,eventTime,trafficCountSF)
                fpLog.write("\n\n\t[Info]: Done!\n")
            else:
                fpLog.write("\t[Warning]: Missing passer info file\n")

            # Though we iterate over all categories found in the passerinfo header, the
            # traffic for a certain category might be zero. When this happens, the way the loops
            # and if condition is structured, trafficCounts might not contain all the categories
            # To keep things consistent, we are going to add in the categories that did
            # not make it through the if block
            to_add = set(catList).difference(trafficCounts.keys())
            for cat in to_add:
                trafficCounts[cat] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),0)

            fpLog.write("Following categories were found:\n")
            for idx, id in enumerate(sorted(catList)):
                fpLog.write("\t\t%d) %s\n"%(idx+1,id))
            fpLog.close()
        return trafficCounts

    def getCategoryList_SingleDay(self, dt):
        categoryList = []
        dirName = os.path.join(self.dataDir,'PasserInfo', 'wholestore')
        fname = dirName + "\\" + dt + ".csv"
        try:
            row = csv.reader(open(fname)).next()
        except IOError as e:
            if e.errno == errno.ENOENT:
                print "Wholestore file not found %r" %fname
                return categoryList
        fileSize = os.path.getsize(fname)
        if fileSize != 0:
            categoryList.extend(row[row.index('wholestore')+1:row.index('Gender')])
        return categoryList

    def loadProductCategoryPerDayMongo(self, dt,logFile , nodeList, project_type, mongoColl, getPOSData=None):
        fpLog = open(logFile,'w')
        fpLog.write("----------------------\n")
        fpLog.write("Loading Category  data\n----------------------\n")
        muObj = MongoUtils()
        muObj.connectToMongo('vision')
        # for nodeId in nodeList:
        dataDict = muObj.fetchDocument(mongoColl, dt, nodeList, project_type)
            #load data for the node from the mongo table
        #inserting into db

        sql = "REPLACE INTO %s.%s"%(self.schema, self.tableName) + \
                    """(node_id,
                        start_date, start_time,
                        first_dest_shopper_count,
                        shopper_count, traffic_count,
                        buyer_count,
                        shopping_points_avg, shopping_time_avg, nav_time_avg,
                        shopping_time_0_10, shopping_time_10_20,
                        shopping_time_20_30, shopping_time_30_40,
                        shopping_time_40_50, shopping_time_50_60,
                        shopping_time_60_70, shopping_time_70_80,
                        shopping_time_80_90, shopping_time_90
                        )
                    VALUES """
        firstrow = 1
        fpLog.write("Loading buyer counts for  data\n----------------------\n")
        buyers = getPOSData(self.storeId, nodeList,dt)
        for nodeId in dataDict:
            fpLog.write("Loading final upload  data for node_id %r\n----------------------\n"%(nodeId))
            for hour in dataDict[nodeId]:
                buyer = 0
                if nodeId in buyers[dt].keys():
                    if hour in buyers[dt][nodeId].keys():
                        buyer = int(buyers[dt][nodeId][hour])
                shop_point_avg = 0
                shop_time_avg = 0
                nav_time_avg = 0
                if dataDict[nodeId][hour]['shopper_count'] > 0:
                    shop_point_avg = float(dataDict[nodeId][hour]['shopping_points'])/float(dataDict[nodeId][hour]['shopper_count'])
                    shop_time_avg = dataDict[nodeId][hour]['shopping_time']/dataDict[nodeId][hour]['shopper_count']
                    nav_time_avg = dataDict[nodeId][hour]['nav_time']/dataDict[nodeId][hour]['shopper_count']
                if firstrow:
                    sql += """('%s',
                            '%s','%s',
                            %d,
                            %d,%d,
                            %d,
                            %s, %s,
                            %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f
                            )""" % (nodeId,dt,hour,dataDict[nodeId][hour]['first_dest_shopper_count'],
                                    dataDict[nodeId][hour]['shopper_count'],dataDict[nodeId][hour]['traffic_count'],
                                    buyer,
                                    shop_point_avg,shop_time_avg,nav_time_avg,
                                    dataDict[nodeId][hour]['shop_dist'][0],dataDict[nodeId][hour]['shop_dist'][1],
                                    dataDict[nodeId][hour]['shop_dist'][2],dataDict[nodeId][hour]['shop_dist'][3],
                                    dataDict[nodeId][hour]['shop_dist'][4],dataDict[nodeId][hour]['shop_dist'][5],
                                    dataDict[nodeId][hour]['shop_dist'][6],dataDict[nodeId][hour]['shop_dist'][7],
                                    dataDict[nodeId][hour]['shop_dist'][8],dataDict[nodeId][hour]['shop_dist'][9])
                    firstrow = 0
                else:
                    sql += """,('%s',
                            '%s','%s',
                            %d,
                            %d,%d,
                            %d,
                            %s, %s,
                            %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f
                            )""" % (nodeId,dt,hour,dataDict[nodeId][hour]['first_dest_shopper_count'],
                                    dataDict[nodeId][hour]['shopper_count'],dataDict[nodeId][hour]['traffic_count'],
                                    buyer,
                                    shop_point_avg,shop_time_avg,nav_time_avg,
                                    dataDict[nodeId][hour]['shop_dist'][0],dataDict[nodeId][hour]['shop_dist'][1],
                                    dataDict[nodeId][hour]['shop_dist'][2],dataDict[nodeId][hour]['shop_dist'][3],
                                    dataDict[nodeId][hour]['shop_dist'][4],dataDict[nodeId][hour]['shop_dist'][5],
                                    dataDict[nodeId][hour]['shop_dist'][6],dataDict[nodeId][hour]['shop_dist'][7],
                                    dataDict[nodeId][hour]['shop_dist'][8],dataDict[nodeId][hour]['shop_dist'][9])
        sql += ";"
        try:
            self._execute(sql)
        except:
            pdb.set_trace()
            fpLog.write("\n\n\n------------------------------------------\n")
            fpLog.write("ERROR in uploading data on %s\n\n"%(dt))
            fpLog.write("%s"%sql)
            fpLog.write("\n\n------------------------------------------\n")
            raise
        fpLog.write("Loading Category  data complete\n----------------------\n")
        fpLog.close()
    def loadProductCategoryPerDay(self, id, dt, logFile, trafficCountSF, shopperCountSF, nav_time_found, segments=[None], numSegmentCategories=0, getPOSData=None, trafficCounts={}):
        '''
        computes and uploads category data. The category is specified by id and the date is dt.
        '''
        counts = {}
        returnData = {}
        if nav_time_found:
            colTotalTraffic = 0
            colNumShoppers = 1
            colFirstDestShoppers = 2
            colSecondDestShoppers = 3
            colAvgShopTime = 4
            colShoppingTimeSlot1 = 5
            colShopperVelocity = 10
            colShopperVelocityCount = 11
            colPasserByVelocity = 12
            colPasserByVelocityCount = 14
            colBuyers = 15
            colTotalSales = 16
            colAvgStops = 17
            colShoppingTime2Slot1 = 18
            maxB4Index = 12


            if self.segmentId != None:
                nodeId += "_" + self.segmentId
            try:
                dbId = self._getDatabaseNodeIdNew(self.storeId, id)
            except Exception, e:
                print "Error %s" % (e)
                print "\t\t\tNo node id found for %s!"%id
                # pdb.set_trace()
                return 0
            t = stringToTime(dt) # self.startTime
        


            # open log file and update it with category info
            fpLog = open(logFile,'a')
            fpLog.write("\n\n\n========================================================\n")
            fpLog.write("Loading category data - {0:30}\t{1:15}\n".format(id, dbId))
            fpLog.write("========================================================\n")
            for segment in segments:
                counts[segment] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])


            #########################################################################
            #                    COMPUTING CATEGORY STATS BLOCK                     #
            #########################################################################
            # constructing path to access and load shop-infop regions files for this category on the supplied date
            dirName = os.path.join(self.dataDir,'shopInfo-Regions', self.storeRegionId)
            if not os.path.exists(dirName):
                raise Exception("Directory not found: " + dirName)
            fname = dirName + "\\" + dateToFileName(t) + '_' + id + ".csv"
            fpLog.write("[Info]: Reading shopInfo-Regions\%s file: %s\n"%(self.storeRegionId,fname))

            completeShopperList = []
            lineNum =2
            if os.path.exists(fname):
                firstLine = True
                print fname
                for row in csv.reader(open(fname)):
                    if not firstLine and 'Start Time' not in row:
                        shopper = {}
                        shopTime = float(row[9])
                        for index in range(len(row)):
                            if row[index] == id:
                                lineNum += 1
                                shopTime += float(row[index+1])

                        if validShopTime(self.storeId, id, shopTime):
                            eventTime = stringToTime(row[0])
                            self._addColVal(counts[None],colNumShoppers,eventTime,shopperCountSF)
                            if int(row[7]) == 0:
                                self._addColVal(counts[None],colFirstDestShoppers,eventTime,shopperCountSF)
                                shopper['firstDest'] = True
                            else:
                                self._addColVal(counts[None],colSecondDestShoppers,eventTime,shopperCountSF)
                                shopper['firstDest'] = False

                            categoryStops = 1
                            for index in range(maxB4Index, len(row), 2):
                                if row[index] == id:
                                    categoryStops += 1
                            shopper['numStops'] = categoryStops
                            self._addColVal(counts[None],colAvgStops,eventTime,categoryStops*shopperCountSF)

                            self._addColVal(counts[None],colAvgShopTime,eventTime,shopperCountSF*shopTime)
                            self._addColVal(counts[None],colShoppingTimeSlot1 + self._shopTimeSlot(shopTime) -1,eventTime,shopperCountSF)
                            self._addColVal(counts[None],colShoppingTime2Slot1 + self._shopTime2Slot(shopTime) -1,eventTime,shopperCountSF)


                            shopper['shopTime'] = shopTime
                            completeShopperList.append(shopper)
                            for i in range(0, numSegmentCategories):
                                segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                if segmentName in counts.keys():
                                    self._addColVal(counts[segmentName],colNumShoppers,eventTime,shopperCountSF)
                                    if int(row[7]) == 0:
                                        self._addColVal(counts[segmentName],colFirstDestShoppers,eventTime,shopperCountSF)
                                    else:
                                        self._addColVal(counts[segmentName],colSecondDestShoppers,eventTime,shopperCountSF)

                                    categoryStops = 1
                                    for index in range(maxB4Index, len(row), 2):
                                        if row[index] == id:
                                            categoryStops += 1
                                    self._addColVal(counts[segmentName],colAvgStops,eventTime,categoryStops*shopperCountSF)
                                    shopTime = float(row[9])
                                    for index in range(len(row)):
                                        if row[index] == id:
                                            shopTime += float(row[index+1])

                                    self._addColVal(counts[segmentName],colAvgShopTime,eventTime,shopperCountSF*shopTime)
                                    self._addColVal(counts[segmentName],colShoppingTimeSlot1 + self._shopTimeSlot(shopTime) -1,eventTime,shopperCountSF)
                                    self._addColVal(counts[segmentName],colShoppingTime2Slot1 + self._shopTime2Slot(shopTime) -1,eventTime,shopperCountSF)
                    else:
                        firstLine = False
                fpLog.write("\t[Info]: Done\n")
            else:
                fpLog.write("\t[Warning: shopInfo-Regions file is missing\n")

            #########################################################################
            #                    COMPUTING CATEGORY VELOCITIES                      #
            #########################################################################
            shopperVelocities = []
            passerbyVelocities = []
            fpLog.write("\n-------------------\n")
            fpLog.write("[Info]: Uploading velocities\n")
            try:
                dirName = os.path.join(self.dataDir, 'Velocities', 'wholestore');#self.storeRegionId);
                if not os.path.exists(dirName):
                    raise Exception("Directory not found: " + dirName)

                fname = dirName + "\\" + dateToFileName(t) + '_' + id + ".csv"
                fpLog.write("[Info]: Reading velocity file: %s\n"%fname)
                if os.path.exists(fname):
                    firstLine = 1
                    for row in csv.reader(open(fname)):
                        if not firstLine:
                            eventTime = stringToTime(row[0])
                            if row[3].strip() == 'Stopped':
                                shopperVelocities
                                self._addColVal(counts[None],colShopperVelocity,eventTime,float(row[2]))
                                self._addColVal(counts[None],colShopperVelocityCount,eventTime,1)
                                shopperVelocities.append(float(row[2]))
                            else:
                                self._addColVal(counts[None],colPasserByVelocity,eventTime,float(row[2]))
                                self._addColVal(counts[None],colPasserByVelocityCount,eventTime,1)
                                passerbyVelocities.append(float(row[2]))

                            for i in range(0, numSegmentCategories):
                                segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                if segmentName in counts.keys():
                                    if row[3] == 'Stopped':
                                        self._addColVal(counts[segmentName],colShopperVelocity,eventTime,float(row[2]))
                                        self._addColVal(counts[segmentName],colShopperVelocityCount,eventTime,1)
                                    else:
                                        self._addColVal(counts[segmentName],colPasserByVelocity,eventTime,float(row[2]))
                                        self._addColVal(counts[segmentName],colPasserByVelocityCount,eventTime,1)
                        else:
                            firstLine = 0
                    fpLog.write("\t[Info]: Done\n")
                else:
                    fpLog.write("\t[Warning]: File is missing\n")
            except:
                fpLog.write("\t[Error]: Skipping\n")
                pass


            #########################################################################
            #                    UPLOADING CATEGORY STATS BLOCK                     #
            #########################################################################
            fpLog.write("\n-------------------\n")
            fpLog.write("[Info]: Uploading category data\n")
            for segment in segments:
                times = counts[segment].keys()
                times.sort()
                if segment is None:
                    sql = "REPLACE INTO %s.%s"%(self.schema, self.tableName) + \
                    """(node_id,
                        start_date, start_time,
                        first_dest_shopper_count,
                        shopper_count, traffic_count,
                        buyer_count,
                        shopping_points_avg, shopping_time_avg,
                        nav_time_avg,
                        shopping_time_0_10, shopping_time_10_20,
                        shopping_time_20_30, shopping_time_30_40,
                        shopping_time_40_50, shopping_time_50_60,
                        shopping_time_60_70, shopping_time_70_80,
                        shopping_time_80_90, shopping_time_90
                        )
                    VALUES """
                else:
                    sql = "REPLACE INTO %s_%s" % (self.tableName,segment) + \
                    """(node_id,
                        start_date, start_time,
                        first_dest_shopper_count,
                        shopper_count, traffic_count,
                        buyer_count,
                        shopping_points_avg, shopping_time_avg,
                        nav_time_avg,
                        shopping_time_0_10, shopping_time_10_20,
                        shopping_time_20_30, shopping_time_30_40,
                        shopping_time_40_50, shopping_time_50_60,
                        shopping_time_60_70, shopping_time_70_80,
                        shopping_time_80_90, shopping_time_90
                        )
                    VALUES """

                try:
                    dbId = self._getDatabaseNodeIdNew(self.storeId, id)
                except:
                    print "Node Id not Found"
                    # no need to process any dates if the node id isn't found at all
                    fpLog.write("\t[Error]: Aborting: node id wasn't found for store: %s and category: %s\n"%(self.storeId, id))
                    fpLog.close()
                    return 0
                if getPOSData is not None:
                    buyers = getPOSData(self.storeId, dbId)
                else:
                    buyers = {}
                firstRow = 1
                for t1 in times:
                    if t1.hour < self.settings.StartTimeOfDay or t1.hour >= self.settings.EndTimeOfDay:
                        continue
                    if not firstRow: sql += ",";
                    row = counts[segment][t1];

                    shoppingTimes = []
                    shoppingTimes.extend(row[colShoppingTimeSlot1:colShoppingTimeSlot1+4])
                    shoppingTimes.extend(row[colShoppingTime2Slot1:colShoppingTime2Slot1+6])

                    # print shoppingTimes, row[colShoppingTimeSlot1:colShoppingTimeSlot1+4]
                    # print shoppingTimes, row[colShoppingTime2Slot1:colShoppingTime2Slot1+6]
                    # adjustedShoppingTimes = map(int, adjustValues(int(round(row[colNumShoppers])), shoppingTimes))
                    # print adjustedShoppingTimes, int(round(row[colNumShoppers]))
                    dtList = dateToString(t1).split('T')
                    dtStr = dtList[0].replace('-',"")
                    hourStr = dtList[1]
                    nav_time = 0.0
                    if dtStr in self.navTimeDict:
                        if hourStr in self.navTimeDict[dtStr]:
                            if dbId in self.navTimeDict[dtStr][hourStr]:
                                nav_time =  float(self.navTimeDict[dtStr][hourStr][dbId])
                    if segment is None and t1 in buyers.keys():
                        buyerCount = buyers[t1]
                    else:
                        buyerCount = 0
                    sql += """('%s',
                            '%s','%s',
                            %d,
                            %d,%d,
                            %d,
                            %s, %s,
                            %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f,
                            %f, %f
                            )""" % (
                        dbId,
                        datetimeToSql(t1).split(' ')[0], datetimeToSql(t1).split(' ')[1],
                        int(round(row[colFirstDestShoppers])),
                        int(round(row[colNumShoppers])), int(trafficCounts.get(t1,0)),
                        buyerCount,
                        sqlDiv(row[colAvgStops],row[colNumShoppers]), sqlDiv(row[colAvgShopTime],row[colNumShoppers]),
                        nav_time,
                        shoppingTimes[0], shoppingTimes[1],
                        shoppingTimes[2], shoppingTimes[3],
                        shoppingTimes[4], shoppingTimes[5],
                        shoppingTimes[6], shoppingTimes[7],
                        shoppingTimes[8], shoppingTimes[9],
                        )

                    firstRow = 0
                sql += ";"

                try:
                    self._execute(sql)
                except:
                    pdb.set_trace()
                    fpLog.write("\n\n\n------------------------------------------\n")
                    fpLog.write("ERROR in uploading category %s data on %s\n\n"%(id,t1.strftime("%Y-%m-%dT%H")))
                    fpLog.write("%s"%sql)
                    fpLog.write("\n\n------------------------------------------\n")
                    raise
            returnData.update(counts[None])
            fpLog.write("\t[Info]: Done\n")
            fpLog.close()
        return returnData
            

        
            
    def getCategoryList(self, dateToLogFileDict):
        '''
        Returns a union of all categories found in passer info files across the date range self.startTime to self.endTime
        '''
        dirName = os.path.join(self.dataDir,'PasserInfo', 'wholestore')
        delta = datetime.timedelta(1,0,0)
        t = self.startTime
        categoryList = set([])
        # pdb.set_trace()
        while t <= self.endTime:
            fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
            fpLog.write("\n\n\n------------------------------------------\n")
            fpLog.write("Loading categories to upload from passer info files\n")
            fpLog.write("------------------------------------------\n")
            fname = dirName + "\\" + dateToFileName(t) + ".csv"
            fileSize = os.path.getsize(fname)
            if fileSize != 0:
                row = csv.reader(open(fname)).next()
                categoryList.update(row[row.index('wholestore')+1:row.index('Gender')])
                fpLog.write("Done!\n------------------------------------------\n")
            else:
                fpLog.write("[Warning]: passer info file: '%s' has NO headers\n"%fname)
            fpLog.close()
            t += delta
        categoryList.difference_update(set([""]))
        return list(categoryList)
        
    
    def loadProductCategory(self,id,dateToLogFileDict,trafficCountSF,shopperCountSF, segments = [None], numSegmentCategories = 0, getPOSData=None):
        '''
        computes and loads category data across the entire date range
        id: full name of category
        '''

        colTotalTraffic = 0
        colNumShoppers = 1
        colFirstDestShoppers = 2
        colSecondDestShoppers = 3
        colAvgShopTime = 4
        colShoppingTimeSlot1 = 5
        colShopperVelocity = 10
        colShopperVelocityCount = 11
        colPasserByVelocity = 12
        colPasserByVelocityCount = 14
        colBuyers = 15
        colTotalSales = 16
        colAvgStops = 17
        colShoppingTime2Slot1 = 18
        maxB4Index = 12

        nodeId = "category_" + self.storeId + "_" + self.storeRegionId + "_" + id
        if self.segmentId != None:
            nodeId += "_" + self.segmentId
        #try:
        #    dbId = self._getDatabaseNodeId(nodeId);
        #except:
        try:
            dbId = self._getDatabaseNodeIdNew(self.storeId, id)
            # print "\t\t\t%s"%dbId
        except Exception, e:
            print "Error %s" % (e)
            print "\t\t\tNo node id found for %s!"%id
            # pdb.set_trace()
            return 0
        t = self.startTime
        
        counts = {}
        returnData = {}
        # if getPOSData is not None:
            # buyers = getPOSData(self.storeId, dbId)
        # else:
            # buyers = {}
        # pdb.set_trace()
        while t <= self.endTime:
            fpLog = open(dateToLogFileDict[dateToFileName(t)],'a')
            fpLog.write("\n\n\n========================================================\n")
            fpLog.write("Loading category data - {0:30}\t{1:15}\n".format(id, dbId))
            fpLog.write("========================================================\n")
            for segment in segments:
                counts[segment] = self._getTimeDict(t,t + datetime.timedelta(days=1),datetime.timedelta(hours=1),[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
        
            dirName = os.path.join(self.dataDir,'PasserInfo', 'wholestore')#self.storeRegionId)
            
            if not os.path.exists(dirName):
                raise Exception("Directory not found: " + dirName)
            fname = dirName + "\\" + dateToFileName(t) + ".csv"
            fpLog.write("[Info]: Reading passer info file: %s\n"%fname)
            if os.path.exists(fname):
                # print fname
                firstLine = 1
##                print fname
                for row in csv.reader(open(fname)):
                    numCols = len(row)
                    if not firstLine:
                        # print fname, row[0]
                        if numCols > categoryIndex and row[categoryIndex] == '1':
##                            if self.segmentId is None or lowerSegment == row[genderIndex].replace(' ','').lower() or lowerSegment == row[ageIndex].replace(' ','').lower() or lowerSegment == row[ethnicityIndex].replace(' ','').lower():
                            eventTime = stringToTime(row[0])
                            #pdb.set_trace()
                            self._addColVal(counts[None],colTotalTraffic,eventTime,trafficCountSF)
                            for i in range(0, numSegmentCategories):
                                segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                if segmentName in counts.keys():
                                    self._addColVal(counts[segmentName],colTotalTraffic,eventTime,trafficCountSF);
                    else:
##                        print id, fname
                        categoryIndex = row.index(id)
                        firstLine = 0
                fpLog.write("\t[Info]: Done!\n")
            else:
                fpLog.write("\t[Warning]: Missing passer info file\n")
            
            fpLog.write("\n-------------------\n")
            dirName = os.path.join(self.dataDir,'shopInfo-Regions', self.storeRegionId)
            if not os.path.exists(dirName):
                raise Exception("Directory not found: " + dirName)
            fname = dirName + "\\" + dateToFileName(t) + '_' + id + ".csv"
            fpLog.write("[Info]: Reading shopInfo-Regions\%s file: %s\n"%(self.storeRegionId,fname))
            completeShopperList = []
            #pdb.set_trace()
            if os.path.exists(fname):
                firstLine = True
                for row in csv.reader(open(fname)):
                    if not firstLine and 'Start Time' not in row:
                        shopper = {}
##                        print row
                        shopTime = float(row[9])
                        for index in range(len(row)):
                            if row[index] == id:
                                shopTime += float(row[index+1])
                        
##                        print self.storeId, id, shopTime
                        if validShopTime(self.storeId, id, shopTime):
##                            print 'in'
                            eventTime = stringToTime(row[0])
                            #pdb.set_trace()
                            self._addColVal(counts[None],colNumShoppers,eventTime,shopperCountSF)
                            if int(row[7]) == 0:
                                self._addColVal(counts[None],colFirstDestShoppers,eventTime,shopperCountSF)
                                shopper['firstDest'] = True
                            else:
                                self._addColVal(counts[None],colSecondDestShoppers,eventTime,shopperCountSF)
                                shopper['firstDest'] = False
                            
                            categoryStops = 1
                            for index in range(maxB4Index, len(row), 2):
                                if row[index] == id:
                                    categoryStops += 1
                            shopper['numStops'] = categoryStops
                            self._addColVal(counts[None],colAvgStops,eventTime,categoryStops*shopperCountSF)

                            self._addColVal(counts[None],colAvgShopTime,eventTime,shopperCountSF*shopTime)
                            self._addColVal(counts[None],colShoppingTimeSlot1 + self._shopTimeSlot(shopTime) -1,eventTime,shopperCountSF)
                            self._addColVal(counts[None],colShoppingTime2Slot1 + self._shopTime2Slot(shopTime) -1,eventTime,shopperCountSF)
                            
                            
                            shopper['shopTime'] = shopTime
                            completeShopperList.append(shopper)
                            for i in range(0, numSegmentCategories):
                                segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                if segmentName in counts.keys():
                                    self._addColVal(counts[segmentName],colNumShoppers,eventTime,shopperCountSF)
                                    if int(row[7]) == 0:
                                        self._addColVal(counts[segmentName],colFirstDestShoppers,eventTime,shopperCountSF)
                                    else:
                                        self._addColVal(counts[segmentName],colSecondDestShoppers,eventTime,shopperCountSF)
                                    
                                    categoryStops = 1
                                    for index in range(maxB4Index, len(row), 2):
                                        if row[index] == id:
                                            categoryStops += 1
                                    self._addColVal(counts[segmentName],colAvgStops,eventTime,categoryStops*shopperCountSF)
                                    shopTime = float(row[9])
                                    for index in range(len(row)):
                                        if row[index] == id:
                                            shopTime += float(row[index+1])

                                    self._addColVal(counts[segmentName],colAvgShopTime,eventTime,shopperCountSF*shopTime)
                                    self._addColVal(counts[segmentName],colShoppingTimeSlot1 + self._shopTimeSlot(shopTime) -1,eventTime,shopperCountSF)
                                    self._addColVal(counts[segmentName],colShoppingTime2Slot1 + self._shopTime2Slot(shopTime) -1,eventTime,shopperCountSF)
                    else:
                        firstLine = False
                fpLog.write("\t[Info]: Done\n")
            else:
                fpLog.write("\t[Warning: shopInfo-Regions file is missing\n")
            
            shopperVelocities = []
            passerbyVelocities = []
            fpLog.write("\n-------------------\n")
            fpLog.write("[Info]: Uploading velocities\n")
            try:
                dirName = os.path.join(self.dataDir, 'Velocities', 'wholestore');#self.storeRegionId);
                if not os.path.exists(dirName):
                    raise Exception("Directory not found: " + dirName)
                
                fname = dirName + "\\" + dateToFileName(t) + '_' + id + ".csv"
                fpLog.write("[Info]: Reading velocity file: %s\n"%fname)
                if os.path.exists(fname):
                    firstLine = 1
                    for row in csv.reader(open(fname)):
                        if not firstLine:
                            eventTime = stringToTime(row[0])
                            if row[3].strip() == 'Stopped':
                                shopperVelocities 
                                self._addColVal(counts[None],colShopperVelocity,eventTime,float(row[2]))
                                self._addColVal(counts[None],colShopperVelocityCount,eventTime,1)
                                shopperVelocities.append(float(row[2]))
                            else:
                                self._addColVal(counts[None],colPasserByVelocity,eventTime,float(row[2]))
                                self._addColVal(counts[None],colPasserByVelocityCount,eventTime,1)
                                passerbyVelocities.append(float(row[2]))
                            
                            for i in range(0, numSegmentCategories):
                                segmentName = row[len(row)-1-i].strip().replace(' ','').lower()
                                if segmentName in counts.keys():
                                    if row[3] == 'Stopped':
                                        self._addColVal(counts[segmentName],colShopperVelocity,eventTime,float(row[2]))
                                        self._addColVal(counts[segmentName],colShopperVelocityCount,eventTime,1)
                                    else:
                                        self._addColVal(counts[segmentName],colPasserByVelocity,eventTime,float(row[2]))
                                        self._addColVal(counts[segmentName],colPasserByVelocityCount,eventTime,1)
                        else:
                            firstLine = 0
                    fpLog.write("\t[Info]: Done\n")
                else:
                    fpLog.write("\t[Warning]: File is missing\n")
            except:
                fpLog.write("\t[Error]: Skipping\n")
                pass

            
            times = counts[None].keys()
            times.sort()
            fpLog.write("\n-------------------\n")
            fpLog.write("[Info]: Uploading category data\n")
            for segment in segments:
                times = counts[segment].keys()
                times.sort()
                if segment is None:
                    sql = "REPLACE INTO %s"%(self.tableName) + \
                    "(node_id, start_time, traffic_count,shopper_count,first_dest_shopper_count,shopping_time_avg,shopping_time_slot_1,shopping_time_slot_2,shopping_time_slot_3,shopping_time_slot_4,shopping_time_slot_5,shopper_vel,passerby_vel,shopping_points_avg,shopping_time2_slot_1,shopping_time2_slot_2,shopping_time2_slot_3,shopping_time2_slot_4,shopping_time2_slot_5,shopping_time2_slot_6,shopping_time2_slot_7,shopping_time2_slot_8,shopping_time2_slot_9,buyer_count) VALUES "
                    nodeId = "category_" + self.storeId + "_" + self.storeRegionId + "_" + id
                else:
                    sql = "REPLACE INTO %s_%s" % (self.tableName,segment) + "(node_id,start_time,traffic_count,shopper_count,first_dest_shopper_count,shopping_time_avg,shopping_time_slot_1,shopping_time_slot_2,shopping_time_slot_3,shopping_time_slot_4,shopping_time_slot_5,shopper_vel,passerby_vel,shopping_points_avg,shopping_time2_slot_1,shopping_time2_slot_2,shopping_time2_slot_3,shopping_time2_slot_4,shopping_time2_slot_5,shopping_time2_slot_6,shopping_time2_slot_7,shopping_time2_slot_8,shopping_time2_slot_9,buyer_count) VALUES "
                    nodeId = "category_" + self.storeId + "_" + self.storeRegionId + "_" + id + '_' + segment
                #try:
                 #   dbId = self._getDatabaseNodeId(nodeId);
                #except:
                try:
                    dbId = self._getDatabaseNodeIdNew(self.storeId, id)
                except:
                    print "Node Id not Found"
                    # no need to process any dates if the node id isn't found at all
                    fpLog.write("\t[Error]: Aborting: node id wasn't found for store: %s and category: %s\n"%(self.storeId, id))
                    fpLog.close()
                    return 0
                if getPOSData is not None:
                    buyers = getPOSData(self.storeId, dbId)
                else:
                    buyers = {}
                firstRow = 1
                for t1 in times:
                    if t1.hour < self.settings.StartTimeOfDay or t1.hour >= self.settings.EndTimeOfDay:
                        continue
                    if not firstRow: sql += ",";
                    row = counts[segment][t1];
                    
                    shoppingTimes = []
                    shoppingTimes.extend(row[colShoppingTimeSlot1:colShoppingTimeSlot1+4])
    ##                print shoppingTimes, row[colShoppingTimeSlot1:colShoppingTimeSlot1+4]
                    shoppingTimes.extend(row[colShoppingTime2Slot1:colShoppingTime2Slot1+6])
                    # pdb.set_trace()
    ##                print shoppingTimes, row[colShoppingTime2Slot1:colShoppingTime2Slot1+6]
                    # adjustedShoppingTimes = map(int, adjustValues(int(round(row[colNumShoppers])), shoppingTimes))
    ##                print adjustedShoppingTimes, int(round(row[colNumShoppers]))

                    if segment is None and t1 in buyers.keys():
                        buyerCount = buyers[t1]
                    else:
                        buyerCount = 0

                    
                    sql += "('%s','%s',%d,%d,%d,%s,%f,%f,%f,%f,%d,%s,%s,%s,%f,%f,%f,%f,%f,%f,%d,%d,%d,%d)" % (
                        dbId,
                        datetimeToSql(t1),
                        int(round(row[colTotalTraffic])),
                        int(round(row[colNumShoppers])),
                        int(round(row[colFirstDestShoppers])),
                        sqlDiv(row[colAvgShopTime],row[colNumShoppers]),
                        shoppingTimes[0],
                        shoppingTimes[1],
                        shoppingTimes[2],
                        shoppingTimes[3],
                        int(round(row[colShoppingTimeSlot1+4])),
                        sqlDiv(row[colShopperVelocity],row[colShopperVelocityCount]),
                        sqlDiv(row[colPasserByVelocity],row[colPasserByVelocityCount]),
                        # int(round(row[colBuyers])),
                        # row[colTotalSales],
                        sqlDiv(row[colAvgStops],row[colNumShoppers]),
                        shoppingTimes[4],
                        shoppingTimes[5],
                        shoppingTimes[6],
                        shoppingTimes[7],
                        shoppingTimes[8],
                        shoppingTimes[9],
                        int(round(row[colShoppingTime2Slot1+6])),
                        int(round(row[colShoppingTime2Slot1+7])),
                        int(round(row[colShoppingTime2Slot1+8])),
                        buyerCount
                        )
                    firstRow = 0
                sql += ";"

                self._execute(sql)
            returnData.update(counts[None])
            fpLog.write("\t[Info]: Done\n")
            t += datetime.timedelta(days=1)
            fpLog.close()
        return returnData
    
    def setStoreRegionId(self, storeRegionId):
        self.storeRegionId = storeRegionId
