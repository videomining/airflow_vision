/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

Put machine learning data to the specific location for its use.

- For OmniSensr_DMG_Reconizer:
	
	From: 
		omnidata/models/ml
		omnidata/config/OmniSensr_DMG_Recognizer/AutoDmgRecog.xml
	To:			
		~/omnisensr-apps/config/OmniSensr_DMG_Recognizer/ml
		~/omnisensr-apps/config/OmniSensr_DMG_Recognizer/AutoDmgRecog.xml


- For Face_Postprocessor:

	From:
		omnidata/config/Face_Postprocessor/configFPP.xml
		omnidata/models/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml
		omnidata/models/ml/fv/shape_predictor_68_face_landmarks.dat	
		omnidata/models/ml/fv/dlib_face_recognition_resnet_model_v1.dat

	To:
		<at location of Face_Postprocessorbinary file> 
		 ./conifg/Face_Postprocessor/configFPP.xml
		 ./config/Face_Postprocessor/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml
		 ./config/Face_Postprocessor/ml/fv/shape_predictor_68_face_landmarks.dat
		 ./config/Face_Postprocessor/ml/fv/dlib_face_recognition_resnet_model_v1.dat	

