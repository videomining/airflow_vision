import pymongo
# import datetime
# MONGODB_HOST = 'ds041654-a1.mongolab.com'
# MONGODB_PORT = 41654
MONGODB_HOST = 'vmdbmongo'
MONGODB_PORT = 27017
import csv
from datetime import datetime,timedelta


class MongoUtils(object):
    def __init__(self):
        self.conn = pymongo.MongoClient(host=MONGODB_HOST, port=MONGODB_PORT)
    def __init__(self,MONGODB_HOST=MONGODB_HOST,MONGODB_PORT=MONGODB_PORT):
        self.conn = pymongo.MongoClient(host=MONGODB_HOST, port=MONGODB_PORT)
    def connectToMongo(self,dbName):
        self.db = self.conn[dbName]
        #self.db.authenticate("vmdatasvc", "irrelev@nt")
        self.db.authenticate("vmdatasvc", "irrelev@nt")
        #print 'hi'

    def insertDocument(self,collectionName,dataDict):
        coll = self.db[collectionName]
        for row in dataDict:
            coll.insert(dataDict[row])

    def deleteDocuments(self,collectionName,date,projectType):
        print 'Deleting existing documents from %r with date %r' %(collectionName,date)
        # coll =self.db['raw_vision_2030']
        # result = coll.count({'nav_time': {'$lt':0}, 'start_date': '20160702'})
        coll = self.db[collectionName]
        result = coll.delete_many({'start_date':date,'project_type': projectType})
        print 'num of lines deleted %r' %result.deleted_count

    def deleteDocuments_test(self, collectionName, projectType):
        # coll =self.db['raw_vision_2030']
        # result = coll.count({'start_date':{'$lt': '20160601'})
        coll = self.db[collectionName]
        result = coll.delete_many({'start_date': {'$lte':'20160601'}})
        # print 'num of lines deleted %r' % result.deleted_count
        # result = coll.find({'start_date': '20160701'})
        print 'num of lines deleted %r' % result.count()
    
    def fetchDocument_video(self,collectionName,vpu_name, date,cam):
        coll = self.db[collectionName]
        # cursor = coll.find({'date': date,'vpu-name':vpu_name,'cam':cam})
        cursor = coll.find({'date':date , 'vpu-name': vpu_name , 'cam' : cam})
        for docu in cursor:
            availability = docu['availability']
        return availability
    
    def fetchDocument_DOE(self,collectionName,dateList):
        coll = self.db[collectionName]
        finalDataDict  = {}
        timeList = ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23']
        # timeList = ['07']
        # dateVal = datetime.strptime(date,'%Y%m%d')
        for date in dateList:
            cursor = coll.find({"node_type" : "Aisle" , "start_date" : date})
        # cursor = coll.distinct("node_name", {"node_type" : "Aisle" , "start_date" : date})
        # cursor = coll.distinct(({},{"parent_region":1, "_id":0}))
            for document in cursor:
                aisleName = document["node_name"]
                if aisleName not in finalDataDict:
                    finalDataDict[aisleName] = {}
                if date not in finalDataDict[aisleName]:
                    finalDataDict[aisleName][date] = {}
                    for time in timeList:
                        finalDataDict[aisleName][date][time] = []

                # ast.literal_eval(json.dumps(document))
                try:
                    finalDataDict[aisleName][date][document["start_hour"]].append((document["D0E"],document["D0X"]))
                except Exception as e:
                    print e

                # finalDataDict[aisleName][date][document]["start_hour"]["DOX"] += document["D0X"]

        return finalDataDict
    def fetchDocument(self,collectionName,date,nodeIdList, project_type):
        coll = self.db[collectionName]
        finalDataDict  = {}
        dateVal = datetime.strptime(date,'%Y%m%d')
        for nodeId in nodeIdList:
            for i in range(0,24):
                time = '{:%H:%M:%S}'.format(dateVal + timedelta(hours=i))
                if nodeId not in finalDataDict:
                    finalDataDict[nodeId] = {}
                if time not in finalDataDict[nodeId]:
                    finalDataDict[nodeId][time] = {'first_dest_shopper_count': 0, 'shopper_count': 0,
                                                   'traffic_count': 0,
                                                   'shopping_points': 0, 'shopping_time': 0, 'nav_time': 0,
                                                   'shop_dist': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}
        for i in range(0, 24):
            time = '{:%H:%M:%S}'.format(dateVal + timedelta(hours=i))

            #first lets calculate the traffic values
            cursor = coll.find({'start_date': date, 'project_type': project_type,
                                'start_hour': str(i).zfill(2)})
            # print 'number of records found for hour %r: %r' %(time,cursor.count())
            for document in cursor:
                nodeId = document['node_id']
                if nodeId in nodeIdList:
                    finalDataDict[nodeId][time]['traffic_count'] += 1
            #calculating the shopper values
            cursor1 = coll.find(
                {'start_date': date, 'project_type': project_type, 'start_hour': str(i).zfill(2),
                 'nav_time': {'$gt': 0.0}})
            for document in cursor1:
                nodeId = document['node_id']
                if nodeId in nodeIdList:
                    finalDataDict[nodeId][time]['shopping_points'] += document['num_of_shops']
                    finalDataDict[nodeId][time]['nav_time'] += document['nav_time']
                    finalDataDict[nodeId][time]['shopping_time'] += document['shop_time']
                    finalDataDict[nodeId][time]['shopper_count'] += 1
                    shop_time = document['shop_time']
                    shop_bin = int(shop_time / 10)
                    if shop_bin < 9:
                        finalDataDict[nodeId][time]['shop_dist'][shop_bin] += 1
                    else:
                        finalDataDict[nodeId][time]['shop_dist'][9] += 1
                    if 'first_dest_flag' in document:
                        if document['first_dest_flag']:
                            finalDataDict[nodeId][time]['first_dest_shopper_count'] += 1

        return finalDataDict
        # for nodeId in nodeIdList:
        #     print nodeId
        #     finalDataDict[nodeId] = {}
        #     for i in range(0,24):
        #         time = '{:%H:%M:%S}'.format(dateVal+timedelta(hours=i))
        #         finalDataDict[nodeId][time]={'first_dest_shopper_count': 0,'shopper_count': 0,'traffic_count': 0,'shopping_points': 0,'shopping_time':0,'nav_time':0,'shop_dist':[0,0,0,0,0,0,0,0,0,0]}
        #         cursor = coll.find({'start_date':date,'node_id':nodeId,'project_type':project_type,'start_hour':str(i).zfill(2)})
        #         # cursor = coll.find({'start_date':date})
        #         finalDataDict[nodeId][time]['traffic_count'] = cursor.count()
        #         # for document in cursor:
        #         #     finalDataDict[nodeId][time]['traffic_count']+= 1
        #         cursor = coll.find({'start_date':date,'node_id':nodeId, 'project_type':project_type,'start_hour':str(i).zfill(2),'nav_time':{'$gt':0.0}})
        #         finalDataDict[nodeId][time]['shopper_count'] = cursor.count()
        #         for document in cursor:
        #             finalDataDict[nodeId][time]['shopping_points'] += document['num_of_shops']
        #             finalDataDict[nodeId][time]['nav_time'] += document['nav_time']
        #             finalDataDict[nodeId][time]['shopping_time'] += document['shop_time']
        #             shop_time = document['shop_time']
        #             shop_bin = int(shop_time/10)
        #             if shop_bin < 9:
        #                 finalDataDict[nodeId][time]['shop_dist'][shop_bin]+= 1
        #             else:
        #                 finalDataDict[nodeId][time]['shop_dist'][9]+= 1
        #             if 'first_dest_flag' in document:
        #                 if document['first_dest_flag']:
        #                     finalDataDict[nodeId][time]['first_dest_shopper_count'] += 1
        #     # break
        # return finalDataDict

    def fetchDocumentOld(self,collectionName):
        coll = self.db[collectionName]
        cursor = coll.find({'start_date':'20140515'})
        traffic_count = {}
        for document in cursor:
            if document['node_id'] not in traffic_count:
                traffic_count[document['node_id']] = {}
            if document['start_hour'] not in traffic_count[document['node_id']]:
                traffic_count[document['node_id']][document['start_hour']] = 0
            traffic_count[document['node_id']][document['start_hour']] += 1
        cursor = coll.find({'start_date':'20140515','nav_time':{'$gt':0.0}})
        shopper_count = {}
        for document in cursor:
            if document['node_id'] not in shopper_count:
                shopper_count[document['node_id']] = {}
            if document['start_hour'] not in shopper_count[document['node_id']]:
                shopper_count[document['node_id']][document['start_hour']] = {'nav_time':0,'shopping_time_avg':0,'count_stops':0,'num_of_shoppers':0,'first_dest_count':0}

            shopper_count[document['node_id']][document['start_hour']]['num_of_shoppers'] += 1
            shopper_count[document['node_id']][document['start_hour']]['count_stops'] += document['num_of_shops']
            shopper_count[document['node_id']][document['start_hour']]['nav_time']+= document['nav_time']
            shopper_count[document['node_id']][document['start_hour']]['shopping_time_avg']+= document['shop_time']/document['num_of_shops']
            if 'first_dest_flag' in document:
                if document['first_dest_flag']:
                    shopper_count[document['node_id']][document['start_hour']]['first_dest_count'] += 1
        # for key in shopper_count:
        #     for hour in shopper_count[key]:
        #         print 'hi'
        with open('D:\\work\Test\\final.csv','wb') as csvfile:
            csvWriter = csv.writer(csvfile)
            header = ['node_id','traffic_count','shopper_count']
            csvWriter.writerow(header)
            for key in traffic_count:
                if key in shopper_count:
                    row = [key,traffic_count[key],shopper_count[key]]
                    # print 'node--> %r  traffic_count-->%r  shopper_count--> %r'% (key,traffic_count[key],shopper_count[key])
                else:
                    row = [key,traffic_count[key],0]
                    # print 'node--> %r  traffic_count-->%r  shopper_count--> 0'% (key,traffic_count[key])
                csvWriter.writerow(row)
        print 'completed'
