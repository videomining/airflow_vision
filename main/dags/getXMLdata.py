import pdb
import sys
from bs4 import BeautifulSoup as bs
import xml.etree.ElementTree as ET
import os
import DBConnector
import datetime
# import cStringIO
# from lxml import etree
import math
from xml.etree.ElementTree import Element, SubElement, tostring, XML
from xml.etree import ElementTree
from xml.dom import minidom
from yaml import load, Loader, load_all
import re



# get Calibration xml file and save it at location
def getXMLdata(project, dateList):
    XMLDict = {}
    db_obj = DBConnector.DBConnector(host = 'vmvisiondb.videomining.com' ,user = 'vmdatasvc', password = 'irrelev@nt', database='calibration')

    start_date = datetime.datetime.strptime(dateList[0], "%Y%m%d").date()
    end_date = datetime.datetime.strptime(dateList[-1], "%Y%m%d").date() 
    
    #Find the project_id and all the associated versions
    if '_calib' not in project:
        proj_name = project + '_calib'
    else:
        proj_name = project
    proj_id = getProjectId(db_obj, proj_name)
    proj_version = getProjVersion(db_obj,proj_id) #Contains all the associated versions of given project
    #Find the required version based on date range
    versions_needed = findCalibFile(proj_version,start_date, end_date, 'xml')
    print '####################################################### calibration ##############################################'
    print versions_needed
    for versions in versions_needed:
        # len(versions_needed)
        # pdb.set_trace()
        soup_data = getProjFile(db_obj,versions,proj_id) # Contains the blob, ie. the calibration_vivek.vmproj file
        dateKey = str(str(versions[1]) + '_' + str(versions[2]))
        XMLDict[dateKey] = createXML(soup_data)
    return XMLDict


def getXMLdataAllCams(project, dateList):
    XMLDict = {}
    db_obj = DBConnector.DBConnector(host='vmvisiondb', user='vmdatasvc', password='irrelev@nt', database='calibration')

    start_date = datetime.datetime.strptime(dateList[0], "%Y%m%d").date()
    end_date = datetime.datetime.strptime(dateList[-1], "%Y%m%d").date()

    # Find the project_id and all the associated versions
    if '_calib' not in project:
        proj_name = project + '_calib'
    else:
        proj_name = project
    proj_id = getProjectId(db_obj, proj_name)
    proj_version = getProjVersion(db_obj, proj_id)  # Contains all the associated versions of given project
    # Find the required version based on date range
    versions_needed = findCalibFile(proj_version, start_date, end_date, 'xml')
    print '####################################################### calibration ##############################################'
    print versions_needed
    for versions in versions_needed:
        # len(versions_needed)
        # pdb.set_trace()
        soup_data = getProjFile(db_obj, versions, proj_id)  # Contains the blob, ie. the calibration_vivek.vmproj file
        dateKey = str(str(versions[1]) + '_' + str(versions[2]))
        XMLDict[dateKey] = createXMLAllCams(soup_data)
    return XMLDict
# Gets the project_id in the calibration_vivek.project_info table
def getProjectId(db_obj, proj_name):
    # Get Project Id for store calibration file in db
    base_sql = '''
    SELECT
        project_id
    FROM
        calibration.project_info
    WHERE
        name = '<proj_name>';
    '''

    sql = base_sql.replace('<proj_name>', proj_name)
    proj_id_ret = db_obj.execute(sql)  
    try:
        return int(proj_id_ret[0][0])
    except IndexError:
        print proj_name
        print 'Check the file name/storeID.\nUse the \'List\' command to view all the calibration files in DB.'
        print 'line 63'
        exit()
        
#Gets all the associated versions and their date ranges of given project_id  
def getProjVersion(db_obj,proj_id): 
    
    base_sql = '''
    SELECT
        version, start_date, end_date
    FROM
        calibration.project
    WHERE
        id = '<proj_id>'        
        ORDER BY start_date;
    '''
    
    sql = base_sql.replace('<proj_id>', str(proj_id))
    proj_version = db_obj.execute(sql)    
    return list(proj_version)  

#Gets the version(s) that lie within the date range provided
def findCalibFile(proj_version,start_date, end_date, returnVersion = None):
    versions_needed = []
    # pdb.set_trace()
    delta = datetime.timedelta(days=1)
    for x in proj_version:
        # if start_date < proj_version[0][1]:
            # print 'Check the Start/End date provided.\nUse the \'List\' command to view all the calibration files in DB.'
            # print 'line89'
            # exit()
        if start_date <= x[2]:
            if end_date <= x[2]:
                versions_needed.append(x)
                break
            versions_needed.append(x)
    if not versions_needed:
            print 'Check the Start/End date provided.\nUse the \'List\' command to view all the calibration files in DB.'
            print 'line99'
            exit()
    if returnVersion == 'xml':
        return versions_needed
        
# Used by getCalibrationXML to get and create xml     
def getProjFile(db_obj,versions,proj_id):    
    base_sql = '''
    SELECT
        data, start_date, end_date
    FROM
        calibration.project
    WHERE
        id = '<proj_id>' and version = '<versions>';
    '''
    
    sql = base_sql.replace('<proj_id>', str(proj_id)).replace('<versions>',str(versions[0]))
    dataFile = db_obj.execute(sql) 
    print sql
    soup_data = bs(dataFile[0][0], 'xml')    
    return soup_data


def createXMLAllCams(soup):
    # get measurement guide
    max_dist = 0.0
    dist_between_coord = 0.0
    points = []
    FinalDict = {}
    tempDict = {}
    tempCamDIct = {}
    guides = soup.find_all('measurement_guide')
    for guide in guides:
        if str(guide['is_location_guide']) == '0':
            # if float(guide['length_in_meters']) > max_dist:
            max_dist += float(guide['length_in_meters'])
            points = str(guide['points']).split(',')
            dist_between_coord += math.hypot(float(points[0]) - float(points[2]), float(points[1]) - float(points[3]))

    # dist_between_coord = math.hypot(float(points[0]) - float(points[2]), float(points[1]) - float(points[3]))
    metresPerpixel = float(max_dist / float(dist_between_coord))

    world1 = float(metresPerpixel * 100)
    world2 = 2 * world1
    world1 = round(world1, 6)
    world2 = round(world2, 6)

    defaultType, defaultHt = getDefaultCamSettings(soup)
    camid_to_cam = getMappingIdToCam(soup)
    floorplan_visioncams = get_floorplan_visioncams(soup)
    CamDict = getCamDict(soup)  # cam001:['x,y','rotation']
    typeDict = getMappingIdToCamTypeAllCams(soup, defaultType, defaultHt)  # cam001"['Axis 212','height']
    for key in CamDict.keys():
        coordinates = CamDict[key][0].split(',')
        CamDict[key][0] = str(float(coordinates[0]) * metresPerpixel) + ',' + str(
            float(coordinates[1]) * metresPerpixel)

    tempDict['image'] = 'floorplan.png'
    tempDict['local'] = [(100, 100), (100, 200), (200, 100), (200, 200)]

    floorplan_metres = [[str(world1), str(world1)], [str(world1), str(world2)], [str(world2), str(world1)],
                        [str(world2), str(world2)]]

    tempDict['world'] = floorplan_metres
    FinalDict['floorplan'] = tempDict

    # latter part
    for keys in CamDict.keys():

        temp2Dict = {}
        temp2Dict['name'] = keys
        temp2Dict['ccdSize'] = getCamPpts(typeDict[keys][0])['ccdSize']
        temp2Dict['focalLength'] = getCamPpts(typeDict[keys][0])['focalLength']
        temp2Dict['imageSize'] = getCamPpts(typeDict[keys][0])['imageSize']
        temp2Dict['location'] = [str(CamDict[keys][0].split(',')[0]), str(CamDict[keys][0].split(',')[1]),
                                 str(typeDict[keys][1])]
        temp2Dict['orientation'] = str('-' + str(CamDict[keys][1]))
        # keyForDict = re.findall('\d+', keys)[0].lstrip('0')
        tempCamDIct[keys] = temp2Dict

    FinalDict['camera'] = tempCamDIct
    # f = open(oplocation + '\\' + project + '_calib_' + str(start_date) + '_' +  str(end_date) + '.xml', 'w')
    # f.write(prettify(top))
    # f.close
    return FinalDict



def createXML(soup):
    #get measurement guide
    max_dist = 0.0
    dist_between_coord = 0.0
    points = []
    FinalDict = {}
    tempDict = {}
    tempCamDIct = {}
    guides = soup.find_all('measurement_guide')
    for guide in guides:
        if str(guide['is_location_guide']) == '0':
            # if float(guide['length_in_meters']) > max_dist:
            max_dist += float(guide['length_in_meters'])
            points = str(guide['points']).split(',')
            dist_between_coord += math.hypot(float(points[0]) - float(points[2]), float(points[1]) - float(points[3]))

    # dist_between_coord = math.hypot(float(points[0]) - float(points[2]), float(points[1]) - float(points[3]))
    metresPerpixel = float(max_dist / float(dist_between_coord))
    
    world1 = float(metresPerpixel*100)
    world2 = 2*world1
    world1 = round(world1,6)
    world2 = round(world2,6)
    
    
    
    defaultType,defaultHt = getDefaultCamSettings(soup)
    camid_to_cam = getMappingIdToCam(soup)
    floorplan_visioncams = get_floorplan_visioncams(soup)
    CamDict = getCamDict(soup) # cam001:['x,y','rotation']
    typeDict = getMappingIdToCamType(soup,defaultType,defaultHt) #cam001"['Axis 212','height']
    for key in CamDict.keys():
        coordinates = CamDict[key][0].split(',')
        CamDict[key][0] = str(float(coordinates[0]) * metresPerpixel) + ',' + str(float(coordinates[1]) * metresPerpixel)

    
    tempDict['image'] = 'floorplan.png' 
    tempDict['local'] = [(100,100),(100,200),(200,100),(200,200)]

    floorplan_metres = [[str(world1),str(world1)],[str(world1),str(world2)],[str(world2),str(world1)],[str(world2),str(world2)]]

    tempDict['world'] = floorplan_metres
    FinalDict['floorplan'] = tempDict

    
    #latter part
    for keys in CamDict.keys():
        if 'cam' in keys.lower() :
            temp2Dict = {}
            temp2Dict['name'] = keys
            temp2Dict['ccdSize'] = getCamPpts(typeDict[keys][0])['ccdSize']
            temp2Dict['focalLength'] = getCamPpts(typeDict[keys][0])['focalLength']
            temp2Dict['imageSize'] = getCamPpts(typeDict[keys][0])['imageSize']
            temp2Dict['location'] = [str(CamDict[keys][0].split(',')[0]),str(CamDict[keys][0].split(',')[1]),str(typeDict[keys][1])]
            temp2Dict['orientation'] = str('-'+str(CamDict[keys][1]))
            if typeDict[keys][0].lower() == 'vm sense 100':
                type = 'OmniSensr_4x3'
            else:
                type = typeDict[keys][0]
            temp2Dict['type'] = type
            keyForDict =  re.findall('\d+',keys)[0].lstrip('0')
            tempCamDIct[keyForDict] = temp2Dict
    
    FinalDict['camera'] = tempCamDIct
    # f = open(oplocation + '\\' + project + '_calib_' + str(start_date) + '_' +  str(end_date) + '.xml', 'w')
    # f.write(prettify(top))
    # f.close
    return FinalDict

    
def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ",encoding="utf-8")   
    
    
def getCamPpts(camType):
    camPpty = {}
    camPpty['Axis 212'] = {'focalLength' : "0.0027", 'ccdSize' : [0.0064,0.0048], 'imageSize' : [320.0,240.0]}
    camPpty['Axis212_4x3'] = {'focalLength': "0.0027", 'ccdSize': [0.0064, 0.0048], 'imageSize': [320.0, 240.0]}
    camPpty['Axis 206'] = {'focalLength' : "0.0021", 'ccdSize' : [0.0032,0.0024], 'imageSize' : [320.0,240.0]}
    camPpty['VM Sense 100'] = {'focalLength' : "0.0031", 'ccdSize' : [0.00367,0.00274], 'imageSize' : [320.0,240.0]}
    camPpty['Cisco 6500PD'] = {'focalLength' : "0.0031", 'ccdSize' : [0.00584,0.003286], 'imageSize' : [320.0,240.0]}
    camPpty['OmniSensr_4x3'] = {'focalLength': "0.0031", 'ccdSize': [0.00367, 0.00274], 'imageSize': [320.0, 240.0]}
    return camPpty[camType]
    

# Create and return a mapping dictionary of cameras to cam ids
def getMappingCamToId(soup):
    camdict = {}
    camviews = soup.find_all('camera_view')
    for camview in camviews:
        if camview.find('type_label').text == 'Camera View':
            camid = camview['id']
            camname = str(camview.find('name').text)
            camdict[camname.lower()] = str(camid)

        elif camview.find('type_label').text == 'Floorplan':
            camid = camview['id']
            camdict['floorplan'] = str(camid)

    if 'floorplan' not in camdict:
        sys.exit('No floorplan detected')

    return camdict



# Create and return a mapping dictionary of camids to camnames
def getMappingIdToCam(soup):
    idDict = {}
    cams = soup.find_all('camera_view')
    for eachCam in cams:
        if eachCam.find('type_label').text == 'Camera View':
            camId = eachCam['id']
            camName = str(eachCam.find('name').text)
            idDict[str(camId)] = camName

        elif eachCam.find('type_label').text == 'Floorplan':
            camid = eachCam['id']
            idDict[str(camid)] = 'floorplan'

    return idDict
    
# Create and return a mapping dictionary of camname to camtypes , eg.212
def getMappingIdToCamType(soup,defaultType,defaultHt):
    typeDict = {}
    camType = defaultType
    camHt = defaultHt
    cams = soup.find_all('camera_view')
    for eachCam in cams:
        if eachCam.find('type_label').text == 'Camera View':
            camName = eachCam.find('name').text
            if 'cam' in camName or 'omni' in camName.lower():
                setName = eachCam.find('variable_set')
                varname = setName.find_all('variable')
                camType = defaultType
                camHt = defaultHt
                for var in varname:
                    if str(var['name']) == 'Camera.Model':
                        camType = str(var['value'])
                    elif str(var['name']) == 'TopCamera.Height':
                        camHt = str(var['value'])  
                    elif not camType or not camHt:
                        raise Exception('what the fuck !!')
                typeDict[str(camName)] = [camType,camHt]

        elif eachCam.find('type_label').text == 'Floorplan':
            camName = 'floorplan'
            typeDict[str(camName)] = 0.0

    return typeDict    

def getMappingIdToCamTypeAllCams(soup,defaultType,defaultHt):
    typeDict = {}
    camType = defaultType
    camHt = defaultHt
    cams = soup.find_all('camera_view')
    for eachCam in cams:
        if eachCam.find('type_label').text == 'Camera View':
            camName = eachCam.find('name').text
            setName = eachCam.find('variable_set')
            varname = setName.find_all('variable')
            camType = defaultType
            camHt = defaultHt
            for var in varname:
                if str(var['name']) == 'Camera.Model':
                    camType = str(var['value'])
                elif str(var['name']) == 'TopCamera.Height':
                    camHt = str(var['value'])
                elif not camType or not camHt:
                    raise Exception('what the fuck !!')
            typeDict[str(camName)] = [camType,camHt]

        elif eachCam.find('type_label').text == 'Floorplan':
            camName = 'floorplan'
            typeDict[str(camName)] = 0.0

    return typeDict
def getDefaultCamSettings(soup):
    var_sets = soup.find_all('variable_sets')
    # var_set = var_sets.find('variable_set')
    for var_set in var_sets:
        some = var_set.find_all('variable_set')
        for var in some:
            if str(var.get('name')) == 'DefaultCameraSettings':
                varname = var.find_all('variable')
                for var in varname:
                    if str(var['name']) == 'TopCamera.Height':
                        defaultHt = str(var['value'])
                    if str(var['name']) == 'Camera.Model':
                        defaultType = str(var['value'])
                # defaultType = var.find('variable_set').find().text
    return defaultType,defaultHt

# Gets all floorplan cameras that are prepended with "cam" (vision cameras)
def get_floorplan_visioncams(soup):
    floorplan_cams = []
    cams = soup.find_all('floorplan_camera')
    floorplan_cams = [str(each_cam.camera_id.text) for each_cam in cams]
    return floorplan_cams

#Parse through vmproj file and get dictionary with cam_names as keys and coordinates as values    
def getCamDict(soup_data):   
    camDict = {}
    camNameDict = {}
    cams = soup_data.find_all('floorplan_camera')
    for eachCam in cams:
        camName = str(eachCam.find('camera_id').text)
        camLocation = str(eachCam.find('location').text)
        camRotation = str(eachCam.find('rotation').text)
        camDict[camName] = [camLocation]
        camDict[camName].append(camRotation)
    
    
    cv = soup_data.find_all('camera_view')
    for cvs in cv:
        id = str(cvs.get('id'))
        
        name = str(cvs.find('name').text)
        
        if 'floorplan' in name.lower():
            continue
        camNameDict[name] =  id
        # pdb.set_trace()
    ultimateDict = {}
    for key in camNameDict.keys():
        try:
            ultimateDict[key] = camDict[camNameDict[key]]
        except KeyError:
            if 'floorplan' in key.lower() or '_' in key.lower() or 'fac' in key.lower() or 'hi' not in key.lower():
                pass
            else:
                raise Exception("check Modules\getXMLdata.py")
  
    return ultimateDict 
        
if __name__ == '__main__':

    optsFile = r'\\vmnode5\ProjectData\kroger-503\Documents\optsFiles\optsFile_SF_2015_with_HeatMaps.yml'
    fp = open(optsFile,'r')
    for optsDict in load_all(fp,Loader=Loader):

        dateList = ['20151001','20141002','20141003','20141004']
        calibFileName = optsDict['calibFile'] 
        
        data = getXMLdata(calibFileName, dateList)
    
        # pdb.set_trace()
        
        
        
    
    

    
    
    
    
    
    
    
    
